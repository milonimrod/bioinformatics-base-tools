# README #
This repository contains the source code for a set of Bioinformatics tools that I developed in my PhD. For example:

* Several variants of Sequence alignment including: Global, Local, Semi-local.
* Maximal weighted clique algorithm 
* Huffman code Compression

The code is written in Java and provided under the [MIT License](http://opensource.org/licenses/MIT) without any warranty.