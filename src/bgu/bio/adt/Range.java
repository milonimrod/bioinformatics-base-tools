package bgu.bio.adt;

public class Range{
	
	public double value;
	private long tuplePassed;
	private long tupleFailed;

	private Object lockPassed = new Object();
	private Object lockFailed = new Object();

	public long getTuplePassed() {
		return tuplePassed;
	}

	public void setTuplePassed(long tuplePassed) {
		synchronized (lockPassed) {
			this.tuplePassed = tuplePassed;
		}

	}

	public long getTupleFailed() {
		synchronized (lockFailed) {
			return tupleFailed;
		}
	}

	public void setTupleFailed(long tupleFailed) {
		synchronized (lockFailed) {
			this.tupleFailed = tupleFailed;
		}
	}

	public void increasePassed(long amount) {
		synchronized (lockPassed) {
			this.tuplePassed += amount;
		}
	}

	public void increaseFailed(long amount) {
		synchronized (lockFailed) {
			this.tupleFailed += amount;
		}
	}

	public Range(double value) {
		this.value = value;
		tupleFailed = 0;
		tuplePassed = 0;
	}
	
	/**
	 * Merge a given {@link Range} in to the current range
	 * 
	 * @param other the other to be merged into the current one.
	 */
	public void mergeInto(Range other){
		this.increaseFailed(other.getTupleFailed());
		this.increasePassed(other.getTuplePassed());
	}
	
	public void clear(){
		this.setTupleFailed(0);
		this.setTuplePassed(0);
	}

	@Override
	public String toString() {
		return this.value + "," + tuplePassed + "," + tupleFailed;
	}

	public String toJSON(){
		return "[" + this.value + "," + tuplePassed + "," + tupleFailed + "]";
	}
}
