package bgu.bio.adt;

public interface RangesTable extends Iterable<Range> {
	public Range getRange(double value);

	public void clear();

	public int size();

	public String getName();

	public void setName(String name);
}
