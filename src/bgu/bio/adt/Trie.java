package bgu.bio.adt;

import java.util.ArrayList;
import java.util.Collection;

import gnu.trove.map.hash.TCharObjectHashMap;

public class Trie {
	public final char END_CHAR;
	private Node root;

	public Trie(char endChar) {
		END_CHAR = endChar;
		root = new Node('.');
	}

	public Trie() {
		this('$');
	}

	public void add(String value) {
		Node current = root;
		for (int i = 0; i < value.length(); i++) {
			final char c = value.charAt(i);
			if (current.containChar(c)) {
				current = current.getChild(c);
			} else {
				// found the enter spot
				current.addChild(value, i);
				return;
			}
		}
		// in case we finished and still didn't find then we need to add current
		// information to the tree
		current.addChild(END_CHAR, new Node(END_CHAR));
	}

	public boolean find(String value) {
		Node current = root;
		for (int i = 0; i < value.length(); i++) {
			final char c = value.charAt(i);
			if (current.containChar(c)) {
				current = current.getChild(c);
			} else {
				// found the enter spot
				current.addChild(value, i);
				return false;
			}
		}
		// in case we finished and still didn't find then we need to add current
		return current.containChar(END_CHAR);
	}

	public String print() {
		StringBuilder sb = new StringBuilder();
		sb.append("digraph {");
		ArrayList<Node> stack = new ArrayList<Node>();
		stack.add(root);
		while (stack.size() > 0) {
			Node current = stack.remove(stack.size() - 1);
			sb.append("n" + current.getId() + "[label=\"" + current.getChr()
					+ "\"];\n");
			for (Node c : current.getChilds()) {
				sb.append("n" + current.getId() + " -> n" + c.getId() + ";\n");
				stack.add(c);
			}
		}
		sb.append("}");
		return sb.toString();
	}
}

class Node {
	private static int num = 0;
	private final int id;
	private char chr;
	private TCharObjectHashMap<Node> childs;

	public Node(char chr) {
		this.id = num;
		num++;
		this.chr = chr;
		this.childs = new TCharObjectHashMap<Node>();
	}

	public void addChild(char c, Node node) {
		this.childs.put(c, node);
	}

	public void addChild(String value, int i) {
		if (i == value.length()) {
			// create end
			childs.put('$', new Node('$'));
		} else {
			Node child = new Node(value.charAt(i));
			child.addChild(value, i + 1);
			childs.put(value.charAt(i), child);
		}
	}

	public boolean containChar(char c) {
		return this.childs.contains(c);
	}

	public Node getChild(char c) {
		return this.childs.get(c);
	}

	public Collection<Node> getChilds() {
		return this.childs.valueCollection();
	}

	/**
	 * @return the chr
	 */
	public final char getChr() {
		return chr;
	}

	public int getId() {
		return this.id;
	}
}