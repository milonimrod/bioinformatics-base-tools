package bgu.bio.adt.graphs;

import java.util.Arrays;

import bgu.bio.util.MatrixUtils;

/**
 * A standard implementation of the Graph model. this implementation represents
 * a graph with directed edges and holds the information in adjacents table.
 * 
 * @author milon
 * 
 */
public class Graph {
	protected int nodeNum, edgeNum;
	public int[][] outAdjLists;
	protected int[][] inAdjLists;

	private int[] inAdjCountes;

	/** index the "in" neighborIx for an "out" neighbor. */
	protected int[][] inToOutNeighborIx;

	protected int maxOutDeg;
	protected int numOfLeaves;

	public Graph() {
		nodeNum = 0;
		edgeNum = 0;
		maxOutDeg = 0;
		numOfLeaves = 0;
	}

	public Graph(int[][] outAdjLists) {
		init(outAdjLists);
	}

	public void init(int[][] outAdjLists) {
		nodeNum = outAdjLists.length;
		maxOutDeg = 0;
		numOfLeaves = 0;

		this.outAdjLists = outAdjLists;
		reBuildInAdjList();
	}

	public void reBuildInAdjList() {
		if (inAdjLists == null || inAdjLists.length != nodeNum) {
			inAdjLists = new int[nodeNum][];
			inToOutNeighborIx = new int[nodeNum][];
			inAdjCountes = new int[nodeNum];
		} else {
			Arrays.fill(inAdjCountes, 0);
		}

		edgeNum = 0;
		for (int from = 0; from < nodeNum; ++from) {
			int outDeg = outAdjLists[from].length;
			edgeNum += outDeg;
			if (maxOutDeg < outDeg)
				maxOutDeg = outDeg;
			if (outDeg == 1) {
				numOfLeaves++;
			}
			for (int to : outAdjLists[from]) {
				++inAdjCountes[to];
			}
		}

		for (int to = 0; to < nodeNum; ++to) {
			if (inAdjLists[to] == null
					|| inAdjLists[to].length != inAdjCountes[to]) {
				inAdjLists[to] = new int[inAdjCountes[to]];
				inToOutNeighborIx[to] = new int[inAdjCountes[to]];
			}
			inAdjCountes[to] = 0;
		}

		for (int from = 0; from < nodeNum; ++from) {
			final int[] is = outAdjLists[from];
			for (int neighborIx = 0; neighborIx < is.length; ++neighborIx) {
				int to = is[neighborIx];
				inAdjLists[to][inAdjCountes[to]] = from;
				inToOutNeighborIx[to][inAdjCountes[to]] = neighborIx;
				++inAdjCountes[to];
			}
		}
	}

	public int getNodeNum() {
		return nodeNum;
	}

	/**
	 * Return the out degree of a node
	 * 
	 * @param nodeIx
	 *            the node ix
	 * @return the number of adjacent nodes
	 */
	public int outDeg(int nodeIx) {
		return outAdjLists[nodeIx].length;
	}

	public int inDeg(int nodeIx) {
		return inAdjCountes[nodeIx];
	}

	/**
	 * Gets the neighbor given an index in the list.
	 * 
	 * @param node
	 *            the node index
	 * @param neighborIx
	 *            the neighbor index in the list
	 * @return the neighbor id
	 */
	public int getNeighbor(int node, int neighborIx) {
		return outAdjLists[node][neighborIx];
	}

	/**
	 * Gets the neighbor index in the out adjacent list.
	 * 
	 * @param node
	 *            the node
	 * @param neighbor
	 *            the neighbor id
	 * @return the neighbor index in the list of the node
	 */
	public int getNeighborIx(int node, int neighbor) {
		final int[] is = outAdjLists[node];
		for (int ix = 0; ix < is.length; ++ix) {
			if (is[ix] == neighbor) {
				return ix;
			}
		}
		return -1;
	}

	public int getEdgeNum() {
		return this.edgeNum;
	}

	public int getMaxOutDeg() {
		return maxOutDeg;
	}

	public int getNumberOfLeaves() {
		return this.numOfLeaves;
	}

	public int[][] getEdges() {
		return this.outAdjLists;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + edgeNum;
		result = prime * result + Arrays.hashCode(inAdjCountes);
		result = prime * result + Arrays.hashCode(inAdjLists);
		result = prime * result + Arrays.hashCode(inToOutNeighborIx);
		result = prime * result + maxOutDeg;
		result = prime * result + nodeNum;
		result = prime * result + numOfLeaves;
		result = prime * result + Arrays.hashCode(outAdjLists);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Graph)) {
			return false;
		}

		Graph other = (Graph) obj;
		if (edgeNum != other.edgeNum) {
			return false;
		}
		if (maxOutDeg != other.maxOutDeg) {
			return false;
		}
		if (nodeNum != other.nodeNum) {
			return false;
		}
		if (numOfLeaves != other.numOfLeaves) {
			return false;
		}
		if (!Arrays.equals(inAdjCountes, other.inAdjCountes)) {
			return false;
		}
		if (!MatrixUtils.equals(inAdjLists, other.inAdjLists)) {
			return false;
		}
		if (!MatrixUtils.equals(inToOutNeighborIx, other.inToOutNeighborIx)) {
			return false;
		}
		if (!MatrixUtils.equals(outAdjLists, other.outAdjLists)) {
			return false;
		}
		return true;
	}

}
