package bgu.bio.adt.graphs;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;

import bgu.bio.adt.rna.NodeLabel;
import bgu.bio.util.MatrixUtils;


/**
 * A representation of the tree model
 * @author milon
 *
 */
public class Tree extends WeightedGraph {

	private static final int UNSEEN = -1;

	protected int[][] neighborsToEdge;

	/** a sorted array of edges. */
	public int[][] edgeToNeighbors;

	public double[] smoothCost;
	public NodeLabel[] labels;

	public Tree(){
		super();
	}

	/**
	 * Instantiates a new tree. assume the input is legal
	 *
	 * @param edges the list of neighbors for each node in the tree (directed, un-rooted)
	 * @param weights the costs of pruning the subtree of each edge.
	 * @param smoothCost the smooth cost
	 * @param labels the labels
	 */
	public Tree(int[][] edges, double[][] weights, double[] smoothCost, NodeLabel[] labels) {
		super(edges,weights);
		init(edges, labels);
		this.smoothCost = smoothCost;
	}

	public Tree(int[][] edges, NodeLabel[] labels) {
		this(edges,null,null,labels);
	}

	public void init(int[][] edges, NodeLabel[] labels) {
		super.init(edges);

		this.labels = labels;
		neighborsToEdge = new int[nodeNum][];

		for (int nodeIx=0; nodeIx<nodeNum; ++nodeIx){
			neighborsToEdge[nodeIx] = new int[outAdjLists[nodeIx].length];
		}
		edgeToNeighbors = new int[edgeNum][2];
		indexEdges();
	}


	//	protected void indexEdges() {
	//		int nextEdgeIx = 0, secondNodeIx;
	//		for (int firstNodeIx=0; firstNodeIx<nodeNum; ++firstNodeIx){
	//			for (int neighborIx=0; neighborIx<outAdjLists[firstNodeIx].length; ++neighborIx){
	//				secondNodeIx = outAdjLists[firstNodeIx][neighborIx];
	//				neighborsToEdge[firstNodeIx][neighborIx] = nextEdgeIx;
	//				edgeToNeighbors[nextEdgeIx][0] = firstNodeIx;
	//				edgeToNeighbors[nextEdgeIx][1] = secondNodeIx;
	//				++nextEdgeIx;
	//			}
	//		}
	//	}

	public void indexEdges(){
		int nextEdgeIx = 0;

		// marking all edges as "un-indexed":
		for (int firstNodeIx=0; firstNodeIx<nodeNum; ++firstNodeIx){
			Arrays.fill(neighborsToEdge[firstNodeIx], UNSEEN);
		}

		// indexing edges according to a topological order:
		for (int firstNodeIx=0; firstNodeIx<nodeNum; ++firstNodeIx){
			for (int neighborIx=0; neighborIx<outAdjLists[firstNodeIx].length; ++neighborIx){
				nextEdgeIx = indexEdges(firstNodeIx, neighborIx, nextEdgeIx);
			}
		}
	}

	private int indexEdges(int firstNodeIx, int neighborIx, int nextEdgeIx) {
		if (neighborsToEdge[firstNodeIx][neighborIx] == UNSEEN){
			//		neighborsToEdge[firstNodeIx][neighborIx] = SEEN;
			int secondNodeIx = outAdjLists[firstNodeIx][neighborIx];
			for (int secondNeighborIx=0; secondNeighborIx<outAdjLists[secondNodeIx].length; ++secondNeighborIx){
				if (outAdjLists[secondNodeIx][secondNeighborIx] != firstNodeIx){
					nextEdgeIx = indexEdges(secondNodeIx, secondNeighborIx, nextEdgeIx);
				}
			}
			neighborsToEdge[firstNodeIx][neighborIx] = nextEdgeIx;
			edgeToNeighbors[nextEdgeIx][0] = firstNodeIx;
			edgeToNeighbors[nextEdgeIx][1] = neighborIx;
			++nextEdgeIx;
		}
		return nextEdgeIx;
	}

	public int[] edgeAtIndex(int index){
		return edgeToNeighbors[index];
	}

	/**
	 * Gets the label of the node.
	 *
	 * @param index the index of the node
	 * @return the label attached to the label
	 */
	public NodeLabel getLabel(int index) {
		return labels[index];
	}

	public double[] getSmoothCosts() {
		return this.smoothCost;
	}

	public void setSmoothCosts(double[] smoothCosts) {
		this.smoothCost = smoothCosts;
	}

	/**
	 * Print the tree in a graphviz DOT format.
	 *
	 * @param fileName the file name of the dot file
	 * @param extendedInformation should extended information be printed
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toDotFile(String fileName,boolean extendedInformation) throws IOException{
		this.toDotFile(fileName,0, extendedInformation);
	}
	
	/**
	 * Print the tree in a graphviz DOT format.
	 *
	 * @param fileName the file name of the dot file
	 * @param startFrom the node to start the scan from
	 * @param extendedInformation should extended information be printed
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toDotFile(String fileName,int startFrom,boolean extendedInformation) throws IOException{
		FileWriter writer = new FileWriter(fileName);
		toDotFile(writer,startFrom,extendedInformation);
		writer.close();
	}

	/**
	 * To graphviz DOT format. dosen't create the entity but only the data of the tree.
	 *
	 * @param nodePrefix the node prefix
	 * @param startFrom the node to start from
	 * @param out the writer
	 * @param extendedInformation should extended information be printed
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toDotFormat(String nodePrefix,int startFrom, Writer out,boolean extendedInformation) throws IOException {
		for (int i = 0; i < labels.length; i++) {			
			out.write(nodePrefix + i + "[ label=\""+ (extendedInformation ? "("+i+")" : "") +" " + labels[i].toDotLabel() + "\"];\n");
		}

		toDotFileRec(nodePrefix,-1, startFrom, out,extendedInformation);
	}
	
	/**
	 * To graphviz DOT format. dosen't create the entity but only the data of the tree.
	 *
	 * @param nodePrefix the node prefix
	 * @param out the writer
	 * @param extendedInformation should extended information be printed
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toDotFormat(String nodePrefix, Writer out,boolean extendedInformation) throws IOException {
		this.toDotFormat(nodePrefix,0,out,extendedInformation);
	}

	/**
	 * Print the tree in a graphviz DOT format
	 *
	 * @param out the output writer to print the tree to
	 * @param extendedInformation should extended information be printed
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toDotFile(Writer out,boolean extendedInformation) throws IOException{
		this.toDotFile(out, 0, extendedInformation);
	}
	
	/**
	 * Print the tree in a graphviz DOT format.
	 *
	 * @param out the output writer to print the tree to
	 * @param startFrom the node to start the scan from
	 * @param extendedInformation should extended information be printed
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void toDotFile(Writer out,int startFrom,boolean extendedInformation) throws IOException{
			out.write("graph {\n");
	
			this.toDotFormat("n",startFrom,out,extendedInformation);
	
			out.write("}\n");

	}

	private void toDotFileRec(String nodePrefix, int from, int node,Writer out,boolean extendedInformation) throws IOException{		
		for (int x=0;x<outAdjLists[node].length;x++){
			if (from != outAdjLists[node][x]){			

				out.write(nodePrefix + node + " -- " + nodePrefix + outAdjLists[node][x] + (extendedInformation ? "[label=\""+x+"\"]" : "") +";\n");

				toDotFileRec(nodePrefix,node,outAdjLists[node][x], out,extendedInformation);
			}
		}

		if (outAdjLists[node].length > 2){			
			boolean first = true;
			int left = outAdjLists[node].length - 2;
			for (int x=0;x<outAdjLists[node].length;x++){
				if (from != outAdjLists[node][x]){			
					if (first){
						out.write("{rank = same; ");
					}
					first = false;
				}
				out.write(nodePrefix + outAdjLists[node][x]);
				
				if (!first && left >= 0){
					out.write(" -- ");
				}
				left--;
			}
			out.write("[style=invis]};\n");
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(edgeToNeighbors);
		result = prime * result + Arrays.hashCode(labels);
		result = prime * result + Arrays.hashCode(neighborsToEdge);
		result = prime * result + Arrays.hashCode(smoothCost);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Tree)) {
			return false;
		}
		Tree other = (Tree) obj;
		if (!MatrixUtils.equals(edgeToNeighbors, other.edgeToNeighbors)) {
			return false;
		}
		if (!Arrays.equals(labels, other.labels)) {
			return false;
		}
		if (!MatrixUtils.equals(neighborsToEdge, other.neighborsToEdge)) {
			return false;
		}
		if (!Arrays.equals(smoothCost, other.smoothCost)) {
			return false;
		}
		return true;
	}
}
