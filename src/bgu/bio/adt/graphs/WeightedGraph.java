package bgu.bio.adt.graphs;

import java.util.Arrays;

import bgu.bio.util.MatrixUtils;


/**
 * A weighted graph
 * @author milon
 *
 */
public class WeightedGraph extends Graph {

	public double[][] weights;
//	protected double[] totalOutWeight;
	
	public WeightedGraph(){
		super();
	}

	public WeightedGraph(int[][] outAdjLists, double[][] weights) {
		this();
		init(outAdjLists, weights);
	}
	
	public void init(int[][] outAdjLists, double[][] weights) {
		super.init(outAdjLists);
		setWeights(weights);
	}

	public void setWeights (double[][] weights){
//		if (this.weights == null || this.weights.length != weights.length){
//			totalOutWeight = new double[weights.length];
//		}
//		else {
//			Arrays.fill(totalOutWeight, 0);
//		}

		this.weights = weights;
//		for (int i=0; i<weights.length; ++i){
//			for (int j=0; j<weights[i].length; ++j)
//			totalOutWeight[i] += weights[i][j];
//		}
	}
	
	public double[][] getWeights(){
		return weights;
	}
	
	public double getWeight(int from,int to){
		return this.weights[from][to];
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(weights);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof WeightedGraph)) {
			return false;
		}
		WeightedGraph other = (WeightedGraph) obj;
		if (!MatrixUtils.equals(weights, other.weights)) {
			return false;
		}
		return true;
	}
	
	
}
