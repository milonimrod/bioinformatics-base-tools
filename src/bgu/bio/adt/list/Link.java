package bgu.bio.adt.list;

/**
 * Class Representing a link in a {@link PooledLinkedList}.
 */
public class Link<T> {

	/** The next link in the list. */
	private Link<T> next;

	/** The previous link in the list. */
	private Link<T> prev;

	/** The contained data. */
	private T data;

	private boolean isDummy;

	/**
	 * Instantiates a new link.
	 * 
	 * @param data
	 *            the data
	 * @param next
	 *            the next link for the current link
	 * @param prev
	 *            the previous link for the current link
	 */
	public Link(T data, Link<T> next, Link<T> prev) {
		this(data, next, prev, false);
	}

	public Link(T data, Link<T> next, Link<T> prev, boolean isDummy) {
		this.data = data;
		this.next = next;
		this.prev = prev;
		this.isDummy = isDummy;
	}

	/**
	 * Gets the next link.
	 * 
	 * @return the next link
	 */
	public Link<T> getNext() {
		return next;
	}

	/**
	 * Sets the next link.
	 * 
	 * @param next
	 *            the new next link
	 */
	public void setNext(Link<T> next) {
		this.next = next;
	}

	/**
	 * Gets the data .
	 * 
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * Sets the data.
	 * 
	 * @param data
	 *            the new data
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * Gets the previous link.
	 * 
	 * @return the previous link
	 */
	public Link<T> getPrev() {
		return prev;
	}

	/**
	 * Sets the previous link.
	 * 
	 * @param prev
	 *            the new previous link
	 */
	public void setPrev(Link<T> prev) {
		this.prev = prev;
	}

	public boolean isDummy() {
		return this.isDummy;
	}

	public void setDummy(boolean b) {
		this.isDummy = b;
	}

	@Override
	public String toString() {
		return "[" + data.toString() + "]";
	}
}
