package bgu.bio.adt.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * List {@link Iterator} for the {@link PooledLinkedList} class
 * 
 * @author milon
 * 
 * @param <T>
 */
public class ListIterator<T> implements Iterator<T> {
	private Link<T> curr;

	public ListIterator(Link<T> first) {
		curr = first;
	}

	@Override
	public boolean hasNext() {
		return !curr.isDummy();
	}

	@Override
	public T next() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}

		T ans = curr.getData();
		curr = curr.getNext();
		return ans;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
