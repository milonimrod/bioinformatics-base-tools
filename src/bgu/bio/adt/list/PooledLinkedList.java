package bgu.bio.adt.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A pooled liked list that reuse the created {@link Link} objects
 * @author milon
 *
 * @param <T>
 */
public class PooledLinkedList<T> implements Iterable<T> {
	private Link<T> head;
	private Link<T> tail;
	private ArrayList<Link<T>> pool;
	private int size;

	/**
	 * Instantiates a new linked list.
	 * 
	 */
	public PooledLinkedList() {
		this(new ArrayList<Link<T>>());
	}

	public PooledLinkedList(ArrayList<Link<T>> pool) {
		head = new Link<T>(null, null, null, true);
		tail = new Link<T>(null, null, null, true);
		head.setNext(tail);
		tail.setPrev(head);
		this.pool = pool;
		this.size = 0;
	}

	/**
	 * Adds the given data as the first element in the list.
	 * 
	 * @param data
	 *            the data
	 */
	public void addFirst(T data) {
		// create the link and set its next link to the head next link

		Link<T> temp = getLink(data, head, head.getNext());

		// override the next link for the header to be the current added link
		head.setNext(temp);

		// change the prev for the next link
		temp.getNext().setPrev(temp);
		size++;
	}

	/**
	 * Removes the first element in the list.
	 */
	public T removeFirst() {
		if (isEmpty())
			throw new NoSuchElementException();

		Link<T> todelete = head.getNext();

		this.head.setNext(todelete.getNext());
		todelete.getNext().setPrev(this.head);

		T ans = todelete.getData();
		enqueue(todelete);
		size--;
		return ans;
	}

	/**
	 * Adds the given data as the last element in the list.
	 * 
	 * @param data
	 *            the data
	 */
	public void addLast(T data) {
		// create the link and set its next link to the head next link
		Link<T> temp = getLink(data, tail.getPrev(), tail);

		tail.setPrev(temp);

		temp.getPrev().setNext(temp);
		size++;
	}

	/**
	 * Removes the last element in the list.
	 * 
	 * @author <b>students</b>
	 */
	public T removeLast() {
		if (isEmpty())
			throw new NoSuchElementException();

		Link<T> todelete = tail.getPrev();

		this.tail.setPrev(todelete.getPrev());
		todelete.getPrev().setNext(this.tail);

		T ans = todelete.getData();
		enqueue(todelete);
		size--;
		return ans;
	}

	/**
	 * Removes the first occurrence of data in the list (if exist). this method
	 * is using {@link Object#equals(Object)}.
	 * 
	 * @param data
	 *            the data
	 */
	public void remove(T data) {
		Link<T> current = head.getNext();
		while (current != tail && !current.getData().equals(data)) {
			current = current.getNext();
		}

		if (current != tail) { // found it
			current.getPrev().setNext(current.getNext());
			current.getNext().setPrev(current.getPrev());

			enqueue(current);
			size--;
		}
	}

	/**
	 * Gets the first element data field.
	 * 
	 * @return the first element data
	 */
	public T getFirst() {
		if (isEmpty())
			throw new NoSuchElementException();

		return this.head.getNext().getData();
	}

	public Link<T> getFirstLink() {
		if (isEmpty())
			throw new NoSuchElementException();

		return this.head.getNext();
	}

	/**
	 * Gets the last element data field.
	 * 
	 * @return the last element data
	 */
	public T getLast() {
		if (isEmpty())
			throw new NoSuchElementException();

		return this.tail.getPrev().getData();
	}

	public Link<T> getLastLink() {
		if (isEmpty())
			throw new NoSuchElementException();

		return this.tail.getPrev();
	}

	/**
	 * Checks if the list is empty.
	 * 
	 * @return true, if is empty
	 */
	public boolean isEmpty() {
		return head.getNext() == tail;
	}

	public void clear() {
		while (!isEmpty()) {
			removeFirst();
		}
	}

	public int size() {
		return this.size;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(head.getNext());
	}

	@Override
	public String toString() {
		if (isEmpty())
			return "[]";

		StringBuilder sb = new StringBuilder();
		for (T data : this) {
			sb.append(',');
			sb.append(data);
		}
		sb.append(']');
		sb.setCharAt(0, '[');
		return sb.toString();
	}

	private Link<T> getLink(T data, Link<T> prev, Link<T> next) {
		Link<T> ans = pool.size() > 0 ? pool.remove(pool.size() - 1) : null;
		if (ans == null) {
			ans = new Link<T>(data, next, prev);
		} else {
			ans.setData(data);
			ans.setNext(next);
			ans.setPrev(prev);
			ans.setDummy(false);
		}
		return ans;
	}

	private void enqueue(Link<T> link) {
		link.setData(null);
		link.setNext(null);
		link.setPrev(null);
		pool.add(link);
	}

	public static void main(String[] args) {
		PooledLinkedList<String> list = new PooledLinkedList<String>();
		list.addLast("Nimrod");
		System.out.println(list.toString());
	}
}
