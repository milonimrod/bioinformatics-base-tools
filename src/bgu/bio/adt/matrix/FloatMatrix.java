package bgu.bio.adt.matrix;

import java.util.Arrays;

/**
 * Represents a multi dimension matrix of floats using a one dimension array.
 */
public class FloatMatrix {

	/** The array to save the data. */
	protected float[] array;

	/** The multipliers for each dimension. */
	protected final int[] multipliers;

	/** numbers of dimensions. */
	protected final int dimensions;

	/** the size of each dimension. */
	protected final int[] dimensionsSizes;

	/**
	 * Instantiates a new double matrix.
	 * 
	 * @param dimensionsSize
	 *            the dimensions sizes
	 */
	public FloatMatrix(int... dimensionsSize) {
		this.multipliers = new int[dimensionsSize.length];

		// set up the multipliers so the i'th multiplier will be the
		multipliers[multipliers.length - 1] = 1;
		for (int i = multipliers.length - 2; i >= 0; i--) {
			multipliers[i] = multipliers[i + 1] * dimensionsSize[i + 1];
		}
		this.array = new float[dimensionsSize[0] * multipliers[0]];
		this.dimensions = dimensionsSize.length;
		this.dimensionsSizes = Arrays.copyOf(dimensionsSize, this.dimensions);
	}

	/**
	 * Gets the correct value of the coordinates in the array.
	 * 
	 * @param coordinates
	 *            the coordinates array. assume that the array is in size
	 *            {@link this#dimensions}
	 * 
	 * @return the int value of the correct coordinate
	 */
	protected float get(int[] coordinates) {
		int value = 0;
		for (int i = 0; i < dimensions; i++) {
			value += coordinates[i] * multipliers[i];
		}
		return array[value];
	}

	/**
	 * Set the correct value in the right coordinates in the array.
	 * 
	 * @param coordinates
	 *            the coordinates array. assume that the array is in size
	 *            {@link this#dimensions}
	 * @param value
	 *            the value to put in the array
	 */
	protected void set(float value, int[] coordinates) {
		int index = 0;
		for (int i = 0; i < dimensions; i++) {
			index += coordinates[i] * multipliers[i];
		}
		this.array[index] = value;
	}

	/**
	 * Gets the dimension size.
	 * 
	 * @param dimension
	 *            the dimension (starting from 0 as the first dimension)
	 * 
	 * @return the dimension size
	 */
	public int getDimensionSize(int dimension) {
		return this.dimensionsSizes[dimension];
	}

	/**
	 * Initiate the matrix with a given value
	 * 
	 * @param value
	 *            the value to fill the matrix with.
	 */
	public void init(float value) {
		Arrays.fill(this.array, value);
	}

}
