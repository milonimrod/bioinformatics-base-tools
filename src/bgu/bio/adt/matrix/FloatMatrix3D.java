package bgu.bio.adt.matrix;

/**
 * Represent a 3 dimension matrix of floats using a one dimension array. this is
 * a faster customized version of the {@link FloatMatrix}
 */
public class FloatMatrix3D extends FloatMatrix {
	final int mul1, mul2, mul3;

	public FloatMatrix3D(int... dimensions) {
		super(dimensions);
		mul1 = multipliers[0];
		mul2 = multipliers[1];
		mul3 = multipliers[2];
	}

	public final float get(int a, int b, int c) {
		return array[a * mul1 + b * mul2 + c * mul3];
	}

	public final void set(float value, int a, int b, int c) {
		this.array[a * mul1 + b * mul2 + c * mul3] = value;
	}
}
