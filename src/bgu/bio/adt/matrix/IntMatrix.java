package bgu.bio.adt.matrix;

import java.util.Arrays;

/**
 * Represents a multi dimension matrix of integers using a one dimension array.
 */
public class IntMatrix {
	protected int[] array;
	private final int[] multipliers;
	private final int dimensions;
	private final int[] dimensionsSizes;

	public IntMatrix(int... dimensionsSize) {
		this.multipliers = new int[dimensionsSize.length];

		// set up the multipliers so the i'th multiplier will be the
		multipliers[multipliers.length - 1] = 1;
		for (int i = multipliers.length - 2; i >= 0; i--) {
			multipliers[i] = multipliers[i + 1] * dimensionsSize[i + 1];
		}

		this.array = new int[dimensionsSize[0] * multipliers[0]];
		this.dimensions = dimensionsSize.length;
		this.dimensionsSizes = Arrays.copyOf(dimensionsSize, this.dimensions);
	}

	/**
	 * Gets the correct value of the coordinates in the array.
	 * 
	 * @param coordinates
	 *            the coordinates array. assume that the array is in size
	 *            {@link this#dimensions}
	 * 
	 * @return the int value of the correct coordinate
	 */
	protected final int get(int[] coordinates) {
		int value = 0;
		for (int i = 0; i < dimensions; i++) {
			value += coordinates[i] * multipliers[i];
		}
		return array[value];
	}

	/**
	 * Set the correct value in the right coordinates in the array.
	 * 
	 * @param coordinates
	 *            the coordinates array. assume that the array is in size
	 *            {@link this#dimensions}
	 * @param value
	 *            the value to put in the array
	 * 
	 */
	protected final void set(int value, int[] coordinates) {
		int index = 0;
		for (int i = 0; i < dimensions; i++) {
			index += coordinates[i] * multipliers[i];
		}
		this.array[index] = value;
	}

	public int getDimensionSize(int dimension) {
		return this.dimensionsSizes[dimension];
	}

	/**
	 * put the given value in every cell in the matrix
	 * 
	 * @param value
	 *            the value to be inserted to every cell in the matrix
	 */
	public void init(int value) {
		Arrays.fill(this.array, value);
	}

}
