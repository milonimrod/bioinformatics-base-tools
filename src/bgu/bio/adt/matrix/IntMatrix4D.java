package bgu.bio.adt.matrix;

/**
 * Represent a 4 dimension matrix of integers using a one dimension array. this
 * is a faster customized version of the {@link IntMatrix}
 */
public class IntMatrix4D extends IntMatrix {

	private final int[] coordinates;

	public IntMatrix4D(int... dimensions) {
		super(dimensions);
		this.coordinates = new int[4];
	}

	public int get(int a, int b, int c, int d) {
		this.coordinates[0] = a;
		this.coordinates[1] = b;
		this.coordinates[2] = c;
		this.coordinates[3] = d;
		return super.get(coordinates);
	}

	public void set(int value, int a, int b, int c, int d) {
		this.coordinates[0] = a;
		this.coordinates[1] = b;
		this.coordinates[2] = c;
		this.coordinates[3] = d;
		super.set(value, coordinates);
	}
}
