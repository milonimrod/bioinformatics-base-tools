package bgu.bio.adt.queue;

import java.util.ArrayList;
import java.util.Collection;

import bgu.bio.adt.list.PooledLinkedList;

/**
 * This is an implementation of a blocking queue for multi-threading
 * programming. this class is profiled and uses the {@link ArrayList} adt for
 * better Memory / Runtime performance.
 */
public class BlockingQueue<T> {
	private boolean closed = false;

	/** The Constant DEFAULT_SIZE will hold the default limit if non is given. */
	public static final int DEFAULT_SIZE = 1000;

	/** The list that keeps the information . */
	private PooledLinkedList<T> list;

	/** The top limit of the queue size. */
	private int limit;

	private int waitingD, waitingE;

	/**
	 * Instantiates a new blocking queue.
	 * 
	 * @param limit
	 *            the limit of the queue
	 */
	public BlockingQueue(int limit) {
		this.list = new PooledLinkedList<T>();
		this.limit = limit;
	}

	/**
	 * Instantiates a new blocking queue.
	 */
	public BlockingQueue() {
		this(DEFAULT_SIZE);
	}

	/**
	 * Enqueue data into the queue, if the size is above the limit then the
	 * running thread will wait.
	 * 
	 * @param data
	 *            the data
	 */
	public synchronized boolean enqueue(T data) {
		while (this.list.size() >= this.limit) {
			try {
				waitingE++;
				wait();
				waitingE--;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (this.closed) {
			return false;
		}

		this.list.addLast(data);

		// notify only if the size is now 1 - it means that there might be some
		// thread waiting
		if (this.list.size() == 1 && this.waitingD > 0) {
			notifyAll();
		}
		return true;
	}

	/**
	 * Dequeue. thread will wait until the queue has an object.
	 * 
	 * @return the next object in the queue
	 */
	public synchronized T dequeue() {
		while (this.list.size() == 0) {
			if (this.closed) {
				return null;
			}
			try {
				waitingD++;
				wait();
				waitingD--;
			} catch (InterruptedException e) {
				return null;
			}
		}

		T ans = this.list.removeFirst();

		// notify on dequeue if someone is waiting for queue to have room
		if (this.list.size() == this.limit - 1 && waitingE > 0) {
			notifyAll();
		}

		return ans;
	}

	public synchronized T dequeueNoWait() {
		if (this.list.size() == 0) {
			return null;
		}

		T ans = this.list.removeFirst();

		// notify on dequeue if someone is waiting for queue to have room
		if (this.list.size() == this.limit - 1 && waitingE > 0) {
			notifyAll();
		}

		return ans;
	}

	public synchronized void emptyToCollection(Collection<T> collection) {
		while (!this.isEmpty()) {
			collection.add(this.list.removeLast());
		}
	}

	public synchronized void copyToCollection(Collection<T> collection) {
		for (T data : list) {
			collection.add(data);
		}
	}

	public synchronized void addFromCollection(Collection<T> collection) {
		for (T data : collection) {
			this.enqueue(data);
		}
	}

	public synchronized boolean isEmpty() {
		return this.size() == 0;
	}

	public synchronized int size() {
		return this.list.size();
	}

	@Override
	public synchronized String toString() {
		return list.toString();
	}

	public synchronized void close() {
		this.closed = true;
		notifyAll();
	}
}
