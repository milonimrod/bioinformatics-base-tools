package bgu.bio.adt.queue;

import java.util.HashMap;

import bgu.bio.adt.list.PooledLinkedList;

/**
 * Generic Pool that can save memory.
 */
public class Pool<T> {

	/** The queue that is used by the pool. */
	private PooledLinkedList<T> queue;

	/** The bound value (max value if unbounded). */
	private final int bound;

	private long enqueuedAmount, dequeuedAmount, nullAmount, tossedAmount,
			maxAmount;

	/**
	 * Instantiates a new pool.
	 * 
	 * @param size
	 *            the initial size of the pool
	 */
	public Pool(int size, boolean bounded) {
		this.queue = new PooledLinkedList<T>();

		this.bound = bounded ? size : Integer.MAX_VALUE;
	}

	/**
	 * Instantiates a new pool.
	 */
	public Pool() {
		this(1024, false);
	}

	/**
	 * Size of the pool.
	 * 
	 * @return the size
	 */
	public synchronized final int size() {
		return this.queue.size();
	}

	/**
	 * Dequeue an Object from the pool.
	 * 
	 * @return the object
	 */
	public synchronized T dequeue() {

		if (this.queue.size() == 0) {
			this.nullAmount++;
			return null;
		}
		this.dequeuedAmount++;
		return this.queue.removeFirst();
	}

	/**
	 * Enqueue an Object to the pool.
	 * 
	 * @param obj
	 *            the object (can be null).
	 * @return true, if successfully pooled the Object, else if obj is null or
	 *         no room
	 */
	public synchronized boolean enqueue(T obj) {
		if (obj == null)
			return false;

		// only enqueue if the size is larger the bound
		final int CURRENT_SIZE = this.queue.size();
		if (this.bound != Integer.MAX_VALUE || CURRENT_SIZE >= this.bound) {
			this.tossedAmount++;
			return false;
		}

		this.enqueuedAmount++;
		this.queue.addLast(obj);

		if (this.maxAmount < CURRENT_SIZE + 1)
			this.maxAmount = CURRENT_SIZE + 1;
		return true;
	}

	/**
	 * Return an array with the statistics values.
	 * 
	 * @return the long[] the array containing the values
	 */
	public synchronized long[] statistics() {
		return new long[] { this.enqueuedAmount, this.dequeuedAmount,
				this.nullAmount, this.tossedAmount, this.maxAmount, this.size() };
	}

	/**
	 * Return an array with the statistics headers.
	 * 
	 * @return the long[] the array containing the headers
	 */
	public synchronized String[] statisticsHeaders() {
		return new String[] { "enqueued amount", "dequeued amount",
				"null amount", "tossed amount", "max size", "current size" };
	}

	public HashMap<String, Long> statisticsMap() {
		HashMap<String, Long> ans = new HashMap<String, Long>();
		long[] values = statistics();
		String[] headers = statisticsHeaders();
		int i = 0;
		for (String header : headers) {
			ans.put(header, values[i]);
			i++;
		}
		return ans;
	}
}
