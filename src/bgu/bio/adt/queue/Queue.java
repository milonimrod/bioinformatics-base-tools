package bgu.bio.adt.queue;

import bgu.bio.adt.list.PooledLinkedList;

/**
 * Implementation of the {@link java.util.Queue} ADT using
 * {@link PooledLinkedList} to save link memory allocation
 * 
 * @author milon
 * 
 * @param <T>
 */
public class Queue<T> {
	/**
	 * the list that holds the data
	 */
	private PooledLinkedList<T> list;

	/**
	 * Basic constructor
	 */
	public Queue() {
		list = new PooledLinkedList<T>();
	}

	/**
	 * Extract from the queue
	 * 
	 * @return the data
	 */
	public T dequeue() {
		return list.removeFirst();
	}

	/**
	 * @param data
	 */
	public void enqueue(T data) {
		this.list.addLast(data);
	}

	public boolean isEmpty() {
		return this.list.isEmpty();
	}

	public int size() {
		return this.list.size();
	}

	@Override
	public String toString() {
		return list.toString();
	}
}
