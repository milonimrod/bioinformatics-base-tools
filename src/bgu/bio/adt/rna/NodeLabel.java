package bgu.bio.adt.rna;

import gnu.trove.list.array.TCharArrayList;

public class NodeLabel {
	
	protected int type;
	protected TCharArrayList labelValue;
	
	
	public NodeLabel(int type, TCharArrayList labelValue) {
		super();
		this.type = type;
		this.labelValue = labelValue;
	}
	
	public NodeLabel(String str) {
		super();
		type = 0;
		labelValue = new TCharArrayList();
		for (int i=0;i<str.length();i++){
			labelValue.add(str.charAt(i));
		}
	}

	public final int getType() {
		return type;
	}

	public final void setType(int type) {
		this.type = type;
	}

	public final TCharArrayList getLabelValue() {
		return labelValue;
	}

	public final void setLabelValue(TCharArrayList labelValue) {
		this.labelValue = labelValue;
	}

	public String toDotLabel(){
		return new String(labelValue.toArray());
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<labelValue.size();i++){
			sb.append(labelValue.get(i));
		}
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((labelValue == null) ? 0 : labelValue.hashCode());
		result = prime * result + type;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof NodeLabel)) {
			return false;
		}
		
		NodeLabel other = (NodeLabel) obj;
		if (labelValue == null) {
			if (other.labelValue != null) {
				return false;
			}
		} else if (type != other.type) {
			return false;
		}
		else if (!labelValue.equals(other.labelValue)) {
			return false;
		}
		
		return true;
	}
}
