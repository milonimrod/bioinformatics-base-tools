package bgu.bio.adt.rna;

import gnu.trove.list.array.TCharArrayList;

public class RNANodeLabel extends NodeLabel {

	public static final int BASE_PAIR = 0;
	public static final int BASE_INTERVAL = 1;
	public static final int HP_LOOP = 2;
	public static final int ML_LOOP = 3;
	public static final int IL_LOOP = 4;
	public static final int EXTERNAL = 5;
	public static final int ROOT = 6;
	public static final int NUMBER_OF_TYPES = 7;

	protected int sequenceIndStart;
	protected int sequenceIndEnd;
	protected int group;

	public RNANodeLabel(int type, TCharArrayList labelValue) {
		super(type, labelValue);
	}

	public RNANodeLabel(int type, TCharArrayList labelValue,
			int sequenceIndStart, int sequenceIndEnd, int group) {
		super(type, labelValue);
		this.sequenceIndStart = sequenceIndStart;
		this.sequenceIndEnd = sequenceIndEnd;
		this.group = group;
	}

	public final int getSequenceIndStart() {
		return sequenceIndStart;
	}

	public final void setSequenceIndStart(int sequenceIndStart) {
		this.sequenceIndStart = sequenceIndStart;
	}

	public final int getSequenceIndEnd() {
		return sequenceIndEnd;
	}

	public final void setSequenceIndEnd(int sequenceIndEnd) {
		this.sequenceIndEnd = sequenceIndEnd;
	}

	@Override
	public String toDotLabel() {
		return (type == RNANodeLabel.BASE_PAIR ? labelValue.get(0) + ":"
				+ labelValue.get(1) : super.toDotLabel());
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}
}
