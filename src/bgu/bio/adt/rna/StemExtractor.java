package bgu.bio.adt.rna;

import gnu.trove.list.array.TIntArrayList;

import java.util.ArrayList;

import bgu.bio.ds.rna.StemStructure;

public class StemExtractor {
	public static void main(String[] args) {

	}

	public void fillStems(RNASpecificTree tree, ArrayList<StemStructure> list) {
		for (int from = 0; from < tree.getNodeNum(); from++) {
			// start from the external and expand the run to all the rest
			if (tree.getLabel(from).equals(RNANodeLabel.EXTERNAL)) {
				for (int d = 0; d < tree.outDeg(from); d++) {
					int to = tree.getNeighborIx(from, d);
					if (tree.getLabel(to).equals(RNANodeLabel.BASE_PAIR)) {
						fillStems(tree, from, to, list, new TIntArrayList());
					}
				}
			}
		}
	}

	public void fillStems(RNASpecificTree tree, int parent, int node,
			ArrayList<StemStructure> list, TIntArrayList nodes) {

		for (int d = 0; d < tree.outDeg(node); d++) {
			int to = tree.getNeighborIx(node, d);
		}
	}
}
