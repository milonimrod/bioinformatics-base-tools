package bgu.bio.adt.rna.costs;

import bgu.bio.adt.rna.NodeLabel;
import bgu.bio.adt.rna.RNANodeLabel;
import bgu.bio.algorithms.alignment.SequenceAlignment;
import bgu.bio.algorithms.graphs.CostFunction;
import bgu.bio.util.MathOperations;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.RNABasePairAlphabet;

public class RnaSpecificCostFunction implements CostFunction {

	private ScoringMatrix bpScoringMatrix;
	private RNABasePairAlphabet alphabet;
	private SequenceAlignment sequenceAligner;

	public RnaSpecificCostFunction(ScoringMatrix bpScoringMatrix,
			SequenceAlignment sequenceAligner) {
		this.bpScoringMatrix = bpScoringMatrix;
		alphabet = RNABasePairAlphabet.getInstance();
		this.sequenceAligner = sequenceAligner;
	}

	@Override
	public double cost(NodeLabel l1, NodeLabel l2) {
		if (l1.getType() > l2.getType()) {
			NodeLabel tmp = l1;
			l1 = l2;
			l2 = tmp;
		}

		switch (l1.getType()) {
		case RNANodeLabel.BASE_PAIR:
			switch (l2.getType()) {
			case RNANodeLabel.BASE_PAIR:
				return -bpScoringMatrix.score(
						alphabet.mapAndHash(l1.getLabelValue()),
						alphabet.mapAndHash(l2.getLabelValue()));
			default:
				return MathOperations.INFINITY;
			}

		case RNANodeLabel.BASE_INTERVAL:
			switch (l2.getType()) {
			case RNANodeLabel.BASE_INTERVAL:
				sequenceAligner.setSequences(l1.getLabelValue(),
						l2.getLabelValue());
				sequenceAligner.buildMatrix();
				return -sequenceAligner.getAlignmentScore();
			default:
				return MathOperations.INFINITY;
			}
		case RNANodeLabel.HP_LOOP:
			switch (l2.getType()) {
			case RNANodeLabel.HP_LOOP:
				return -10;
			case RNANodeLabel.ML_LOOP:
				return 0;
			case RNANodeLabel.IL_LOOP:
				return 0;
			case RNANodeLabel.EXTERNAL:
				return 0;
			default:
				return MathOperations.INFINITY;
			}
		case RNANodeLabel.ML_LOOP:
			switch (l2.getType()) {
			case RNANodeLabel.ML_LOOP:
				return -7;
			case RNANodeLabel.IL_LOOP:
				return 0;
			case RNANodeLabel.EXTERNAL:
				return -7;
			default:
				return MathOperations.INFINITY;
			}
		case RNANodeLabel.IL_LOOP:
			switch (l2.getType()) {
			case RNANodeLabel.IL_LOOP:
				return -5;
			case RNANodeLabel.EXTERNAL:
				return 10;
			default:
				return MathOperations.INFINITY;
			}
		case RNANodeLabel.EXTERNAL:
			switch (l2.getType()) {
			case RNANodeLabel.EXTERNAL:
				return -10;
			default:
				return MathOperations.INFINITY;
			}
		case RNANodeLabel.ROOT:
			switch (l2.getType()) {
			case RNANodeLabel.ROOT:
				return 0;
			default:
				return MathOperations.INFINITY;
			}
		}

		return MathOperations.INFINITY;
	}

	public final void setBpScoringMatrix(ScoringMatrix bpScoringMatrix) {
		this.bpScoringMatrix = bpScoringMatrix;
	}

	public final void setSequenceAligner(SequenceAlignment sequenceAligner) {
		this.sequenceAligner = sequenceAligner;
	}
}
