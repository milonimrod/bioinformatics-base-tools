package bgu.bio.adt.rna.costs;

import java.util.Arrays;

import bgu.bio.adt.graphs.Tree;
import bgu.bio.adt.rna.NodeLabel;
import bgu.bio.adt.rna.RNANodeLabel;
import bgu.bio.algorithms.graphs.costs.NodeSmoothingCostCalculator;
import bgu.bio.util.MathOperations;

public class RnaSpecificSmoothCost implements NodeSmoothingCostCalculator {

	@Override
	public void calculateCost(Tree tree) {
		double[] smoothCosts = tree.getSmoothCosts();
		if (smoothCosts == null || smoothCosts.length != tree.getNodeNum()){
			 smoothCosts = new double[tree.getNodeNum()];
		}else{
			Arrays.fill(smoothCosts,0.0);
		}
		for (int i=0;i<smoothCosts.length;i++){
			NodeLabel label = tree.getLabel(i);
			switch (label.getType()){
			case RNANodeLabel.BASE_INTERVAL:
				smoothCosts[i] = MathOperations.INFINITY;
				break;
			case RNANodeLabel.BASE_PAIR:
				smoothCosts[i] = 3d;
				break;
			case RNANodeLabel.ML_LOOP:
				smoothCosts[i] = 11d;
				break;
			case RNANodeLabel.HP_LOOP:
				smoothCosts[i] = MathOperations.INFINITY;
				break;
			case RNANodeLabel.IL_LOOP:
				smoothCosts[i] = 5d;
				break;
			case RNANodeLabel.EXTERNAL:
				smoothCosts[i] = 11d;
				break;
			}
		}
		tree.setSmoothCosts(smoothCosts);
	}

}
