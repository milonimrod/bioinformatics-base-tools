package bgu.bio.adt.rna.costs;

import java.util.Arrays;

import bgu.bio.adt.graphs.Tree;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.rna.NodeLabel;
import bgu.bio.adt.rna.RNANodeLabel;
import bgu.bio.algorithms.graphs.costs.TreePruningCostCalculator;
import bgu.bio.util.MathOperations;

/**
 * The Class SizeRelatedTreePruning calculates the cost of pruning a sub-tree
 * according to the size of the sub-tree (i.e. the number of nodes in the
 * sub-tree). This class is not thread safe. it uses inner fields to store data
 * and prevent memoryAllocation
 */
public class RnaSpecificTypeRelatedTreePruning implements
		TreePruningCostCalculator {

	private double[] costs;
	private Pool<int[]> pool;
	int[][][] elementCounters;

	private static int elementNum = 0;
	private static final int UNPAIRED_BASES = elementNum++;
	private static final int BASE_PAIRS = elementNum++;
	private static final int HAIRPINS = elementNum++;
	private static final int MULTI_LOOPS = elementNum++;
	private static final int INTERNAL_LOOPS = elementNum++;
	private static final int INTERVAL_BASES = elementNum++;
	private static final int EXTERNAL_LOOP = elementNum++;
	private static final int ROOT = elementNum++;
	private final double deletionCost;

	/**
	 * Instantiates a new size related tree pruning.
	 * 
	 * @param cost
	 *            the cost of deleting a node
	 */
	public RnaSpecificTypeRelatedTreePruning(double[] costs,double initalDeletionCost, boolean rooted) {
		assert costs.length == elementNum;
		this.costs = costs;
		this.setRooted(rooted);
		this.pool = new Pool<int[]>();
		this.deletionCost = initalDeletionCost;
	}

	public RnaSpecificTypeRelatedTreePruning(boolean local) {
		this(local,false);
	}

	public RnaSpecificTypeRelatedTreePruning(boolean local,boolean rooted) {
		this(local ? new double[] { 0, 0, 0, 0, 0, 0, 0, 0 } : new double[] { 1, 2.5, 5, 3, 2, 2, 0, 0 },0, rooted);
	}

	@Override
	public void calculateCost(Tree tree) {
		int[][] edges = tree.getEdges();
		double[][] weights = tree.getWeights();
		if (weights == null || weights.length != edges.length) {
			weights = new double[edges.length][];
		}
		if (elementCounters == null || elementCounters.length < weights.length) {
			elementCounters = new int[weights.length][][];
		}
		for (int i = 0; i < weights.length; i++) {
			if (weights[i] == null || weights[i].length != edges[i].length) {
				weights[i] = new double[edges[i].length];
			} else {
				Arrays.fill(weights[i], 0);
			}

			if (elementCounters[i] == null
					|| elementCounters[i].length < edges[i].length) {
				elementCounters[i] = new int[edges[i].length][elementNum];
			} else {
				for (int x = 0; x < edges[i].length; x++) {
					int[] ans = pool.dequeue();
					if (ans == null) {
						ans = new int[elementNum];
					}
					elementCounters[i][x] = ans;
				}
			}
		}
		
		final int num = tree.getEdgeNum();
		for (int i = 0; i < num; i++) {
			int[] edge = tree.edgeAtIndex(i);
			final int from = edge[0];
			final int to = tree.getNeighbor(from, edge[1]);
			final int toIdx = edge[1];

			final int deg = tree.outDeg(to);

			for (int n = 0; n < deg; n++) {
				final int sub = tree.getNeighbor(to, n);
				if (sub != from) {
					for (int c = 0; c < elementNum; c++) {
						elementCounters[from][toIdx][c] += elementCounters[to][n][c];
					}
				}
			}

			// add to's data
			NodeLabel label = tree.getLabel(to);
			switch (label.getType()) {
			case RNANodeLabel.BASE_INTERVAL:
				elementCounters[from][toIdx][UNPAIRED_BASES] += label.getLabelValue().size();
				elementCounters[from][toIdx][INTERVAL_BASES]++;
				break;
			case RNANodeLabel.BASE_PAIR:
				elementCounters[from][toIdx][BASE_PAIRS]++;
				break;
			case RNANodeLabel.ML_LOOP:
				elementCounters[from][toIdx][MULTI_LOOPS]++;
				break;
			case RNANodeLabel.HP_LOOP:
				elementCounters[from][toIdx][HAIRPINS]++;
				break;
			case RNANodeLabel.IL_LOOP:
				elementCounters[from][toIdx][INTERNAL_LOOPS]++;
				break;
			case RNANodeLabel.EXTERNAL:
				elementCounters[from][toIdx][EXTERNAL_LOOP]++;
				break;
			case RNANodeLabel.ROOT:
				elementCounters[from][toIdx][ROOT]++;
				break;
			}
			weights[from][toIdx] = deletionCost;
			for (int c = 0; c < elementNum; c++) {
				if (elementCounters[from][toIdx][c] != 0) {
					weights[from][toIdx] += MathOperations.isInfinity(costs[c]) ? costs[c]
							: costs[c] * elementCounters[from][toIdx][c];
				}
			}
		}
		//set the root deletion cost to 0
		if (!MathOperations.isInfinity(weights[1][0])){
			weights[1][0] =  0;
		}
		tree.setWeights(weights);

		// return the arrays to the pool
		for (int i = 0; i < weights.length; i++) {
			for (int j = 0; j < weights[i].length; j++) {				
				Arrays.fill(elementCounters[i][j], 0);
				pool.enqueue(elementCounters[i][j]);
				elementCounters[i][j] = null;

			}
		}
	}

	public void setRooted(boolean rooted) {
		costs[ROOT] = rooted ? MathOperations.INFINITY : 0;
	}
}
