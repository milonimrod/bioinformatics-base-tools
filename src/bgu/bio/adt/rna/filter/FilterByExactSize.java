package bgu.bio.adt.rna.filter;

import bgu.bio.adt.rna.RNASpecificTree;

public class FilterByExactSize implements RNATreeFilter {

	private int size;
	private int delta;

	public FilterByExactSize(int size) {
		this(size,0);
	}
	
	public FilterByExactSize(int size,int delta) {
		super();
		this.size = size;
		this.delta = delta;
	}

	@Override
	public boolean shouldPass(RNASpecificTree obj) {
		return Math.abs(obj.getNodeNum() - size) <= delta;
	}

}
