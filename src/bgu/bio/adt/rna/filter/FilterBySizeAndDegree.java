package bgu.bio.adt.rna.filter;

import bgu.bio.adt.rna.RNASpecificTree;

public class FilterBySizeAndDegree implements RNATreeFilter {

	private int minimalSize;
	private int minimalDegree;
	
	public FilterBySizeAndDegree(int minimalSize, int minimalDegree) {
		super();
		this.minimalSize = minimalSize;
		this.minimalDegree = minimalDegree;
	}



	@Override
	public boolean shouldPass(RNASpecificTree obj) {
		return obj.getNodeNum() >= minimalSize && obj.getMaxOutDeg() >= minimalDegree;
	}

}
