package bgu.bio.adt.rna.filter;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.util.Filter;

public interface RNATreeFilter extends Filter<RNASpecificTree>{

}
