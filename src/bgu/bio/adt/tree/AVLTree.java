package bgu.bio.adt.tree;

import java.util.Comparator;

import bgu.bio.adt.queue.Pool;

/**
 * The Class that represents an AVL tree.
 */
public class AVLTree<K,V> {
	
	/** The root. */
	private AVLNode<K,V> root;
	
	/** The comparator. */
	private Comparator<K> comp;

	private Pool<AVLNode<K, V>> pool;
	/**
	 * Instantiates a new AVL tree.
	 *
	 * @param comp the {@link Comparator} to be used in the tree
	 */
	public AVLTree(Comparator<K> comp) {
		this.root = null;
		this.comp = comp;
		this.pool = new Pool<AVLNode<K,V>>();
	}

	/**
	 * Adds a new Node into the tree.
	 *
	 * @param key the key of the new node
	 * @param data the data of the new node
	 */
	public void add(K key,V data){
		if (isEmpty()){
			this.root = new AVLNode<K,V>(key,data,comp,null,pool);
		}
		else{
			root = this.root.add(key,data);			
		}
	}

	/**
	 * Removes a node n from the tree where n.key is equal (by {@link Comparator}) to the given key.
	 *
	 * @param key the key
	 */
	public void remove(K key){
		if (isEmpty()){
			return;	
		}
		else
			this.root = this.root.remove(key);

	}

	/**
	 * Finds a node n from the tree where n.key is equal (by {@lik Comparator}) to the given key.
	 *
	 * @param key the key
	 * @return the data of the found node, null if node isn't found
	 */
	public V find(K key){
		if (isEmpty()){
			return null;
		}
		return this.root.find(key);
	}
	
	public AVLNode<K, V> findNode(K key){
		if (isEmpty()){
			return null;
		}
		else
			return this.root.findNode(key);
	}

	/**
	 * Finds a node n from the tree where n is the K'th element in the in-Order sequence of the tree.
	 *
	 * @param k a number between 1 and {@link #size()}
	 * @return the data of the found node, null if node isn't found
	 */
	public V findKthElement(int k){
		if (isEmpty()){
			return null;
		}
		else
			return this.root.findKthElement(k);
	}


	/**
	 * Print the current tree in a in-order way.
	 *
	 * @return the string representing the tree in a in-order way
	 */
	public String inOrderToString(){
		StringBuilder sb = new StringBuilder();
		sb.append("digraph {\n");
		sb.append("{rank = same ; \""+root.getKey()+"\"};\n");
		if (root != null)
			root.inOrderToString(sb);
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Size of the tree.
	 *
	 * @return the int
	 */
	public int size(){
		if (isEmpty())
			return 0;
		return root.size();
	}

	/**
	 * Height of the tree.
	 *
	 * @return the int
	 */
	public int height(){
		if (isEmpty()){
			return 0;
		}
		return 0;
	}
	
	/**
	 * Checks if the tree is empty.
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty(){
		return this.root == null;
	}
	
	public AVLNode<K, V> findMinNode(){
		if (isEmpty()){
			return null;
		}
		return this.root.findMin();
	}
	
	public AVLNode<K, V> findMaxNode(){
		if (isEmpty()){
			return null;
		}
		return this.root.findMax();
	}
}


