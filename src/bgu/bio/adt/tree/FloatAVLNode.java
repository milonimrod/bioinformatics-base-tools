package bgu.bio.adt.tree;

import java.util.ArrayList;
import java.util.Comparator;

import bgu.bio.adt.queue.Pool;

/**
 * AVL Node in the {@link AVLTree}.
 */
public class FloatAVLNode<V> {
	/** The size of the tree. */
	private int size;

	/** The height of the tree. */
	private int height;

	/** The key of the current node. */
	private float key;

	/** The data of the current node. */
	private V data;

	private Pool<FloatAVLNode<V>> pool;

	/** All the nodes pointed by the current node. */
	private FloatAVLNode<V> left, right, parent, succ, pred;

	/**
	 * Instantiates a new AVL node.
	 * 
	 * @param key
	 *            the key of the node
	 * @param data
	 *            the data that the node should keep
	 * @param comp
	 *            the comparator to be used in the tree
	 */
	public FloatAVLNode(float key, V data, Pool<FloatAVLNode<V>> pool) {
		this(key, data, null, pool);
	}

	/**
	 * Instantiates a new AVL node.
	 * 
	 * @param key
	 *            the key of the node
	 * @param data
	 *            the data that the node should keep
	 * @param comp
	 *            the comparator to be used in the tree
	 * @param parent
	 *            the parent of the created node
	 */
	public FloatAVLNode(float key, V data, FloatAVLNode<V> parent,
			Pool<FloatAVLNode<V>> pool) {
		init(key, data, parent, pool);
	}

	private void init(float key, V data, FloatAVLNode<V> parent,
			Pool<FloatAVLNode<V>> pool) {
		this.data = data;
		this.key = key;
		this.parent = parent;
		this.pool = pool;

		this.left = null;
		this.right = null;
		this.succ = null;
		this.pred = null;

		this.size = 1;
		this.height = 0;
	}

	/**
	 * Adds the given data to the tree.
	 * 
	 * @param key
	 *            the key
	 * @param data
	 *            the data
	 * @return the root of the tree after insertion and rotations
	 * @author <b>students</b>
	 */
	public FloatAVLNode<V> add(float key, V data) {
		float diff = key - this.key;
		this.size++;
		if (diff < 0) {
			// into the left tree
			if (this.left == null) {
				this.left = pool.dequeue();
				if (this.left == null)
					this.left = new FloatAVLNode<V>(key, data, this, pool);
				else
					this.left.init(key, data, this, pool);

				this.left.setSucc();
				this.left.setPred();
				if (this.left.succ != null) {
					this.left.succ.pred = this.left;
				}
				if (this.left.pred != null) {
					this.left.pred.succ = this.left;
				}
				return this.rotate();

			}
			return this.left.add(key, data);

		} else if (diff >= 0) {
			// into the right tree
			if (this.right == null) {
				this.right = pool.dequeue();
				if (this.right == null) {
					this.right = new FloatAVLNode<V>(key, data, this, pool);
				} else {
					this.right.init(key, data, this, pool);
				}

				this.right.setSucc();
				this.right.setPred();
				if (this.right.succ != null) {
					this.right.succ.pred = this.right;
				}
				if (this.right.pred != null) {
					this.right.pred.succ = this.right;
				}
				return this.rotate();
			}
			return this.right.add(key, data);
		}
		return null;
	}

	/**
	 * Removes a Node which key is equal (by {@link Comparator}) to the given
	 * argument.
	 * 
	 * @param key
	 *            the key
	 * @return the root after deletion and rotations
	 * @author <b>students</b>
	 */
	public FloatAVLNode<V> remove(float key) {
		FloatAVLNode<V> curr = this.findNode(key);

		if (curr == null)
			return null;

		if (curr.right != null && curr.left != null) {
			// we chose the successor to be the replacer node.
			FloatAVLNode<V> replacerNode = curr.getSucc();

			FloatAVLNode<V> startRotationFrom = replacerNode.parent == curr ? replacerNode
					: replacerNode.parent;

			// change the pointer from and to the parent
			if (replacerNode.parent.left == replacerNode) {
				replacerNode.parent.left = replacerNode.right;
			} else {
				replacerNode.parent.right = replacerNode.right;
			}
			if (replacerNode.right != null) {
				replacerNode.right.parent = replacerNode.parent;
			}

			// move the parent
			if (curr.parent != null) {
				if (curr.parent.left == curr) {
					curr.parent.left = replacerNode;
				} else {
					curr.parent.right = replacerNode;
				}
			}
			replacerNode.parent = curr.parent;
			// move the kids
			replacerNode.left = curr.left;
			replacerNode.right = curr.right;
			if (curr.left != null) {
				curr.left.parent = replacerNode;
			}
			if (curr.right != null) {
				curr.right.parent = replacerNode;
			}

			replacerNode.pred = curr.pred;
			if (curr.pred != null) {
				curr.pred.succ = replacerNode;
			}

			// remove the replacer node from the size counter
			FloatAVLNode<V> temp = startRotationFrom;
			while (temp != replacerNode) {
				temp.size--;
				temp = temp.parent;
			}
			replacerNode.size--;

			pool.enqueue(curr);

			// start the rotation process from the successor;
			return startRotationFrom.rotate();
		} else if (curr.right != null && curr.left == null) {
			if (curr.parent != null) {
				if (curr.parent.left == curr) {
					curr.parent.left = curr.right;

				} else {
					curr.parent.right = curr.right;
				}
			}

			if (curr.succ != null)
				curr.succ.pred = curr.pred;

			if (curr.pred != null)
				curr.pred.succ = curr.succ;

			curr.right.parent = curr.parent;

			pool.enqueue(curr);
			return curr.right.rotate();

		} else if (curr.right == null && curr.left != null) {
			if (curr.parent != null) {
				if (curr.parent.left == curr)
					curr.parent.left = curr.left;
				else {
					curr.parent.right = curr.left;
				}
			}

			curr.left.parent = curr.parent;

			if (curr.succ != null)
				curr.succ.pred = curr.pred;

			if (curr.pred != null)
				curr.pred.succ = curr.succ;

			pool.enqueue(curr);
			return curr.left.rotate();
		} else {// leaf
			if (curr.parent == null) {
				pool.enqueue(curr);
				return null;
			}

			if (curr.parent.left == curr) {
				curr.parent.left = null;
			} else {
				curr.parent.right = null;
			}

			if (curr.succ != null)
				curr.succ.pred = curr.pred;

			if (curr.pred != null)
				curr.pred.succ = curr.succ;

			pool.enqueue(curr);
			return curr.parent.rotate();
		}
	}

	/**
	 * Finds a Node which key is equal (by {@link Comparator}) to the given
	 * argument.
	 * 
	 * @param key
	 *            the key of the node
	 * @return the data of the found Node, returns null if node isn't found
	 * @author <b>students</b>
	 */
	public V find(float key) {
		FloatAVLNode<V> tmp = findNode(key);
		if (tmp != null)
			return tmp.data;
		return null;

	}

	/**
	 * Find K'th element in the tree.
	 * 
	 * @param k
	 *            is a number between 1 and {@link #size()}
	 * @return the object
	 * @author <b>students</b>
	 */
	public V findKthElement(int k) {
		FloatAVLNode<V> x = findKthNode(k);
		if (x != null)
			return x.data;
		return null;
	}

	/**
	 * Return the size of the tree.
	 * 
	 * @return the size of the tree.
	 */
	public int size() {
		return this.size;
	}

	/**
	 * Return the height of the tree.
	 * 
	 * @return the height of the tree.
	 */
	public int height() {
		return this.height;
	}

	/**
	 * Gets the key of the current node.
	 * 
	 * @return the key
	 */
	public float getKey() {
		return this.key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "(" + this.key + ")";
	}

	/**
	 * Load the current tree (starting with the current node) into a
	 * {@link StringBuilder} in a in-order way.
	 * 
	 * @param sb
	 *            the {@link StringBuilder}
	 */
	public void inOrderToString(StringBuilder sb) {
		if (this.left != null) {
			this.left.inOrderToString(sb);
			sb.append(this.key + " -> " + left.key + ";\n");
		} else {
			sb.append("nulll" + this.key + " [shape=point];\n");
			sb.append(this.key + " -> nulll" + this.key + ";\n");
		}

		sb.append(this.key + "[label=\"" + this.key + ",s:" + this.size()
				+ ",h:" + this.height() + " + suc:" + succ + " pred: " + pred
				+ "\"];\n");
		if (this.parent != null)
			sb.append(this.key + " -> " + parent.key + "[style=dashed];\n");

		if (this.right != null) {
			this.right.inOrderToString(sb);
			sb.append(this.key + " -> " + right.key + ";\n");
		} else {
			sb.append("nullr" + this.key + " [shape=point];\n");
			sb.append(this.key + " -> nullr" + this.key + ";\n");
		}
	}

	/**
	 * Gets the predecessor of the current node.
	 * 
	 * @return the predecessor
	 */
	public FloatAVLNode<V> getPred() {
		return pred;
	}

	/**
	 * Gets the successor of the current node.
	 * 
	 * @return the successor
	 */
	public FloatAVLNode<V> getSucc() {
		return succ;
	}

	/**
	 * Find node.
	 * 
	 * @param key
	 *            the key
	 * @return the aVL node
	 */
	public FloatAVLNode<V> findNode(float key) {
		float ans = key - this.key;
		if (ans == 0)
			return this;

		if (ans < 0 && this.left != null)
			return this.left.findNode(key);

		if (ans > 0 && this.right != null)
			return this.right.findNode(key);

		return null;
	}

	/**
	 * Left height.
	 * 
	 * @return the int
	 */
	private int leftHeight() {
		int l = -1;
		if (this.left != null)
			l = this.left.height();

		return l;
	}

	/**
	 * Right height.
	 * 
	 * @return the int
	 */
	private int rightHeight() {
		int r = -1;
		if (this.right != null)
			r = this.right.height();

		return r;
	}

	/**
	 * Left size.
	 * 
	 * @return the int
	 */
	private int leftSize() {
		int l = 0;
		if (this.left != null)
			l = this.left.size();

		return l;
	}

	/**
	 * Right size.
	 * 
	 * @return the int
	 */
	private int rightSize() {
		int r = 0;
		if (this.right != null)
			r = this.right.size();

		return r;
	}

	/**
	 * Sets the size.
	 */
	private void setSize() {
		this.size = leftSize() + rightSize() + 1;
	}

	/**
	 * Sets the height.
	 */
	private void setHeight() {
		this.height = Math.max(this.leftHeight(), this.rightHeight()) + 1;
	}

	/**
	 * Sets the succ.
	 */
	private void setSucc() {
		FloatAVLNode<V> curr = this;

		if (curr.right != null) {
			this.succ = curr.right.findMin();
			return;
		}
		while (curr.parent != null) {
			if (curr.parent.left == curr) {
				this.succ = curr.parent;
				return;
			} else
				curr = curr.parent;
		}
		this.succ = null;
	}

	/**
	 * Sets the pred.
	 */
	private void setPred() {
		FloatAVLNode<V> curr = this;
		if (curr.left != null) {
			this.pred = curr.left.findMax();
			return;
		}
		while (curr.parent != null) {
			if (curr.parent.right == curr) {
				this.pred = curr.parent;
				return;
			} else
				curr = curr.parent;
		}
		this.pred = null;
	}

	/**
	 * Find min.
	 * 
	 * @return the aVL node
	 */
	public FloatAVLNode<V> findMin() {
		FloatAVLNode<V> curr = this;
		while (curr.left != null) {
			curr = curr.left;
		}
		return curr;
	}

	/**
	 * Find max.
	 * 
	 * @return the aVL node
	 */
	public FloatAVLNode<V> findMax() {
		FloatAVLNode<V> curr = this;
		while (curr.right != null) {
			curr = curr.right;
		}
		return curr;
	}

	/**
	 * Find kth node.
	 * 
	 * @param k
	 *            the k
	 * @return the aVL node
	 */
	private FloatAVLNode<V> findKthNode(int k) {
		if (k > this.size)
			return null;
		int l = leftSize();
		if (k == 1 + l)
			return this;
		if (k > l + 1) {
			return this.right.findKthNode(k - l - 1);
		}
		return this.left.findKthNode(k);
	}

	/**
	 * Rotate right.
	 * 
	 * @param k2
	 *            the k2
	 * @return the aVL node
	 */
	private FloatAVLNode<V> rotateRight(FloatAVLNode<V> k2) {

		FloatAVLNode<V> k1 = k2.left;
		FloatAVLNode<V> b = k1.right;

		k1.right = k2;
		k1.parent = k2.parent;
		k2.parent = k1;
		k2.left = b;
		if (b != null)
			b.parent = k2;
		if (k1.parent != null)
			if (k1.parent.left == k2)
				k1.parent.left = k1;
			else
				k1.parent.right = k1;

		k2.setHeight();
		k1.setHeight();
		k2.setSize();
		k1.setSize();

		return k1;
	}

	/**
	 * Rotate left.
	 * 
	 * @param k1
	 *            the k1
	 * @return the aVL node
	 */
	private FloatAVLNode<V> rotateLeft(FloatAVLNode<V> k1) {

		FloatAVLNode<V> k2 = k1.right;
		FloatAVLNode<V> b = k2.left;

		k2.left = k1;
		k2.parent = k1.parent;
		k1.parent = k2;
		k1.right = b;
		if (b != null)
			b.parent = k1;
		if (k2.parent != null)
			if (k2.parent.left == k1)
				k2.parent.left = k2;
			else
				k2.parent.right = k2;

		k1.setSize();
		k2.setSize();
		k1.setHeight();
		k2.setHeight();

		return k2;
	}

	/**
	 * Rotate.
	 * 
	 * @return the aVL node
	 */
	private FloatAVLNode<V> rotate() {
		// update the heights
		FloatAVLNode<V> curr = this;
		FloatAVLNode<V> prev = curr;
		while (curr != null) {
			int l = curr.leftHeight();
			int r = curr.rightHeight();

			curr.setHeight();
			curr.setSize();

			// case 1 or 3
			if (l - r >= 2) {
				// case 1
				if (curr.left.leftHeight() >= curr.left.rightHeight()) {
					curr = rotateRight(curr);

				}
				// case 3
				else {
					rotateLeft(curr.left);
					curr = rotateRight(curr);
				}
			} else if (r - l >= 2) {
				// case 2
				if (curr.right.rightHeight() >= curr.right.leftHeight()) {
					curr = rotateLeft(curr);
				}
				// case 4
				else {
					rotateRight(curr.right);
					curr = rotateLeft(curr);
				}
			}
			prev = curr;
			curr = curr.parent;

		}
		return prev;
	}

	/**
	 * @return the data
	 */
	public final V getData() {
		return data;
	}

	public void traversePreOrder(ArrayList<V> list) {
		list.add(this.data);

		if (this.left != null) {
			this.left.traversePreOrder(list);
		}

		if (this.right != null) {
			this.right.traversePreOrder(list);
		}
	}

	public String inOrder() {
		String ans = "";
		if (this.left != null) {
			ans += this.left.inOrder();
		}

		ans += " " + this.data;

		if (this.right != null) {
			ans += this.right.inOrder();
		}

		return ans;
	}
}
