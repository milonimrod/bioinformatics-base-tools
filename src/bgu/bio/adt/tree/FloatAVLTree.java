package bgu.bio.adt.tree;

import java.util.ArrayList;
import java.util.Comparator;

import bgu.bio.adt.queue.Pool;

/**
 * The Class that represents an AVL tree.
 */
public class FloatAVLTree<V> {
	
	/** The root. */
	private FloatAVLNode<V> root;

	private Pool<FloatAVLNode<V>> pool;
	/**
	 * Instantiates a new AVL tree.
	 *
	 */
	public FloatAVLTree() {
		this.root = null;
		this.pool = new Pool<FloatAVLNode<V>>();
	}

	/**
	 * Adds a new Node into the tree.
	 *
	 * @param key the key of the new node
	 * @param data the data of the new node
	 */
	public void add(float key,V data){
		if (isEmpty()){
			this.root = new FloatAVLNode<V>(key,data,null,pool);
		}
		else{
			root = this.root.add(key,data);			
		}
	}

	/**
	 * Removes a node n from the tree where n.key is equal (by {@link Comparator}) to the given key.
	 *
	 * @param key the key
	 */
	public void remove(float key){
		if (isEmpty()){
			return;	
		}
		this.root = this.root.remove(key);

	}

	/**
	 * Finds a node n from the tree where n.key is equal (by {@lik Comparator}) to the given key.
	 *
	 * @param key the key
	 * @return the data of the found node, null if node isn't found
	 */
	public V find(float key){
		if (isEmpty()){
			return null;
		}
		return this.root.find(key);
	}
	
	public FloatAVLNode<V> findNode(float key){
		if (isEmpty()){
			return null;
		}
		return this.root.findNode(key);
	}

	/**
	 * Finds a node n from the tree where n is the K'th element in the in-Order sequence of the tree.
	 *
	 * @param k a number between 1 and {@link #size()}
	 * @return the data of the found node, null if node isn't found
	 */
	public V findKthElement(int k){
		if (isEmpty()){
			return null;
		}
		return this.root.findKthElement(k);
	}


	/**
	 * Print the current tree in a in-order way.
	 *
	 * @return the string representing the tree in a in-order way
	 */
	public String inOrderToString(){
		StringBuilder sb = new StringBuilder();
		sb.append("digraph {\n");
		
		if (root != null){
			sb.append("{rank = same ; \""+root.getKey()+"\"};\n");
			root.inOrderToString(sb);
		}
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Size of the tree.
	 *
	 * @return the int
	 */
	public int size(){
		if (isEmpty())
			return 0;
		return root.size();
	}

	/**
	 * Height of the tree.
	 *
	 * @return the int
	 */
	public int height(){
		if (isEmpty()){
			return 0;
		}
		return 0;
	}
	
	/**
	 * Checks if the tree is empty.
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty(){
		return this.root == null;
	}
	
	public FloatAVLNode<V> findMinNode(){
		if (isEmpty()){
			return null;
		}
		return this.root.findMin();
	}
	
	public FloatAVLNode<V> findMaxNode(){
		if (isEmpty()){
			return null;
		}
		return this.root.findMax();
	}

	public void clear() {
		FloatAVLNode<V> current = root;
		while (current != null){
			FloatAVLNode<V> tmp = current.getSucc();
			pool.enqueue(current);
			current = tmp;
		}
		this.root = null;
	}
	
	public void clear(Pool<V> dataPool) {
		FloatAVLNode<V> current = root;
		while (current != null){
			FloatAVLNode<V> tmp = current.getSucc();
			dataPool.enqueue(current.getData());
			pool.enqueue(current);
			current = tmp;
		}
		this.root = null;
	}
	
	public ArrayList<V> traversePreOrder(){
		ArrayList<V> list = new ArrayList<V>();
		if (root != null){
			root.traversePreOrder(list);
		}
		return list;
	}
	
	@Override
	public String toString() {
		if (root == null){
			return "< empty >";
		}return "<" + root.inOrder() + ">";
	}
}


