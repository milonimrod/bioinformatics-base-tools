package bgu.bio.adt.tuples;

public class DoubleObjectPair<T> {
	private double first;
	private T second;

	public DoubleObjectPair(double first, T second) {
		this.first = first;
		this.second = second;
	}

	public DoubleObjectPair() {
		this.first = 0;
		this.second = null;
	}

	public double getFirst() {
		return first;
	}

	public void setFirst(double first) {
		this.first = first;
	}

	public T getSecond() {
		return second;
	}

	public void setSecond(T second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DoubleObjectPair<?>) {
			DoubleObjectPair<?> other = (DoubleObjectPair<?>) obj;
			return (this == other || (this.first == other.first && this.second
					.equals(other.second)));
		}
		return false;
	}
}
