package bgu.bio.adt.tuples;

public class IntDoublePair {
	private int first;
	private double second;

	public IntDoublePair(int first, double second) {
		this.first = first;
		this.second = second;
	}

	public IntDoublePair() {
		this.first = 0;
		this.second = 0;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public double getSecond() {
		return second;
	}

	public void setSecond(double second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IntDoublePair) {
			IntDoublePair other = (IntDoublePair) obj;
			return (this == other || (this.first == other.first && this.second == other.second));
		}
		return false;
	}
}
