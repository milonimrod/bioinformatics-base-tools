package bgu.bio.adt.tuples;

/**
 * A class that represents an ordered tuple.
 * 
 * @param <int> the type of the first element
 * @param <int> the type of the second element
 * @param <double> the type of the third element
 * @author milon
 */
public class IntIntDoubleTriplet {
	/**
	 * The first element in the pair
	 */
	private int first;
	/**
	 * The second element in the pair
	 */
	private int second;
	/**
	 * The third element in the pair
	 */
	private double third;

	/**
	 * A constructor that sets the three elements
	 * 
	 * @param first
	 * @param second
	 * @param third
	 */
	public IntIntDoubleTriplet(int first, int second, double third) {
		super();
		this.first = first;
		this.second = second;
		this.third = third;
	}

	/**
	 * Return the data contained in the first element
	 * 
	 * @return the data contained in the first element
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * Set the value of the first element
	 * 
	 * @param first
	 *            the new data to be kept in the first element. can be null.
	 */
	public void setFirst(int first) {
		this.first = first;
	}

	/**
	 * Return the data contained in the second element
	 * 
	 * @return the data contained in the second element
	 */
	public int getSecond() {
		return second;
	}

	/**
	 * Set the value of the second element
	 * 
	 * @param first
	 *            the new data to be kept in the second element. can be null.
	 */
	public void setSecond(int second) {
		this.second = second;
	}

	/**
	 * Return the data contained in the third element
	 * 
	 * @return the data contained in the third element
	 */
	public double getThird() {
		return third;
	}

	/**
	 * Set the value of the third element
	 * 
	 * @param first
	 *            the new data to be kept in the third element. can be null.
	 */
	public void setThird(double third) {
		this.third = third;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "<" + first + "," + second + "," + third + ">";
	}
}
