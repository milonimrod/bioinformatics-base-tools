package bgu.bio.adt.tuples;

public class IntLongPair {
	private int first;
	private long second;

	public IntLongPair(int first, long second) {
		this.first = first;
		this.second = second;
	}

	public IntLongPair() {
		this.first = 0;
		this.second = 0;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public long getSecond() {
		return second;
	}

	public void setSecond(long second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IntLongPair) {
			IntLongPair other = (IntLongPair) obj;
			return (this == other || (this.first == other.first && this.second == other.second));
		}
		return false;
	}
}
