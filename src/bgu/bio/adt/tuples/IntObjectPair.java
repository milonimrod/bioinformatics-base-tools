package bgu.bio.adt.tuples;

public class IntObjectPair<T> {
	private int first;
	private T second;

	public IntObjectPair(int first, T second) {
		this.first = first;
		this.second = second;
	}

	public IntObjectPair() {
		this.first = 0;
		this.second = null;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public T getSecond() {
		return second;
	}

	public void setSecond(T second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IntObjectPair<?>) {
			IntObjectPair<?> other = (IntObjectPair<?>) obj;
			return (this == other || (this.first == other.first && this.second
					.equals(other.second)));
		}
		return false;
	}
}
