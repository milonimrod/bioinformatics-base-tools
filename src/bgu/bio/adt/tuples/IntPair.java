package bgu.bio.adt.tuples;

public class IntPair {
	private int first;
	private int second;
	
	public IntPair(int first, int second) {
		this.first = first;
		this.second = second;
	}
	
	public IntPair() {
		this.first = 0;
		this.second = 0;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}
	
	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IntPair)
		{
			IntPair other = (IntPair)obj;
			return (this == other || (this.first == other.first && this.second == other.second));
		}
		return false;
	}
}
