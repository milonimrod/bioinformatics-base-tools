package bgu.bio.adt.tuples;

import java.util.Comparator;

/**
 * @author milon Compares two given {@link IntPair} instances according to the
 *         first value and then the second.
 */
public class IntPairComparator implements Comparator<IntPair> {

	@Override
	public int compare(IntPair arg0, IntPair arg1) {
		if (arg0.getFirst() != arg1.getFirst()) {
			return arg0.getFirst() - arg1.getFirst();
		}
		return arg0.getSecond() - arg1.getSecond();
	}

}
