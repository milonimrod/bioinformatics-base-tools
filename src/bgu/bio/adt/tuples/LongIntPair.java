package bgu.bio.adt.tuples;

public class LongIntPair {
	private long first;
	private int second;

	public LongIntPair(long first, int second) {
		this.first = first;
		this.second = second;
	}

	public LongIntPair() {
		this.first = 0;
		this.second = 0;
	}

	public long getFirst() {
		return first;
	}

	public void setFirst(long first) {
		this.first = first;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LongIntPair) {
			LongIntPair other = (LongIntPair) obj;
			return (this == other || (this.first == other.first && this.second == other.second));
		}
		return false;
	}
}
