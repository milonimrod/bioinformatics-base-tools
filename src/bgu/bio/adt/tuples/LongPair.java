package bgu.bio.adt.tuples;

public class LongPair {
	private long first;
	private long second;

	public LongPair(long first, long second) {
		this.first = first;
		this.second = second;
	}

	public LongPair() {
		this.first = 0;
		this.second = 0;
	}

	public long getFirst() {
		return first;
	}

	public void setFirst(long first) {
		this.first = first;
	}

	public long getSecond() {
		return second;
	}

	public void setSecond(long second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LongPair) {
			LongPair other = (LongPair) obj;
			return (this == other || (this.first == other.first && this.second == other.second));
		}
		return false;
	}
}
