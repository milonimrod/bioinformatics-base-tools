package bgu.bio.adt.tuples;

public class ObjectDoublePair<T> {
	private T first;
	private int second;

	public ObjectDoublePair(T first, int second) {
		this.first = first;
		this.second = second;
	}

	public ObjectDoublePair() {
		this.first = null;
		this.second = 0;
	}

	public T getFirst() {
		return first;
	}

	public void setFirst(T first) {
		this.first = first;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ObjectDoublePair<?>) {
			ObjectDoublePair<?> other = (ObjectDoublePair<?>) obj;
			return (this == other || (this.first.equals(other.first) && this.second == other.second));
		}
		return false;
	}
}
