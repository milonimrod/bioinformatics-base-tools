package bgu.bio.adt.tuples;

public class ObjectIntPair<T> {
	private T first;
	private double second;

	public ObjectIntPair(T first, double second) {
		this.first = first;
		this.second = second;
	}

	public ObjectIntPair() {
		this.first = null;
		this.second = 0;
	}

	public T getFirst() {
		return first;
	}

	public void setFirst(T first) {
		this.first = first;
	}

	public double getSecond() {
		return second;
	}

	public void setSecond(double second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return "<" + getFirst() + "|" + getSecond() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ObjectIntPair<?>) {
			ObjectIntPair<?> other = (ObjectIntPair<?>) obj;
			return (this == other || (this.first.equals(other.first) && this.second == other.second));
		}
		return false;
	}
}
