package bgu.bio.adt.tuples;

/**
 * A class that represents an ordered pair.
 * 
 * @param <T1>
 *            the type of the first element
 * @param <T2>
 *            the type of the second element
 * @author milon
 */
public class Pair<T1, T2> {
	/**
	 * The first element in the pair
	 */
	private T1 first;

	/**
	 * The second element in the pair
	 */
	private T2 second;

	/**
	 * A constructor that sets the two elements
	 * 
	 * @param first
	 * @param second
	 */
	public Pair(T1 first, T2 second) {
		super();
		this.first = first;
		this.second = second;
	}

	/**
	 * Empty constructor
	 */
	public Pair() {
	}

	/**
	 * Return the data contained in the first element
	 * 
	 * @return the data contained in the first element
	 */
	public T1 getFirst() {
		return first;
	}

	/**
	 * Set the value of the first element
	 * 
	 * @param first
	 *            the new data to be kept in the first element. can be null.
	 */
	public void setFirst(T1 first) {
		this.first = first;
	}

	/**
	 * Return the data contained in the second element
	 * 
	 * @return the data contained in the second element
	 */
	public T2 getSecond() {
		return second;
	}

	/**
	 * Set the value of the second element
	 * 
	 * @param first
	 *            the new data to be kept in the second element. can be null.
	 */
	public void setSecond(T2 second) {
		this.second = second;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return first.toString() + "," + second.toString() + "\n";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Pair))
			return false;
		Pair<?, ?> other = (Pair<?, ?>) obj;
		if (this.first == null && this.second == null) {
			return other.first == null && other.second == null;
		} else if (this.first == null) {
			if (this.second.equals(other.second)) {
				return other.first == null;
			} else if (this.second.equals(other.first)) {
				return other.second == null;
			}
			return false;
		} else if (this.second == null) {
			if (this.first.equals(other.second)) {
				return other.first == null;
			} else if (this.first.equals(other.first)) {
				return other.second == null;
			}
			return false;
		} else {
			return (this.first.equals(other.first) && this.second
					.equals(other.second))
					|| (this.first.equals(other.second) && this.second
							.equals(other.first));
		}
	}

}
