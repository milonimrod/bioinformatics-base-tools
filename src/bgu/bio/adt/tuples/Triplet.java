package bgu.bio.adt.tuples;

/**
 * A class that represents an ordered tuple.
 * 
 * @param <T1>
 *            the type of the first element
 * @param <T2>
 *            the type of the second element
 * @param <T3>
 *            the type of the third element
 * @author milon
 */
public class Triplet<T1, T2, T3> {
	/**
	 * The first element in the pair
	 */
	private T1 first;
	/**
	 * The second element in the pair
	 */
	private T2 second;
	/**
	 * The third element in the pair
	 */
	private T3 third;

	/**
	 * A constructor that sets the three elements
	 * 
	 * @param first
	 * @param second
	 * @param third
	 */
	public Triplet(T1 first, T2 second, T3 third) {
		super();
		this.first = first;
		this.second = second;
		this.third = third;
	}

	/**
	 * Return the data contained in the first element
	 * 
	 * @return the data contained in the first element
	 */
	public T1 getFirst() {
		return first;
	}

	/**
	 * Set the value of the first element
	 * 
	 * @param first
	 *            the new data to be kept in the first element. can be null.
	 */
	public void setFirst(T1 first) {
		this.first = first;
	}

	/**
	 * Return the data contained in the second element
	 * 
	 * @return the data contained in the second element
	 */
	public T2 getSecond() {
		return second;
	}

	/**
	 * Set the value of the second element
	 * 
	 * @param first
	 *            the new data to be kept in the second element. can be null.
	 */
	public void setSecond(T2 second) {
		this.second = second;
	}

	/**
	 * Return the data contained in the third element
	 * 
	 * @return the data contained in the third element
	 */
	public T3 getThird() {
		return third;
	}

	/**
	 * Set the value of the third element
	 * 
	 * @param first
	 *            the new data to be kept in the third element. can be null.
	 */
	public void setThird(T3 third) {
		this.third = third;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "<" + first + "," + second + "," + third + ">";
	}
}
