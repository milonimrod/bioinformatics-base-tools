package bgu.bio.algorithms.alignment;

import gnu.trove.list.array.TCharArrayList;

import java.util.Arrays;

import bgu.bio.util.AffineGapScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class AffineGapGlobalSequenceAlignmentNoMatrix implements
		SequenceAlignment {

	/**
	 * The first input string
	 */
	private char[] str1;

	/**
	 * The second input String
	 */
	private char[] str2;

	/**
	 * The lengths of the input strings
	 */
	private int length1, length2;

	/**
	 * The score matrix. The true scores should be divided by the normalization
	 * factor.
	 */
	private double[] dpRow1;
	private double[] dpRow2;
	private double dpCellE;
	private double[] dpRowF;

	private AffineGapScoringMatrix scoringMatrix;

	/**
	 * The normalization factor. To get the true score, divide the integer score
	 * used in computation by the normalization factor.
	 */
	private static final double NORM_FACTOR = 1.0; // default value
	private double normFactor = NORM_FACTOR;

	private AlphabetUtils alphabet;

	public AffineGapGlobalSequenceAlignmentNoMatrix(char[] str1, char[] str2,
			AlphabetUtils alphabet, AffineGapScoringMatrix matrix) {
		this.scoringMatrix = matrix;
		this.alphabet = alphabet;
		this.setSequences(str1, str2);
	}

	public AffineGapGlobalSequenceAlignmentNoMatrix(int size1, int size2,
			AlphabetUtils alphabet, AffineGapScoringMatrix matrix) {
		this.scoringMatrix = matrix;
		this.alphabet = alphabet;
		length1 = size1;
		length2 = size2;

		dpRow1 = new double[length2 + 1];
		dpRow2 = new double[length2 + 1];
		dpRowF = new double[length2 + 1];
		dpCellE = 0d;
	}

	@Override
	public void setSequences(String str1, String str2) {
		this.setSequences(str1.toCharArray(), str2.toCharArray());
	}

	@Override
	public void setSequences(TCharArrayList str1, TCharArrayList str2) {
		if (this.str1 == null || this.str1.length < str1.size()) {
			this.str1 = str1.toArray();
		} else {
			str1.toArray(this.str1);
		}
		if (this.str2 == null || this.str2.length < str2.size()) {
			this.str2 = str2.toArray();
		} else {
			str2.toArray(this.str2);
		}

		boolean init = ((dpRow1 == null) || (str2.size() >= dpRow1.length));
		length1 = str1.size();
		length2 = str2.size();

		if (init) {
			dpRow1 = new double[length2 + 1];
			dpRow2 = new double[length2 + 1];
			dpRowF = new double[length2 + 1];
		}
		dpCellE = 0d;
	}

	@Override
	public void setSequences(char[] str1, char[] str2) {
		setSequences(str1, str1.length, str2, str2.length);
	}

	@Override
	public void setSequences(char[] str1, int size1, char[] str2, int size2) {
		this.str1 = str1;
		this.str2 = str2;

		boolean init = ((dpRow1 == null) || (size2 >= dpRow1.length));
		length1 = size1;
		length2 = size2;

		if (init) {
			dpRow1 = new double[length2 + 1];
			dpRow2 = new double[length2 + 1];
			dpRowF = new double[length2 + 1];
		}
		dpCellE = 0d;
	}

	/**
	 * Compute the similarity score of substitution: use a substitution matrix
	 * if the cost model The position of the first character is 1. A position of
	 * 0 represents a gap.
	 * 
	 * @param i
	 *            Position of the character in str1
	 * @param j
	 *            Position of the character in str2
	 * @return Cost of substitution of the character in str1 by the one in str2
	 */
	protected final double similarity(int i, int j) {
		final char c1 = i == 0 ? this.alphabet.emptyLetter() : str1[i - 1];
		final char c2 = j == 0 ? this.alphabet.emptyLetter() : str2[j - 1];
		return this.scoringMatrix.score(c1, c2);
	}

	/**
	 * Build the score matrix using dynamic programming. Note: The indel scores
	 * must be negative. Otherwise, the part handling the first row and column
	 * has to be modified.
	 */
	@Override
	public void buildMatrix() {
		int i; // length of prefix substring of str1
		int j; // length of prefix substring of str2

		// base case
		dpRow1[0] = 0.0;
		dpCellE = dpRow1[0];
		dpRowF[0] = dpRow1[0];
		// the first row
		for (j = 1; j <= length2; j++) {
			dpRow1[j] = this.scoringMatrix.extendGapCost() * j
					+ this.scoringMatrix.openGapCost();
			dpRowF[j] = dpRow1[j];
		}
		double[] temp;
		// the rest of the matrix
		for (i = 1; i <= length1; i++) {

			// first column
			dpRow2[0] = this.scoringMatrix.extendGapCost() * i
					+ this.scoringMatrix.openGapCost();
			dpCellE = dpRow2[0];

			// rest of he columns
			for (j = 1; j <= length2; j++) {
				dpCellE = max(dpCellE,
						dpRow2[j - 1] + this.scoringMatrix.openGapCost())
						+ this.scoringMatrix.extendGapCost();
				dpRowF[j] = max(dpRowF[j],
						dpRow1[j] + this.scoringMatrix.openGapCost())
						+ this.scoringMatrix.extendGapCost();
				dpRow2[j] = max(dpRow1[j - 1] + similarity(i, j),
						max(dpCellE, dpRowF[j]));
			}
			temp = dpRow2;
			dpRow2 = dpRow1;
			dpRow1 = temp;

		}
	}

	private final double max(double a, double b) {
		return a > b ? a : b;
	}

	/**
	 * Get the maximum value in the score matrix.
	 */
	private double getMaxScore() {
		return this.dpRow1[length2];
	}

	/**
	 * Get the alignment score between the two input strings.
	 */
	@Override
	public double getAlignmentScore() {
		return getMaxScore() / normFactor;
	}

	/**
	 * print the dynamic programming matrix
	 */
	public void printDPMatrix() {
		System.out.println(Arrays.toString(dpRow2));
		System.out.println(Arrays.toString(dpRow1));
	}

	@Override
	public void setNormFactor(double normFactor) {
		this.normFactor = normFactor;
	}

	@Override
	public String[] getAlignment() {
		throw new UnsupportedOperationException("Need to implement");
	}

	@Override
	public AffineGapGlobalSequenceAlignmentNoMatrix cloneAligner() {
		AffineGapGlobalSequenceAlignmentNoMatrix other = new AffineGapGlobalSequenceAlignmentNoMatrix(
				length1, length2, alphabet, scoringMatrix.cloneMatrix());
		return other;
	}
}
