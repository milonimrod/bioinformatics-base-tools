package bgu.bio.algorithms.alignment;

import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;



public class EditDistance {

	public static final boolean TRACE = true;
	/**
	 * The first input string
	 */
	private String str1;

	/**
	 * The second input String
	 */
	private String str2;

	/**
	 * The lengths of the input strings
	 */
	private int length1, length2;

	/**
	 * The score matrix.
	 * The true scores should be divided by the normalization factor.
	 */
	private double[][] dpTable;
	
	

	/**
	 * The normalization factor.
	 * To get the true score, divide the integer score used in computation
	 * by the normalization factor.
	 */
	static final double NORM_FACTOR = 1.0;

	/**
	 * Constants of directions.
	 * Multiple directions are stored by bits.
	 * The zero direction is the starting point.
	 */
	static final int DR_LEFT = 1; // 0001
	static final int DR_UP = 2;   // 0010
	static final int DR_DIAG = 4; // 0100
	static final int DR_ZERO = 8; // 1000
	
	private ScoringMatrix matrix;

	/**
	 * The directions pointing to the cells that
	 * give the maximum score at the current cell.
	 * The first index is the column index.
	 * The second index is the row index.
	 */
	private int[][] prevCells;
	private AlphabetUtils alphabet;


	public EditDistance(String str1,String str2,ScoringMatrix matrix){
		this.dpTable = new double[0][0];
		this.matrix = matrix;
		this.alphabet = matrix.getAlphabet();
		this.setSequences(str1, str2);
	}
	
	public EditDistance(int size1,int size2,ScoringMatrix matrix) {
		length1 = size1;
		length2 = size2;
		
		dpTable = new double[length1+1][length2+1];
		if (TRACE){
			prevCells = new int[length1+1][length2+1];
		}
		this.matrix = matrix;
		this.alphabet = matrix.getAlphabet();
	}

	public void setSequences(String str1, String str2){
		this.str1 = str1;
		this.str2 = str2;
		
		boolean init = str1.length() > dpTable.length || str2.length() > dpTable[0].length;
		length1 = str1.length();
		length2 = str2.length();

		if (init){
			dpTable = new double[length1+1][length2+1];
			if (TRACE){
				prevCells = new int[length1+1][length2+1];
			}
		}
		buildMatrix();
	}

	/**
	 * Compute the similarity score of substitution: use a substitution matrix if the cost model
	 * The position of the first character is 1.
	 * A position of 0 represents a gap.
	 * @param i Position of the character in str1
	 * @param j Position of the character in str2
	 * @return Cost of substitution of the character in str1 by the one in str2
	 */
	private final double similarity(int i, int j) {
		final char c1 = i==0 ? this.alphabet.emptyLetter() : str1.charAt(i - 1);
		final char c2 = j==0 ? this.alphabet.emptyLetter() : str2.charAt(j - 1);
		return this.matrix.score(c1,c2); 
	}
	
	/**
	 * Build the score matrix using dynamic programming.
	 * Note: The indel scores must be negative. Otherwise, the
	 * part handling the first row and column has to be
	 * modified.
	 */
	public void buildMatrix() {

		int i; // length of prefix substring of str1
		int j; // length of prefix substring of str2

		// base case
		dpTable[0][0] = 0.0;
		if (TRACE){
			prevCells[0][0] = DR_ZERO; // starting point
		}

		// the first row
		for (i = 1; i <= length1; i++) {
			dpTable[i][0] = 1 + dpTable[i-1][0];
			if (TRACE){
				prevCells[i][0] = DR_UP;
			}
		}

		// the first column
		for (j = 1; j <= length2; j++) {
			dpTable[0][j] = 1 + dpTable[0][j-1];
			if (TRACE){
				prevCells[0][j] = DR_LEFT;
			}
		}

		// the rest of the matrix
		for (i = 1; i <= length1; i++) {
			for (j = 1; j <= length2; j++) {
				
				final double diagScore = dpTable[i - 1][j - 1] + similarity(i, j);
				final double leftScore = dpTable[i][j - 1] + similarity(0, j);
				final double upScore = dpTable[i - 1][j] + similarity(i, 0);

				
				dpTable[i][j] = min(diagScore, min(upScore, leftScore)); 
				

				// find the directions that give the maximum scores.
				// the bitwise OR operator is used to record multiple
				// directions.
				if (TRACE){
					prevCells[i][j] = 0;
					if (diagScore == dpTable[i][j]) {
						prevCells[i][j] |= DR_DIAG;
					}
					if (leftScore == dpTable[i][j]) {
						prevCells[i][j] |= DR_LEFT;
					}
					if (upScore == dpTable[i][j]) {
						prevCells[i][j] |= DR_UP;
					}
				}
			}
		}
	}
	
	private final double min(double a,double b){
		return a < b ? a : b;
	}
	/**
	 * Get the maximum value in the score matrix.
	 */
	private double getMaxScore() {
		return this.dpTable[length1][length2];
	}

	/**
	 * Get the alignment score between the two input strings.
	 */
	public double getAlignmentScore() {
		return getMaxScore() / NORM_FACTOR;
	}

	/**
	 * Output the local alignments ending in the (i, j) cell.
	 * aligned1 and aligned2 are suffixes of final aligned strings
	 * found in backtracking before calling this function.
	 * Note: the strings are replicated at each recursive call.
	 * Use buffers or stacks to improve efficiency.
	 */
	private String[] printAlignments(int i, int j, String aligned1, String aligned2,String operations,int lastCommand) {
		// we've reached the starting point, so print the alignments	

		if ((prevCells[i][j] & DR_ZERO) > 0) {
			//System.out.println(aligned1);
			//System.out.println(aligned2);
			//System.out.println("");

			// Note: we could check other directions for longer alignments
			// with the same score. we don't do it here.
			return new String[]{aligned1,operations,aligned2};
		}

		// find out which directions to backtrack
		if ((prevCells[i][j] & lastCommand) > 0) {
			if ((lastCommand & DR_UP) > 0) {
				return printAlignments(i-1, j, str1.charAt(i-1) + aligned1, "_" + aligned2," " + operations,DR_UP);
			}
			if ((lastCommand & DR_LEFT) > 0) {
				return printAlignments(i, j-1, "_" + aligned1, str2.charAt(j-1) + aligned2," " + operations,DR_LEFT);
			}
			if ((lastCommand & DR_DIAG) > 0) {
				String op = str1.charAt(i-1) == str2.charAt(j-1) ? "|" : ":";
				return printAlignments(i-1, j-1, str1.charAt(i-1) + aligned1, str2.charAt(j-1) + aligned2,op + operations,DR_DIAG);
			}
		}
		if ((prevCells[i][j] & DR_UP) > 0) {
			return printAlignments(i-1, j, str1.charAt(i-1) + aligned1, "_" + aligned2," " + operations,DR_UP);
		}
		if ((prevCells[i][j] & DR_LEFT) > 0) {
			return printAlignments(i, j-1, "_" + aligned1, str2.charAt(j-1) + aligned2," " + operations,DR_LEFT);
		}
		if ((prevCells[i][j] & DR_DIAG) > 0) {
			String op = str1.charAt(i-1) == str2.charAt(j-1) ? "|" : ":";
			return printAlignments(i-1, j-1, str1.charAt(i-1) + aligned1, str2.charAt(j-1) + aligned2,op + operations,DR_DIAG);
		}
		
		return null;
	}

	public String[] printAlignments() {
		return printAlignments(length1, length2, "", "","",DR_ZERO);
	}
	
	/**
	 * print the dynamic programming matrix
	 */
	public void printDPMatrix()
	{
		System.out.print("   ");
		for (int j=1; j<=length2;j++)
			System.out.print ("   "+str2.charAt(j-1));
		System.out.println();
		for (int i=0; i<=length1; i++)
		{
			if (i>0)
				System.out.print(str1.charAt(i-1)+" ");
			else 
				System.out.print("  ");
			for (int j=0; j<=length2; j++)
			{
				System.out.print(dpTable[i][j]/NORM_FACTOR+" ");
			}
			System.out.println();
		}
	}
}
