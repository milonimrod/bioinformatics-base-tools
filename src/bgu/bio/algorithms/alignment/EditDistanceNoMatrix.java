package bgu.bio.algorithms.alignment;

import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class EditDistanceNoMatrix {

	/**
	 * The first input string
	 */
	private String str1;

	/**
	 * The second input String
	 */
	private String str2;

	/**
	 * The lengths of the input strings
	 */
	private int length1, length2;

	/**
	 * The score matrix. The true scores should be divided by the normalization
	 * factor.
	 */
	private float[] dpRow1;
	private float[] dpRow2;

	private ScoringMatrix matrix;

	private AlphabetUtils alphabet;

	public EditDistanceNoMatrix(String str1, String str2, ScoringMatrix matrix) {
		dpRow1 = new float[length2 + 1];
		dpRow2 = new float[length2 + 1];
		this.matrix = matrix;
		this.alphabet = matrix.getAlphabet();
		this.setSequences(str1, str2);
	}

	public EditDistanceNoMatrix(int size1, int size2, ScoringMatrix matrix) {
		length1 = size1;
		length2 = size2;

		this.matrix = matrix;
		this.alphabet = matrix.getAlphabet();
	}

	public void setSequences(String str1, String str2) {
		this.str1 = str1;
		this.str2 = str2;

		boolean init = ((dpRow1 == null) || (str2.length() >= dpRow1.length));
		length1 = str1.length();
		length2 = str2.length();

		if (init) {
			dpRow1 = new float[length2 + 1];
			dpRow2 = new float[length2 + 1];
		}
		buildMatrix();
	}

	/**
	 * Compute the similarity score of substitution: use a substitution matrix
	 * if the cost model The position of the first character is 1. A position of
	 * 0 represents a gap.
	 * 
	 * @param i
	 *            Position of the character in str1
	 * @param j
	 *            Position of the character in str2
	 * @return Cost of substitution of the character in str1 by the one in str2
	 */
	private final float similarity(int i, int j) {
		final char c1 = i == 0 ? this.alphabet.emptyLetter() : str1
				.charAt(i - 1);
		final char c2 = j == 0 ? this.alphabet.emptyLetter() : str2
				.charAt(j - 1);
		return this.matrix.score(c1, c2);
	}

	/**
	 * Build the score matrix using dynamic programming. Note: The indel scores
	 * must be negative. Otherwise, the part handling the first row and column
	 * has to be modified.
	 */
	public void buildMatrix() {

		int i; // length of prefix substring of str1
		int j; // length of prefix substring of str2
		float[] temp = null;
		// base case
		dpRow1[0] = 0.0f;

		// the first row
		for (j = 1; j <= length2; j++) {
			dpRow1[j] = this.matrix.score(alphabet.emptyLetter(),
					str2.charAt(j - 1))
					+ dpRow1[j - 1];
		}

		// the rest of the matrix
		for (i = 1; i <= length1; i++) {
			// first column
			dpRow2[0] = dpRow1[0]
					+ this.matrix.score(str1.charAt(i - 1),
							alphabet.emptyLetter());

			// rest of the table
			for (j = 1; j <= length2; j++) {
				dpRow2[j] = min(dpRow1[j - 1] + similarity(i, j), dpRow2[j - 1]
						+ similarity(0, j), dpRow1[j] + similarity(i, 0));
			}

			temp = dpRow2;
			dpRow2 = dpRow1;
			dpRow1 = temp;
		}
	}

	private final float min(float a, float b, float c) {
		float min = a;
		if (b < min)
			min = b;
		if (c < min)
			min = c;
		return min;
	}

	/**
	 * Get the maximum value in the score matrix.
	 */
	private float getMaxScore() {
		return this.dpRow1[length2];
	}

	/**
	 * Get the alignment score between the two input strings.
	 */
	public float getAlignmentScore() {
		return getMaxScore();
	}
}
