package bgu.bio.algorithms.alignment;

import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

public class GlobalSequenceAlignment extends AbsSequenceAlignment {
	public GlobalSequenceAlignment(String str1, String str2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(str1, str2, alphabet, matrix);
	}

	public GlobalSequenceAlignment(int size1, int size2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(size1, size2, alphabet, matrix);
	}

	private final double max(double a, double b) {
		return a > b ? a : b;
	}

	/**
	 * Get the maximum value in the score matrix.
	 */
	@Override
	protected double getMaxScore() {
		return this.dpTable[length1][length2];
	}

	public static void main(String[] args) {
		GlobalSequenceAlignment alignment = new GlobalSequenceAlignment(
				"AGCGCGUU", "GUCAGACG", RnaAlphabet.getInstance(),
				new IdentityScoringMatrix(RnaAlphabet.getInstance()));
		alignment.buildMatrix();

		// alignment.printAlignments();
		alignment.printDPMatrix();
		System.out.println("score is: " + alignment.getAlignmentScore());
		// alignment.printDPMatrixInLatexMatrix();
	}

	@Override
	protected void initTable() {
		int i; // length of prefix substring of str1
		int j; // length of prefix substring of str2

		// base case
		dpTable[0][0] = 0.0;
		prevCells[0][0] = DR_ZERO; // starting point

		// the first column
		for (i = 1; i <= length1; i++) {
			dpTable[i][0] = this.scoringMatrix.score(str1.get(i - 1),
					alphabet.emptyLetter())
					+ dpTable[i - 1][0];
			prevCells[i][0] = DR_UP; // marks that we arrived "from above" to
										// i,0
		}

		// the first row
		for (j = 1; j <= length2; j++) {
			dpTable[0][j] = this.scoringMatrix.score(alphabet.emptyLetter(),
					str2.get(j - 1)) + dpTable[0][j - 1];
			prevCells[0][j] = DR_LEFT; // marks that we arrived "from left" to
										// 0,j
		}
	}

	@Override
	protected void computeCell(int i, int j) {
		final double diagScore = dpTable[i - 1][j - 1] + similarity(i, j);
		final double leftScore = dpTable[i][j - 1] + similarity(0, j);
		final double upScore = dpTable[i - 1][j] + similarity(i, 0);

		dpTable[i][j] = max(diagScore, max(upScore, leftScore));

		// find the directions that give the maximum scores.
		// the bitwise OR operator is used to record multiple
		// directions.
		prevCells[i][j] = 0;
		if (diagScore == dpTable[i][j]) {
			prevCells[i][j] |= DR_DIAG;
		}
		if (leftScore == dpTable[i][j]) {
			prevCells[i][j] |= DR_LEFT;
		}
		if (upScore == dpTable[i][j]) {
			prevCells[i][j] |= DR_UP;
		}
	}

	@Override
	public GlobalSequenceAlignment cloneAligner() {
		System.out.println(length1 + " " +length2);
		GlobalSequenceAlignment other = new GlobalSequenceAlignment(length1,
				length2, alphabet, scoringMatrix.cloneMatrix());
		return other;
	}
}
