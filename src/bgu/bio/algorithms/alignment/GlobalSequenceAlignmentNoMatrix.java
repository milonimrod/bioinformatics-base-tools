package bgu.bio.algorithms.alignment;

import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class GlobalSequenceAlignmentNoMatrix extends
		AbsSequenceAlignmentNoMatrix {

	public GlobalSequenceAlignmentNoMatrix(char[] str1, char[] str2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(str1, str2, alphabet, matrix);
	}

	public GlobalSequenceAlignmentNoMatrix(int size1, int size2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(size1, size2, alphabet, matrix);
	}

	private final double max(double a, double b, double c) {
		double max = a;
		if (b > max)
			max = b;
		if (c > max)
			max = c;
		return max;
	}

	@Override
	protected void initTable() {
		int i, j;
		for (j = 1; j <= length2; j++) {
			dpRow1[j] = this.scoringMatrix.score(alphabet.emptyLetter(),
					str2.get(j - 1))
					+ dpRow1[j - 1];
		}

		// the rest of the matrix
		for (i = 1; i <= length1; i++) {
			// first column
			dpRow2[0] = dpRow1[0]
					+ this.scoringMatrix.score(str1.get(i - 1),
							alphabet.emptyLetter());
		}
	}

	@Override
	protected void calculateCell(int i, int j) {
		dpRow2[j] = max(dpRow1[j - 1] + similarity(i, j), dpRow2[j - 1]
				+ similarity(0, j), dpRow1[j] + similarity(i, 0));
	}

	@Override
	protected double getMaxScore() {
		return this.dpRow1[length2];
	}

	@Override
	public String[] getAlignment() {
		return new String[2];
	}

	@Override
	public GlobalSequenceAlignmentNoMatrix cloneAligner() {
		GlobalSequenceAlignmentNoMatrix other = new GlobalSequenceAlignmentNoMatrix(
				length1, length2, alphabet, scoringMatrix.cloneMatrix());
		return other;
	}
}
