package bgu.bio.algorithms.alignment;

import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class LocalSequenceAlignment extends AbsSequenceAlignment {

	protected double maxScore;
	protected int maxI;
	protected int maxJ;

	public LocalSequenceAlignment(String str1, String str2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(str1, str2, alphabet, matrix);
	}

	public LocalSequenceAlignment(int size1, int size2, AlphabetUtils alphabet,
			ScoringMatrix matrix) {
		super(size1, size2, alphabet, matrix);
	}

	private final double max(double a, double b) {
		return a > b ? a : b;
	}

	/**
	 * Get the maximum value in the score matrix.
	 */
	@Override
	protected double getMaxScore() {
		return maxScore;
	}

	@Override
	protected void initTable() {
		int i; // length of prefix substring of str1
		int j; // length of prefix substring of str2

		maxScore = 0;
		// base case
		dpTable[0][0] = 0.0;
		prevCells[0][0] = DR_ZERO; // starting point

		// the first column
		for (i = 1; i <= length1; i++) {
			dpTable[i][0] = 0;
			prevCells[i][0] = DR_ZERO; // marks that we arrived "from above" to
										// i,0
		}

		// the first row
		for (j = 1; j <= length2; j++) {
			dpTable[0][j] = 0;
			prevCells[0][j] = DR_ZERO; // marks that we arrived "from left" to
										// 0,j
		}
	}

	@Override
	protected void computeCell(int i, int j) {
		final double diagScore = dpTable[i - 1][j - 1] + similarity(i, j);
		final double leftScore = dpTable[i][j - 1] + similarity(0, j);
		final double upScore = dpTable[i - 1][j] + similarity(i, 0);

		dpTable[i][j] = max(0, max(diagScore, max(upScore, leftScore)));

		if (dpTable[i][j] > maxScore) {
			maxScore = dpTable[i][j];
			maxI = i;
			maxJ = j;
		}

		// find the directions that give the maximum scores.
		// the bitwise OR operator is used to record multiple
		// directions.
		prevCells[i][j] = 0;
		if (diagScore == dpTable[i][j]) {
			prevCells[i][j] |= DR_DIAG;
		}
		if (leftScore == dpTable[i][j]) {
			prevCells[i][j] |= DR_LEFT;
		}
		if (upScore == dpTable[i][j]) {
			prevCells[i][j] |= DR_UP;
		}
		if (0 == dpTable[i][j]) {
			prevCells[i][j] |= DR_ZERO;
		}
	}

	@Override
	public void printAlignments() {
		printAlignments(maxI, maxJ, "", "");
	}

	@Override
	public LocalSequenceAlignment cloneAligner() {
		LocalSequenceAlignment other = new LocalSequenceAlignment(length1,
				length2, alphabet, scoringMatrix.cloneMatrix());
		return other;
	}
}
