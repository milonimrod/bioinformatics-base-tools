package bgu.bio.algorithms.alignment;

import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class LocalSequenceAlignmentNoMatrix extends
		AbsSequenceAlignmentNoMatrix {

	private double maxScore;

	public LocalSequenceAlignmentNoMatrix(char[] str1, char[] str2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(str1, str2, alphabet, matrix);
	}

	public LocalSequenceAlignmentNoMatrix(int size1, int size2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(size1, size2, alphabet, matrix);
	}

	private final double max(double a, double b, double c) {
		double max = a;
		if (b > max)
			max = b;
		if (c > max)
			max = c;
		return max;
	}

	@Override
	protected void initTable() {
		maxScore = 0;
		int i, j;
		for (j = 1; j <= length2; j++) {
			dpRow1[j] = 0;
		}

		// the rest of the matrix
		for (i = 1; i <= length1; i++) {
			// first column
			dpRow2[0] = 0;
		}
	}

	@Override
	protected void calculateCell(int i, int j) {
		dpRow2[j] = Math.max(
				0,
				max(dpRow1[j - 1] + similarity(i, j), dpRow2[j - 1]
						+ similarity(0, j), dpRow1[j] + similarity(i, 0)));
		if (dpRow2[j] > maxScore) {
			maxScore = dpRow2[j];
		}
	}

	@Override
	protected double getMaxScore() {
		return maxScore;
	}
	
	@Override
	public String[] getAlignment() {
		return new String[2];
	}
	
	@Override
	public LocalSequenceAlignmentNoMatrix cloneAligner() {
		LocalSequenceAlignmentNoMatrix other = new LocalSequenceAlignmentNoMatrix(
				length1, length2, alphabet, scoringMatrix.cloneMatrix());
		return other;
	}
}
