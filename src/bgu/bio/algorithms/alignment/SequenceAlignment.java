package bgu.bio.algorithms.alignment;

import gnu.trove.list.array.TCharArrayList;

public interface SequenceAlignment {
	public void setSequences(String str1, String str2);

	public void setSequences(char[] str1, char[] str2);

	public void setSequences(char[] str1, int size1, char[] str2, int size2);

	public void setSequences(TCharArrayList str1, TCharArrayList str2);

	public void buildMatrix();

	public double getAlignmentScore();
	
	public String[] getAlignment();

	public void setNormFactor(double factor);
	
	
	/**
	 * Create a new copy of the Stem alignment
	 * @return
	 */
	SequenceAlignment cloneAligner();
}
