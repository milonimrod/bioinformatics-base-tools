package bgu.bio.algorithms.alignment;

import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;

public class SuffixFreeLocalAlignment extends LocalSequenceAlignmentNoMatrix {

	double maxScore;

	/**
	 * @param str1
	 * @param str2
	 * @param alphabet
	 * @param matrix
	 */
	public SuffixFreeLocalAlignment(char[] str1, char[] str2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(str1, str2, alphabet, matrix);
	}

	/**
	 * @param size1
	 * @param size2
	 * @param alphabet
	 * @param matrix
	 */
	public SuffixFreeLocalAlignment(int size1, int size2,
			AlphabetUtils alphabet, ScoringMatrix matrix) {
		super(size1, size2, alphabet, matrix);
	}

	@Override
	protected double getMaxScore() {
		// the maximal score is in the best alignment between the ends
		return this.dpRow1[length2];
	}

}
