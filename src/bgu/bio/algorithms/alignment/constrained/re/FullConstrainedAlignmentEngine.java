package bgu.bio.algorithms.alignment.constrained.re;

import bgu.bio.ds.automata.TransitionTable;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.constrain.ConstrainedAlphabet;

public abstract class FullConstrainedAlignmentEngine extends ConstrainedAlignmentEngine{
	protected float cutoff;
	protected boolean foundUsingCutoff;
	
	public FullConstrainedAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet,boolean trace) {

		super(score, tr, constAlphabet,trace);
		cutoff = Float.NaN;
	}

	/**
	 * Instantiates a new full constrained alignment engine. initiating the tables according to the given dimensions
	 * 
	 * @param score the score
	 * @param tr the transition table to use in the alignment
	 * @param constAlphabet the constraint alphabet
	 * @param dimension2Size the dimension2 size
	 * @param dimension1Size the dimension1 size
	 */
	public FullConstrainedAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet,int dimension1Size,int dimension2Size,boolean trace) {

		this(score, tr, constAlphabet,trace);
		this.rebuildTheTables(dimension1Size, dimension2Size);
		cutoff = Float.NaN;
	}

	public void setCutoff(float cutoff){
		this.cutoff = cutoff;
	}

	/** dynamic programming */	
	public void align(char[] s1, char[] s2){
		this.foundUsingCutoff = false;
		this.s1 = s1;
		this.s2 = s2;

		s1Length = s1.length+1;
		s2Length = s2.length+1;

		this.rebuildTheTables(s1Length, s2Length);
		
		
		init();
		
		int[] acceptingStates = tr.getAcceptingStates();
		
		//Run on all the cells 
		for (int i=0; i < this.s1Length; i++){
			for (int j=0; j < this.s2Length; j++){
				//initialize each cell data
				for (int k=0;k<tr.getNumOfStates();k++){ 
					dpTable.set(Float.NEGATIVE_INFINITY,i,j,k);
				}
				
				calculateSingleCell(i,j);
				
				//if cutoff is given the pass on all the accepting states for the cutoff value or bigger
				if (!Float.isNaN(cutoff)){
					for (int x=0;x<acceptingStates.length;x++){
						if (dpTable.get(i, j, acceptingStates[x]) >= cutoff){
							this.foundUsingCutoff = true;
							return;
						}
					}		
				}
			}
		}
	}
}
