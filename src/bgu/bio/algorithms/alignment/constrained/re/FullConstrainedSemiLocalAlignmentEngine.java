package bgu.bio.algorithms.alignment.constrained.re;

import bgu.bio.ds.automata.TransitionTable;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.constrain.ConstrainedAlphabet;


public class FullConstrainedSemiLocalAlignmentEngine extends FullConstrainedAlignmentEngine {
	// Optimal score data
	protected int optimalI, optimalJ;
	protected int DPcounter;

	public FullConstrainedSemiLocalAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet,boolean trace) {
		super(score, tr, constAlphabet,trace);
	}

	public FullConstrainedSemiLocalAlignmentEngine(ScoringMatrix scoring,
			TransitionTable tr, ConstrainedAlphabet coAlphabet, int dimension1Size, int dimension2Size,boolean trace) {
		super(scoring, tr, coAlphabet, dimension1Size, dimension2Size,trace);
	}
	
	public FullConstrainedSemiLocalAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet) {
		this(score, tr, constAlphabet, false);
	}

	public FullConstrainedSemiLocalAlignmentEngine(ScoringMatrix scoring,
			TransitionTable tr, ConstrainedAlphabet coAlphabet, int dimension1Size, int dimension2Size) {
		this(scoring, tr, coAlphabet, dimension1Size, dimension2Size, false);
	}

	@Override
	public double getOptimalScore() {
		double optimalScore=Double.NEGATIVE_INFINITY;
		int[] acceptingStates = tr.getAcceptingStates();
		// Find final state in the last column with maximal score
		for (int i=0; i< this.s1Length; i++)
		{
			for (int j=0;j<acceptingStates.length;j++){
				int accepting = acceptingStates[j];
				double temp = dpTable.get(i,this.s2Length - 1,accepting); // final state

				if ((!Double.isNaN(temp)) && (optimalScore < temp)) {
					optimalScore=temp;
				}
			}
		}

		return optimalScore;
	}

	public String[] getAlignment(){
		double optimalScore=Double.NEGATIVE_INFINITY;
		int bestI = 0;
		int bestT = 0;
		int[] acceptingStates = tr.getAcceptingStates();
		// Find final state in the last column with maximal score
		for (int i=0; i< this.s1Length; i++)
		{
			for (int j=0;j<acceptingStates.length;j++){
				int accepting = acceptingStates[j];
				double temp = dpTable.get(i,this.s2Length - 1,accepting); // final state
				if ((!Double.isNaN(temp)) && (optimalScore < temp)) {
					optimalScore=temp;
					bestI = i;
					bestT = accepting;
				}
			}
		}
		return this.getAlignment(bestI, bestT);
	}
	
	public String[] getAlignment(int bestI,int bestT){
		String align1 = "";
		String align2 = "";
		String types = "";
		int bestJ = this.s2Length - 1;
		//int counter = dpTraceCounter.get(bestI,bestJ,bestT);
		while(bestJ != 0){ //running on all the columns until the first one
			int nextI = dpTrace.get(bestI,bestJ,bestT,0);
			int nextJ = dpTrace.get(bestI,bestJ,bestT,1);
			int nextT = dpTrace.get(bestI,bestJ,bestT,2);
			if (!((nextI == bestI) && (nextJ == bestJ))){
				align1 = ((nextI == bestI)? "-" : this.s1[nextI]) + align1;
				align2 = ((nextJ == bestJ)? "-" : this.s2[nextJ]) + align2;
				char c = '*';
				if ((nextI != bestI) && (nextJ != bestJ) && this.score.match(this.s1[nextI] , this.s2[nextJ])) {
					c = '|';
				}
				types = c + types;
				bestI = nextI ;
				bestJ = nextJ;
			}
			bestT = nextT;
		}
		
		return new String[]{align1, types, align2};
	}
	
	@Override
	public String optimalAlignmentToHTML() {
		double optimalScore=Double.NEGATIVE_INFINITY;
		int bestI = 0;
		int bestT = 0;
		int[] acceptingStates = tr.getAcceptingStates();
		// Find final state in the last column with maximal score
		for (int i=0; i< this.s1Length; i++)
		{
			for (int j=0;j<acceptingStates.length;j++){
				int accepting = acceptingStates[j];
				double temp = dpTable.get(i,this.s2Length - 1,accepting); // final state
				if ((!Double.isNaN(temp)) && (optimalScore < temp)) {
					optimalScore=temp;
					bestI = i;
					bestT = accepting;
				}
			}
		}	

		return printAlignment(bestI,bestT);
	}

	@Override
	public int countSubOptimalScores(double delta, double optimal) {
		int counter = 0;
		int[] acceptingStates = tr.getAcceptingStates();
		// Find final state in the last column with maximal score
		for (int i=0; i< this.s1Length; i++)
		{
			for (int j=0;j<acceptingStates.length;j++){
				int accepting = acceptingStates[j];
				double temp = dpTable.get(i,this.s2Length - 1,accepting); // final state
				if ((!Double.isNaN(temp)) && (Math.abs(optimal - temp) < delta) )
				{
					counter++;
				}
			}
		}
		return counter;
	}

	/**
	 * Print the alignment starting from cell (bestI,bestJ) in the final state.
	 * @param bestI
	 * @param bestJ
	 */
	private String printAlignment(int bestI,int bestT) {
		String align1 = "";
		String align2 = "";
		int bestJ = this.s2Length - 1;
		//int counter = dpTraceCounter.get(bestI,bestJ,bestT);
		while(bestJ != 0){ //running on all the columns until the first one
			int nextI = dpTrace.get(bestI,bestJ,bestT,0);
			int nextJ = dpTrace.get(bestI,bestJ,bestT,1);
			int nextT = dpTrace.get(bestI,bestJ,bestT,2);
			if (!((nextI == bestI) && (nextJ == bestJ))){
				align1 = ((nextI == bestI)? "-" : this.s1[nextI]) + align1;
				align2 = ((nextJ == bestJ)? "-" : this.s2[nextJ]) + align2;
				bestI = nextI ;
				bestJ = nextJ;
			}
			bestT = nextT;
		}
		String ans = align1;
		ans += "\n" + align2;
		
		return ans;
	}

	@Override
	protected void init() {
		dpTable.set(0.0f,0, 0, startingStateId);
	}

	@Override
	public void calculateSingleCell(int i, int j) {
		if(i==0 && j==0){

			//dpTable[0][0][startingStateId]=0.0;
			dpTable.set(0.0f,0, 0, startingStateId);
		}
		else {
			// Update optimal score per state in the [i][j] cell to be zero - because of the local alignment

			//dpTable[i][j][startingStateId]=Double.NaN;
			dpTable.set(Float.NEGATIVE_INFINITY,i, j, startingStateId);
			DPcounter++;

			if (j == 0) // the semi local allows zeros in the first column
			{
				//dpTable[i][j][startingStateId]=0.0;
				dpTable.set(0.0f,i, j, startingStateId);
				DPcounter++;
			}

			// Replace
			if ((i!=0) && (j!=0)) {
				this.calculateByOtherCell(i, j, 1 , 1, s1[i-1], s2[j-1]);
			}
			// Delete
			if (i!=0) {
				this.calculateByOtherCell(i, j, 1, 0, s1[i-1], this.score.getAlphabet().emptyLetter());
			}
			// Insert
			if (j!=0)  {
				this.calculateByOtherCell(i, j, 0, 1, this.score.getAlphabet().emptyLetter(),s2[j-1]);
			}


		}	
	}

	private final void calculateByOtherCell(int i,int j,int iDiff,int jDiff,char s1Char,char s2Char){

		float actionScore = score.score(s1Char, s2Char);
		// Get all possible constraint characters that fit the two read characters				
		int [] possibleConstraintHashedChars = this.constAlphabet.getCharMapping(s1Char, s2Char);
		for (int charIdx=0; charIdx<possibleConstraintHashedChars.length;charIdx++){
			int[][] charTransitions = this.tr.getTransitions(possibleConstraintHashedChars[charIdx]);

			for (int originIdx=0; originIdx< charTransitions.length; originIdx++){
				
				final float val = dpTable.get(i - iDiff, j -jDiff, originIdx) + actionScore;
				
				for (int destIdx=0; destIdx< charTransitions[originIdx].length; destIdx++){	
					int destStateId = charTransitions[originIdx][destIdx];
					
					if (val > dpTable.get(i, j, destStateId)){

						dpTable.set(val,i, j, destStateId);
						DPcounter++;

						// Update optimal in trace table

						if (trace){
							dpTrace.set(i - iDiff ,i,j,destStateId,0);
							dpTrace.set(j - jDiff ,i,j,destStateId,1);
							dpTrace.set(originIdx ,i,j,destStateId,2);
						}
					}

				}
			}
		}
	}
	
	public void align(char[] s1, char[] s2){
		DPcounter = 0;
		super.align(s1, s2);
	}
	
	public int getDPcounter(){
		return DPcounter;
	}
}
