package bgu.bio.algorithms.alignment.constrained.re;

import bgu.bio.ds.automata.TransitionTable;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.constrain.ConstrainedAlphabet;


public abstract class PartialConstrainedAlignmentEngine extends ConstrainedAlignmentEngine{
	
	protected final int finalStateId;
	protected final int initStateId;
	// Optimal score data
	protected int optimalI, optimalJ;
	
	public PartialConstrainedAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet,boolean trace) {
		
		super(score, tr, constAlphabet,trace);
		
		this.initStateId = tr.getNumOfStates();
		this.finalStateId = tr.getNumOfStates() + 1;
		this.additionalStatesCount = 2;
	}
	
	/** dynamic programming */
	public void align(char[] s1, char[] s2){
		this.s1 = s1;
		this.s2 = s2;
		
		s1Length = s1.length+1;
		s2Length = s2.length+1;
		
		this.rebuildTheTables(s1Length, s2Length);
		
		init();
		
		for (int i=0; i < s1Length; i++){
			for (int j=0; j < s2Length; j++){
				//initialize each cell data
				for (int k=0;k<tr.getNumOfStates()+additionalStatesCount;k++){ 
					dpTable.set(Float.NEGATIVE_INFINITY,i,j,k);
				}
				calculateSingleCell(i,j);	
			}
		}
	}
	
	protected void init() {
		dpTable.set(0.0f, 0, 0, startingStateId);
		dpTable.set(0.0f, 0, 0, this.initStateId); // special initial state
	}
}
