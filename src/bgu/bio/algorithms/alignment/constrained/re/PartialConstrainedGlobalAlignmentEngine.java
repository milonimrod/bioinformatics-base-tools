package bgu.bio.algorithms.alignment.constrained.re;

import bgu.bio.ds.automata.TransitionTable;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.constrain.ConstrainedAlphabet;


public class PartialConstrainedGlobalAlignmentEngine extends PartialConstrainedAlignmentEngine {

	public PartialConstrainedGlobalAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet, boolean trace) {
		super(score, tr, constAlphabet, trace);
	}

	public PartialConstrainedGlobalAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet) {
		this(score, tr, constAlphabet, false);
	}

	@Override
	public double getOptimalScore() {
		double temp = dpTable.get(s1Length - 1, s2Length - 1, this.finalStateId); // final state
		return temp;
	}

	/**
	 * Print the alignment starting from cell (bestI,bestJ) in the final state.
	 * 
	 * @param bestI
	 * @param bestJ
	 */
	public void printAlignment(int bestI, int bestJ) {
		String[] arr = this.getAlignment(bestI, bestJ);

		System.out.println(arr[0]);
		System.out.println(arr[1]);
	}


	@Override
	public void calculateSingleCell(int i, int j) {
		if (i == 0 && j == 0) {
			dpTable.set(0.0f, 0, 0, startingStateId);
			dpTable.set(0.0f, 0, 0, this.initStateId); // special initial state
		} else {

			///////////////// Replace
			if ((i != 0) && (j != 0)) {
				this.calculateByOtherCell(i, j, 1, 1, s1[i - 1], s2[j - 1]);
			}
			// Delete
			if ( i != 0 ) {
				this.calculateByOtherCell(i, j, 1, 0, s1[i - 1], this.score
						.getAlphabet().emptyLetter());
			}
			// Insert
			if ( j != 0 ) {
				this.calculateByOtherCell(i, j, 0, 1, this.score.getAlphabet()
						.emptyLetter(), s2[j - 1]);
			}

			// Start state
			float val = dpTable.get(i, j, this.initStateId);
			if (val >= dpTable.get(i, j, this.startingStateId)) {

				dpTable.set(val, i, j, this.startingStateId);

				if (trace) {
					dpTrace.set(i, i, j, this.startingStateId, 0);
					dpTrace.set(j, i, j, this.startingStateId, 1);
					dpTrace.set(this.initStateId, i, j, this.startingStateId, 2);
				}
			}

			// Final states
			for (int idx : tr.getAcceptingStates()) {
				val = dpTable.get(i, j, idx);
				if (val >= dpTable.get(i, j, this.finalStateId)) {

					dpTable.set(val, i, j, this.finalStateId);

					// Update optimal in trace table
					if (trace) {
						dpTrace.set(i, i, j, this.finalStateId, 0);
						dpTrace.set(j, i, j, this.finalStateId, 1);
						dpTrace.set(idx, i, j, this.finalStateId, 2);
					}

				}
			}
		}
	}

	private final void calculateByOtherCell(int i, int j, int iDiff, int jDiff,
			char s1Char, char s2Char) {

		float actionScore = score.score(s1Char, s2Char);
		// Get all possible constraint characters that fit the two read
		// characters
		int[] possibleConstraintHashedChars = this.constAlphabet
		.getCharMapping(s1Char, s2Char);
		for (int charIdx = 0; charIdx < possibleConstraintHashedChars.length; charIdx++) {

			int[][] charTransitions = this.tr
			.getTransitions(possibleConstraintHashedChars[charIdx]);

			for (int originIdx = 0; originIdx < charTransitions.length; originIdx++) {

				// hold the value of the origin
				final float val = dpTable.get(i - iDiff, j - jDiff, originIdx)
				+ actionScore;

				for (int destIdx = 0; destIdx < charTransitions[originIdx].length; destIdx++) {

					int destStateId = charTransitions[originIdx][destIdx];

					if (val > dpTable.get(i, j, destStateId)) {

						dpTable.set(val, i, j, destStateId);

						// Update optimal in trace table
						if (trace) {
							dpTrace.set(i - iDiff, i, j, destStateId, 0);
							dpTrace.set(j - jDiff, i, j, destStateId, 1);
							dpTrace.set(originIdx, i, j, destStateId, 2);
						}
					}

				}
			}
		}

		// initial state
		float val = dpTable.get(i - iDiff, j - jDiff, this.initStateId)
		+ actionScore;
		if (val > dpTable.get(i, j, this.initStateId)) {

			dpTable.set(val, i, j, this.initStateId);

			// Update optimal in trace table
			if (trace) {
				dpTrace.set(i - iDiff, i, j, this.initStateId, 0);
				dpTrace.set(j - jDiff, i, j, this.initStateId, 1);
				dpTrace.set(this.initStateId, i, j, this.initStateId, 2);
			}
		}

		// final state
		val = dpTable.get(i - iDiff, j - jDiff, this.finalStateId)
		+ actionScore;
		if (val > dpTable.get(i, j, this.finalStateId)) {

			dpTable.set(val, i, j, this.finalStateId);

			// Update optimal in trace table
			if (trace) {
				dpTrace.set(i - iDiff, i, j, this.finalStateId, 0);
				dpTrace.set(j - jDiff, i, j, this.finalStateId, 1);
				dpTrace.set(this.finalStateId, i, j, this.finalStateId, 2);
			}
		}
	}

	@Override
	public int countSubOptimalScores(double delta, double optimal) {
		return 0;
	}

	@Override
	public String optimalAlignmentToHTML() {
		return null;
	}

	@Override
	public String[] getAlignment() {
		return this.getAlignment(s1Length - 1, s2Length - 1);
	}

	/**
	 * Return the alignment starting from cell (bestI,bestJ) in the final state.
	 * 
	 * @param bestI
	 *            the best i
	 * @param bestJ
	 *            the best j
	 * @return an array with two cells, first aligned sequence and the second.
	 */
	public String[] getAlignment(int bestI, int bestJ) {
		String align1 = "";
		String align2 = "";
		String types = "";
		int currentT = this.finalStateId, currentI = bestI, currentJ = bestJ;
		int nextI, nextJ, nextT;
		while (currentI != 0 || currentJ != 0) {
			nextI = dpTrace.get(currentI, currentJ, currentT, 0);
			nextJ = dpTrace.get(currentI, currentJ, currentT, 1);
			nextT = dpTrace.get(currentI, currentJ, currentT, 2);
			if (nextI != currentI || nextJ != currentJ) {
				align1 = ((nextI == currentI) ? "-" : this.s1[nextI]) + align1;
				align2 = ((nextJ == currentJ) ? "-" : this.s2[nextJ]) + align2;
				char c = '*';
				if ((nextI != currentI) && (nextJ != currentJ) && this.score.match(this.s1[nextI] , this.s2[nextJ])) {
					c = '|';
				}
				types = c + types;
			}

			currentI = nextI;
			currentJ = nextJ;
			currentT = nextT;
		}

		return new String[] { align1, align2, types };
	}


}
