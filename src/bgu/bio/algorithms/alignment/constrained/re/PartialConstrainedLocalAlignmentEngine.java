package bgu.bio.algorithms.alignment.constrained.re;

import bgu.bio.ds.automata.TransitionTable;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.constrain.ConstrainedAlphabet;


public class PartialConstrainedLocalAlignmentEngine extends PartialConstrainedAlignmentEngine {

	public PartialConstrainedLocalAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet,boolean trace) {
		super(score, tr, constAlphabet,trace);
	}

	public PartialConstrainedLocalAlignmentEngine(ScoringMatrix score,
			TransitionTable tr, ConstrainedAlphabet constAlphabet) {
		this(score, tr, constAlphabet, false);
	}

	@Override
	public double getOptimalScore() {
		double optimalScore=Double.NEGATIVE_INFINITY;
		int bestI = 0,bestJ = 0;
		// Find final state with maximal score
		for (int i=0; i< dpTable.getDimensionSize(0); i++)
			for (int j=0; j< dpTable.getDimensionSize(1); j++){
				double temp = dpTable.get(i,j,this.finalStateId); // final state
				if ((!Double.isNaN(temp)) && (optimalScore < temp)) {
					optimalScore=temp;
					bestI = i;
					bestJ = j;
				}
			}
		System.out.println(bestI + " " + bestJ);
		System.out.println(optimalScore);
		//		printAlignment(bestI,bestJ);
		return optimalScore;
	}

	/**
	 * Print the alignment starting from cell (bestI,bestJ) in the final state.
	 * @param bestI
	 * @param bestJ
	 */
	public void printAlignment(int bestI, int bestJ) {
		String align1 = "";
		String align2 = "";
		int bestT = this.finalStateId;
		boolean shouldContinue = true;
		while(shouldContinue){
			int nextI = dpTrace.get(bestI,bestJ,bestT,0);
			int nextJ = dpTrace.get(bestI,bestJ,bestT,1);
			int nextT = dpTrace.get(bestI,bestJ,bestT,2);

			shouldContinue = !((nextI == bestI) && (nextJ == bestJ));			
			if (shouldContinue){
				align1 = ((nextI == bestI)? "-" : this.s1[nextI]) + align1;
				align2 = ((nextJ == bestJ)? "-" : this.s2[nextJ]) + align2;
				bestI = nextI ;
				bestJ = nextJ;
			}

			bestT = nextT;
		}

		System.out.println(align1);
		System.out.println(align2);
	}
	

	@Override
	public void calculateSingleCell(int i, int j) {
		if(i == 0 && j == 0) {
			dpTable.set(0.0f, 0, 0, startingStateId);
			dpTable.set(0.0f, 0, 0, this.initStateId); // special initial state
		} else {
			// Update optimal score per state in the [i][j] cell to be zero - because of the local alignment
			dpTable.set(0.0f, 0, 0, this.initStateId); // special initial state

			///////////////// Replace
			if ((i != 0) && (j != 0)) {
				this.calculateByOtherCell(i, j, 1, 1, s1[i - 1], s2[j - 1]);
			}
			// Delete
			if ( i != 0 ) {
				this.calculateByOtherCell(i, j, 1, 0, s1[i - 1], this.constAlphabet.emptyLetter());
			}
			// Insert
			if ( j != 0 )  {
				this.calculateByOtherCell(i, j, 0, 1, this.constAlphabet.emptyLetter(),s2[j - 1]);
			}

			// Start state 
			float val = dpTable.get(i,j,this.initStateId);
			if (val >= dpTable.get(i, j, this.startingStateId)){

				dpTable.set(val ,i,j,this.startingStateId);


				if (trace){
					dpTrace.set(i,i,j,this.startingStateId,0);
					dpTrace.set(j,i,j,this.startingStateId,1);
					dpTrace.set(this.initStateId ,i,j,this.startingStateId,2);
				}					
			}
			// Final states
			for (int idx : tr.getAcceptingStates()){
				val =  dpTable.get(i,j,idx);
				if (val >= dpTable.get(i, j, this.finalStateId)){

					dpTable.set(val ,i,j,this.finalStateId);


					// Update optimal in trace table
					if (trace){
						dpTrace.set(i,i,j,this.finalStateId,0);
						dpTrace.set(j,i,j,this.finalStateId,1);
						dpTrace.set(idx ,i,j,this.finalStateId,2);
					}

				}
			}
		}	
	}

	private final void calculateByOtherCell(int i,int j,int iDiff,int jDiff,char s1Char,char s2Char){

		float actionScore = score.score(s1Char, s2Char);
		// Get all possible constraint characters that fit the two read characters				
		int [] possibleConstraintHashedChars = this.constAlphabet.getCharMapping(s1Char, s2Char);
		for (int charIdx=0; charIdx<possibleConstraintHashedChars.length;charIdx++){

			int[][] charTransitions = this.tr.getTransitions(possibleConstraintHashedChars[charIdx]);

			for (int originIdx=0; originIdx< charTransitions.length; originIdx++){

				//hold the value of the origin
				final float val = dpTable.get(i - iDiff, j -jDiff, originIdx) + actionScore;

				for (int destIdx=0; destIdx< charTransitions[originIdx].length; destIdx++){	

					int destStateId = charTransitions[originIdx][destIdx];

					if (val > dpTable.get(i, j, destStateId)){

						dpTable.set( val
								,i, j, destStateId);

						// Update optimal in trace table
						if (trace){
							dpTrace.set(i - iDiff ,i,j,destStateId,0);
							dpTrace.set(j - jDiff ,i,j,destStateId,1);
							dpTrace.set(originIdx ,i,j,destStateId,2);
						}
					}

				}
			}
		}

		// initial state
		float val = dpTable.get(i-iDiff,j-jDiff,this.initStateId)+ actionScore;
		if (val > dpTable.get(i, j, this.initStateId)){

			dpTable.set(val ,i,j,this.initStateId);

			// Update optimal in trace table
			if (trace){
				dpTrace.set(i - iDiff ,i,j,this.initStateId,0);
				dpTrace.set(j - jDiff ,i,j,this.initStateId,1);
				dpTrace.set(this.initStateId ,i,j,this.initStateId,2);
			}					
		}

		// final state
		val = dpTable.get(i-iDiff,j-jDiff,this.finalStateId)+ actionScore;
		if (val > dpTable.get(i, j, this.finalStateId)){

			dpTable.set(val ,i,j,this.finalStateId);

			// Update optimal in trace table
			if (trace){
				dpTrace.set(i - iDiff ,i,j,this.finalStateId,0);
				dpTrace.set(j - jDiff ,i,j,this.finalStateId,1);
				dpTrace.set(this.finalStateId ,i,j,this.finalStateId,2);
			}			
		}
	}

	@Override
	public int countSubOptimalScores(double delta, double optimal) {
		return 0;
	}

	@Override
	public String optimalAlignmentToHTML() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getAlignment() {
		// TODO Auto-generated method stub
		return null;
	}


}
