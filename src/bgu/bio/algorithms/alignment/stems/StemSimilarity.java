package bgu.bio.algorithms.alignment.stems;

import bgu.bio.algorithms.alignment.SequenceAlignment;
import bgu.bio.ds.rna.StemStructure;

public interface StemSimilarity extends SequenceAlignment {
	double getDanglingScore();
	
	/**
	 * Set to true to consider the score of the dangling sequences in the given structure
	 * @param value
	 */
	void setDanglingScoreConsideration(boolean value);

	void setStem1(StemStructure stem);

	void setStem2(StemStructure stem);

	void run(); 
}
