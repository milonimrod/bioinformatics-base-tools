package bgu.bio.algorithms.clustering;

public interface DistanceManager<T> {
	public double distance(T o1,T o2);
	public DistanceManager<T> duplicate();
}
