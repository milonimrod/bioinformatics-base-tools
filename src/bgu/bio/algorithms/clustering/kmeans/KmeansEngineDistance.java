package bgu.bio.algorithms.clustering.kmeans;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

import bgu.bio.algorithms.clustering.DistanceManager;
import bgu.bio.io.file.FormatParser;

public class KmeansEngineDistance<T> {

	private int dataSize;
	private int k;
	private boolean change;
	private boolean working;

	private List<Data> dataList;
	private ArrayList<Cluster> clusteringDivision;
	private int[] assignment;
	private Executor executor;
	private CyclicBarrier barrier;
	private int[] coordsPerThread;
	private int numThreads;
	private ArrayList<KmeansWorker> workers;
	private Logger logger;
	private DistanceManager<T> distance;
	private int iterationLimit;
	private int currentIteration;

	public KmeansEngineDistance(DistanceManager<T> distance, int numThreads,
			int iterationLimit, Logger logger) {
		this.logger = logger;
		this.numThreads = numThreads;
		this.distance = distance;
		this.iterationLimit = iterationLimit;
		this.logger.info("Kmeans running with " + numThreads + " threads.");

		this.working = true;
		executor = Executors.newFixedThreadPool(numThreads);
		barrier = new CyclicBarrier(numThreads, new Runnable() {
			public void run() {
				workersDone();
			}
		});
		this.coordsPerThread = new int[numThreads];
		this.workers = new ArrayList<KmeansWorker>();
		for (int i = 0; i < numThreads; i++) {
			workers.add(new KmeansWorker(distance.duplicate(), "Worker" + i));
		}
	}

	public KmeansEngineDistance(DistanceManager<T> distance, int numThreads,
			Logger logger){
		this(distance,numThreads,10000,logger);
	}

	public final void setDataList(List<T> dataList, int randomSample) {
		this.dataList = new ArrayList<Data>();
		for (T d : dataList){
			this.dataList.add(new Data(d));
		}
		this.dataSize = randomSample;
		this.assignment = new int[this.dataList.size()];
	}

	public final void setK(int k) {
		this.k = k;
	}

	public void run() {
		// Initiate the data;
		clusteringDivision = new ArrayList<Cluster>();
		Arrays.fill(this.assignment, -1);

		for (int i = 0; i < k; i++) {
			clusteringDivision.add(new Cluster(this.dataList.get(i)));
			logger.info("INIT: Assigned " + this.dataList.get(i)
					+ " to be center of cluster " + i);
		}

		// run until there are no more changes
		change = true;
		this.currentIteration = 0;
		while (change && currentIteration <= this.iterationLimit) {
			logger.info("Iteration number: " + ++currentIteration);

			change = false;
			// Assign centers to data points
			logger.info("Iter " + currentIteration + ": assignning clusters");
			assignCenters();

			// Find centers in the clusters
			logger.info("Iter " + currentIteration + ": setting clusters");
			findCenters(false);
		}

		// after finishing the run on the sampling size. assign the rest of the
		// points
		logger.info("Assigning rest of the data to the clusters");
		//assignRestOfData();
		assignRestOfDataUsingInnerData();

		if (executor instanceof ThreadPoolExecutor) {
			// This terminates the threads in the thread pool.
			((ThreadPoolExecutor) executor).shutdownNow();
		}
		logger.info("Done");
	}
	
	private void assignRestOfDataUsingInnerData(){
		if (this.dataList.size() - this.dataSize == 0)
			return;
		
		findCenters(true);
		logger.info("after find cluster with inner data");
		// split to threads
		
		Data currentData;
		double temp,best = Double.POSITIVE_INFINITY;
		int bestCenter = 0;
		
		for (int coord = this.dataSize;coord < this.dataList.size();coord++){
			currentData = this.dataList.get(coord);
			//first locate the best center of them all.
			for (int j = 0; j < this.k; j++) {
				temp = distance.distance(currentData.data,
						clusteringDivision.get(j).center.data);
				if (temp < best){
					best = temp;
					bestCenter = j;
				}
			}
			//assigning the data point to the best center
			this.clusteringDivision.get(bestCenter).list.add(currentData);
			
			//run the workers to recalculate the center using the inner data
			Cluster cluster = clusteringDivision.get(bestCenter);
			int startCoord = 0;
			//minus 1 is because we don't include the last added
			buildClusterThreadsAmounts(cluster.list.size() - 1);
			this.working = true;
			barrier.reset();

			for (int w = 0; w < workers.size(); w++) {
				KmeansWorker worker = workers.get(w);
				worker.updateCenter(startCoord, this.coordsPerThread[w],
						cluster, currentData);
				startCoord += this.coordsPerThread[w];
				executor.execute(worker);
			}
			waitOnWorkers();
			
			//get the best new center out of all - and the new point
			double minValue = currentData.accumelator;
			Data minData = currentData;
			for (int c = 0; c < workers.size(); c++) {
				if (minValue > workers.get(c).bestValue) {
					minValue = workers.get(c).bestValue;
					minData = workers.get(c).bestData;
				}
			}
			
			//save the best center as new center
			cluster.center = minData;
		}
		
	}

	private void assignRestOfData() {
		if (this.dataList.size() - this.dataSize == 0)
			return;
		// split to threads
		int startCoord = this.dataSize;
		// the size is the size of the list
		buildClusterThreadsAmounts(this.dataList.size() - this.dataSize);
		this.working = true;
		barrier.reset();
		for (int w = 0; w < workers.size(); w++) {
			KmeansWorker worker = workers.get(w);
			worker.makeAssignments(startCoord, this.coordsPerThread[w]);
			startCoord += this.coordsPerThread[w];
			executor.execute(worker);
		}
		waitOnWorkers();
	}

	private void buildClusterThreadsAmounts(int size) {
		Arrays.fill(coordsPerThread, size / numThreads);
		int leftOvers = size - numThreads * coordsPerThread[0];
		for (int i = 0; i < leftOvers; i++) {
			coordsPerThread[i]++;
		}
	}

	private void findCenters(boolean useInnerData) {
		for (int i = 0; i < k; i++) {
			Cluster cluster = clusteringDivision.get(i);
			int startCoord = 0;
			buildClusterThreadsAmounts(cluster.list.size());
			this.working = true;
			barrier.reset();

			// Calculate the distance value of the current center
			int size = cluster.list.size();
			double minValue = 0;
			Data minData = cluster.center;
			for (int j = 0; j < size; j++) {
				minValue += Math.pow(distance.distance(minData.data,
						cluster.list.get(j).data), 2);
			}

			logger.info("Divide cluster " + i + " for workers (size "
					+ cluster.list.size() + ")");
			for (int w = 0; w < workers.size(); w++) {
				KmeansWorker worker = workers.get(w);
				worker.findCenters(startCoord, this.coordsPerThread[w],
						cluster, minValue, minData,useInnerData);
				startCoord += this.coordsPerThread[w];
				executor.execute(worker);
			}
			waitOnWorkers();

			// choose the minimal between all the workers
			minValue = Double.POSITIVE_INFINITY;
			for (int c = 0; c < numThreads; c++) {
				double temp = workers.get(c).minValue;
				if (minValue > temp) {
					minValue = temp;
					minData = workers.get(c).minData;
				}
			}

			// save the change if needed - that is, if the cluster value has changed
			if (minData != cluster.center) {
				change = true;
				logger.info("Center of cluster " + i + " changed from "
						+ cluster.center + " to " + minData);
				cluster.center = minData;
			}	cluster.value = minValue;
		}
		
	}

	private void assignCenters() {
		for (int i = 0; i < k; i++) {
			// clear all the lists of clusters
			clusteringDivision.get(i).list.clear();
		}

		// split to threads
		int startCoord = 0;
		buildClusterThreadsAmounts(this.dataSize);
		this.working = true;
		barrier.reset();
		for (int w = 0; w < workers.size(); w++) {
			KmeansWorker worker = workers.get(w);
			worker.makeAssignments(startCoord, this.coordsPerThread[w]);
			startCoord += this.coordsPerThread[w];
			executor.execute(worker);
		}
		waitOnWorkers();

	}

	private synchronized void workersDone() {
		working = false;
		notifyAll();
	}

	private synchronized void waitOnWorkers() {
		while (working) {
			try {
				// Blocks until workersDone() is called.
				wait();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
				break;
			}
		}
	}

	class Cluster {
		ArrayList<Data> list;
		Data center;
		double value;

		public Cluster(Data center) {
			this.center = center;
			this.list = new ArrayList<Data>();
			this.value = Double.POSITIVE_INFINITY;
		}
	}
	
	private class Data{
		T data;
		volatile double accumelator;
		
		public Data(T d) {
			this.data = d;
		}
	}

	private class KmeansWorker implements Runnable {
		private String name;
		private boolean MAKE_ASSIGNMENTS;
		private boolean CHOOSE_CENTER;
		private boolean UPDATE_CENTER;

		private int start;
		private int amount;

		private Cluster cluster;
		private Data minData;
		private double minValue;
		private DistanceManager<T> distManagerPerWorker;
		
		private double bestValue;
		private Data lastAdded;
		private Data bestData;
		
		/** Indicate if the choose center shouldn't trim the search and keep the value in the inner data. */
		private boolean useInnerData;

		public KmeansWorker(DistanceManager<T> dist, String name) {
			this.distManagerPerWorker = dist;
			this.name = name;
		}
		
		public void makeAssignments(int startInd, int amountToCheck) {
			this.MAKE_ASSIGNMENTS = true;
			this.CHOOSE_CENTER = false;
			this.UPDATE_CENTER = false;

			this.start = startInd;
			this.amount = amountToCheck;
		}
		
		public void updateCenter(int startInd, int amountToCheck,
				Cluster clusterToCheck,Data lastAdded){
			this.MAKE_ASSIGNMENTS = false;
			this.CHOOSE_CENTER = false;
			this.UPDATE_CENTER = true;
			
			this.start = startInd;
			this.amount = amountToCheck;
			this.cluster = clusterToCheck;
			this.lastAdded = lastAdded;
		}

		public void findCenters(int startInd, int amountToCheck,
				Cluster clusterToCheck, double minValue,Data minData,boolean useInnerData) {
			this.MAKE_ASSIGNMENTS = false;
			this.CHOOSE_CENTER = true;
			this.UPDATE_CENTER = false;

			this.start = startInd;
			this.amount = amountToCheck;
			this.cluster = clusterToCheck;
			this.minValue = minValue;
			this.minData = minData;
			this.useInnerData = useInnerData;
		}

		public void run() {
			if (this.MAKE_ASSIGNMENTS) {
				this.runAssignments();
			} else if (this.CHOOSE_CENTER) {
				this.runChooseCenters();
			} else if (this.UPDATE_CENTER){
				this.runUpdateCenter();
			}

			try {
				barrier.await();
			} catch (InterruptedException ex) {
			} catch (BrokenBarrierException ex) {
			}
		}

		private void runUpdateCenter() {
			Data currentData;
			double temp = 0;
			bestValue = Double.POSITIVE_INFINITY;
			minData = null;
			for (int i = 0; i < amount; i++) {
				currentData = cluster.list.get(start + i);
				temp = Math.pow(distManagerPerWorker.distance(currentData.data,lastAdded.data), 2);
				currentData.accumelator += temp;
				lastAdded.accumelator += temp;
				
				//save the best data so far
				if (bestValue > currentData.accumelator){
					bestValue = currentData.accumelator;
					bestData = currentData;
				}
			}
		}

		private void runChooseCenters() {
			double temp = 0.0;
			double dist = 0.0;
			int size = cluster.list.size();
			double diff;
			Data currentData;
			int percent = amount / 4;
			for (int i = 0; i < amount; i++) {
				temp = 0.0;
				currentData = cluster.list.get(start + i);
				if (i == percent) {
					logger.fine(name + " choose center " + (100.0 * percent)
							/ amount + "%)");
					percent += amount / 4;
				}

				for (int j = 0; j < size; j++) {
					dist = Math.pow(distManagerPerWorker.distance(currentData.data,cluster.list.get(j).data), 2);
					temp += dist;
					
					//in the case where we want to update the inner data
					if (useInnerData){
						currentData.accumelator += dist;
						cluster.list.get(j).accumelator += dist;
					}
					else if (temp > minValue) {
						// in the case where temp is already larger then the current
						// minDistance then we can stop checking
						break;
					}
				}
				diff = minValue - temp;
				if (0 <= diff) {
					if (diff == 0) {
						if (Math.random() > 0.5) {
							this.minData = currentData;
							this.minValue = temp;
						}
					} else {
						this.minData = currentData;
						this.minValue = temp;
					}
				}
			}
			System.out.println("Finished choose centers");
		}

		private void runAssignments() {
			double temp = 0.0;
			int size = k;
			double diff;
			int bestCenter = -100;
			Data currentData;
			int percent = amount / 4;
			for (int i = 0; i < amount; i++) {
				temp = 0.0;

				if (i == percent) {
					logger.fine(name + " assigned " + (100.0 * percent)
							/ (amount) + "%");
					percent += amount / 4;
				}

				currentData = dataList.get(start + i);
				this.minValue = Double.POSITIVE_INFINITY;

				for (int j = 0; j < size; j++) {
					temp = distManagerPerWorker.distance(currentData.data,
							clusteringDivision.get(j).center.data);
					// in the case where temp is already larger then the current
					// minDistance then we can stop checking
					diff = minValue - temp;
					if (0 <= diff) {
						if (diff == 0) {
							if (Math.random() > 0.5) {
								this.minValue = temp;
								bestCenter = j;
							}
						} else {
							this.minValue = temp;
							bestCenter = j;
						}
					}
				}

				// change the assignment if needed
				if (assignment[i + start] != bestCenter) {
					change = true;
					// logger.info("changed assignment of " + (i+start) +
					// " from " + assignment[i+start] + " to " + bestCenter);
					assignment[i + start] = bestCenter;
				}

				// copy the current data to the cluster list
				Cluster chosenCluster = clusteringDivision.get(bestCenter);
				synchronized (chosenCluster) {
					chosenCluster.list.add(currentData);
				}
			}
		}
	}

	public void toJSON(Writer out,FormatParser<T> objParser) throws IOException {
		StringBuilder json = new StringBuilder();
		json.append("{\"quality\":");
		json.append(0);
		json.append(",\"iterations\":");
		json.append(this.currentIteration);
		json.append(",\"clusters\": [\n");
		// put the clusters in the JSON format
		for (Cluster cluster : this.clusteringDivision) {
			json.append('[');
			objParser.parse(cluster.center.data,json);
			for (Data data : cluster.list) {
				if (data != cluster.center){
					json.append(',');
					objParser.parse(data.data, json);
				}
			}
			out.write(json.toString());
			json.setLength(0);
			json.append("],\n");
		}
		json.deleteCharAt(json.length() - 2);
		json.append("]}");
		out.write(json.toString());
	}
}