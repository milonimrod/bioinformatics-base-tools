package bgu.bio.algorithms.clustering.kmeans.test;
/**
 * This class describes a Point as might be
 * used in a graphics application. The Point
 * consists of a x,y coordinate, and
 * several operations.
 */
public class Point {

	private int id;
	private String name;
	private double x;
	private double y;

	/**
	 * Construct a new default Point.  The default Point
	 * has x=0, y=0.
	 */
	public Point() {
		x = 0;
		y = 0;
	}

	/**
	 * Construct a new Point with the specified coordinate.
	 *
	 * @param x the x coordinate of the new Point.
	 * @param y the y coordinate of the new Point.
	 */
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Copy constructor. Copies a given point.
	 * 
	 * @param p the point to copy.
	 */
	public Point(Point p) {
		this(p.x, p.y);
	}

	/**
	 * Returns a string representing the coordinates
	 * of a point.
	 * 
	 * @return a string that represents the point coordinates.
	 */
	@Override
	public String toString() {
		return "<" + x + "," + y+">";
	}

	/**
	 * Compares this point to the given object.
	 * The result is true if and only if the argument is not null 
	 * and is a Point object that represents the same coordinate 
	 * as this object.
	 * 
	 * @param obj the object to compare this point against.
	 * @return true if the points are equal; false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		boolean ans = false;
		if (obj instanceof Point) {
			Point p = (Point) obj;	
			ans = (p.getX() == x & p.getY() == y);
		}
		return ans;
	}

	/**
	 * Get the x coordinate of this Point.
	 *
	 * @return the x coordinate of this Point.
	 */
	public double getX() {
		return x;
	}

	/**
	 * Get the y coordinate of this Point.
	 *
	 * @return the y coordinate of this Point.
	 */
	public double getY() {
		return y;
	}

	/**
	 * Get the area of this Point. Note that the
	 * area of a point is always 0.  While this may seem
	 * a bit silly, this method is included mostly so that
	 * its behavior can be overridden in a sub-class.
	 *
	 * @return the area of this Point (i.e. 0).
	 */
	public double getArea() {
		return 0;
	}

	/**
	 * Translate this point by dx in the x direction
	 * and by dy in the y direction.
	 *
	 * @param dx the amount to translate in the x direction.
	 * @param dy the amount to translate in the y direction.
	 */
	public void translate(double dx, double dy) {
		x = x + dx;
		y = y + dy;
	}

	/**
	 * Translate this point by a given point coordinates.
	 * If the given point is null, this point is not changed.
	 *
	 * @param p a point to translate in the x and the y direction.
	 */
	public void translate(Point p) {
		if (p != null)
			translate(p.getX(), p.getY());
	}

	/**
	 * Returns the distance of this point from a point.
	 * If the given point is null, -1.0 is returned.
	 *
	 * @param p a point to compute its distance from this point.
	 * @return the distance of this Point from a point.
	 */ 
	public double distance(Point p) {
		double ans = -1.0;
		if (p != null) 
			ans = Math.sqrt(Math.pow((x-p.getX()),2) + Math.pow((y-p.getY()),2));
		return ans;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public final void setX(double x) {
		this.x = x;
	}

	public final void setY(double y) {
		this.y = y;
	}

}