package bgu.bio.algorithms.clustering.kmeans.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

import bgu.bio.algorithms.clustering.kmeans.KmeansEngineDistance;
import bgu.bio.io.file.FormatParser;

public class PointCluster {
	public static void main(String[] args) throws IOException {
		final int clusters = 9;
		final int amountPerCluster = 100;
		final int square = (int)Math.sqrt(clusters);
		final int yJump = 300;
		final int xJump = 300;
		
		Point[][] points = new Point[clusters][amountPerCluster];
		ArrayList<Point> pointsList = new ArrayList<Point>();
		Random rand = new Random();
		int current = 0;
		int yCenter = 0;
		for (int i = 0;i < square;i++){
			int xCenter = 0;
			for (int j=0;j < square;j++){
				xCenter = 2 * j * xJump;
				yCenter = 2 * i * yJump;
				Point point = new Point(xCenter,yCenter);
				points[current][0] = point;
				pointsList.add(point);
				//build new points
				for (int p = 1;p < amountPerCluster;p++){
					point = new Point();
					double val = rand.nextDouble()*xJump/3;
					val = xCenter + (rand.nextBoolean() ? -val : val);
					point.setX(val);
					
					val = rand.nextDouble()*yJump/3;
					val = yCenter + (rand.nextBoolean() ? -val : val);
					point.setY(val);
					pointsList.add(point);
					points[current][p] = point;
				}
				current++;
			}
			
		}
		
		System.out.println("Starting with " + pointsList.size() + " points");
		Logger logger = Logger.getLogger("points");
		KmeansEngineDistance<Point> engine = new KmeansEngineDistance<Point>(new PointDistance(), 1, logger);
		engine.setK(clusters);
		engine.setDataList(pointsList, pointsList.size()/2);
		engine.run();
		
		//Printing results
		FileWriter fstream = new FileWriter(System.getProperty("user.home") + System.getProperty("file.separator") + "output.json");
		BufferedWriter out = new BufferedWriter(fstream);
		engine.toJSON(out,new FormatParser<Point>() {
			DecimalFormat format = new DecimalFormat("0.000");
			
			public void parse(Point data, StringBuilder builder) {
				builder.append("<" + format.format(data.getX()) + "," + format.format(data.getY()) + ">");
			}

			public String parse(Point data) {
				return null;
			}
		});
		
		out.close();
		
		fstream = new FileWriter(System.getProperty("user.home") + System.getProperty("file.separator") + "points.txt");
		out = new BufferedWriter(fstream);
		DecimalFormat format = new DecimalFormat("0.000");
		for (Point p : pointsList){
			out.write(format.format(p.getX()) + " " + format.format(p.getY()) + "\n");
		}
		out.close();
		
		System.out.println("Finished");
	}
}
