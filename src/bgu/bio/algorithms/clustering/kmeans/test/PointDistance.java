package bgu.bio.algorithms.clustering.kmeans.test;
import bgu.bio.algorithms.clustering.DistanceManager;


public class PointDistance implements DistanceManager<Point>{

	public double distance(Point o1, Point o2) {
		return o1.distance(o2);
	}

	public DistanceManager<Point> duplicate() {
		return new PointDistance();
	}

	
}
