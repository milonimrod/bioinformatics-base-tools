package bgu.bio.algorithms.graphs;

import bgu.bio.adt.rna.NodeLabel;

public interface CostFunction {
	double cost(NodeLabel l1,NodeLabel l2);
}
