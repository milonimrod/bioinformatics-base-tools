package bgu.bio.algorithms.graphs;

import gnu.trove.list.array.TIntArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import bgu.bio.adt.graphs.FlexibleUndirectedGraph;
import bgu.bio.adt.queue.Pool;
import bgu.bio.adt.tuples.IntDoublePair;

/**
 * A class for coloring the nodes of an undirected graph (
 * {@link FlexibleUndirectedGraph}) using the greedy approach. <br/>
 * <br/>
 * <b>This class is not Thread safe</b>
 * 
 * @author milon
 * 
 */
public class GraphColoring {
	/**
	 * represent an uncolored value
	 */
	private static int UNCOLORED = -1;

	/**
	 * The graph we need to color
	 */
	private FlexibleUndirectedGraph graph;
	/**
	 * This array holds the coloring values for the nodes in the graph
	 */
	private int[] coloring;
	/**
	 * Helper array to hold information on the next available color
	 */
	private boolean[] currentColoring;
	/**
	 * Hold the number of color used to color the graph
	 */
	private int numberOfColors;
	/**
	 * Help list. Holds the order for which we want to color the graph
	 */
	private TIntArrayList nodes;
	/**
	 * Help list for cases where we want to order the nodes of the graph
	 */
	private ArrayList<IntDoublePair> helpList;
	/**
	 * A pool of links in the {@link #helpList} list. Used to save memory
	 * allocations
	 */
	private Pool<IntDoublePair> pool;

	/**
	 * A {@link Comparator} to sort the nodes in {@link #helpList}
	 */
	private Comparator<IntDoublePair> comp;

	public GraphColoring() {
		coloring = new int[0];
		currentColoring = new boolean[0];
		nodes = new TIntArrayList();
		helpList = new ArrayList<IntDoublePair>();
		pool = new Pool<IntDoublePair>();
		comp = new Comparator<IntDoublePair>() {
			@Override
			public int compare(IntDoublePair arg0, IntDoublePair arg1) {
				final double diff = arg0.getSecond() - arg1.getSecond();
				if (diff > 0) {
					return -1;
				} else if (diff < 0) {
					return 1;
				}
				return 0;
			}

		};
	}

	/**
	 * Set the graph that we want to color
	 * 
	 * @param graph
	 *            the graph that we want to color
	 */
	public void setGraph(FlexibleUndirectedGraph graph) {
		this.graph = graph;
	}

	/**
	 * Return the coloring of the nodes in the given graph
	 * 
	 * @return the coloring of the nodes in the given graph
	 */
	public int[] getColoring() {
		return this.coloring;
	}

	/**
	 * Return the number of colors used to color the given graph
	 * 
	 * @return the number of colors used to color the given graph
	 */
	public int numberOfColorsUsed() {
		return numberOfColors;
	}

	/**
	 * Color the graph by the Degeneracy of the graph
	 * 
	 * @return same value as {@link #numberOfColorsUsed()}
	 */
	public int greedyColoringByDegenracy() {
		nodes.resetQuick();
		graph.degeneracy(nodes);
		return this.greedyColoring(nodes);
	}

	/**
	 * Color the graph the weights of the nodes in the graphs
	 * 
	 * @param weight
	 *            the weights of the nodes in the graphs
	 * @return same value as {@link #numberOfColorsUsed()}
	 */
	public int greedyColoringByWeight(double[] weight) {
		for (int i = 0; i < graph.getNodeNum(); i++) {
			helpList.add(getFromPool(i, weight[i]));
		}
		Collections.sort(helpList, comp);
		nodes.resetQuick();
		for (int i = 0; i < graph.getNodeNum(); i++) {
			nodes.add(helpList.get(i).getFirst());
		}
		clearToPool();
		return this.greedyColoring(nodes);
	}

	/**
	 * Clear the current {@link #helpList} to the {@link #pool}
	 */
	private void clearToPool() {
		// only clear to the pool if the pool will not increase to more than
		// 1000
		if (pool.size() + helpList.size() < 1000) {
			for (int i = 0; i < helpList.size(); i++) {
				pool.enqueue(helpList.get(i));
			}
		}
		helpList.clear();
	}

	/**
	 * Color the graph the degree of the nodes in the graphs
	 * 
	 * @return same value as {@link #numberOfColorsUsed()}
	 */
	public int greedyColoringByDegree() {
		for (int i = 0; i < graph.getNodeNum(); i++) {
			helpList.add(getFromPool(i, graph.deg(i)));
		}
		try {
			Collections.sort(helpList, comp);
		} catch (Throwable e) {
			System.err.println(e);
			System.out.println(helpList);
		}
		nodes.resetQuick();
		for (int i = 0; i < graph.getNodeNum(); i++) {
			nodes.add(helpList.get(i).getFirst());
		}
		clearToPool();
		return this.greedyColoring(nodes);
	}

	/**
	 * Return a {@link IntDoublePair} representing a node and its value from the
	 * {@link #pool}
	 * 
	 * @param nodeId
	 *            the id of the node to be set
	 * @param nodeValue
	 *            the value of the node to be set
	 * @return a {@link IntDoublePair} representing a node and its value
	 */
	private IntDoublePair getFromPool(int nodeId, double nodeValue) {
		IntDoublePair ans = pool.dequeue();
		if (ans == null) {
			ans = new IntDoublePair();
		}
		ans.setFirst(nodeId);
		ans.setSecond(nodeValue);
		return ans;
	}

	/**
	 * Color the graph according to the given order of the nodes
	 * 
	 * @param order
	 *            the order of the nodes
	 * @return same value as {@link #numberOfColorsUsed()}
	 */
	public int greedyColoring(TIntArrayList order) {

		// init the arrays
		if (coloring.length < graph.getNodeNum()) {
			coloring = new int[graph.getNodeNum()];
			currentColoring = new boolean[graph.getNodeNum() + 1];
		}
		Arrays.fill(coloring, 0, graph.getNodeNum(), UNCOLORED);
		// maximal number of colors
		Arrays.fill(currentColoring, 0, graph.getNodeNum(), false);

		this.numberOfColors = -1;
		int max = 0;
		for (int i = 0; i < order.size(); i++) {
			int current = order.get(i);
			// reset the coloring indicator
			Arrays.fill(currentColoring, 0, max + 1, false);
			max = -1;

			int deg = graph.deg(current);
			for (int j = 0; j < deg; j++) {
				int v = graph.getNeighbor(current, j);
				final int neighborColor = coloring[v];
				if (neighborColor != UNCOLORED) {
					currentColoring[neighborColor] = true;
					// check if maximum
					if (neighborColor > max) {
						max = neighborColor;
					}
				}
			}
			// find the minimum available color and set the current node color
			boolean found = false;
			int c = 0;
			for (; c < currentColoring.length && !found; c++) {
				found = !currentColoring[c];
			}
			coloring[current] = c - 1;
			if (numberOfColors < c - 1) {
				numberOfColors = c - 1;
			}
		}

		numberOfColors++;
		return numberOfColors;
	}

	/**
	 * Copy the current coloring to the given array
	 * 
	 * @param color
	 *            the array that will contain the coloring
	 */
	public void copyColoringTo(int[] color) {
		for (int i = 0; i < graph.getNodeNum(); i++) {
			color[i] = coloring[i];
		}
	}
}
