package bgu.bio.algorithms.graphs;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import bgu.bio.adt.graphs.WeightedGraph;
import bgu.bio.util.MathOperations;


public class ShortestPaths {

	Set<Integer> open;

	public ShortestPaths(){
		open = new HashSet<Integer>();
	}

	//	private final Comparator<double[]> nodeComparator = new Comparator<double[]>() {
	//
	//		@Override
	//		public int compare(double[] arg0, double[] arg1) {
	//			if (arg0[1] < arg1[1]) return -1;
	//			else if (arg0[1] > arg1[1]) return 1;
	//			else return 0;
	//		}
	//	};

	/**
	 * An implementation of the Floyd-Wrashall algorithm for All Pairs Shortest
	 * Paths.
	 * 
	 * @param graph a weighted graph.
	 * @param d an n by n matrix, where n is the number of nodes in the graph.
	 */
	public static void floydWarshall(WeightedGraph graph, double[][] d){
		for (double[] di:d){
			Arrays.fill(di, Double.POSITIVE_INFINITY);
		}

		for (int i=graph.getNodeNum()-1; i>=0; --i){
			for (int neighborIx = graph.outAdjLists[i].length-1; neighborIx>=0; --neighborIx){
				d[i][graph.outAdjLists[i][neighborIx]] = graph.weights[i][neighborIx];
			}
		}

		for (int k=graph.getNodeNum()-1; k>=0; --k){
			for (int i=graph.getNodeNum()-1; i>=0; --i){
				for (int neighborIx = graph.outAdjLists[i].length-1; neighborIx>=0; --neighborIx){
					int j = graph.outAdjLists[i][neighborIx];
					if (MathOperations.greater(d[i][j] , d[i][k]+d[k][j])) d[i][j] = d[i][k]+d[k][j];
				}
			}
		}

	}

	/**
	 * An implementation of the Floyd-Wrashall algorithm for All Pairs Shortest
	 * Paths.
	 * 
	 * @param graph a weighted graph.
	 * @param d an n by n matrix, where n is the number of nodes in the graph.
	 */
	public static void floydWarshall(double[][] d){
		for (int k=d.length-1; k>=0; --k){
			for (int i=d.length-1; i>=0; --i){
				for (int j=d.length-1; j>=0; --j){
					if (MathOperations.greater(d[i][j],d[i][k]+d[k][j])) d[i][j] = d[i][k]+d[k][j];
				}
			}
		}

	}


	public static double[] initDelta(WeightedGraph graph){
		return new double[graph.getNodeNum()];
	}

	public static int[] initPi(WeightedGraph graph){
		return new int[graph.getNodeNum()];
	}

	/**
	 * An implementation of the Dijkstra algorithm.
	 * 
	 * @param graph
	 * @param source
	 * @param delta
	 * @param pi
	 */
	public void dijxtra(WeightedGraph graph, 
			int source, double[] delta, int[] pi){


		Arrays.fill(delta, Double.POSITIVE_INFINITY);
		Arrays.fill(pi, -1);
		open.clear();

		//		PriorityQueue<double[]> queue = new PriorityQueue<double[]>(graph.nodeNum, nodeComparator);
		delta[source] = 0;
		open.add(source);
		//		queue.add(source);

		int secondNode;
		while (!open.isEmpty()){
			int currNode = -1;
			double currDelta = Double.POSITIVE_INFINITY;

			//TODO: inefficient. Replace with a priority queue that allows efficient decrease key.
			for (Integer node:open){
				if (delta[node] < currDelta){
					currNode = node;
					currDelta = delta[currNode];
				}
			}

			if (currNode == -1) break;
			open.remove(currNode);

			for (int neighborIx=0; neighborIx<graph.outAdjLists[currNode].length; ++neighborIx){
				secondNode = graph.outAdjLists[currNode][neighborIx];
				if (MathOperations.greater(delta[secondNode],currDelta + graph.weights[currNode][neighborIx])){
					delta[secondNode] = currDelta + graph.weights[currNode][neighborIx];
					pi[secondNode] = currNode;
					open.add(secondNode);
				}
			}
		}
	}

	/**
	 * An implementation of the Belman-Ford algorithm.
	 * 
	 * @param graph
	 * @param source
	 * @param delta
	 * @param pi
	 */
	public void belmanFord(WeightedGraph graph, 
			int source, double[] delta, int[] pi){


		Arrays.fill(delta, Double.POSITIVE_INFINITY);
		Arrays.fill(pi, -1);
		delta[source] = 0;

		int secondNode;
		boolean improved = true;

		while (improved){
			improved = false;

			for (int firstNode = 0; firstNode<graph.getNodeNum(); ++firstNode){
				int[] neighbors = graph.outAdjLists[firstNode];
				double[] weights = graph.weights[firstNode];
				double firstNodePath = delta[firstNode];

				for (int neighborIx = 0; neighborIx<neighbors.length; ++neighborIx){
					secondNode = neighbors[neighborIx];
					if (MathOperations.greater(delta[secondNode],firstNodePath + weights[neighborIx])){
						delta[secondNode] = firstNodePath + weights[neighborIx];
						pi[secondNode] = firstNode;
						improved = true;
					}
				}
			}
		}
	}



	public double[] computeShortestPaths(WeightedGraph graph, int source) {
		double[] delta = initDelta(graph);
		dijxtra(graph, source, delta, initPi(graph));
		return delta;
	}
}
