package bgu.bio.algorithms.graphs;

import java.util.ArrayList;

import bgu.bio.adt.graphs.FlexibleUndirectedGraph;
import bgu.bio.adt.tuples.IntDoublePair;

/**
 * A class implementing the weighted clique class. this is an abstract class
 * because one can implement the {@link #potential()} and
 * {@link #canJoin(IntDoublePair)} methods in several ways.
 * 
 * Known implementations: {@link WeightedCliqueSimple},
 * {@link WeightedCliqueTree}.
 * 
 * @author milon
 * 
 */
public abstract class WeightedClique {

	protected FlexibleUndirectedGraph graph;
	protected double weight;
	protected int size;
	protected ArrayList<IntDoublePair> clique;
	protected int[] potential;

	public WeightedClique() {
		clique = new ArrayList<IntDoublePair>();
		potential = new int[10];
	}

	public void setGraph(FlexibleUndirectedGraph graph) {
		this.graph = graph;
		weight = 0;
		size = 0;
		clique.clear();
		if (potential.length < graph.getNodeNum() + 10) {
			potential = new int[graph.getNodeNum() + 10];
		}
	}

	public abstract boolean canJoin(IntDoublePair node);

	public void add(IntDoublePair node) {
		// add graph to clique
		clique.add(node);
		size++;
		weight += node.getSecond();
		calcPotential(node);
	}

	protected abstract void calcPotential(IntDoublePair node);

	public IntDoublePair removeLast() {
		IntDoublePair node = this.clique.remove(size - 1);
		size--;
		weight -= node.getSecond();

		return node;
	}

	public int potential() {
		if (size == 0) {
			return graph.getNodeNum();
		}
		return potential[size];
	}

	public double weight() {
		return weight;
	}

	public ArrayList<IntDoublePair> clique() {
		return this.clique;
	}

	public int size() {
		return size;
	}

	public IntDoublePair get(int i) {
		return clique.get(i);
	}

	public void copyTo(ArrayList<IntDoublePair> other) {
		for (int i = 0; i < size; i++) {
			other.add(clique.get(i));
		}
	}

	@Override
	public String toString() {
		return clique.toString();
	}
}
