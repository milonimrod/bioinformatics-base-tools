package bgu.bio.algorithms.graphs;

import bgu.bio.adt.graphs.FlexibleUndirectedGraph;
import bgu.bio.adt.tuples.IntDoublePair;

public class WeightedCliqueSimple extends WeightedClique {

	private boolean[][] adjTable;

	@Override
	public void setGraph(FlexibleUndirectedGraph graph) {
		super.setGraph(graph);
		adjTable = new boolean[graph.getNodeNum()][graph.getNodeNum()];
		for (int u = 0; u < graph.getNodeNum(); u++) {
			int deg = graph.deg(u);
			for (int d = 0; d < deg; d++) {
				int v = graph.getNeighbor(u, d);
				adjTable[u][v] = true;
				adjTable[v][u] = true;
			}
		}
	}

	@Override
	public boolean canJoin(IntDoublePair node) {
		boolean[] current = adjTable[node.getFirst()];
		for (int i = 0; i < size; i++) {
			if (!current[clique.get(i).getFirst()]) {
				return false;
			}
		}
		return true;
	}

	@Override
	protected void calcPotential(IntDoublePair node) {
		if (size == 1) {
			potential[size] = graph.deg(node.getFirst());
		} else {
			potential[size] = Math.min(graph.deg(node.getFirst()),
					potential[size - 1]);
		}
	}
}
