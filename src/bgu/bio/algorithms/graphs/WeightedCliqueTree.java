package bgu.bio.algorithms.graphs;

import java.util.Arrays;

import bgu.bio.adt.graphs.FlexibleUndirectedGraph;
import bgu.bio.adt.tuples.IntDoublePair;

/**
 * A class extend {@link WeightedClique}, with the improvement of
 * {@link #potential()} and {@link #canJoin(IntDoublePair)} to run in constant
 * time. The {@link #add(IntDoublePair)} and {@link #removeLast()} methods run
 * in time proportional to the degree of the inserted / removed node.
 * 
 * @author milon
 * 
 */
public class WeightedCliqueTree extends WeightedClique {

	private int[] lst;

	public WeightedCliqueTree() {
		lst = new int[100];
	}

	@Override
	public void setGraph(FlexibleUndirectedGraph graph) {
		super.setGraph(graph);
		if (lst.length < graph.getNodeNum()) {
			lst = new int[graph.getNodeNum()];
		} else {
			Arrays.fill(lst, 0, graph.getNodeNum(), 0);
		}
	}

	@Override
	public boolean canJoin(IntDoublePair node) {
		if (size == 0) {
			return true;
		}
		final int nodeId = node.getFirst();
		// if the node in the map and its value is equal to the value in the map
		return lst[nodeId] == clique.size();
	}

	@Override
	protected void calcPotential(IntDoublePair node) {
		int nodePotential = 0;
		// add all the neighbors to the tree
		final int nodeId = node.getFirst();
		final int deg = graph.deg(nodeId);
		for (int i = 0; i < deg; i++) {
			if (++lst[graph.getNeighbor(nodeId, i)] == size) {
				nodePotential++;

			}
		}

		// store potential
		potential[clique.size()] = nodePotential;
	}

	@Override
	public IntDoublePair removeLast() {
		IntDoublePair node = super.removeLast();
		final int u = node.getFirst();

		// update all the neighbors of the removed node
		final int deg = graph.deg(u);
		for (int i = 0; i < deg; i++) {
			lst[graph.getNeighbor(u, i)]--;
		}
		return node;
	}

}
