package bgu.bio.algorithms.graphs.costs;

import java.util.Arrays;

import bgu.bio.adt.graphs.Tree;

/**
 * The Class FreeTreePruning is used for Local alignment. the cost of pruning any sub-tree is free.
 */
public class FreeTreePruning implements TreePruningCostCalculator {

	@Override
	public void calculateCost(Tree tree) {
		int[][] edges = tree.getEdges();
		double[][] weights = tree.getWeights();
		if (weights == null || weights.length != edges.length) {
			weights = new double[edges.length][];
		}
		
		for (int i=0;i<weights.length;i++){
			if (weights[i] == null || weights[i].length != edges[i].length) {
				weights[i] = new double[edges[i].length];
			}else{
				Arrays.fill(weights[i], 0);
			}
		}
	}

}
