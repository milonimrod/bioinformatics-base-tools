package bgu.bio.algorithms.graphs.costs;

import bgu.bio.adt.graphs.Tree;

public interface NodeSmoothingCostCalculator {
	public void calculateCost (Tree tree);
}
