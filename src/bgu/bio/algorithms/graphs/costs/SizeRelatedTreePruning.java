package bgu.bio.algorithms.graphs.costs;

import java.util.Arrays;

import bgu.bio.adt.graphs.Tree;

/**
 * The Class SizeRelatedTreePruning calculates the cost of pruning a sub-tree
 * according to the size of the sub-tree (i.e. the number of nodes in the
 * sub-tree).
 */
public class SizeRelatedTreePruning implements TreePruningCostCalculator {

	private double cost;

	/**
	 * Instantiates a new size related tree pruning.
	 * 
	 * @param cost
	 *            the cost of deleting a node
	 */
	public SizeRelatedTreePruning(double cost) {
		this.cost = cost;
	}

	@Override
	public void calculateCost(Tree tree) {
		int[][] edges = tree.getEdges();
		double[][] weights = tree.getWeights();
		if (weights == null || weights.length != edges.length) {
			weights = new double[edges.length][];
		}
		for (int i = 0; i < weights.length; i++) {
			if (weights[i] == null || weights[i].length != edges[i].length) {
				weights[i] = new double[edges[i].length];
			}else{
				Arrays.fill(weights[i], 0);
			}
		}
		final int num = tree.getEdgeNum();
		for (int i = 0; i < num; i++) {
			int[] edge = tree.edgeAtIndex(i);
			final int from = edge[0];
			final int to = tree.getNeighbor(from, edge[1]);

			// check base condition
			if (tree.outDeg(to) == 1) {
				weights[from][edge[1]] = cost;

			} else {
				final int deg = tree.outDeg(to);
				double sum = cost; // initial cost is the cost for deleting the
									// 'to' node
				for (int n = 0; n < deg; n++) {
					final int sub = tree.getNeighbor(to, n);
					if (sub != from) {
						sum += weights[to][n];
					}
				}

				weights[from][edge[1]] = sum;
			}
		}

		tree.setWeights(weights);
	}

}
