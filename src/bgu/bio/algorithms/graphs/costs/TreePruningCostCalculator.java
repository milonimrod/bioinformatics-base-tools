package bgu.bio.algorithms.graphs.costs;

import bgu.bio.adt.graphs.Tree;

public interface TreePruningCostCalculator {
	public void calculateCost (Tree tree);
}
