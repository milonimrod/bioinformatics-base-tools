package bgu.bio.algorithms.graphs.hsa.matchers;


import gnu.trove.map.hash.TIntObjectHashMap;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMatcherFactory implements MatcherFactory {

	TIntObjectHashMap<TIntObjectHashMap<List<BipartiteCavityMatcher>>> matcherPools;
	TIntObjectHashMap<TIntObjectHashMap<List<BipartiteCavityMatcher>>> noMatcherPools;
	int maxPoolCapacity;

	public AbstractMatcherFactory() {
		matcherPools = new TIntObjectHashMap<TIntObjectHashMap<List<BipartiteCavityMatcher>>>();
		noMatcherPools = new TIntObjectHashMap<TIntObjectHashMap<List<BipartiteCavityMatcher>>>();
		maxPoolCapacity = 2000000;
	}

	@Override
	public BipartiteCavityMatcher make(int sizeX, int sizeY) {
		return make(sizeX, sizeY, true);
	}


	@Override
	public BipartiteCavityMatcher make(int sizeX, int sizeY, boolean allowMatch) {

		BipartiteCavityMatcher matcher;
		int smallSize, bigSize;
		boolean isXSmall;
		if (sizeX <= sizeY){
			smallSize = sizeX;
			bigSize = sizeY;
			isXSmall = true;
		}
		else{
			smallSize = sizeY;
			bigSize = sizeX;
			isXSmall = false;
		}

		if (allowMatch){
			matcher = makeMatcher(sizeX, sizeY, smallSize, bigSize, isXSmall);
		}
		
		else{
			matcher = getMatcherFromPool(smallSize, bigSize, noMatcherPools);
			if (matcher != null) {
				matcher.setSmallX(isXSmall);
			} else {
				matcher = new NoMatchBipartiteCavityMatcher(sizeX, sizeY);
			}
		}
		
		matcher.myFactory = this;
		return matcher;
	}

	protected abstract BipartiteCavityMatcher makeMatcher(int sizeX, int sizeY,
			int smallSize, int bigSize, boolean isXSmall);

	protected BipartiteCavityMatcher getMatcherFromPool(int smallSize, int bigSize, 
			TIntObjectHashMap<TIntObjectHashMap<List<BipartiteCavityMatcher>>> pool) {
		BipartiteCavityMatcher matcher = null;
		TIntObjectHashMap<List<BipartiteCavityMatcher>> xMap = pool.get(smallSize);
		if (xMap != null) {
			List<BipartiteCavityMatcher> xyList = xMap.get(bigSize);
			if (xyList != null && !xyList.isEmpty()) {
				matcher = xyList.remove(xyList.size() - 1);
				matcher.reset();
			}
		}
		return matcher;
	}

	@Override
	public void returnMatcher(BipartiteCavityMatcher matcher) {
		if (matcher instanceof NoMatchBipartiteCavityMatcher){
			addMatcherToPool(matcher, matcher.getSmallSize(), matcher.getBigSize(), noMatcherPools);
		}else {
			addMatcherToPool(matcher, matcher.getSmallSize(), matcher.getBigSize(), matcherPools);
		}
	}

	private void addMatcherToPool(BipartiteCavityMatcher matcher, int smallSize,
			int bigSize, TIntObjectHashMap<TIntObjectHashMap<List<BipartiteCavityMatcher>>> pool) {
		TIntObjectHashMap<List<BipartiteCavityMatcher>> xMap = pool.get(smallSize);
		if (xMap == null) {
			xMap = new TIntObjectHashMap<List<BipartiteCavityMatcher>>();
			pool.put(smallSize, xMap);
		}
		List<BipartiteCavityMatcher> xyList = xMap.get(bigSize);
		if (xyList == null){
			xyList = new ArrayList<BipartiteCavityMatcher>();
			xMap.put(bigSize, xyList);
		}
		if (xyList.size() < maxPoolCapacity) {
			xyList.add(matcher);
		}
	}
}
