package bgu.bio.algorithms.graphs.hsa.matchers;

import gnu.trove.list.array.TIntArrayList;

import java.util.Arrays;

import bgu.bio.util.MathOperations;

/**
 * @author milon
 *
 */
public abstract class BipartiteCavityMatcher {
	/**
	 * 
	 */
	protected static final int SMALL = 0;
	/**
	 * 
	 */
	protected static final int BIG = 1;
	/**
	 * 
	 */
	protected static final int UNMACHED = -1;
	/**
	 * 
	 */
	protected static final int NO_FORCED_UNMATCHED = -1;
	/**
	 * 
	 */
	protected static final int TWO_FORCED_UNMATCHED = -2;
	/**
	 * 
	 */
	protected static final int FULL_MATCHING_INSTANCE_ID = -1;
	/**
	 * 
	 */
	protected static final int NO_INSTANCE_ID = -2;

	/**
	 * 
	 */
	protected final int[] sizes;

	/**
	 * 
	 */
	protected double[][] minCostCavityMatching;
	/**
	 * 
	 */
	private double minCostFullMatching;

	/**
	 * 
	 */
	protected MatcherFactory myFactory;

	/**
	 * 
	 */
	protected boolean[][] forceMatch; 
	/**
	 * 
	 */
	protected int[][] bestMatch;
	/**
	 * 
	 */
	protected int[][] currMatch;
	/**
	 * 
	 */
	protected double[][] matchCosts;
	/**
	 * 
	 */
	protected double[][] delCosts;

	/**
	 * 
	 */
	protected boolean isUpdated;
	/**
	 * 
	 */
	protected double delAll;
	/**
	 * 
	 */
	protected double infDelCost;

	/**
	 * 
	 */
	protected int lastInstanceIx;

	/**
	 * 
	 */
	/**
	 * 
	 */
	protected int X,Y;
	/**
	 * holds the cavity pair information
	 */
	protected int[] pair;

	/**
	 * 
	 */
	private void buildCavityTable() {

		minCostCavityMatching = new double[sizes[SMALL]][sizes[BIG]];
		clearCavityMatchings();
	}

	/**
	 * @param sizeX
	 * @param sizeY
	 */
	public BipartiteCavityMatcher(int sizeX, int sizeY) {
		this(sizeX, sizeY, true);
	}


	/**
	 * @param sizeX
	 * @param sizeY
	 * @param buildCavityTable
	 */
	public BipartiteCavityMatcher(int sizeX, int sizeY, boolean buildCavityTable) {
		super();
		setSmallX(sizeX <= sizeY);

		sizes = new int[2];
		sizes[X] = sizeX;
		sizes[Y] = sizeY;

		pair = new int[2];

		setMinCostFullMatching(Double.NaN);

		matchCosts = new double[sizes[SMALL]][sizes[BIG]];

		delCosts = new double[2][];
		delCosts[SMALL] = new double[sizes[SMALL]];
		delCosts[BIG] = new double[sizes[BIG]];

		bestMatch = new int[2][];
		bestMatch[SMALL] = new int[sizes[SMALL]];
		bestMatch[BIG] = new int[sizes[BIG]];

		lastInstanceIx = NO_INSTANCE_ID;

		currMatch = new int[2][];
		currMatch[SMALL] = new int[sizes[SMALL]];
		currMatch[BIG] = new int[sizes[BIG]];

		forceMatch = new boolean[2][];
		forceMatch[SMALL] = new boolean[sizes[SMALL]];
		forceMatch[BIG] = new boolean[sizes[BIG]];

		if (buildCavityTable){
			buildCavityTable();
		}

	}


	/**
	 * @param x
	 * @param y
	 * @param cost
	 */
	public void setMatchCost(int x, int y, double cost){
		setPairXY(x, y);
		matchCosts[pair[SMALL]][pair[BIG]] = cost;
		isUpdated = true;
	}

	/**
	 * Sets the indexes of the pairs to be ignored in the cavity calculation 
	 * @param x - the index in the small group
	 * @param y - the index in the large group
	 */
	protected void setPairXY(int x, int y) {
		pair[X] = x;
		pair[Y] = y;
	}

	/**
	 * @return
	 */
	public double minCostMatching(){
		if (isUpdated) {
			finalizeMatcher();
		}
		if (Double.isNaN(getMinCostFullMatching())){
			setMinCostFullMatching(computeMinCostMatching());
			lastInstanceIx = FULL_MATCHING_INSTANCE_ID;
		}
		return getMinCostFullMatching();
	}

	/**
	 * @return
	 */
	protected abstract double computeMinCostMatching();


	/**
	 * @param x
	 * @param y
	 * @return
	 */
	public double minCostCavityMatching(int x, int y){
		if (isUpdated) {
			finalizeMatcher();
		}
		setPairXY(x, y);
		if (Double.isNaN(minCostCavityMatching[pair[SMALL]][pair[BIG]])){
			minCostCavityMatching[pair[SMALL]][pair[BIG]] = computeMinCostCavityMatching();
			lastInstanceIx = pair[SMALL] * sizes[BIG] + pair[BIG];
		}
		return minCostCavityMatching[pair[SMALL]][pair[BIG]];
	}

	/**
	 * It is assumed that pair[SMALL] and pair[BIG] hold the corresponding
	 * cavity indices.
	 * @return
	 */
	/**
	 * @return
	 */
	protected abstract double computeMinCostCavityMatching();

	/**
	 * @param x
	 */
	public void processAllCavityMatchingY(int x){
		if (isUpdated) {
			finalizeMatcher();
		}
		computeAllCavityMatchingY(x);
		lastInstanceIx = NO_INSTANCE_ID;
	}

	/**
	 * @param x
	 */
	protected abstract void computeAllCavityMatchingY(int x);

	/**
	 * 
	 */
	public void processAllPairsCavityMatching(){
		if (isUpdated) {
			finalizeMatcher();
		}
		computeAllPairsCavityMatching();
		lastInstanceIx = NO_INSTANCE_ID;
	}

	/**
	 * 
	 */
	protected abstract void computeAllPairsCavityMatching();

	/**
	 * @return
	 */
	public TIntArrayList[] getCurrMatching() {
		TIntArrayList[] matching = new TIntArrayList[2];
		matching[0] = new TIntArrayList();
		matching[1] = new TIntArrayList();
		return getCurrMatching(matching);
	}

	/**
	 * @param matching
	 * @return
	 */
	public TIntArrayList[] getCurrMatching(TIntArrayList[] matching){
		setCurrMatching();
		matching[0].resetQuick();
		matching[1].resetQuick();
		for (int i=0; i<sizes[SMALL]; ++i){
			if (currMatch[SMALL][i] != UNMACHED){
				matching[X].add(i);
				matching[Y].add(currMatch[SMALL][i]);
			}
		}
		return matching;
	}

	/**
	 * @param x
	 * @param y
	 * @return
	 */
	public abstract TIntArrayList[] getCavityMatching(int x, int y);

	/**
	 * 
	 */
	protected abstract void setCurrMatching();


	/**
	 * 
	 */
	public void reset(){
		clearCavityMatchings();
		Arrays.fill(currMatch[SMALL], UNMACHED);
		Arrays.fill(currMatch[BIG], UNMACHED);
		setMinCostFullMatching(Double.NaN);
		isUpdated = true;
		lastInstanceIx = NO_INSTANCE_ID;
		delAll = 0.0;
		for (int i=0;i<delCosts.length;i++){
			Arrays.fill(delCosts[i], 0);
		}
		for (int i=0;i<matchCosts.length;i++){
			Arrays.fill(matchCosts[i], Double.POSITIVE_INFINITY);
		}
		for (int i=0;i<bestMatch.length;i++){
			Arrays.fill(bestMatch[i], 0);
		}
		concreteReset();
	}


	/**
	 * 
	 */
	protected abstract void concreteReset();


	/**
	 * 
	 */
	public void retain() {
		myFactory.returnMatcher(this);
	}

	/**
	 * @param x
	 * @return
	 */
	public int getBestMatchX(int x){
		return getBestMatch(X, x);
	}

	/**
	 * @param y
	 * @return
	 */
	public int getBestMatchY(int y){
		return getBestMatch(Y, y);
	}

	/**
	 * @param group
	 * @param ix
	 * @return
	 */
	private int getBestMatch(int group, int ix) {
		if (isUpdated) {
			finalizeMatcher();
		}
		return bestMatch[group][ix];
	}


	/**
	 * @param x
	 * @param cost
	 */
	public void setDelCostX(int x, double cost) {
		setDelCost(X, x, cost);
	}

	/**
	 * @param y
	 * @param cost
	 */
	public void setDelCostY(int y, double cost) {
		setDelCost(Y, y, cost);
	}

	/**
	 * @param group
	 * @param ix
	 * @param cost
	 */
	private void setDelCost(int group, int ix, double cost) {
		if (MathOperations.isInfinity(cost)){
			forceMatch[group][ix] = true;
		}
		else{
			forceMatch[group][ix] = false;
		}
		delCosts[group][ix] = cost;
		isUpdated = true;
	}


	/**
	 * @param x
	 * @param y
	 * @return
	 */
	public double getMatchCostXY(int x, int y) {
		if (X == SMALL){
			return getMatchCostSmallBig(x, y);
		}
		return getMatchCostSmallBig(y, x);
	}

	/**
	 * @param i
	 * @param j
	 * @return
	 */
	public double getMatchCostSmallBig(int i, int j) {
		if (i >= 0 && i < sizes[SMALL] && 
				j >= 0 && j < sizes[BIG]){
			return matchCosts[i][j];
		}
		else if (i < 0 || i >= sizes[SMALL]){
			return delCosts[BIG][j];
		}
		else return delCosts[SMALL][i];
	}


	/**
	 * @param y
	 * @return
	 */
	public double getDelCostY(int y) {
		return delCosts[Y][y];
	}

	/**
	 * @param x
	 * @return
	 */
	public double getDelCostX(int x) {
		return delCosts[X][x];
	}

	/**
	 * 
	 */
	public void finalizeMatcher(){
		if (isUpdated){
			dealWithForcedMatches();
			delAll = 0;
			Arrays.fill(bestMatch[SMALL], 0);
			Arrays.fill(bestMatch[BIG], 0);

			for (int j=0; j<sizes[BIG]; ++j){
				delAll += safeDelCost(BIG, j);
			}

			for (int i=0; i<sizes[SMALL]; ++i){
				delAll += safeDelCost(SMALL, i);
				for (int j=0; j<sizes[BIG]; ++j){
					if (matchCosts[i][j] - safeDelCost(BIG,j) < matchCosts[i][bestMatch[SMALL][i]] - safeDelCost(BIG,bestMatch[SMALL][i])){
						bestMatch[SMALL][i] = j;
					}
					if (matchCosts[i][j] - safeDelCost(SMALL,i) < matchCosts[bestMatch[BIG][j]][j] - safeDelCost(SMALL,bestMatch[BIG][j])){
						bestMatch[BIG][j] = i;
					}
				}
			}
			setMinCostFullMatching(Double.NaN);
			concreteFinilize();
			isUpdated = false;
		}
	}


	/**
	 * @param group
	 * @param ix
	 * @return
	 */
	protected final double safeDelCost(int group, int ix) {
		if (forceMatch[group][ix]){
			return infDelCost;
		}
		return delCosts[group][ix];
	}


	/**
	 * 
	 */
	protected abstract void concreteFinilize();


	/**
	 * @param x
	 * @return
	 */
	public final boolean forceMatchX(int x) {
		return forceMatch[X][x];
	}

	/**
	 * @param y
	 * @return
	 */
	public final boolean forceMatchY(int y) {
		return forceMatch[Y][y];
	}


	/**
	 * Replaces infinity deletion costs with big enough costs, to avoid numeric
	 * issues (adding and reducing infinities).
	 */
	/**
	 * 
	 */
	protected void dealWithForcedMatches() {
		boolean hasForcedMatches = false;
		for (int x=0; x<sizes[X] && !hasForcedMatches; ++x){
			if(forceMatchX(x)){
				hasForcedMatches = true;
			}
		}

		for (int y=0; y<sizes[Y] && !hasForcedMatches; ++y){
			if(forceMatchY(y)){
				hasForcedMatches = true;
			}
		}

		if (hasForcedMatches){
			double minRegularCost = 0;
			double maxOneForcedCost = 0; //Double.NEGATIVE_INFINITY;
			double minOneForcedCost = 0; //Double.POSITIVE_INFINITY;
			double maxTwoForcedCost = 0; //Double.NEGATIVE_INFINITY;
			double currCost;

			for (int i=0; i<sizes[SMALL]; ++i){
				for (int j=0; j<sizes[BIG]; ++j){
					currCost = matchCosts[i][j];
					if (MathOperations.isInfinity(currCost)){
						continue;
					}
					if (forceMatch[SMALL][i] || forceMatch[BIG][j]){
						if (forceMatch[SMALL][i] && forceMatch[BIG][j]){
							if (maxTwoForcedCost < currCost){
								maxTwoForcedCost = currCost;
							}
						}
						else {
							if (!forceMatch[SMALL][i]){
								currCost -= delCosts[SMALL][i];
							}
							else {
								currCost -= delCosts[BIG][j];
							}
							if (maxOneForcedCost < currCost){
								maxOneForcedCost = currCost;
							}
							if (minOneForcedCost > currCost){
								minOneForcedCost = currCost;
							}
						}
					}
					else{
						currCost -= (delCosts[SMALL][i] + delCosts[BIG][j]);
						if (minRegularCost > currCost){
							minRegularCost = currCost;
						}
					}
				}
			}
			
			

			infDelCost = maxOneForcedCost - minRegularCost + 1;
			if (infDelCost < maxTwoForcedCost - minOneForcedCost + 1){
				infDelCost = maxTwoForcedCost - minOneForcedCost + 1;
			}
		}

	}


	/**
	 * @param group
	 * @return
	 */
	protected int forcedUnmatched(int group) {
		int res = NO_FORCED_UNMATCHED;
		for (int i=0; i<currMatch[group].length; ++i){
			if (forceMatch[group][i] && currMatch[group][i] == UNMACHED){
				if (res == NO_FORCED_UNMATCHED){
					res = i;
				}
				else return TWO_FORCED_UNMATCHED;
			}
		}
		return res;
	}

	/**
	 * 
	 */
	protected void fillBIGMatching() {
		Arrays.fill(currMatch[BIG], UNMACHED);
		for (int i=0; i<sizes[SMALL]; ++i){
			if (currMatch[SMALL][i] != UNMACHED){
				currMatch[BIG][currMatch[SMALL][i]] = i;
			}
		}
	}

	/**
	 * 
	 */
	public void clearCavityMatchings(){
		if (minCostCavityMatching != null){
			for (int i=0; i<sizes[SMALL]; ++i){
				Arrays.fill(minCostCavityMatching[i], Double.NaN);
			}
		}
	}

	/**
	 * @param i
	 * @param costs
	 */
	public void setAllMatchCost(int i, double... costs) {
		assert costs.length == sizes[Y];
		if (X == SMALL){
			matchCosts[i] = costs;
		}
		else{
			for (int y=0; y<sizes[Y]; ++y){
				matchCosts[y][i] = costs[y];
			}
		}
	}

	/**
	 * @param costs
	 */
	public void setAllDelCostX(double... costs) {
		assert costs.length == sizes[X];
		delCosts[X] = costs;
	}

	/**
	 * @param costs
	 */
	public void setAllDelCostY(double... costs) {
		assert costs.length == sizes[Y];
		delCosts[Y] = costs;
	}

	/**
	 * @return
	 */
	public int getXSize() {
		return sizes[X];
	}

	/**
	 * @return
	 */
	public int getYSize() {
		return sizes[Y];
	}

	/**
	 * @param isXSmaller
	 */
	public void setSmallX(boolean isXSmaller) {
		if (isXSmaller){
			X = SMALL;
			Y = BIG;
		}
		else{
			Y = SMALL;
			X = BIG;
		}

	}

	/**
	 * @return
	 */
	public int getSmallSize() {
		return sizes[SMALL];
	}

	/**
	 * @return
	 */
	public int getBigSize() {
		return sizes[BIG];
	}

	/**
	 * @param ix
	 * @return
	 */
	public double safeDelCostX(int ix) {
		return safeDelCost(X, ix);
	}

	/**
	 * @param ix
	 * @return
	 */
	public double safeDelCostY(int ix) {
		return safeDelCost(Y, ix);
	}

	public double getMinCostFullMatching() {
		return minCostFullMatching;
	}

	public void setMinCostFullMatching(double minCostFullMatching) {
		this.minCostFullMatching = minCostFullMatching;
	}
}