package bgu.bio.algorithms.graphs.hsa.matchers;


import gnu.trove.list.array.TIntArrayList;

import java.util.Arrays;

import bgu.bio.adt.graphs.WeightedGraph;
import bgu.bio.algorithms.graphs.ShortestPaths;
import bgu.bio.util.MathOperations;

public class CostFlowNetwork extends WeightedGraph {

	protected int[][] capacities;
	protected int[][] flow;
	protected double[] delta, l;
	protected int[] pi;
	protected int source, target;
	protected int flowSize;
	protected double cost;
	protected TIntArrayList open;
	TIntArrayList currLayer, nextLayer;


	/**
	 * Important: input must be valid!!!
	 * Requirements: all edges are bi-directed, where the weight of reverse 
	 * edges are the negative of the direct ones. In addition, capacities have
	 * to be nonnegative and no negative cycles are allowed.
	 *  
	 * @param outAdjLists
	 * @param weights
	 * @param capacities
	 * @param source
	 * @param target
	 */
	public CostFlowNetwork(int[][] outAdjLists, double[][] weights, int[][] capacities, int source, int target) {
		super(outAdjLists, weights);
		this.capacities = capacities;
		this.source = source;
		this.target = target;

		flow = new int[nodeNum][];
		for (int nodeIx=0; nodeIx<nodeNum; ++nodeIx){
			flow[nodeIx] = new int[outAdjLists[nodeIx].length];
		}

		delta = ShortestPaths.initDelta(this);
		l = initLabels();
		pi = ShortestPaths.initPi(this);
		open = new TIntArrayList(weights.length);
		cost = 0;
		flowSize = 0;

		currLayer = new TIntArrayList(weights.length);
		nextLayer = new TIntArrayList(weights.length);
	}


	public void clearFlow() {
		for (int nodeIx=0; nodeIx<nodeNum; ++nodeIx){
			Arrays.fill(flow[nodeIx],0);
		}
		cost = 0;
		flowSize = 0;
	}


	private double[] initLabels() {
		double[] labels = new double[nodeNum];

		return labels;
	}
	
	public void improveFlowCost(){
		int pathCapacity = Integer.MAX_VALUE;
		int to = target, from, fromNeighborIx, toNeighborIx;
		while (to != source){
			from = pi[to];
			fromNeighborIx = 0;
			while (outAdjLists[from][fromNeighborIx] != to) ++fromNeighborIx;

			if (pathCapacity > capacities[from][fromNeighborIx]-flow[from][fromNeighborIx]) {
				pathCapacity = capacities[from][fromNeighborIx]-flow[from][fromNeighborIx];
			}
			to = from;			
		}
		
		to = target;
		while (to != source){		
			from = pi[to];

			fromNeighborIx = 0;
			while (outAdjLists[to][fromNeighborIx] != from) ++fromNeighborIx;
			toNeighborIx = 0;
			while (outAdjLists[from][toNeighborIx] != to) ++toNeighborIx;

			flow[to][fromNeighborIx] -= pathCapacity;
			//			capacities[to][fromNeighborIx] += pathCapacity;
			//			weights[to][fromNeighborIx] = -weights[from][toNeighborIx];

			flow[from][toNeighborIx] += pathCapacity;
			//			capacities[from][toNeighborIx] -= pathCapacity;
			//			if (capacities[from][toNeighborIx] == 0){
			//				weights[from][toNeighborIx] = Double.POSITIVE_INFINITY;
			//			}

			to = from;
		}

		cost += pathCapacity * (delta[target] + l[target]);
		flowSize += pathCapacity;
		for (to = 0; to<nodeNum; ++to){
			l[to] += delta[to];
		}
		dijkstra(source);

	}


	public void computeMinCostFlow() {
		clearFlow();
		dijkstra(source);
		while(MathOperations.greater(0, delta[target] + l[target])){
			improveFlowCost();
		}
	}

	public void computeMinCostMaxFlow() {
		clearFlow();
		dijkstra(source);
		while(delta[target] < Double.POSITIVE_INFINITY){
			improveFlowCost();
		}
	}

	/**
	 * An implementation of the Dijkstra algorithm, considering edge capacities,
	 * flow, and the labeling function.
	 * @param source the source node.
	 */
	protected void dijkstra(int source){
		Arrays.fill(delta, Double.POSITIVE_INFINITY);
		Arrays.fill(pi, -1);
		open.resetQuick();

		delta[source] = 0;
		open.add(source);

		int to;
		while (!open.isEmpty()){
			int from = open.getQuick(0);
			int fromInd = 0;
			double currDelta = delta[from];
			
			//TODO: inefficient. Replace with a priority queue that allows efficient decrease key.
			for (int i=1;i<open.size();i++){
				final int node = open.getQuick(i);
				if (MathOperations.greater(currDelta,delta[node])){
					from = node;
					fromInd = i;
					currDelta = delta[from];
				}
			}
			
			//remove the last by replacing the last int in the array
			open.setQuick(fromInd, open.getQuick(open.size() - 1));
			open.removeAt(open.size() - 1);

			for (int neighborIx=0; neighborIx<outAdjLists[from].length; ++neighborIx){
				if (capacities[from][neighborIx]-flow[from][neighborIx] > 0){
					to = outAdjLists[from][neighborIx];
					final double relaxedPathWeight = currDelta + l[from] + weights[from][neighborIx] - l[to];
					if (MathOperations.greater(delta[to],relaxedPathWeight)){
						delta[to] = relaxedPathWeight;
						pi[to] = from;
						open.add(to);
					}
				}
			}
		}
	}

	/**
	 * An implementation of the Dijkstra algorithm, considering edge capacities,
	 * flow, and the labeling function. The function computes shortest paths 
	 * into the target node.
	 * @param target the target node.
	 */
	protected void backDijkstra(int target){
		Arrays.fill(delta, Double.POSITIVE_INFINITY);
		Arrays.fill(pi, -1);
		open.resetQuick();

		delta[target] = 0;
		open.add(target);

		int from, backNeighborIx;
		while (!open.isEmpty()){
			int to = open.getQuick(0);
			int toInd = 0;
			double currDelta = delta[to];
			
			//TODO: inefficient. Replace with a priority queue that allows efficient decrease key.
			
			for (int i=1;i<open.size();i++){
				final int node = open.getQuick(i);
				if (delta[node] < currDelta){
					to = node;
					toInd = i;
					currDelta = delta[to];
				}
			}
			
			//remove the last by replacing the last int in the array
			open.setQuick(toInd, open.getQuick(open.size() - 1));
			open.removeAt(open.size() - 1);
			
			for (int neighborIx=0; neighborIx<inAdjLists[to].length; ++neighborIx){
				from = inAdjLists[to][neighborIx];
				backNeighborIx = inToOutNeighborIx[to][neighborIx];
				if (capacities[from][backNeighborIx]-flow[from][backNeighborIx] > 0){
					double relaxedPathWeight = currDelta + l[from] + weights[from][backNeighborIx] - l[to];
					if (MathOperations.greater(delta[from],relaxedPathWeight)){
						delta[from] = relaxedPathWeight;
						pi[from] = to;
						open.add(from);
					}
				}
			}
		}
	}

	/**
	 * An implementation of the Belman-Ford algorithm.
	 * @param source
	 */
	protected void belmanFord(int source){
		Arrays.fill(delta, Double.POSITIVE_INFINITY);
		Arrays.fill(pi, -1);
		delta[source] = 0;

		int secondNode;
		boolean improved = true;

		while (improved){
			improved = false;

			for (int firstNode = 0; firstNode<nodeNum; ++firstNode){
				int[] currNeighbors = outAdjLists[firstNode];
				double[] currWeights = weights[firstNode];
				double firstNodePath = delta[firstNode];

				for (int neighborIx = 0; neighborIx<currNeighbors.length; ++neighborIx){
					secondNode = currNeighbors[neighborIx];
					if (MathOperations.greater(delta[secondNode],firstNodePath + currWeights[neighborIx])){
						delta[secondNode] = firstNodePath + currWeights[neighborIx];
						pi[secondNode] = firstNode;
						improved = true;
					}
				}
			}
		}
	}

	/**
	 * An implementation of the Floyd-Wrashall algorithm for All Pairs Shortest
	 * Paths. 
	 * 
	 * @param d an n by n matrix, where n is the number of nodes in the graph.
	 */
	protected void floydWarshall(double[][] d, int[][] traceback){
		boolean keepTraceback = traceback != null;

		for (int nodeIx=0; nodeIx<nodeNum; ++nodeIx){
			Arrays.fill(d[nodeIx], Double.POSITIVE_INFINITY);
			Arrays.fill(traceback[nodeIx], -1);
		}

		for (int from=0; from<nodeNum; ++from){
			d[from][from] = 0;
			for (int neighborIx = outAdjLists[from].length-1; neighborIx>=0; --neighborIx){
				if (capacities[from][neighborIx] - flow[from][neighborIx] > 0){
					int to = outAdjLists[from][neighborIx];
					d[from][to] = weights[from][neighborIx];
				}
			}
		}

		for (int k=0; k<nodeNum; ++k){
			for (int from=0; from<nodeNum; ++from){
				for (int to=0; to<nodeNum; ++to){
					if (d[from][to] > d[from][k]+d[k][to]) {
						d[from][to] = d[from][k]+d[k][to];
						if (keepTraceback) traceback[from][to] = k;
					}
				}
			}
		}

	}


	public void computeLabelsDag() {
		Arrays.fill(l, Double.POSITIVE_INFINITY);
		l[source] = 0;
		currLayer.resetQuick();
		currLayer.add(source);
		while (!currLayer.isEmpty()){
			nextLayer.resetQuick();
			for (int i=0;i<currLayer.size();i++){
				int node = currLayer.getQuick(i);
				for (int neighborIx = 0; neighborIx<outAdjLists[node].length; ++neighborIx){
					if (capacities[node][neighborIx] > 0){
						int neighbor = outAdjLists[node][neighborIx];
						if (MathOperations.greater(l[neighbor], l[node]+weights[node][neighborIx])){
							if (Double.isInfinite(l[neighbor])){
								nextLayer.add(neighbor);
							}
							l[neighbor] = l[node]+weights[node][neighborIx];
						}
					}
				}
			}
			TIntArrayList tmp = currLayer;
			currLayer = nextLayer;
			nextLayer = tmp;
		}
	}
}
