package bgu.bio.algorithms.graphs.hsa.matchers;

import gnu.trove.list.array.TIntArrayList;


/**
 * 
 * Intended for the case where one of the groups is small (i.e. size 1 or 2).
 *
 */
public class DoubletonUnorderedBipartiteCavityMatcher extends
BipartiteCavityMatcher {

	public static int constractions = 0;

	//	protected int bigGroupSize;
//	protected boolean isXDoubleton;
	//	protected double dellAllBig;
	//	protected int[][] bestMatch;
	//	protected boolean modifiedCosts;
	//	protected int[] currMatching;
	int[] secondBest;

	public DoubletonUnorderedBipartiteCavityMatcher(int bigGroupSize, boolean isXDoubleton) {
		//		super(sizeX(bigGroupSize, isXDoubleton), sizeY(bigGroupSize, isXDoubleton));
		super(2, bigGroupSize);
		setSmallX(isXDoubleton);
		++constractions;
//		this.isXDoubleton = isXDoubleton;
		secondBest = new int[2];
		//		this.bigGroupSize = bigGroupSize;

		//		matchCosts = new double[2][bigGroupSize+1]; // last element in each array are of the deletion cost of the doubleton group.
		//		delCosts = new double[bigGroupSize+1];
		//		bestMatch = new int[2][2];
		//		currMatching = new int[2];
//		if (!isXDoubleton){
//			X = BIG;
//			Y = SMALL;
//		}
		reset();
	}

//	@Override
//	public void setMatchCost(int x, int y, double cost) {
//		if (isXDoubleton){
//			super.setMatchCost(x, y, cost);
//		}
//		else{
//			super.setMatchCost(y, x, cost);
//		}
//	}
//
//	@Override
//	public double getMatchCostXY(int x, int y) {
//		if (isXDoubleton){
//			return super.getMatchCostXY(x, y); 
//		}
//		else{
//			return super.getMatchCostXY(y, x);
//		}
//	}

//	@Override
//	public void setDelCostX(int x, double cost) {
//		if (isXDoubleton) {
//			super.setDelCostX(x, cost);
//		}
//		else {
//			super.setDelCostY(x, cost);
//		}
//	}
//
//	@Override
//	public void setDelCostY(int y, double cost) {
//		if (isXDoubleton) {
//			super.setDelCostY(y, cost);
//		}
//		else {
//			super.setDelCostX(y, cost);
//		}
//	}

	@Override
	protected double computeMinCostMatching() {
		double best0 = 
			getMatchCostSmallBig(0, bestMatch[SMALL][0]) - 
			safeDelCost(BIG, bestMatch[SMALL][0]) - safeDelCost(SMALL, 0); 
		double best1 = 
			getMatchCostSmallBig(1, bestMatch[SMALL][1]) - 
			safeDelCost(BIG, bestMatch[SMALL][1]) - safeDelCost(SMALL, 1); 
		if (bestMatch[SMALL][0] != bestMatch[SMALL][1] || best0>0 || best1 >= 0){
			if (best0 >= 0) {
				best0 = 0;
				currMatch[SMALL][0] = UNMACHED;
			}
			else{
				currMatch[SMALL][0] = bestMatch[SMALL][0]; 
			}
			if (best1 >= 0) {
				best1 = 0;
				currMatch[SMALL][1] = UNMACHED;
			}
			else{
				currMatch[SMALL][1] = bestMatch[SMALL][1]; 
			}
			return delAll + best0 + best1;
		}
		double second0 = getMatchCostSmallBig(0, secondBest[0]) - safeDelCost(BIG, secondBest[0]) - safeDelCost(SMALL, 0); 
		double second1 = getMatchCostSmallBig(1, secondBest[1]) - safeDelCost(BIG, secondBest[1]) - safeDelCost(SMALL, 1); 
		if (second0 >= 0) {
			second0 = 0;
			currMatch[SMALL][0] = UNMACHED;
		}
		else{
			currMatch[SMALL][0] = secondBest[0]; 
		}
		if (second1 >= 0) {
			second1 = 0;
			currMatch[SMALL][1] = UNMACHED;
		}
		else{
			currMatch[SMALL][1] = secondBest[1]; 
		}
		if (best0 + second1 <= second0 + best1){
			currMatch[SMALL][0] = bestMatch[SMALL][0];
			return delAll + best0 + second1;
		}
		else {
			currMatch[SMALL][1] = bestMatch[SMALL][1];
			return delAll + second0 + best1;
		}
	}

//	@Override
//	public double minCostCavityMatching(int x, int y){
//		if (isXDoubleton){
//			return super.minCostCavityMatching(x, y);
//		}
//		else{
//			return super.minCostCavityMatching(y, x);
//		}
//	}

	@Override
	protected double computeMinCostCavityMatching() {
		int smallIx, bigIx, bigRemoved;
//		if (isXDoubleton){
			smallIx = 1-pair[SMALL];
//			bigRemoved = pair[BIG];
			currMatch[SMALL][pair[SMALL]] = UNMACHED;
//		}
//		else {
//			smallIx = 1-y;
//			bigRemoved = x;
//			currMatch[X][y] = UNMACHED;
//		}
		if (bestMatch[SMALL][smallIx] != pair[BIG]){
			bigIx = bestMatch[SMALL][smallIx];
		}
		else{
			bigIx = secondBest[smallIx];
		}
		double bestCost = getMatchCostSmallBig(smallIx, bigIx) - safeDelCost(SMALL, smallIx) - safeDelCost(BIG, bigIx);
		if (bestCost > 0) {
			bestCost = 0;
			currMatch[SMALL][smallIx] = UNMACHED;
		}
		else{
			currMatch[SMALL][smallIx] = bigIx;
		}
		
		return bestCost + delAll - safeDelCost(SMALL, pair[SMALL]) - safeDelCost(BIG, pair[BIG]);
	}

	@Override
	public void computeAllCavityMatchingY(int x) {
	}

	@Override
	public void computeAllPairsCavityMatching() {
	}


	@Override
	public TIntArrayList[] getCavityMatching(int x, int y) {
		currMatch[SMALL][0] = UNMACHED;
		currMatch[SMALL][1] = UNMACHED;
		if (X==SMALL){
			minCostCavityMatching[x][y] = Double.NaN;
		}
		else{
			minCostCavityMatching[y][x] = Double.NaN;
		}
		minCostCavityMatching(x, y);
		return getCurrMatching();
	}

//	@Override
//	protected double getDelCostY(int y) {
//		if (isXDoubleton){
//			return getDelCostY(y);
//		}
//		else {
//			return getDelCostX(y);
//		}
//	}
//
//	@Override
//	protected double getDelCostX(int x) {
//		if (isXDoubleton){
//			return getDelCostX(x);
//		}
//		else {
//			return getDelCostY(x);
//		}
//	}
//
//	@Override
//	public int getBestMatchY(int y) {
//		if (isXDoubleton){
//			return super.getBestMatchY(y);
//		}
//		else {
//			return super.getBestMatchX(y);
//		}
//	}
//
//	@Override
//	public int getBestMatchX(int x) {
//		if (isXDoubleton){
//			return super.getBestMatchX(x);
//		}
//		else {
//			return super.getBestMatchY(x);
//		}
//	}


	@Override
	protected void setCurrMatching() {
		currMatch[SMALL][0] = UNMACHED;
		currMatch[SMALL][1] = UNMACHED;
		if (lastInstanceIx == FULL_MATCHING_INSTANCE_ID){
			computeMinCostMatching();
		}
		else if (lastInstanceIx != NO_INSTANCE_ID){
			pair[SMALL] = lastInstanceIx/sizes[BIG];
			pair[BIG] = lastInstanceIx%sizes[BIG];
			computeMinCostCavityMatching();
		}
		fillBIGMatching();
	}

	@Override
	protected void concreteReset() {
//		secondBest[0] = UNMACHED;
//		secondBest[1] = UNMACHED;
		secondBest[0] = 0;
		secondBest[1] = 0;
	}

	@Override
	protected void concreteFinilize() {
		double cost;
		for (int i = 0; i < 2; ++i){
			cost = Double.POSITIVE_INFINITY;
			if (bestMatch[SMALL][i] != 0){
				secondBest[i] = 0;
			}
			else secondBest[i] = 1;
			
			for (int j=0; j<sizes[BIG]; ++j){
				if (j != bestMatch[SMALL][i] && cost > matchCosts[i][j] - safeDelCost(BIG, j)){
					cost = matchCosts[i][j] - safeDelCost(BIG, j);
					secondBest[i] = j;
				}
			}
		}
	}


}
