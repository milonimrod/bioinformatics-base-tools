package bgu.bio.algorithms.graphs.hsa.matchers;

public interface MatcherFactory {
	
	public BipartiteCavityMatcher make(int sizeX, int sizeY);

	public abstract void returnMatcher(BipartiteCavityMatcher matcher);

	BipartiteCavityMatcher make(int sizeX, int sizeY, boolean allowMatch);

}
