package bgu.bio.algorithms.graphs.hsa.matchers;

import gnu.trove.list.array.TIntArrayList;

import java.util.Arrays;

import bgu.bio.util.MathOperations;

public class NoMatchBipartiteCavityMatcher extends BipartiteCavityMatcher {

	public NoMatchBipartiteCavityMatcher(int sizeX, int sizeY) {
		super(sizeX, sizeY, false);
		Arrays.fill(currMatch[SMALL], UNMACHED);
		Arrays.fill(currMatch[BIG], UNMACHED);
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostMatching()
	 */
	@Override
	public double minCostMatching(){
		return MathOperations.INFINITY;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostCavityMatching(int, int)
	 */
	@Override
	public double minCostCavityMatching(int x, int y){
		return MathOperations.INFINITY;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllCavityMatchingY(int)
	 */
	@Override
	public void computeAllCavityMatchingY(int x){
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllPairsCavityMatching()
	 */
	@Override
	public void computeAllPairsCavityMatching(){
	}

	public String toString(){
		return "Empty matching, infinite cost.";
	}

	@Override
	public TIntArrayList[] getCavityMatching(int x, int y) {
		return getCurrMatching();
	}

	@Override
	protected double computeMinCostMatching() {
		return MathOperations.INFINITY;
	}

	@Override
	protected double computeMinCostCavityMatching() {
		return MathOperations.INFINITY;
	}

	@Override
	protected void setCurrMatching() {
	}

	@Override
	protected void concreteReset() {
	}

	@Override
	protected void concreteFinilize() {
	}


}
