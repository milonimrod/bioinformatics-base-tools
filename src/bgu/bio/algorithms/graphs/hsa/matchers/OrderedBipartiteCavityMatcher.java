package bgu.bio.algorithms.graphs.hsa.matchers;

import gnu.trove.list.array.TIntArrayList;

import java.util.Arrays;
import java.util.List;

import bgu.bio.util.MathOperations;

public class OrderedBipartiteCavityMatcher extends BipartiteCavityMatcher {


	//	protected double[][] weights;
	protected double[][] d;
	//	protected double[] delX, delY;
	//	protected int[] matchingX, matchingY;
	int bestRootSmall, bestRootBig;

	public OrderedBipartiteCavityMatcher(int sizeX, int sizeY) {
		super(sizeX, sizeY);
		//		weights = new double[sizeX][sizeY];
		d = new double[sizes[SMALL]][sizes[BIG]];
		//		delX = new double[sizeX];
		//		delY = new double[sizeY];
		//		matchingX = new int[sizeX];
		//		matchingY = new int[sizeY];
		reset();
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostMatching()
	 */
	@Override
	protected double computeMinCostMatching(){
		double tmp, best; //dellAll = 0, 
		best = Double.POSITIVE_INFINITY;
//		for (int y=0; y<bigSize; ++y){
//			dellAll += getDelCostY(y);
//		}

		for (int i=0; i<sizes[SMALL]; ++i){
			pair[SMALL] = i;
//			dellAll += getDelCostX(i);
			for (int j=0; j<sizes[BIG]; ++j){
				pair[BIG] = j;
				tmp = getMatchCostSmallBig(i, j) + minCostCavityMatching(pair[X], pair[Y]);
				if (MathOperations.greater(best,tmp)){
					best = tmp;
					bestRootSmall = i;
					bestRootBig = j;
				}
			}
		}

		if (MathOperations.greater(best,delAll)){
			best = delAll;
			bestRootSmall = -1;
			bestRootBig = -1;
		}

		return best;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostCavityMatching(int, int)
	 */
	@Override
	public double computeMinCostCavityMatching(){
		// initialization:
		for (int i=0; i<sizes[SMALL]; ++i){
			Arrays.fill(d[i], Double.POSITIVE_INFINITY);
		}

		d[0][0] = 0;
		for (int i=1; i<sizes[SMALL]; ++i){
			d[i][0] = d[i-1][0] + delCosts[SMALL][(i+pair[SMALL])%sizes[SMALL]];
		}
		for (int j=1; j<sizes[BIG]; ++j){
			d[0][j] = d[0][j-1] + delCosts[BIG][(j+pair[BIG])%sizes[BIG]];
		}

		double tmp;
		for (int i=1; i<sizes[SMALL]; ++i){
			for (int j=1; j<sizes[BIG]; ++j){
				d[i][j] = d[i-1][j-1] + matchCosts[(i+pair[SMALL])%sizes[SMALL]][(j+pair[BIG])%sizes[BIG]];
				tmp = d[i-1][j] + delCosts[SMALL][(i+pair[SMALL])%sizes[SMALL]];
				if (MathOperations.greater(d[i][j],tmp)){
					d[i][j] = tmp;
				}
				tmp = d[i][j-1] + delCosts[BIG][(j+pair[BIG])%sizes[BIG]];
				if (MathOperations.greater(d[i][j],tmp)){
					d[i][j] = tmp;
				}
			}
		}
		return d[sizes[SMALL]-1][sizes[BIG]-1];
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllCavityMatchingY(int)
	 */
	@Override
	public void computeAllCavityMatchingY(int x){
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllPairsCavityMatching()
	 */
	@Override
	public void computeAllPairsCavityMatching(){
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#getCurrMatching(int[][])
	 */
	@Override
	public TIntArrayList[] getCurrMatching(TIntArrayList[] matching) {
		matching[0].resetQuick();
		matching[1].resetQuick();

		minCostMatching();
		if (bestRootSmall != -1){
			getCavityMatching(matching, bestRootSmall, bestRootBig);
			matching[X].add(bestRootSmall);
			matching[Y].add(bestRootBig);
		}

		return matching;
	}


	private void getCavityMatching(TIntArrayList[] matching, int smallIx, int bigIx) {
		minCostCavityMatching[smallIx][bigIx] = Double.NaN;
		if (X == SMALL){
			minCostCavityMatching(smallIx, bigIx);
		}
		else{
			minCostCavityMatching(bigIx, smallIx);
		}

		int i=sizes[SMALL]-1, j=sizes[BIG]-1;
		while (i != 0 && j != 0){
			while (i != 0 && MathOperations.equals(d[i][j],d[i-1][j] + delCosts[SMALL][(i+smallIx)%sizes[SMALL]])){
				--i;
			}
			while (j != 0 && MathOperations.equals(d[i][j],d[i][j-1] + delCosts[BIG][(j+bigIx)%sizes[BIG]])){
				--j;
			}

			if (i != 0){
				matching[X].add((i+smallIx)%sizes[SMALL]);
				matching[Y].add((j+bigIx)%sizes[BIG]);
				--i;
				--j;
			}
		}
	}

	//	public String toString(){
	//
	////		StringBuilder str = new StringBuilder();
	////
	////		str.append("Cost: ").append(network.cost);
	////		//		.append(", size: ").append(network.size);
	////
	////		str.append(", Matching: ");
	////		List<Integer>[] matching = getCurrMatching();
	////
	////		for (int nextMatch = 0; nextMatch < matching[0].size(); ++nextMatch){
	////			str.append('(').append(matching[0].get(nextMatch)).append(", ").append(matching[1].get(nextMatch)).append("), ");
	////		}
	////		return str.toString();
	//		
	//	}

	@Override
	public TIntArrayList[] getCavityMatching(int x, int y) {
		TIntArrayList[] matching = new TIntArrayList[2];
		matching[0] = new TIntArrayList();
		matching[1] = new TIntArrayList();
		return getCavityMatching(x, y, matching);
	}


	public TIntArrayList[] getCavityMatching(int x, int y, TIntArrayList[] matching) {
		setPairXY(x, y);
		minCostCavityMatching[pair[SMALL]][pair[BIG]] = Double.NaN;
		minCostCavityMatching(x, y);

		int i=sizes[SMALL]-1, j=sizes[BIG]-1;
		while (i != 0 && j != 0){
			while (i != 0 && MathOperations.equals(d[i][j],d[i-1][j] + delCosts[SMALL][(i+pair[SMALL])%sizes[SMALL]])){
				--i;
			}
			while (j != 0 && MathOperations.equals(d[i][j],d[i][j-1] + delCosts[BIG][(j+pair[BIG])%sizes[BIG]])){
				--j;
			}

			if (i != 0){
				matching[X].add((i+pair[SMALL])%sizes[SMALL]);
				matching[Y].add((j+pair[BIG])%sizes[BIG]);
				--i;
				--j;
			}
		}
		return matching;
	}

	@Override
	public void concreteReset() {


	}

	@Override
	protected void concreteFinilize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setCurrMatching() {
		
	}

}
