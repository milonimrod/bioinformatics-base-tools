package bgu.bio.algorithms.graphs.hsa.matchers;

public class OrderedMatcherFactory extends AbstractMatcherFactory {

	@Override
	protected BipartiteCavityMatcher makeMatcher(int sizeX, int sizeY,
			int smallSize, int bigSize, boolean isXSmall) {
		BipartiteCavityMatcher matcher;
		if (smallSize == 1) {
			matcher = getMatcherFromPool(1, bigSize, matcherPools);
			if (matcher != null) {
				matcher.setSmallX(isXSmall);
				//						((SingletonBipartiteCavityMatcher) matcher).isXSingleton = sizeX == 1;
			} else {
				matcher = new SingletonBipartiteCavityMatcher(
						bigSize, isXSmall);
			}
		} else{
			matcher = getMatcherFromPool(smallSize, bigSize, matcherPools);
			if (matcher != null) {
				matcher.setSmallX(isXSmall);
			} else {
				matcher = new OrderedBipartiteCavityMatcher(sizeX, sizeY);
			}
		}
		return matcher;
	}
}
