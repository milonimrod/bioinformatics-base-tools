package bgu.bio.algorithms.graphs.hsa.matchers;

import gnu.trove.list.array.TIntArrayList;

import java.util.Arrays;

import bgu.bio.util.MathOperations;

/**
 * 
 * Intended for the case where one of the groups is small (i.e. size 1 or 2).
 *
 */
public class SingletonBipartiteCavityMatcher extends
BipartiteCavityMatcher {

	protected int lastCavityMatch;
	private int forceMatchBig;

	public SingletonBipartiteCavityMatcher(int bigGroupSize, boolean isXSingleton) {
		super(1, bigGroupSize);
		setSmallX(isXSingleton);
		matchCosts = new double[1][bigGroupSize];
		//		this.bigGroupSize = bigGroupSize;
		//		delCostsBig = new double[bigGroupSize];
		reset();
	}

//	@Override
//	public void setMatchCost(int x, int y, double cost) {
//		if (isXSingleton){
//			super.setMatchCost(x, y, cost);
//		}
//		else super.setMatchCost(y, x, cost);	}
//
//	@Override
//	public double getMatchCostXY(int x, int y) {
//		if (isXSingleton){
//			return super.getMatchCostXY(x, y);
//		}
//		else return super.getMatchCostXY(y, x);	
//	}
//
//	@Override
//	public void setDelCostX(int x, double cost) {
//		if (isXSingleton) {
//			super.setDelCostX(x, cost);
//		}
//		else{
//			super.setDelCostY(x, cost);
//		}
//		//		if (isXSingleton) {***
//		//			dellAll -= delCostsSingleton;
//		//			delCostsSingleton = cost;
//		//		}
//		//		else {
//		//			dellAll -= delCostsBig[x];
//		//			delCostsBig[x] = cost;
//		//		}
//		//		dellAll += cost;
//		//		modifiedCosts = true;
//	}
//
//	@Override
//	public void setDelCostY(int y, double cost) {
//		if (isXSingleton) {
//			super.setDelCostY(y, cost);
//		}
//		else{
//			super.setDelCostX(y, cost);
//		}
//		//		if (isXSingleton) {
//		//			dellAll -= delCostsBig[y];
//		//			delCostsBig[y] = cost;
//		//		}
//		//		else {
//		//			dellAll -= delCostsSingleton;
//		//			delCostsSingleton = cost;
//		//		}
//		//		dellAll += cost;
//		//		modifiedCosts = true;
//	}

	@Override
	protected double computeMinCostMatching() {
		lastCavityMatch = -1;
		if (forceMatchBig == TWO_FORCED_UNMATCHED){
			return MathOperations.INFINITY;
		}

		int best = bestMatch[SMALL][0];
		double bestEdgeCost = matchCosts[0][best] - safeDelCost(SMALL, 0) - safeDelCost(BIG, best);
		if (forceMatch[SMALL][0] || forceMatch[BIG][best] || bestEdgeCost<0){
			return delAll + bestEdgeCost;
		}
		else return delAll;

		//		if (bestMatch != -1){
		//			minCostFullMatching += 
		//				matchCosts[bestMatch] - delCostsSingleton - delCostsBig[bestMatch];
		//		}


		//		if (modifiedCosts){
		//			minCostFullMatching = Double.NaN;
		//			sortBestMatches();			
		//		}
		//		if (Double.isNaN(minCostFullMatching)){
		//			minCostFullMatching = dellAll;
		//			if (bestMatch != -1){
		//				minCostFullMatching += 
		//					matchCosts[bestMatch] - delCostsSingleton - delCostsBig[bestMatch];
		//			}
		//		}
		//		return minCostFullMatching;
	}

	//	private void sortBestMatches() {
	//		for (int i=0; i<bigGroupSize; ++i){
	//			if (bestMatch == -1 || 
	//					MathOperations.greater(matchCosts[bestMatch]-delCostsBig[bestMatch],matchCosts[i]-delCostsBig[i])){
	//				bestMatch = i;
	//			}
	//		}
	//	}

	@Override
	protected double computeMinCostCavityMatching() {
		lastCavityMatch = pair[BIG];

		if (forceMatchBig != NO_FORCED_UNMATCHED && forceMatchBig != pair[BIG]){
			return MathOperations.INFINITY;
		}

		return delAll - safeDelCost(SMALL, 0) - safeDelCost(BIG, pair[BIG]);
	}
	
//	@Override
//	public double minCostCavityMatching(int x, int y){
//		if (isXSingleton){
//			return super.minCostCavityMatching(x, y);
//		}
//		else{
//			return super.minCostCavityMatching(y, x);
//		}
//	}


	@Override
	public void computeAllCavityMatchingY(int x) {
	}

	@Override
	public void computeAllPairsCavityMatching() {
	}

	@Override
	public TIntArrayList[] getCurrMatching(TIntArrayList[] matching) {
		if (matching[SMALL] == null){
			matching[SMALL] = new TIntArrayList(1);
			matching[BIG] = new TIntArrayList(1);
		}
		else{
			matching[SMALL].resetQuick();
			matching[BIG].resetQuick();
		}
		if (lastCavityMatch == FULL_MATCHING_INSTANCE_ID && bestMatch[0][0] != UNMACHED){
//			if (isXSingleton){
				matching[X].add(0);
				matching[Y].add(bestMatch[0][0]);
//			}
//			else{
//				matching[1].add(0);
//				matching[0].add(bestMatch[0][0]);
//			}
		}
		return matching;
	}

	@Override
	public TIntArrayList[] getCavityMatching(int x, int y) {
		TIntArrayList[] matching = new TIntArrayList[2];
		matching[0] = new TIntArrayList(0);
		matching[1] = new TIntArrayList(0);
		return matching;
	}

	//	@Override
	//	public void reset() {
	//		Arrays.fill(matchCosts, Double.POSITIVE_INFINITY);
	//		Arrays.fill(delCostsBig, 0);
	//		delCostsSingleton = 0;
	//		dellAll = 0;
	//		bestMatch = -1;
	//		lastCavityMatch = -1;
	//		modifiedCosts = true;
	//		minCostFullMatching = Double.NaN;
	//	}

//	@Override
//	protected double getDelCostY(int y) {
//		if (isXSingleton){
//			return super.getDelCostY(y);
//		}
//		else {
//			return super.getDelCostX(y);
//		}
//	}
//
//	@Override
//	protected double getDelCostX(int x) {
//		if (isXSingleton){
//			return super.getDelCostX(x);
//		}
//		else {
//			return super.getDelCostY(x);
//		}
//	}



	//	@Override
	//	protected void setBestMatchY(int y, int x) {
	//		if (isXSingleton){
	//			super.setBestMatchY(y, x);
	//		}
	//		else {
	//			super.setBestMatchX(y, x);
	//		}
	//	}
	//
	//	@Override
	//	protected void setBestMatchX(int x, int y) {
	//		if (isXSingleton){
	//			super.setBestMatchX(x, y);
	//		}
	//		else {
	//			super.setBestMatchY(x, y);
	//		}
	//	}

//	@Override
//	public int getBestMatchY(int y) {
//		if (isXSingleton){
//			return super.getBestMatchY(y);
//		}
//		else {
//			return super.getBestMatchX(y);
//		}
//	}
//
//	@Override
//	public int getBestMatchX(int x) {
//		if (isXSingleton){
//			return super.getBestMatchX(x);
//		}
//		else {
//			return super.getBestMatchY(x);
//		}
//	}

	@Override
	protected void concreteReset() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void concreteFinilize() {
		Arrays.fill(currMatch[SMALL], UNMACHED);
		Arrays.fill(currMatch[BIG], UNMACHED);
		forceMatchBig = forcedUnmatched(BIG);



		//		forceMatchBig = -1;
		//		moreThanOneForceMatchBig = false;
		//		
		//		for (int bigIx=0; bigIx<sizeY; ++bigIx){
		//			if (forceMatchY(bigIx)){
		//				if (forceMatchBig != -1){
		//					moreThanOneForceMatchBig = true;
		//					break;
		//				}
		//				else forceMatchBig = bigIx;
		//			}
		//		}
	}

	@Override
	protected void setCurrMatching() {
		if (lastCavityMatch == FULL_MATCHING_INSTANCE_ID && bestMatch[SMALL][0] != UNMACHED){
			currMatch[SMALL][0] = bestMatch[SMALL][0];
			currMatch[BIG][bestMatch[SMALL][0]] = 0;
		}
	}


}
