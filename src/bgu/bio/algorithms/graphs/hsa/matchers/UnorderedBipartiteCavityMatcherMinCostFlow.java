package bgu.bio.algorithms.graphs.hsa.matchers;

import gnu.trove.list.array.TIntArrayList;

import java.util.Arrays;

import bgu.bio.algorithms.graphs.ShortestPaths;
import bgu.bio.util.MathOperations;


public class UnorderedBipartiteCavityMatcherMinCostFlow extends BipartiteCavityMatcher {


	protected int source, target, firstBIG;
	protected int smallSourceNeighbor, bigTargetNeighbor;

	protected int[][] outAdjLists, capacities;
	protected double[][] weights;

	protected CostFlowNetwork network;
	private double[][] d; // for all pairs shortest paths results
	private int smallUnmatched, bigUnmatched;
	private double[][] maxPath;
	public UnorderedBipartiteCavityMatcherMinCostFlow(int sizeX, int sizeY) {
		super(sizeX, sizeY);

		firstBIG = sizes[SMALL];
		source = sizeX+sizeY;
		target = source+1;

		smallSourceNeighbor = sizes[BIG];
		bigTargetNeighbor = sizes[SMALL];

		int n = target+1;

		//setting outAdjLists:

		outAdjLists = new int[n][];
		outAdjLists[source] = new int[sizes[SMALL]];
		outAdjLists[target] = new int[sizes[BIG]];

		for (int i=0; i<sizes[SMALL]; ++i){
			outAdjLists[i] = new int[sizes[BIG]+1];
			for (int j=0; j<sizes[BIG]; ++j){
				outAdjLists[i][j] = j + firstBIG;
			}
			outAdjLists[i][smallSourceNeighbor] = source;
			outAdjLists[source][i] = i;
		}

		for (int j=0; j<sizes[BIG]; ++j){
			outAdjLists[j+firstBIG] = new int[sizes[SMALL]+1];
			for (int i=0; i<sizes[SMALL]; ++i){
				outAdjLists[j+firstBIG][i] = i;
			}
			outAdjLists[j+firstBIG][bigTargetNeighbor] = target;
			outAdjLists[target][j] = firstBIG+j;
		}

		//setting capacities and weights:
		capacities = new int[n][];
		weights = new double[n][];

		for (int nodeIx=0; nodeIx<n; ++nodeIx){
			capacities[nodeIx] = new int[outAdjLists[nodeIx].length];
			weights[nodeIx] = new double[outAdjLists[nodeIx].length];
		}

		//source:
		Arrays.fill(capacities[source], 1);

		//SMALL:
		for (int i=0; i<sizes[SMALL]; ++i){
			Arrays.fill(capacities[i], 1);
			capacities[i][smallSourceNeighbor] = 0;
		}

		//BIG:
		for (int j=0; j<sizes[BIG]; ++j){
			Arrays.fill(capacities[j+firstBIG], 0);
			capacities[j+firstBIG][bigTargetNeighbor] = 1;
		}

		//target:
		Arrays.fill(capacities[target], 0);

		network = new CostFlowNetwork(outAdjLists, weights, capacities, source, target);
		maxPath = new double[2][];
		maxPath[SMALL] = new double[sizes[SMALL]];
		maxPath[BIG] = new double[sizes[BIG]];

		d = new double[sizes[SMALL]+2][sizes[SMALL]+2];
		bigUnmatched = sizes[SMALL];
		smallUnmatched = sizes[SMALL]+1;
		reset();
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostMatching()
	 */
	@Override
	protected double computeMinCostMatching(){
		network.computeLabelsDag();
		network.computeMinCostFlow();
		setCurrMatching();
		if (forcedUnmatched(SMALL)!=NO_FORCED_UNMATCHED || forcedUnmatched(BIG)!=NO_FORCED_UNMATCHED){
			return MathOperations.INFINITY;
		}
		return network.cost + delAll;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostCavityMatching(int, int)
	 */
	@Override
	protected double computeMinCostCavityMatching(){
		network.capacities[source][pair[SMALL]] = 0;
		network.capacities[pair[BIG]+firstBIG][bigTargetNeighbor] = 0;

		network.computeLabelsDag();
		network.computeMinCostFlow();
		setCurrMatching();

		network.capacities[source][pair[SMALL]] = 1;
		network.capacities[pair[BIG]+firstBIG][bigTargetNeighbor] = 1;

		int forcedUnmatchedSmall = forcedUnmatched(SMALL);
		int forcedUnmatchedBig = forcedUnmatched(BIG);
		if ((forcedUnmatchedSmall!=NO_FORCED_UNMATCHED && forcedUnmatchedSmall != pair[SMALL])
				|| (forcedUnmatchedBig!=NO_FORCED_UNMATCHED && forcedUnmatchedBig != pair[BIG])){
			return MathOperations.INFINITY;
		}

		double cost = network.cost + delAll - safeDelCost(SMALL, pair[SMALL]) - safeDelCost(BIG, pair[BIG]);
		return cost;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllCavityMatchingY(int)
	 */
	@Override
	protected void computeAllCavityMatchingY(int x){
		if (X==SMALL){
			network.capacities[source][x] = 0;
		}
		else{
			network.capacities[x+firstBIG][bigTargetNeighbor] = 0;
		}

		network.computeLabelsDag();
		network.computeMinCostFlow();
		setCurrMatching();

		int forcedUnmatchedX = forcedUnmatched(X);
		int forcedUnmatchedY = forcedUnmatched(Y);
		pair[X] = x;

		if ((forcedUnmatchedX!=NO_FORCED_UNMATCHED && forcedUnmatchedX!=x) || forcedUnmatchedY == TWO_FORCED_UNMATCHED){
			for (int y=0; y<sizes[Y]; ++y){
				pair[Y] = y;
				minCostCavityMatching[pair[SMALL]][pair[BIG]] = MathOperations.INFINITY;
			}
		}

		else{
			// The case where the size of the matching has decreased by 1:
			pair[X] = x;
			double tmp, initValue = network.cost + delAll - safeDelCost(X, x);
			int first, lFactor;
			if (X==SMALL){
				network.backDijkstra(source);
				initValue += network.l[source];
				first = firstBIG;
				lFactor = -1;
			}
			else{
				network.dijkstra(target);
				initValue -= network.l[target];
				first = 0;
				lFactor = 1;
			}

			for (int y=0; y<sizes[Y]; ++y){
				pair[Y] = y;
				minCostCavityMatching[pair[SMALL]][pair[BIG]] = 
					initValue + network.delta[first+y] + 
					(lFactor * network.l[first+y]) - safeDelCost(Y, y);
				double g = MathOperations.EPSILON; //DEBUG!!!
			}

			// The case where the size of the matching has not changed:
			if (X==SMALL){
				network.backDijkstra(target);
			}
			else{
				network.dijkstra(source);
			}
			initValue += network.l[target] - network.l[source];

			for (int y=0; y<sizes[Y]; ++y){
				pair[Y] = y;
				tmp = 
					initValue + network.delta[first+y] + 
					(lFactor * network.l[first+y]) - safeDelCost(Y, y);
				if (minCostCavityMatching[pair[SMALL]][pair[BIG]] > tmp){
					minCostCavityMatching[pair[SMALL]][pair[BIG]] = tmp;
				}
			}

			// Dealing with forced unmatched:
			if (forcedUnmatchedY != NO_FORCED_UNMATCHED){
				for (int y=0; y<sizes[Y]; ++y){
					if (forcedUnmatchedY == y){
						minCostCavityMatching[x][y] = MathOperations.INFINITY;
					}
				}
			}
		}

		if (X==SMALL){
			network.capacities[source][x] = 1;
		}
		else{
			network.capacities[x+firstBIG][bigTargetNeighbor] = 1;
		}
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllPairsCavityMatching()
	 */
	@Override
	protected void computeAllPairsCavityMatching(){
		setMinCostFullMatching(computeMinCostMatching());

		//		updateCurrMatch();

		int forcedUnmatchedSmall = forcedUnmatched(SMALL);
		int forcedUnmatchedBig = forcedUnmatched(BIG);
		if (forcedUnmatchedSmall==TWO_FORCED_UNMATCHED || forcedUnmatchedBig==TWO_FORCED_UNMATCHED){
			for (int i=0; i<sizes[SMALL]; ++i){
				for (int j=0; j<sizes[BIG]; ++j){
					minCostCavityMatching[i][j] = MathOperations.INFINITY;
				}
			}
		}		

		else{
			for (int i=0; i<d.length; ++i){
				Arrays.fill(d[i], Double.POSITIVE_INFINITY);
			}
			for (int i=0; i<d.length; ++i){
				d[i][i] = 0;
			}

			double edgePairCost;

			for (int i=0; i<sizes[SMALL]; ++i){
				int smallMatch = currMatch[SMALL][i];
//				double smallMatchCost = getMatchCostSmallBig(i, smallMatch);
//				if (smallMatch == UNMACHED){
//					//					if (sizes[SMALL] <= bigSize){
//					smallMatch = smallUnmatched;
//					//					}
//					//					else smallMatch = bigUnmatched;
//				}
				for (int j=0; j<sizes[BIG]; ++j){
					if (smallMatch != j){
//						if (sizes[SMALL] <= bigSize){
							int bigMatch = currMatch[BIG][j];
							edgePairCost = getMatchCostSmallBig(i, j) - getMatchCostSmallBig(bigMatch, j);
							if (bigMatch == UNMACHED){
								bigMatch = bigUnmatched;
							}
							if (d[i][bigMatch] > edgePairCost){
								d[i][bigMatch] = edgePairCost;
							}
//						}
//						else{
//							edgePairCost = getMatchCostXY(i, j) - smallMatchCost;
//							if (d[smallMatch][j] > edgePairCost){
//								d[smallMatch][j] = edgePairCost;
//							}
//						}
					}
					else{
//						if (sizes[SMALL] <= bigSize){
							d[bigUnmatched][i] = -getMatchCostSmallBig(i, j) + safeDelCost(BIG, j) ;
//						}
//						else{
//							d[j][bigUnmatched] = -getMatchCostXY(i, j) + safeDelCost(SMALL, i);
//						}
					}
				}
			}

//			if (sizes[SMALL] <= bigSize){
				for (int i=0; i<sizes[SMALL]; ++i){
					if (currMatch[SMALL][i] == UNMACHED){
						// i is unmatched.
						d[smallUnmatched][i] = -safeDelCost(SMALL,i);
					}
					else{
						//					i is matched.
						d[i][smallUnmatched] = safeDelCost(SMALL,i);
					}
				}
				d[bigUnmatched][smallUnmatched] = 0;
				if (network.flowSize > 0){
					d[smallUnmatched][bigUnmatched] = 0;
				}
//			}
//			else{
//				for (int y=0; y<bigSize; ++y){
//					if (currMatch[BIG][y] == UNMACHED){
//						// y is unmatched.
//						d[y][smallUnmatched] = -getDelCostY(y);
//					}
//					else{
//						//					y is matched.
//						d[smallUnmatched][y] = getDelCostY(y);
//					}
//				}
//				d[smallUnmatched][bigUnmatched] = 0;
//				if (network.flowSize > 0){
//					d[bigUnmatched][smallUnmatched] = 0;
//				}
//			}

			ShortestPaths.floydWarshall(d);

			for (int i=0; i<sizes[SMALL]; ++i){
				if (forcedUnmatchedSmall!=NO_FORCED_UNMATCHED && forcedUnmatchedSmall!=i){
					for (int j=0; j<sizes[BIG]; ++j){
						minCostCavityMatching[i][j] = MathOperations.INFINITY;
					}
				}
				else{
					double initValue = network.cost + delAll ;// - safeDelCost(X, x);
					int iMatch = currMatch[SMALL][i];
					double iMatchCost = getMatchCostSmallBig(i, iMatch);
					if (iMatch == UNMACHED){
//						if (sizes[SMALL] <= bigSize){
							iMatch = smallUnmatched;
//						}
//						else{
//							iMatch = bigUnmatched;
//						}
					}
					for (int j=0; j<sizes[BIG]; ++j){
						if (forcedUnmatchedBig!=NO_FORCED_UNMATCHED && forcedUnmatchedBig!=j){
							minCostCavityMatching[i][j] = MathOperations.INFINITY;
						}
						else{
//							if (sizes[SMALL] <= bigSize){
								int jMatch = currMatch[BIG][j];
								if (jMatch == UNMACHED){
//									if (sizes[SMALL] <= bigSize){
										jMatch = bigUnmatched;
//									}
//									else{
//										yMatch = smallUnmatched;
//									}
								}
								minCostCavityMatching[i][j] = initValue - getMatchCostSmallBig(jMatch, j) +
								d[jMatch][i];// - safeDelCost(Y, y);
								double g = MathOperations.EPSILON; //DEBUG!!!
//							}
//							else{
//								minCostCavityMatching[i][j] = initValue - iMatchCost +
//								d[j][iMatch];// - safeDelCost(Y, y);
//							}
						}
					}
				}
			}
		}
	}

	//	private void updateCurrMatch() {
	//		Arrays.fill(currMatch[X], sizeY);
	//		Arrays.fill(currMatch[Y], sizeX);
	//
	//		for (int x=0; x<sizeX; ++x){
	//			for (int y=0; y<sizeY; ++y){
	//				if (network.flow[x][y]==1){
	//					matchings[X][x] = y;
	//					matchings[Y][y] = x;
	//					break;
	//				}
	//			}
	//		}
	//	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){

		StringBuilder str = new StringBuilder();

		str.append("Cost: ").append(network.cost+delAll);
		//		.append(", size: ").append(network.size);

		str.append(", Matching: ");
		TIntArrayList[] matching = getCurrMatching();

		for (int nextMatch = 0; nextMatch < matching[0].size(); ++nextMatch){
			str.append('(').append(matching[0].get(nextMatch)).append(", ").append(matching[1].get(nextMatch)).append("), ");
		}
		return str.toString();
	}

	@Override
	public TIntArrayList[] getCavityMatching(int x, int y) {
		setPairXY(x, y);
		minCostCavityMatching[pair[SMALL]][pair[BIG]] = Double.NaN;
		minCostCavityMatching(x, y);
		return getCurrMatching();
	}

	@Override
	protected void concreteReset() {
		//source and target:
//		Arrays.fill(weights[source], 0);
//		Arrays.fill(weights[target], 0);


		//SMALL:
		for (int i=0; i<sizes[SMALL]; ++i){
			Arrays.fill(weights[i], Double.POSITIVE_INFINITY);
			weights[i][smallSourceNeighbor] = 0;
		}

		//BIG:
		for (int j=0; j<sizes[BIG]; ++j){
			Arrays.fill(weights[j+firstBIG], Double.POSITIVE_INFINITY);
			weights[j+firstBIG][bigTargetNeighbor] = 0;
		}


		delAll = 0;
		network.clearFlow();
		isUpdated = true;
	}


	@Override
	protected void concreteFinilize() {
		//		dealWithForcedMatches();

		for (int i=0; i<sizes[SMALL]; ++i){
			for (int j=0; j<sizes[BIG]; ++j){
				weights[i][j] = getMatchCostSmallBig(i, j) - safeDelCost(SMALL, i) - safeDelCost(BIG, j);
				weights[j+firstBIG][i] = -weights[i][j];
			}
		}

	}

	@Override
	protected void setCurrMatching(){
		Arrays.fill(currMatch[SMALL], UNMACHED);
		Arrays.fill(currMatch[BIG], UNMACHED);

		for (int i=0; i<sizes[SMALL]; ++i){
			if (network.flow[source][i] == 1){
				for (int j=0; j<sizes[BIG]; ++j){
					if (network.flow[i][j] != 0){
						currMatch[SMALL][i] = j;
						currMatch[BIG][j] = i;
						break;
					}
				}
			}
		}

	}

}
