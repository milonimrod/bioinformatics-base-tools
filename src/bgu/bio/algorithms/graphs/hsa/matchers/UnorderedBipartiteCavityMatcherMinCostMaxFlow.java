package bgu.bio.algorithms.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bgu.bio.util.MathOperations;

public class UnorderedBipartiteCavityMatcherMinCostMaxFlow extends BipartiteCavityMatcher {


	protected int delY, delX, source, target, firstY;
	protected int xSourceNeighbor, yTargetNeighbor, xDelNeighbor,yDelNeighbor;

	int[][] outAdjLists, capacities;
	double[][] weights;

	protected double minCostFullMatching;

	CostFlowNetwork network;
	private double[][] d; // for all pairs shortest paths resutls
	private int[][] traceback; // for all pairs shortest paths traceback

	public UnorderedBipartiteCavityMatcherMinCostMaxFlow(int sizeX, int sizeY) {
		super(sizeX, sizeY);

		firstY = sizeX;
		delY = sizeX+sizeY;
		delX = delY+1;
		source = delY+2;
		target = delY+3;

		SMALL = new int[sizeX+1];
		BIG = new int[sizeY+1];

		for (int x=0; x<sizeX; ++x){
			SMALL[x] = x;
		}
		SMALL[sizeX] = delY;

		for (int y=0; y<sizeY; ++y){
			BIG[y] = firstY+y;
		}
		BIG[sizeY] = delX;

		xDelNeighbor = sizeY;
		xSourceNeighbor = sizeY+1;
		yDelNeighbor = sizeX;
		yTargetNeighbor = sizeX+1;

		int n = target+1;

		//setting outAdjLists:

		outAdjLists = new int[n][];
		outAdjLists[source] = new int[sizeX+1];
		outAdjLists[target] = new int[sizeY+1];
		outAdjLists[delY] = new int[sizeY+2]; // +2 for the source and delX
		outAdjLists[delX] = new int[sizeX+2]; // +2 for the target and delY

		for (int x=0; x<sizeX; ++x){
			outAdjLists[source][x] = x;
		}
		for (int y=0; y<sizeY; ++y){
			outAdjLists[target][y] = firstY+y;
		}
		outAdjLists[source][yDelNeighbor] = delY;
		outAdjLists[target][xDelNeighbor] = delX;

		for (int x:SMALL){
			outAdjLists[x] = new int[sizeY+2]; // +2 for the source and delX
			for (int y=0; y<sizeY; ++y){
				outAdjLists[x][y] = firstY+y;
			}
			outAdjLists[x][xSourceNeighbor] = source;
			outAdjLists[x][xDelNeighbor] = delX;
		}

		for (int y:BIG){
			outAdjLists[y] = new int[sizeX+2]; // +2 for the target and delY
			for (int x=0; x<sizeX; ++x){
				outAdjLists[y][x] = x;
			}
			outAdjLists[y][yTargetNeighbor] = target;
			outAdjLists[y][yDelNeighbor] = delY;
		}


		//setting capacities and weights:
		capacities = new int[n][];
		weights = new double[n][];

		for (int nodeIx=0; nodeIx<n; ++nodeIx){
			capacities[nodeIx] = new int[outAdjLists[nodeIx].length];
			weights[nodeIx] = new double[outAdjLists[nodeIx].length];
		}


		//source:
		Arrays.fill(weights[source], 0);
		Arrays.fill(capacities[source], 1);
		capacities[source][yDelNeighbor] = sizeY;

		
		//X:
		for (int x:SMALL){
			Arrays.fill(capacities[x], 1);
			capacities[x][xSourceNeighbor] = 0;
		}
		capacities[delY][xDelNeighbor] = sizeY;
	
		
		//Y:
		for (int y:BIG){
			Arrays.fill(capacities[y], 0);
			capacities[y][yTargetNeighbor] = 1;
		}
		capacities[delX][yTargetNeighbor] = sizeX;

		//target:
		Arrays.fill(weights[target], 0);
		Arrays.fill(capacities[target], 0);
		
		network = new CostFlowNetwork(outAdjLists, weights, capacities, source, target);
		d = new double[network.nodeNum][network.nodeNum];
		traceback = new int[network.nodeNum][network.nodeNum];
		reset();
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#setMatchCost(int, int, double)
	 */
	@Override
	public void setMatchCostConcrete(int x, int y, double cost){
		weights[x][y] = cost;
		weights[y+firstY][x] = -cost;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#setDelCostX(int, double)
	 */
	@Override
	public void setDelCostX(int x, double cost){
		weights[x][xDelNeighbor] = cost;
		weights[delX][x] = -cost;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#setDelCostY(int, double)
	 */
	@Override
	public void setDelCostY(int y, double cost){
		weights[delY][y] = cost;
		weights[y+firstY][yDelNeighbor] = -cost;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostMatching()
	 */
	@Override
	public double minCostMatching(){
		if (Double.isNaN(minCostFullMatching)){
			network.computeMinCostMaxFlow();
			minCostFullMatching = network.cost;
		}
		return minCostFullMatching;
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#minCostCavityMatching(int, int)
	 */
	@Override
	public double minCostCavityMatching(int x, int y){
		if (Double.isNaN(minCostCavityMatching[x][y])){
			network.capacities[source][x] = 0;
			network.capacities[y+firstY][yTargetNeighbor] = 0;
			--network.capacities[delX][yTargetNeighbor];
			--network.capacities[source][yDelNeighbor];

			network.computeLabelsDag();
			network.computeMinCostMaxFlow();
			minCostCavityMatching[x][y] = network.cost;

			network.capacities[source][x] = 1;
			network.capacities[y+firstY][yTargetNeighbor] = 1;
			++network.capacities[delX][yTargetNeighbor];
			++network.capacities[source][yDelNeighbor];
		}
		return minCostCavityMatching[x][y];
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllCavityMatchingY(int)
	 */
	@Override
	public void computeAllCavityMatchingY(int x){
		network.capacities[source][x] = 0;
		--network.capacities[delX][yTargetNeighbor];

		network.computeLabelsDag();
		network.computeMinCostMaxFlow();
		network.backDijkstra(delY);

		for (int y=0; y<bigSize; ++y){
			minCostCavityMatching[x][y] = network.cost - network.l[firstY+y] + network.delta[firstY+y] + network.l[delY];
		}

		network.capacities[source][x] = 1;
		++network.capacities[delX][yTargetNeighbor];
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#computeAllPairsCavityMatching()
	 */
	@Override
	public void computeAllPairsCavityMatching(){
		
		network.computeLabelsDag();
		network.computeMinCostMaxFlow();
		minCostFullMatching = network.cost;
		network.floydWarshall(d, traceback);

		for (int x=0; x<smallSize; ++x){
			for (int y=0; y<bigSize; ++y){
				minCostCavityMatching[x][y] = network.cost + d[firstY+y][x];
			}
		}
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#getCurrMatching()
	 */
	@Override
	public List<Integer>[] getCurrMatching(){
		return getCurrMatching(new List[2]);
	}

	/* (non-Javadoc)
	 * @see bgu.bio.algorithms.graphs.BipartiteCavityMatcher#getCurrMatching(int[][])
	 */
	@Override
	public List<Integer>[] getCurrMatching(List<Integer>[] matching) {
		
		if (matching[0] == null) matching[0] = new ArrayList<Integer>();
		else matching[0].clear();
		if (matching[1] == null) matching[1] = new ArrayList<Integer>();
		else matching[1].clear();

		for (int x=0; x<smallSize; ++x){
			if (network.flow[x][xDelNeighbor] == 0){
				for (int y=0; y<bigSize; ++y){
					if (network.flow[x][y] == 1){
						matching[0].add(x);
						matching[1].add(y);
					}
				}
			}
		}
		return matching;
	}

	public String toString(){

		StringBuilder str = new StringBuilder();

		str.append("Cost: ").append(network.cost);
		//		.append(", size: ").append(network.size);

		str.append(", Matching: ");
		List<Integer>[] matching = getCurrMatching();
		
		for (int nextMatch = 0; nextMatch < matching[0].size(); ++nextMatch){
			str.append('(').append(matching[0].get(nextMatch)).append(", ").append(matching[1].get(nextMatch)).append("), ");
		}
		return str.toString();
	}

	@Override
	public double getMatchCostXY(int x, int y) {
		return weights[x][y];
	}

	@Override
	public List<Integer>[] getCavityMatching(int x, int y) {
		minCostCavityMatching[x][y] = Double.NaN;
		minCostCavityMatching(x, y);
		return getCurrMatching();
	}

	@Override
	public void reset() {
		//X:
		for (int x:SMALL){
			Arrays.fill(weights[x], Double.POSITIVE_INFINITY);
			weights[x][xSourceNeighbor] = 0;
		}
		//Y:
		for (int y:BIG){
			Arrays.fill(weights[y], Double.POSITIVE_INFINITY);
			weights[y][yTargetNeighbor] = 0;
		}
		weights[delY][xDelNeighbor] = 0;
		weights[delX][yDelNeighbor] = 0;
		
		for (double[] arr : minCostCavityMatching){
			Arrays.fill(arr, Double.NaN);
		}
		minCostFullMatching = Double.NaN;

		network.clearFlow();
	}

	@Override
	protected double getDelCostY(int y) {
		return weights[delY][y];
	}

	@Override
	protected double getDelCostX(int x) {
		return weights[x][delX];
	}



}
