package bgu.bio.com.protocol;

public class EchoProtocolFactory implements ServerProtocolFactory {

	public AsyncServerProtocol create() {
		return new EchoProtocol();
	}

}
