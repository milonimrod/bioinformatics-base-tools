package bgu.bio.com.protocol;

public interface ServerProtocolFactory {
   AsyncServerProtocol create();
}
