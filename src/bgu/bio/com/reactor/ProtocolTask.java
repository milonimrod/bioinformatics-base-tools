package bgu.bio.com.reactor;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.Vector;

import bgu.bio.com.protocol.ServerProtocol;



/**
 * This class supplies some data to the protocol, which then processes the data,
 * possibly returning a reply. This class is implemented as an executor task.
 * 
 */
public class ProtocolTask implements Runnable {

	private final ServerProtocol _protocol;
	private final StringMessageTokenizer _tokenizer;

	private final ConnectionHandler _handler;

	/**
	 * the fifo queue, which holds data coming from the socket. Access to the
	 * queue is serialized, to ensure correct processing order.
	 */
	private final Vector<ByteBuffer> _buffers = new Vector<ByteBuffer>();

	public ProtocolTask(final ServerProtocol protocol, final StringMessageTokenizer tokenizer, final ConnectionHandler h) {
		this._protocol = protocol;
		this._tokenizer = tokenizer;
		this._handler = h;
	}

	// we synchronize on ourselves, in case we are executed by several threads
	// from the thread pool.
	public synchronized void run() {
      // first, add all the bytes we have to the tokenizer
      synchronized (_buffers) {
         while(_buffers.size() > 0) {
            ByteBuffer buf = _buffers.remove(0);
            this._tokenizer.addBytes(buf);
         }
      }

      // now, go over all complete messages and process them.
      while (_tokenizer.hasMessage()) {
         String msg = _tokenizer.nextMessage();
         String response = this._protocol.processMessage(msg);
         if (response != null) {
            try {
               ByteBuffer bytes = _tokenizer.getBytesForMessage(response);
               this._handler.addOutData(bytes);
            } catch (CharacterCodingException e) { e.printStackTrace(); }
         }
      }
	}

	public void addBytes(ByteBuffer b) {
      // we synchronize on _buffers and not on "this" because
      // run() is synchronized on "this", and it might take a long time
      // to run.
		synchronized (_buffers) {
			_buffers.add(b);
		}
	}


}
