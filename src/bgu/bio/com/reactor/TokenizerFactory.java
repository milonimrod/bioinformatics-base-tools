package bgu.bio.com.reactor;

public interface TokenizerFactory {
   StringMessageTokenizer create();
}
