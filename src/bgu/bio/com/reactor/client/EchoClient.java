package bgu.bio.com.reactor.client;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class EchoClient {

	private static void run(String serverName, int port)
	{
		PrintWriter out = null;
		BufferedReader networkIn = null;
		try {
			Socket theSocket = new Socket(serverName, port);
			networkIn = new BufferedReader(
					new InputStreamReader(theSocket.getInputStream()));
			BufferedReader userIn = new BufferedReader(
					new InputStreamReader(System.in));
			out = new PrintWriter(theSocket.getOutputStream());
			System.out.println("Connected to echo server");
			String theLine = "";
			while (true) {
				if (theLine.equals("{\"message\":\"TERM\"}")) {
					break;
				}
				theLine = userIn.readLine();
				out.println(theLine);
				out.flush();
				theLine = networkIn.readLine();
				System.out.println(theLine);
			}

		}  // end try
		catch (IOException e) {
			System.err.println(e);
		}
		finally {
			try {
				if (networkIn != null) networkIn.close();
				if (out != null) out.close();
			}
			catch (IOException e) {}
		}
		System.out.println("End");
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: java TcpClient host port");
			return;
		}
		run(args[0], Integer.parseInt(args[1]));
	}
}