package bgu.bio.com.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Class for wrapping the mySQL connection using the JDBC package
 */
public class MySqlConnector {

	/** The connection itself. */
	private Connection conn;

	/** The credentials for the server. */
	private String server, database, username, password;

	/** logger for printing information (can be null). */
	private final Logger logger;

	/**
	 * Instantiates a new mySQL connector.
	 * 
	 * @param logger
	 *            the logger
	 */
	public MySqlConnector(Logger logger) {
		this.logger = logger;
	}

	/**
	 * Instantiates a new mySQL connector with logger as null.
	 */
	public MySqlConnector() {
		this(null);
	}

	/**
	 * Connect to a mySQL database.
	 * 
	 * @param serverName
	 *            The name of the server to connect to
	 * @param databaseName
	 *            The name of the database to use
	 * @param user
	 *            username
	 * @param pass
	 *            password
	 * @return
	 */
	public boolean connect(String serverName, String databaseName, String user,
			String pass) {
		this.password = pass;
		this.username = user;
		this.server = serverName;
		this.database = databaseName;

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		conn = null;
		try {
			try {
				Class.forName("com.mysql.jdbc.Driver").newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			conn = DriverManager.getConnection("jdbc:mysql://" + server + "/"
					+ database + "?", username, password);
		} catch (SQLException ex) {
			if (logger != null) {
				logger.log(Level.SEVERE, "Couldn't connect to the server", ex);
			}
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Re connect to a database using the same credentials as the {
	 * {@link #connect(String, String, String, String)} method.
	 * 
	 * @return true, if successful
	 */
	public boolean reConnect() {
		try {
			if (conn != null && conn.isValid(3))
				return true;
		} catch (SQLException e) {
			if (logger != null) {
				logger.log(Level.SEVERE, "Couldn't check if server is valid", e);
			}
		}

		return this.connect(server, database, username, password);
	}

	/**
	 * Close the connection to the database.
	 * 
	 * @return true, if successful
	 */
	public boolean close() {
		try {
			conn.close();
		} catch (SQLException e) {
			if (logger != null) {
				logger.log(Level.SEVERE, "Can't close connection", e);
			}
			return false;
		}
		return true;
	}

	/**
	 * Run SELECT query on the connection. the method check if the connection is
	 * valid
	 * 
	 * @param sql
	 *            the SQL query to run
	 * @return the result set
	 */
	public synchronized ResultSet runSelect(String sql) {
		try {
			if (conn != null || !conn.isValid(3))
				this.reConnect();

			Statement stmt = conn.createStatement();
			ResultSet ans = stmt.executeQuery(sql);
			return ans;
		} catch (SQLException e) {
			if (logger != null) {
				logger.log(Level.SEVERE, "Can't run SELECT query " + sql, e);
			}
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Run DML queries (INSERT / UPDATE) on the database connection.
	 * 
	 * @param sql
	 *            the SQL query
	 * @return the number of affected rows
	 */
	public synchronized int runDML(String sql) {
		try {
			if (conn != null || !conn.isValid(3))
				this.reConnect();

			PreparedStatement stmt = conn.prepareStatement(sql);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans;
		} catch (SQLException e) {
			if (logger != null) {
				logger.log(Level.SEVERE, "Can't run DML query " + sql, e);
			}
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * Append field to a query.
	 * 
	 * @param sb
	 *            the string builder
	 * @param data
	 *            the data
	 * @param last
	 *            if not last then append ',' on the end
	 */
	public final void appendField(StringBuilder sb, String data, boolean last) {
		if (data == null) {
			sb.append(data);
		} else {
			sb.append('\'');
			sb.append(data.replace("'", "''"));
			sb.append('\'');
		}

		if (!last) {
			sb.append(',');
		}
	}

	/**
	 * Append field to a query.
	 * 
	 * @param sb
	 *            the string builder
	 * @param data
	 *            the data
	 * @param last
	 *            if not last then append ',' on the end
	 */
	public final void appendField(StringBuilder sb, int data, boolean last) {
		sb.append(data);

		if (!last) {
			sb.append(',');
		}
	}

	/**
	 * Append field to a query.
	 * 
	 * @param sb
	 *            the string builder
	 * @param data
	 *            the data
	 * @param last
	 *            if not last then append ',' on the end
	 */
	public final void appendField(StringBuilder sb, double data, boolean last) {
		// sb.append('\'');
		sb.append(data);
		// sb.append('\'');

		if (!last) {
			sb.append(',');
		}
	}

	/**
	 * Gets the database name.
	 * 
	 * @return the database name
	 */
	public String getDatabaseName() {
		return this.database;
	}
}
