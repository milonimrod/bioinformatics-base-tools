package bgu.bio.com.webservice;

import java.util.Arrays;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import bgu.bio.algorithms.alignment.EditDistance;
import bgu.bio.util.IdentityEditDistanceScoringMatrix;
import bgu.bio.util.alphabet.RnaAlphabet;

@WebService
public class GeneralScriptsWebService {

	@WebMethod
	public ServerResponse reverse(@WebParam(name = "input") String string) {
		ServerResponse response = new ServerResponse();
		if (string == null) {
			response.setError(true);
			response.setResponse("No String is given");
			return response;
		}
		String ans = rev(string);

		response.setResponse(ans);

		return response;
	}

	/**
	 * @param string
	 * @return
	 */
	private String rev(String string) {
		StringBuilder sb = new StringBuilder();
		for (int i = string.length() - 1; i >= 0; i--) {
			sb.append(string.charAt(i));
		}
		return sb.toString();
	}

	@WebMethod
	public ServerResponse complement(@WebParam(name = "input") String string,
			@WebParam(name = "reverse") boolean reverse) {
		ServerResponse response = new ServerResponse();
		if (string == null) {
			response.setError(true);
			response.setResponse("No String is given");
			return response;
		}

		if (reverse) {
			string = rev(string);
		}

		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			sb.append(alphabet.complement(string.charAt(i)));
		}

		response.setResponse(sb.toString());

		return response;
	}

	@WebMethod
	public ServerResponse editDistance(
			@WebParam(name = "input1") String string1,
			@WebParam(name = "input2") String string2) {
		ServerResponse response = new ServerResponse();
		if (string1 == null && string2 == null) {
			response.setError(true);
			response.setResponse("No Strings are given");
			return response;
		}
		string1 = string1 == null ? "" : string1;
		string2 = string2 == null ? "" : string2;
		EditDistance ed = new EditDistance(string1, string2,
				new IdentityEditDistanceScoringMatrix());

		int score = (int) ed.getAlignmentScore();
		String[] res = ed.printAlignments();
		response.setResponse(Arrays.toString(res));
		return response;
	}
}
