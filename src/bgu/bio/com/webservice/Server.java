package bgu.bio.com.webservice;

import java.util.concurrent.Executors;

import javax.xml.ws.Endpoint;

public class Server {

	public static void main(String[] args) {
		Endpoint e = Endpoint.create(new GeneralScriptsWebService());
		e.setExecutor(Executors.newFixedThreadPool(4));
		e.publish("http://0.0.0.0:9898/GeneralScriptsWebService");

		System.out.println("GeneralScriptsWebService service is ready");

	}

}