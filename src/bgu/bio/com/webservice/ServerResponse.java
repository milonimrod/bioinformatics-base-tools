package bgu.bio.com.webservice;

/**
 * 
 * A class for WebService response. this class can return if there was an error
 * in the request
 * 
 * @author milon
 * 
 */
public class ServerResponse {
	private boolean error;
	private String response;

	/**
	 * @return the error
	 */
	public final boolean isError() {
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public final void setError(boolean error) {
		this.error = error;
	}

	/**
	 * @return the response
	 */
	public final String getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the response to set
	 */
	public final void setResponse(String response) {
		this.response = response;
	}

}
