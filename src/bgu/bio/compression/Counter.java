package bgu.bio.compression;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import gnu.trove.list.linked.TIntLinkedList;

public class Counter {

	TIntLinkedList matches;
	
	TIntLinkedList indelReplace;
	
	StringBuffer alignment;
	
	public Counter(StringBuffer alignment){
		this.alignment = alignment;
		matches = new TIntLinkedList();
		indelReplace = new TIntLinkedList();
	}
	
	public TIntLinkedList getMatchesList(){
		return matches;
	}
	
	public TIntLinkedList getIndelsReplaceList(){
		return indelReplace;
	}
	
	/**
	 * Counts the matches or replacements/insertions/deletions in the alignment and adds each continuity as a part of the suitable list
	 */
	public void count(){
		
		int countM = 0;
		int countIDR = 0;
		
		for(int i=0; i<alignment.length(); i++){
			if(alignment.charAt(i) == 'M'){
				countM++;
				if((i<alignment.length()-1 && alignment.charAt(i+1)!='M') || (i==alignment.length()-1)){
					matches.add(countM);
					countM=0;
				}
			}
			else{
				countIDR++;
				if((i<alignment.length()-1 && alignment.charAt(i+1)=='M') || (i==alignment.length()-1)){
					indelReplace.add(countIDR);
					countIDR=0;
				}
			}
		}

	}
	
	
	public static void main(String[] args){

		FileReader input1=null;

		//reading s1 from a file
		StringBuffer s1 = new StringBuffer();
		
		try {
			input1 = new FileReader(args[0]); //get the argument
		} catch (FileNotFoundException e) {
			System.out.println("where is file 1?");
			e.printStackTrace();
		}
		BufferedReader buf1 = new BufferedReader(input1);
		String lineFromF1;

		try {
			lineFromF1 = buf1.readLine();
			while(lineFromF1!=null){
				s1.append(lineFromF1); //connects the string to the StringBuffer
				lineFromF1 = buf1.readLine();
			}
			buf1.close(); //close the buffered reader
			
		} catch (IOException e1) {
			System.out.println("IO problem in F1");
			e1.printStackTrace();
		}
		String s = s1.toString(); //cast the StringBuffer to string, so we could handle it in the DiagoalSequenceAlignment class
		
		Counter c = new Counter(new StringBuffer(s));
		c.count();
		TIntLinkedList m = c.getMatchesList();
		TIntLinkedList idr = c.getIndelsReplaceList();
	/*	System.out.println("matches: ");
		for(int i=0; i<m.size(); i++){
			System.out.print(m.get(i) + " ");
		}
		System.out.println("");
		System.out.println("R/I/D: ");
		for(int i=0; i<idr.size(); i++){
			System.out.print(idr.get(i) + " ");
		}
		*/
		try {
			FileWriter fw = new FileWriter("stat_res_" + args[0].substring(args[0].lastIndexOf(File.separator)+1, args[0].lastIndexOf('.')) + "_" + args[1].substring(args[1].lastIndexOf(File.separator)+1, args[1].lastIndexOf('.')) + "_.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("matches: ");
			bw.newLine();
			for(int i=0; i<m.size(); i++){
				bw.write(m.get(i) + " ");
			}
			bw.newLine();
			bw.write("R/I/D: ");
			bw.newLine();
			for(int i=0; i<idr.size(); i++){
				bw.write(idr.get(i) + " ");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
}
