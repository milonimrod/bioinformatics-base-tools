package bgu.bio.compression;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.PriorityQueue;

abstract class HuffmanTree implements Comparable<HuffmanTree> {
	public final int frequency; // the frequency of this tree

	public HuffmanTree(int freq) {
		frequency = freq;
	}

	// compares on the frequency
	@Override
	public int compareTo(HuffmanTree tree) {
		return frequency - tree.frequency;
	}
}

class HuffmanLeaf extends HuffmanTree {
	public final char value; // the character this leaf represents

	public HuffmanLeaf(int freq, char val) {
		super(freq);
		value = val;
	}
}

class HuffmanNode extends HuffmanTree {
	public final HuffmanTree left, right; // subtrees

	public HuffmanNode(HuffmanTree l, HuffmanTree r) {
		super(l.frequency + r.frequency);
		left = l;
		right = r;
	}
}

public class HuffmanCode {

	private HuffmanTree tree;
	private char current;

	// input is an array of frequencies, indexed by character code
	public void buildTree(HashMap<Character, Integer> freq) {
		PriorityQueue<HuffmanTree> trees = new PriorityQueue<HuffmanTree>();
		// initially, we have a forest of leaves
		// one for each non-empty character
		for (Entry<Character, Integer> entry : freq.entrySet()) {
			trees.offer(new HuffmanLeaf(entry.getValue().intValue(), entry
					.getKey().charValue()));
		}

		assert trees.size() > 0;
		// loop until there is only one tree left
		while (trees.size() > 1) {
			// two trees with least frequency
			HuffmanTree a = trees.poll();
			HuffmanTree b = trees.poll();

			// put into new node and re-insert into queue
			trees.offer(new HuffmanNode(a, b));
		}
		this.tree = trees.poll();
	}

	public int readNext(BitSet set, int start) {
		HuffmanTree curTree = tree;
		int pos = start;
		while (!(curTree instanceof HuffmanLeaf)) {
			final HuffmanNode node = (HuffmanNode) curTree;
			if (set.get(pos)) {
				// 1
				curTree = node.right;
			} else {
				// 0
				curTree = node.left;
			}
			pos++;
		}

		current = ((HuffmanLeaf) curTree).value;
		return pos;
	}

	public char getChar() {
		return current;
	}

	public HashMap<Character, int[]> toMap() {
		HashMap<Character, int[]> map = new HashMap<Character, int[]>();
		StringBuffer prefix = new StringBuffer();
		toMap(tree, prefix, map);
		return map;
	}

	private void toMap(HuffmanTree rootTree, StringBuffer prefix,
			HashMap<Character, int[]> map) {
		assert rootTree != null;
		if (rootTree instanceof HuffmanLeaf) {
			HuffmanLeaf leaf = (HuffmanLeaf) rootTree;

			// print out character, frequency, and code for this leaf (which is
			// just the prefix)
			int[] code = new int[prefix.length()];
			for (int i = 0; i < prefix.length(); i++) {
				code[i] = prefix.charAt(i) == '1' ? 1 : 0;
			}
			map.put(leaf.value, code);

		} else if (rootTree instanceof HuffmanNode) {
			HuffmanNode node = (HuffmanNode) rootTree;

			// traverse left
			prefix.append('0');
			toMap(node.left, prefix, map);
			prefix.deleteCharAt(prefix.length() - 1);

			// traverse right
			prefix.append('1');
			toMap(node.right, prefix, map);
			prefix.deleteCharAt(prefix.length() - 1);
		}
	}

	public void print() {
		print(tree, new StringBuffer());
	}

	private void print(HuffmanTree rootTree, StringBuffer prefix) {
		assert rootTree != null;
		if (rootTree instanceof HuffmanLeaf) {
			HuffmanLeaf leaf = (HuffmanLeaf) rootTree;

			// print out character, frequency, and code for this leaf (which is
			// just the prefix)
			System.out.println(leaf.value + " " + prefix);

		} else if (rootTree instanceof HuffmanNode) {
			HuffmanNode node = (HuffmanNode) rootTree;

			// traverse left
			prefix.append('0');
			print(node.left, prefix);
			prefix.deleteCharAt(prefix.length() - 1);

			// traverse right
			prefix.append('1');
			print(node.right, prefix);
			prefix.deleteCharAt(prefix.length() - 1);
		}
	}

	public static void main(String[] args) {
		String test = "AAAAUUUCCCGGGN";

		// we will assume that all our characters will have
		// code less than 256, for simplicity
		int[] charFreqs = new int[256];
		// read each character and record the frequencies
		for (char c : test.toCharArray()) {
			charFreqs[c]++;
		}
		HashMap<Character, Integer> freq = new HashMap<Character, Integer>();
		for (int i = 0; i < charFreqs.length; i++) {
			if (charFreqs[i] > 0) {
				freq.put((char) i, charFreqs[i]);
			}
		}

		// build tree
		HuffmanCode code = new HuffmanCode();
		code.buildTree(freq);

		HashMap<Character, int[]> ans = code.toMap();
		System.out.println(ans);

		BitSet set = new BitSet();
		set.set(0, false);
		set.set(1, false);
		code.readNext(set, 0);
	}
}