package bgu.bio.compression;

import gnu.trove.list.linked.TIntLinkedList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bgu.bio.algorithms.alignment.DiagonalSequenceAlignment;
import bgu.bio.algorithms.alignment.DiagonalSequenceAlignmentNoMatrix;
import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

/**
 * Represents the main procedure.
 * Operates under the assumption that the used scoring matrix is symetrical
 */
public class Main {

	private String s1, s2;
	
	private SequenceMatchUp smu;
	
	private List<StringRange> srList;
	private List<StringRange> backUp;
	
//	private DiagonalSequenceAlignmentNoMatrix topAlignment, bottomAlignment;
	
	private AlphabetUtils alphabet;
	
	private ScoringMatrix matrix;
	
	/*private List<StringRange>*/ 
//	private StringRange[] bestAlignments;
	
	private int workers, cells; //how many workers will we have, and how many cells will they fill on each job
	
//	private final static int BEST = 10;
	
	private StringBuffer alignment;
	
	private final long POSSIBLE_CELLS = 5000000000L;///*7000000;*/12000000000L; //12*10^9 since that is the maximal amount of cells when using 180 GB
	
	private double score;
	
	private int k;
	
	/**
	 * Constructor
	 * @param s1
	 * @param s2
	 */
	public Main(String s1, String s2, int workers, int cells, int k){
		//match up the strings (find long matches = diagonals)
		this.k = k;
		if(s1.length() <= s2.length()){
			this.s1 = s1;
			this.s2 = s2;
		}
		else{
			this.s1 = s2;
			this.s2 = s1;
		}
		this.smu = new SequenceMatchUp(this.s1, this.s2, k);
		smu.matchKTuples();
	//	smu.filterIndices();
		smu.sortIndicesList();
		srList = smu.getIndices();
		backUp = smu.getIndices();
		
//		bestAlignments = new StringRange[BEST];
		
		alphabet = RnaAlphabet.getInstance();
		matrix = new IdentityScoringMatrix(RnaAlphabet.getInstance());
		this.workers = workers;
		this.cells = cells;
		score = 0;
		this.alignment = this.mainProcedure(); //start the main procedure
	}
	
	
//	private FileWriter fw;
//	private BufferedWriter bw;
	
	public StringBuffer mainProcedure(){
		/*try {
			fw = new FileWriter("list.txt");
			bw = new BufferedWriter(fw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		StringBuffer ans = mainRec(0, s1.length()-1, 0, s2.length()-1, k, srList);
		//StringBuffer ans = mainRec(15412, 15917, 15411, 16187);
		/*try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return ans;
	}
	
	
	
	/**
	 * 
	 * @param b1 the beginning index of str1
	 * @param e1 the ending index of str1
	 * @param b2 the beginning index of str2
	 * @param e2 the ending index of str2
	 * @return the alignment of str1 against str2
	 */
//	int level = 0;
//	int scoreSoFar = 0;
	
	public StringBuffer mainRec(int b1, int e1, int b2, int e2, int num, List<StringRange> listOfSR){
		
		srList = backUp;
		
		if((long)(e1-b1+1)*(e2-b2+1) <= POSSIBLE_CELLS){
			//System.out.println("rectangle1");
		//	System.out.println("sending DSA. window: s1: " + b1 + "-" + e1 + " s2: " + b2 + "-" + e2);
			
			DiagonalSequenceAlignment alignment = new DiagonalSequenceAlignment(s1, b1, e1, s2, b2, e2, alphabet, matrix, workers, cells);
			score = score + alignment.getAlignmentScore();
		//	System.out.println("rectangle2");
			/*
			try {
				bw.write("Main(DSA): this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + alignment.getAlignmentScore());
				bw.newLine();
				if((e1-b1+1)>0 && (e2-b2+1)>0){
						DiagonalSequenceAlignmentNoMatrix dsanm = new DiagonalSequenceAlignmentNoMatrix(s1, b1, e1, s2, b2, e2, alphabet, matrix, workers, cells);
						dsanm.buildMatrix();
						if(dsanm.getMaxScore()!=alignment.getAlignmentScore()){
							bw.write("WARNING! the difference is: " + (dsanm.getMaxScore()-alignment.getAlignmentScore()));
							bw.newLine();
						}
						bw.write("only DSANM: this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + dsanm.getMaxScore());
						bw.newLine();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			return alignment.getAlignmentString();
		}
		
		//find a string range (SR) which one of its edges is bounded in the window defined by the strings' borders
		List<StringRange> list = listOfSR;
		
		boolean foundBasicMatch = false;
		
		int i1=0, i2=0, seqLength=0, listLength=0;
		boolean upperEdge, lowerEdge;
		StringRange sr = null;
//		List<StringRange> possible=null;// = new ArrayList<StringRange>();
		int divide = num;
		
		//boolean debug = false;
		
		int upEnd1=0, upEnd2=0, upBegin1=b1, upBegin2=b2, downEnd1=e1, downEnd2=e2, downBegin1=0, downBegin2=0; 
		
		while(!foundBasicMatch){
			i1 = 0;
			i2 = 0;
			upEnd1=0;
			upEnd2=0;
			upBegin1=b1;
			upBegin2=b2;
			downEnd1=e1;
			downEnd2=e2;
			downBegin1=0;
			downBegin2=0;
			seqLength=0;
			
		//	System.out.println("looking up matches of length " + divide);
	//		possible = new ArrayList<StringRange>();
			upperEdge = false;
			lowerEdge = false;
		//	sr.setBottomEdgeIn(false);
		//	sr.setUpEdgeIn(false);
			listLength=list.size();
			
			for(int i=0; i<listLength; i++){
				upperEdge = false;
				lowerEdge = false;
				sr = list.get(i);
				seqLength = sr.getLength();

				i1 = sr.getI1();
				i2 = sr.getI2();
				if (((i1>=b1 && i1<=e1) && (i2>=b2 && i2<=e2))){ //upper edge included in the window
					upperEdge = true;
				//	sr.setUpEdgeIn(true);
				}
				if(((i1+seqLength-1)>=b1 && (i1+seqLength-1)<=e1) && ((i2+seqLength-1)>=b2 && (i2+seqLength-1)<=e2)){ //lower edge included in the window
					lowerEdge = true;
				//	sr.setBottomEdgeIn(true);
				}
				/*only one of the edges is included within the window - possible longer match than a sequence match which 
				 * is totally included in the window */
	//			if((upperEdge && !lowerEdge) || (!upperEdge && lowerEdge)){  
		//			possible.add(sr);
					//seqLength = 0;
			//	}
				if(upperEdge && lowerEdge) {
					//definitions for the new windows
					//upEnd1 = (i1-1)>=0 ? i1-1 : -1;
					if(i1-1>=0){
						upEnd1 = i1-1;
						/*
						if(i1==b1){
							upBegin1 = b1+1;
							upEnd1 = b1;
						}
						else
							upEnd1 = i1-1;
							*/
					}
					else
						upEnd1 = -1;
					
				//	upEnd2 = (i2-1)>=0 ? i2-1 : -1; 
					if(i2-1>=0){
						upEnd2 = i2-1;
						/*
						if(i2==b2){ //str2 is empty
							upBegin2 = b2+1;
							upEnd2 = b2;
						}
						else
							upEnd2 = i2-1;
							*/
					}
					else
						upEnd2 = -1;
					
					//downBegin1= (i1+seqLength) < s1.length() ? i1+seqLength : i1+seqLength+1;
					if((i1+seqLength) < s1.length()){
						downBegin1 = i1+seqLength;
						/*if(i1+seqLength==e1){
							downBegin1 = e1+1;
							downEnd1 = e1;
						}
						else
							downBegin1 = i1+seqLength;
							*/
					}
					else
						downBegin1 = i1+seqLength+1;
					
					//downBegin2= (i2+seqLength) < s2.length() ? i2+seqLength : i2+seqLength+1; 
					if((i2+seqLength) < s2.length()){
						downBegin2 = i2+seqLength;
						/*if(i2+seqLength==e2){
							downBegin2 = e2+1;
							downEnd2 = e2;
						}
						else
							downBegin2 = i2+seqLength;
						*/
					}
					else
						downBegin2 = i2+seqLength+1;
					
					break;
				}
			}
			
			
			if(!(upperEdge && lowerEdge)){ //in case the first for loop ended without finding any string range within the window
				
				//System.out.println("problem! no string range within the window boundries. current k is " + divide);
				//System.out.println("window size: " + (e1-b1+1) + "X" + (e2-b2+1) + " insices are s1:" + b1 + "-" + e1 + " s2:" + b2 + "-" + e2);
			//	debug = true;
				
				divide = divide-1;
				if(divide==0){ //no matches in this window, the alignment should be composed of insertions and deletions only
					StringBuffer str = new StringBuffer();
					String i = "", d = "";
					if(e2-b2+1>0){
						char insertions[] = new char[e2-b2+1]; //#insertions = length of s2
						for(int m=0; m<insertions.length; m++)
							insertions[m] = 'I';
						i = new String(insertions);
					}
					if(e1-b1+1>0){
						char deletions[] = new char[e1-b1+1]; //#deletions = length of s1
						for(int m=0; m<deletions.length; m++)
							deletions[m] = 'D';
						d = new String(deletions);
					}
					str.append(new StringBuffer(i)).append(new StringBuffer(d)); /* score is 0 and thus not added.
					basically should be S(D)*deletions.length + S(I)*isertions.length */
					//double add  = 0;
					/*try {
						bw.write("Main: this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + "0 (fake)");
						bw.newLine();
						
						
						if((e1-b1+1)>0 && (e2-b2+1)>0){
								DiagonalSequenceAlignmentNoMatrix dsanm = new DiagonalSequenceAlignmentNoMatrix(s1, b1, e1, s2, b2, e2, alphabet, matrix, workers, cells);
								dsanm.buildMatrix();
								bw.write("divide = 0: this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + dsanm.getMaxScore());
								bw.newLine();
								add = dsanm.getMaxScore();
								DiagonalSequenceAlignment dsa = new DiagonalSequenceAlignment(s1, b1, e1, s2, b2, e2, alphabet, matrix, workers, cells);
								bw.write("divide = 0 DSA: this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + dsa.getAlignmentScore());
								bw.newLine();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					
				//	score += add;
					
					return str;
				}
				
				SequenceMatchUp seqMatch = new SequenceMatchUp(s1, b1, e1, s2, b2, e2, divide); 
				seqMatch.matchKTuples();
			//	seqMatch.filterIndices();
				seqMatch.sortIndicesList();
				list = seqMatch.getIndices();
			}
			else{
				foundBasicMatch = true;
				//srList = backUp;
			}	
			
			
		}
		
		char[] matches = new char[seqLength];
		for(int i=0; i<seqLength; i++)
			matches[i] = 'M';
		StringBuffer ans = new StringBuffer(new String(matches));
		
		score = score + seqLength;

//		double scoreBeforeLeft = score;
		//divide = 20;
	//	level++;
		StringBuffer sbLeft = mainRec(upBegin1, upEnd1, upBegin2, upEnd2, divide, list);
	//	level--;
		
	//	double thisWindowScore = score - scoreBeforeLeft;
		
	//	double scoreBeforeRight = score;
	//	level++;
		StringBuffer sbRight = mainRec(downBegin1, downEnd1, downBegin2, downEnd2, divide, list);
	//	level--;

	//	thisWindowScore += (score - scoreBeforeRight);
	//	thisWindowScore += seqLength;
		
		/*
		try {
			bw.write("Main: this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + thisWindowScore);
			bw.newLine();
		
			if((e1-b1+1)>0 && (e2-b2+1)>0){
					DiagonalSequenceAlignmentNoMatrix dsanm = new DiagonalSequenceAlignmentNoMatrix(s1, b1, e1, s2, b2, e2, alphabet, matrix, workers, cells);
					dsanm.buildMatrix();
					bw.write("DSANM: this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + dsanm.getMaxScore());
					bw.newLine();
					if ((long)(e1-b1+1)*(e2-b2+1) <= 1000000) {
						DiagonalSequenceAlignment dsa = new DiagonalSequenceAlignment(s1, b1, e1, s2, b2, e2, alphabet, matrix, workers, cells);
						bw.write("DSA: this window- S1: " + b1 + "-" + e1 + " S2: " +  b2 + "-" + e2 + " Score is: " + dsa.getAlignmentScore());
						bw.newLine();
					}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return sbLeft.append(ans).append(sbRight);
		
		
		//return mainRec(upBegin1, upEnd1, upBegin2, upEnd2).append(ans).append(mainRec(downBegin1, downEnd1, downBegin2, downEnd2));

	}	
		
	
	
	
	
		/*
		//insert the first elements to the "best" array 
		double scoreMinBest = sortFirst();
		
		System.out.println("finished first BEST");
		
		/* inserting and sorting the BEST array such that the bestAlignments will include the BEST string ranges representing
		 * the middle part of the alignment created by the top rectangle, the long match (string range) anf the bottom rectangle */
		/*
		boolean runTopFirst = false;
		boolean topIsZero = false, bottomIsZero = false;
		StringRange curr;
		int i, index1, index2, top, bottom;
		double score, secondScore;
		for(i=BEST; i<srList.size(); i++){
			curr = srList.get(i);
			index1 = curr.getI1();
			index2 = curr.getI2();
			//compute top and bottom rectangle area, and check which area is smaller (will run DSANM on it first)
			top = s1.substring(0, index1).length() * s2.substring(0, index2).length();
			bottom = s1.substring(index1+curr.getLength(), s1.length()).length() * s2.substring(index2+curr.getLength(), s2.length()).length();
			if(top < bottom)
				runTopFirst = true;
			
			if(runTopFirst && top!=0){
				topAlignment = new DiagonalSequenceAlignmentNoMatrix(s1.substring(0, index1), 
						s2.substring(0, index2), alphabet, matrix, workers, cells);
				topAlignment.buildMatrix();
				score = topAlignment.getMaxScore() + curr.getLength();
				//compute best case scenario (the bottom rectangle will be only matches), and see if it's worth the computation
				secondScore = Math.sqrt((s1.substring(index1+curr.getLength(), s1.length()).length()*s1.substring(index1+curr.getLength(), s1.length()).length())
						+ (s2.substring(index2+curr.getLength(), s2.length()).length()*s2.substring(index2+curr.getLength(), s2.length()).length()));
				if(((secondScore + score) > scoreMinBest) && bottom!=0){ //worth a shot - send to alignment and find out
					bottomAlignment = new DiagonalSequenceAlignmentNoMatrix(s1.substring(index1+curr.getLength(), s1.length()), 
							s2.substring(index2+curr.getLength(), s2.length()), alphabet, matrix, workers, cells);
					score += bottomAlignment.getMaxScore();
					curr.setScore(score);
					if(score > scoreMinBest){
						//find a place in the BEST array
						insert(curr);
					}
				}
				runTopFirst = false;
			}
			else if(!runTopFirst && bottom!=0){ //mirror case
				bottomAlignment = new DiagonalSequenceAlignmentNoMatrix(s1.substring(index1+curr.getLength(), s1.length()), 
						s2.substring(index2+curr.getLength(), s2.length()), alphabet, matrix, workers, cells);
				bottomAlignment.buildMatrix();
				score = bottomAlignment.getMaxScore() + curr.getLength();
				//compute best case scenario (the top rectangle will be only matches), and see if it's worth the computation
				secondScore = Math.sqrt((s1.substring(0, index1).length()*s1.substring(0, index1).length()) + (s2.substring(0, index2).length()*s2.substring(0, index2).length()));
				if(((secondScore + score) > scoreMinBest) && top!=0){ //worth a shot - send to alignment and find out
					topAlignment = new DiagonalSequenceAlignmentNoMatrix(s1.substring(0, index1), 
							s2.substring(0, index2), alphabet, matrix, workers, cells);
					score += topAlignment.getMaxScore();
					curr.setScore(score);
					if(score > scoreMinBest){
						//find a place in the BEST array
						insert(curr);
					}
				}
			}
			else{
				score = curr.getLength();
				if(score > scoreMinBest){
					//find a place in the BEST array
					insert(curr);
				}
			}
		}
		*/
		/*create full alignments (with matrices) for the best scored alignments without matrices, compare the full alignments to 
		 * the ones created by the string ranges (with the bottom and top rectangles) and take the best scored alignment as the 
		 * alignment of the two sequences.*/
		/* 
		StringBuilder fullAlignment = new StringBuilder();
		boolean allEquale = true;
		int who;
		int length; 
		double scoreAlignment;
		String s1Top, s2Top, s1Bottom, s2Bottom;
		DiagonalSequenceAlignment topAlignment, bottomAlignment;
		for(i=0; i<BEST & allEquale; i++){
			index1 = bestAlignments[i].getI1();
			index2 = bestAlignments[i].getI2();
			length = bestAlignments[i].getLength();
			s1Top = s1.substring(0, index1);
			s2Top = s2.substring(0, index2);
			s1Bottom = s1.substring(index1+length, s1.length());
			s2Bottom = s2.substring(index2+length, s2.length());
			topAlignment = new DiagonalSequenceAlignment(s1Top, s2Top, alphabet, matrix, workers);
			bottomAlignment = new DiagonalSequenceAlignment(s1Bottom, s2Bottom, alphabet, matrix, workers);
			scoreAlignment = topAlignment.getAlignmentScore() + length + bottomAlignment.getAlignmentScore();
			if(scoreAlignment != bestAlignments[i].getScore()){
				allEquale = false;
				who = i;
				System.out.println(who + " wasn't equal to the non matrix version");
				break; //??
			}
			if(i==BEST-1){ //the best scored alignment - get it!
				if(s1Top.length() > s2Top.length())
					topAlignment.invertAlignment();
				if(s1Bottom.length() > s2Bottom.length())
					bottomAlignment.invertAlignment();
				char[] matches = new char[length];
				for(int t=0; t<length; t++)
					matches[t] = 'M';
				fullAlignment.append(topAlignment.getAlignment());
				fullAlignment.append(matches);
				fullAlignment.append(bottomAlignment.getAlignment());
			}
		}
		return fullAlignment.toString();
	}
		*/
	
	
	public StringBuffer getAlignment(){
		return this.alignment;
	}
	
	
	
	/**
	 * Insert the given String Range to the BEST array, in it's correct position
	 * @param sr the given String Range
	 
	private void insert(StringRange sr) {
		
		double tempScore, srScore = sr.getScore();
		int i=0, index=0;
		boolean found = false;
		
		while(i!=BEST && !found){ //find the index to which the given sr should enter
			tempScore = bestAlignments[i].getScore();
			if(srScore > tempScore)
				i++;
			else{
				index = i-1;
				found = true;
			}
		}
		
		//arrange all of the previous string ranges which are smaller than sr
		i=0;
		while(i!=index){
			bestAlignments[i] = bestAlignments[i+1];
			i++;
		}
		bestAlignments[index] = sr; //insert sr in its correct location
	}*/


	/**
	 * Inserts the first BEST String Ranges to the BEST array, in a sorted order according to their scores
	 * @return the smallest score among the String Ranges 
	
	public double sortFirst(){
		
		//insert the first ten string ranges
		int i, j;
		for(i=0; i<BEST; i++){
			System.out.println("in sortFirst, doing iteration " + i);
			StringRange sr = srList.get(i);
			topAlignment = new DiagonalSequenceAlignmentNoMatrix(s1.substring(0, sr.getI1()), 
					s2.substring(0, sr.getI2()), alphabet, matrix, workers, cells);
			if(s1.substring(0, sr.getI1())==null){
				System.out.println("s1 top is null");
			}
			if(s2.substring(0, sr.getI2())==null){
				System.out.println("s2 top is null");
			}
			bottomAlignment = new DiagonalSequenceAlignmentNoMatrix(s1.substring(sr.getI1()+sr.getLength(), s1.length()), 
					s2.substring(sr.getI2()+sr.getLength(), s2.length()), alphabet, matrix, workers, cells);
			if(s1.substring(sr.getI1()+sr.getLength(), s1.length())==null){
				System.out.println("s1 bottom is null");
			}
			if(s2.substring(sr.getI2()+sr.getLength(), s2.length())==null){
				System.out.println("s2 bottom is null");
			}
			
			//align
			if(s1.substring(0, sr.getI1())!=null && s1.substring(0, sr.getI1()).length()>0 && s2.substring(0, sr.getI2())!=null && s2.substring(0, sr.getI2()).length()>0){
				System.out.println("size of top square: |s1|= " +  s1.substring(0, sr.getI1()).length() + ", |s2|= " + s2.substring(0, sr.getI2()).length());
				topAlignment.buildMatrix();
				System.out.println("finished the top alignment of sr " + i + " score so far = " + topAlignment.getMaxScore());
			}
			if(s1.substring(sr.getI1()+sr.getLength(), s1.length())!=null && s1.substring(sr.getI1()+sr.getLength(), s1.length()).length()>0 && s2.substring(sr.getI2()+sr.getLength(), s2.length())!=null && s2.substring(sr.getI2()+sr.getLength(), s2.length()).length()>0){
				System.out.println("size of bottom square: |s1|= " + s1.substring(sr.getI1()+sr.getLength(), s1.length()).length() + ", |s2|= " + s2.substring(sr.getI2()+sr.getLength(), s2.length()).length());
				bottomAlignment.buildMatrix();
				System.out.println("finished the bottom alignment of sr " + i + " score so far = " + (bottomAlignment.getMaxScore()+topAlignment.getMaxScore()));
			}
			//the score of the alignment:
			sr.setScore(topAlignment.getMaxScore() + bottomAlignment.getMaxScore() + sr.getLength());		
			bestAlignments[i] = sr;
		}
		
		//sort the first ten elements (bubble sort)
		StringRange temp;
		for(i=0; i<BEST; i++){
			for(j=0; j<BEST; j++){
				if(bestAlignments[j].getScore() > bestAlignments[j+1].getScore()){
					temp = bestAlignments[j];
					bestAlignments[j] = bestAlignments[j+1];
					bestAlignments[j+1] = temp;
				}
			}
		}
		
		
		for(i=0; i<BEST; i++){
			System.out.println("value of cell " + i + " is: " + bestAlignments[i].getScore());
		}
		
		//return the minimal score
		return bestAlignments[0].getScore();
	} */
	

	
	public double getScore(){
		return this.score;
	}
	
	
	/**************
	 * Main
	 **************/
	
	public static void main(String args[]){
		

		FileReader input1=null, input2=null;

		//reading s1 from a file
		StringBuffer s1 = new StringBuffer();
		
		try {
			input1 = new FileReader(args[0]); //get the argument
		} catch (FileNotFoundException e) {
			System.out.println("where is file 1?");
			e.printStackTrace();
		}
		BufferedReader buf1 = new BufferedReader(input1);
		String lineFromF1;

		try {
			buf1.readLine(); //reads the first line of the FASTA file - unnecessary for us (just a heading)
			lineFromF1 = buf1.readLine();
			while(lineFromF1!=null){
				s1.append(lineFromF1); //connects the string to the StringBuffer
				lineFromF1 = buf1.readLine();
			}
			buf1.close(); //close the buffered reader
			
		} catch (IOException e1) {
			System.out.println("IO problem in F1");
			e1.printStackTrace();
		}
		String str1 = s1.toString(); //cast the StringBuffer to string, so we could handle it in the DiagoalSequenceAlignment class
		
		//same process for s2...
		StringBuffer s2 = new StringBuffer();
		try {
			input2 = new FileReader(args[1]);
		} catch (FileNotFoundException e) {
			System.out.println("where is file 2?");
			e.printStackTrace();
		}

		BufferedReader buf2 = new BufferedReader(input2);
		String lineFromF2;

		try {
			buf2.readLine(); //reads the first line of the FASTA file - unnecessary for us (just a heading)
			lineFromF2 = buf2.readLine();
			while(lineFromF2!=null){
				s2.append(lineFromF2); //connects the string to the StringBuffer
				lineFromF2 = buf2.readLine();
			}
			buf2.close(); //close the buffered reader
			
		} catch (IOException e1) {
			System.out.println("IO problem in F2");
			e1.printStackTrace();
		}
		
		
		String str2 = s2.toString(); //cast the StringBuffer to string, so we could handle it in the DiagoalSequenceAlignment class
		
		int n = Integer.parseInt(args[2]);
		
		int jobSize = Integer.parseInt(args[3]);

		int k = Integer.parseInt(args[4]);
		
		
		System.out.println(new Date());
	
	
		Main m = new Main(str1, str2, n, jobSize, k);
		
		
		//m.mainProcedure();
		StringBuffer align = m.getAlignment();
		
//		String alignmentString = align.toString();
		Counter counter = new Counter(align); 
		
		counter.count();
		TIntLinkedList mat = counter.getMatchesList();
		TIntLinkedList idr = counter.getIndelsReplaceList();
	/*	System.out.println("matches: ");
		for(int i=0; i<m.size(); i++){
			System.out.print(m.get(i) + " ");
		}
		System.out.println("");
		System.out.println("R/I/D: ");
		for(int i=0; i<idr.size(); i++){
			System.out.print(idr.get(i) + " ");
		}
		*/
		try {
			FileWriter fw = new FileWriter("stat_res_" + args[0].substring(args[0].lastIndexOf(File.separator)+1, args[0].lastIndexOf('.')) + "_" + args[1].substring(args[1].lastIndexOf(File.separator)+1, args[1].lastIndexOf('.')) + "_.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("matches: ");
			bw.newLine();
			for(int i=0; i<mat.size(); i++){
				bw.write(mat.get(i) + " ");
			}
			bw.newLine();
			bw.write("R/I/D: ");
			bw.newLine();
			for(int i=0; i<idr.size(); i++){
				bw.write(idr.get(i) + " ");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//int l = align.length();
	//	int count = 0;
		/*for(int i=0; i<l; i++){
			if(align.charAt(i) == 'M')
				count++;
		}*/
/*
		try {
			FileWriter fw = new FileWriter("main_output.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(align.toString());
			bw.newLine();
			bw.write("alignment length: " + l);
			bw.newLine();
			bw.write("score: " + m.getScore());
			bw.newLine();
			bw.write("str1 length: " + str1.length() + " str2 length: " + str2.length());
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	*/	
	
		/**
		System.out.println(align);
		System.out.println("alignment length (n+m at most): " + l);
		System.out.println("score is: " + m.getScore());
		
		System.out.println("str1 length: " + str1.length() + " str2 length: " + str2.length());
*/
		//(15412, 15917, 15411, 16187)
/*		System.out.println("S1: " + str2.substring(15466, 15477));
		System.out.println("S2: " + str1.substring(16161, 16172));
		System.out.println("S1-sub: " + str2.substring(15412, 15918));
		System.out.println("S2-sub: " + str1.substring(15411, 16188));
	*/	//System.out.println("s1 ");
		
		
	//	System.out.println("count of matches is: " + count);
		System.out.println(new Date());
	}
	
	
	
}
