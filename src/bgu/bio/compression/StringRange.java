package bgu.bio.compression;

import java.util.Comparator;

/**
 * This class represents the beginning indices and the length of the k-tuple match (found between two strings)
 * @author Nitzan
 */
public class StringRange {

	/**
	 * Beginning indices - i1 from s1 and i2 from s2 (will be represented as [i1, i2] in a table)
	 */
	private int i1, i2;
	
	/**
	 * The length of the k-tuple
	 */
	private int length;
	
	/**
	 * The score of the best alignment which is created from the this diagonal + alignments on its bottom and top "leftovers"
	 */
	private double alignmentScore;
	
	private boolean bottomEdgeIn = false, upEdgeIn = false;
	
	/**
	 * Constructor
	 * @param i1 beginning index on s1
	 * @param i2 beginning index on s2
	 * @param length length of k-tuple starting in these indices
	 */
	public StringRange(int i1, int i2, int length){
		this.i1 = i1;
		this.i2= i2;
		this.length = length;
		this.alignmentScore = 0;
	}
	
	
	/*********************
	 * Getters and Setters:
	 *********************/
	public int getI1(){
		return i1;
	}
	
	public int getI2(){
		return i2;
	}
	
	public int getLength(){
		return length;
	}
	
	public double getScore(){
		return alignmentScore;
	}
	
	
	public boolean isBottomEdgeIn() {
		return bottomEdgeIn;
	}


	public boolean isUpEdgeIn() {
		return upEdgeIn;
	}

	
	public void setBottomEdgeIn(boolean bottomEdgeIn) {
		this.bottomEdgeIn = bottomEdgeIn;
	}
	
	
	public void setUpEdgeIn(boolean upEdgeIn) {
		this.upEdgeIn = upEdgeIn;
	}


	public void setI1(int i){
		this.i1 = i;
	}
	
	public void setI2(int i){
		this.i2 = i;
	}
	
	public void setLength(int k){
		this.length = k;
	}
	
	public void setScore(double score){
		this.alignmentScore = score;
	}
	
	/**
	 * Comparator used for comparing two string ranges according to the length of their match.
	 */
	public static class ComparingSR implements Comparator<StringRange>{
		
		@Override
		public int compare(StringRange sr1, StringRange sr2) {
			//if(s1 instanceof StringRange && s2 instanceof StringRange){
				//StringRange sr1 = (StringRange)s1;
				//StringRange sr2 = (StringRange)s2;
				int length1 = sr1.getLength();
				int length2 = sr2.getLength();
				if (length1 < length2)
					return 1;
				else if (length1 > length2)
					return -1;
				else{
					int delta1 = Math.abs(sr1.getI1() - sr1.getI2());
					int delta2 = Math.abs(sr2.getI1() - sr2.getI2());
					if (delta1 < delta2)
						return -1;
					else if(delta1 > delta2)
						return 1;
					else{
							
						if(sr1.getI2() > sr2.getI2())
							return 1;
						else if (sr1.getI2() < sr2.getI2())
							return -1;
						else
							return 0;						
					}
				}
		//	}
		//	return 0;
		}
		
	}
	
}
