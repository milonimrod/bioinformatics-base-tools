package bgu.bio.ds.rna;

import gnu.trove.list.array.TCharArrayList;

public class InnerStructure {
	private TCharArrayList sequenceLeft;
	private TCharArrayList sequenceRight;

	public InnerStructure() {
		// create the lists
		this.sequenceLeft = new TCharArrayList();
		this.sequenceRight = new TCharArrayList();
	}

	public TCharArrayList getSequenceLeft() {
		return sequenceLeft;
	}

	public TCharArrayList getSequenceRight() {
		return sequenceRight;
	}

	public void reset() {
		this.sequenceLeft.resetQuick();
		this.sequenceRight.resetQuick();
	}

	public void appeandLeft(char c) {
		this.sequenceLeft.add(c);
	}

	public void appeandRight(char c) {
		this.sequenceRight.add(c);
	}

	@Override
	public String toString() {
		return this.sequenceLeft + ":" + this.sequenceRight;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InnerStructure other = (InnerStructure) obj;
		if (sequenceLeft == null) {
			if (other.sequenceLeft != null)
				return false;
		} else if (!sequenceLeft.equals(other.sequenceLeft))
			return false;
		if (sequenceRight == null) {
			if (other.sequenceRight != null)
				return false;
		} else if (!sequenceRight.equals(other.sequenceRight))
			return false;
		return true;
	}

}