package bgu.bio.ds.rna;

import gnu.trove.list.array.TCharArrayList;
import gnu.trove.list.array.TIntArrayList;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.ds.rna.StemStructure.StemStructureShuffleData;

public class TestStemStructure {

	@Test
	public void testDangling1() {
		StemStructure s1 = new StemStructure("UAGUGCAGGAUUACCCGGCUUACUAA",
				".(((((.((.....)))...))))..");

		Assert.assertEquals("wrong loop", "AUUAC",
				new String(trim(s1.getStemLoop())));
		Assert.assertEquals("wrong dangling left", "U",
				new String(trim(s1.getDanglingLeft())));
		Assert.assertEquals("wrong dangling right", "AA",
				new String(trim(s1.getDanglingRight())));
	}

	@Test
	public void testDangling2() {
		StemStructure s1 = new StemStructure("AGUGCAGGAUUACCCGGCUUACU",
				"(((((.((.....)))...))))");

		Assert.assertEquals("wrong dangling left", "",
				new String(trim(s1.getDanglingLeft())));
		Assert.assertEquals("wrong dangling right", "",
				new String(trim(s1.getDanglingRight())));

		s1.allowShuffle();
		System.out.println(s1.shuffleInformation.stackingLengths);
	}

	@Test
	public void testShuffleinformation() {
		StemStructure s1 = new StemStructure("AGUGCAGGAUUACCCGGCUUACU",
				"(((((.((.....)))...))))");

		s1.allowShuffle();

		Assert.assertEquals("Wrong stacking counters",
				s1.shuffleInformation.stackingLengths, new TIntArrayList(
						new int[] { 2, 1, 4 }));

	}

	@Test
	public void testToVienna1() {
		String sequence = "UAGUGCAGGAUUACCCGGCUUACUAA";
		String structure = ".(((((.((.....)))...))))..";
		toViennaHelper(sequence, structure);
	}

	@Test
	public void testToVienna2() {
		String sequence = "AAAAAAAAAGGGGGUUUUUUUUU";
		String structure = "(((((((((.....)))))))))";
		toViennaHelper(sequence, structure);
	}

	private void toViennaHelper(String sequence, String structure) {
		StemStructure s1 = new StemStructure(sequence, structure);

		String[] ans = s1.toVienna();
		Assert.assertEquals("Wrong sequence", ans[0], sequence);
		Assert.assertEquals("Wrong structure", ans[1], structure);
	}

	private char[] trim(TCharArrayList arr) {
		return arr.toArray();
	}

}
