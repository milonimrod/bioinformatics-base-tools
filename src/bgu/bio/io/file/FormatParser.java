package bgu.bio.io.file;

public interface FormatParser<T> {
	String parse(T data);
	void parse(T data,StringBuilder builder);
}
