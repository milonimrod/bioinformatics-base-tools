package bgu.bio.io.file.json;

public class JSONBuilder {
	public static final  void append(StringBuilder sb, String name, String value,boolean addComma){
		sb.append('\"');
		sb.append(name);
		sb.append("\" : \"");
		sb.append(value);
		if (addComma)
			sb.append("\",");
		else
			sb.append('\"');
	}
	
	public static final  void append(StringBuilder sb, String name, int value,boolean addComma){
		sb.append('\"');
		sb.append(name);
		sb.append("\" : ");
		sb.append(value);
		if (addComma)
			sb.append(',');
	}
	
	public static final  void append(StringBuilder sb, String name, float value,boolean addComma){
		sb.append('\"');
		sb.append(name);
		sb.append("\" : ");
		sb.append(value);
		if (addComma)
			sb.append(',');
	}
}
