package bgu.bio.io.file.preferences;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.prefs.Preferences;

public class PreferencesUtils {

	public static Object instantiateObject(Preferences node, Class<?> superclass)
			throws IOException {
		Object object = null;
		String classpath = node.get("classpath", null);

		try {
			Class<?> objectClass = Class.forName(classpath);

			// check for ctor parameters
			ArrayList<Class<?>> parameterTypes = new ArrayList<Class<?>>();
			ArrayList<Object> parameterValues = new ArrayList<Object>();

			for (int i = 0; true; i++) {
				String parameterData = node.get("param" + i, null);
				if (parameterData != null) {
					addParameter(parameterData, parameterTypes, parameterValues);
				} else {
					break;
				}
			}

			// construct
			Constructor<?> ctor = null;

			if (parameterTypes.isEmpty()) {
				ctor = objectClass.getConstructor();
				object = ctor.newInstance();
			} else {
				ctor = objectClass.getConstructor(parameterTypes
						.toArray(new Class<?>[parameterTypes.size()]));
				object = ctor.newInstance(parameterValues
						.toArray(new Object[parameterValues.size()]));
			}
		} catch (Exception e) {
			throw new IOException(e);
		}
		if (!(superclass.isInstance(object))) {
			throw new IOException("New instance of class " + classpath
					+ " not an instance of " + superclass.getName());
		}

		return object;
	}

	private static void addParameter(String parameterData,
			ArrayList<Class<?>> parameterTypes,
			ArrayList<Object> parameterValues) throws Exception {
		int offset = parameterData.indexOf(':');
		String className = parameterData.substring(0, offset);
		String value = parameterData.substring(offset + 1);

		if (className.equals("int")) {
			parameterTypes.add(int.class);
			parameterValues.add(Integer.parseInt(value));
			return;
		} else if (className.equals("long")) {
			parameterTypes.add(long.class);
			parameterValues.add(Long.parseLong(value));
			return;
		} else if (className.equals("float")) {
			parameterTypes.add(float.class);
			parameterValues.add(Float.parseFloat(value));
			return;
		} else if (className.equals("double")) {
			parameterTypes.add(double.class);
			parameterValues.add(Double.parseDouble(value));
			return;
		} else if (className.equals("boolean")) {
			parameterTypes.add(boolean.class);
			parameterValues.add(Boolean.parseBoolean(value));
			return;
		} else {
			Class<?> paramType = Class.forName(className);
			parameterTypes.add(paramType);
			Constructor<?> paramCtor = paramType
					.getConstructor(new Class[] { String.class });
			parameterValues.add(paramCtor.newInstance(new Object[] { value }));
		}
	}

}
