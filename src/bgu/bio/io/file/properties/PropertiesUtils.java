package bgu.bio.io.file.properties;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author milon A set of function to help load classes from a property file /
 *         variable. The utils support loading of Global parameters that mus
 *         appear in the following format:
 * 
 *         <pre>
 * ??name??
 * </pre>
 * 
 *         where the name must be in capitals with '_' as a separator between
 *         words.
 */
public class PropertiesUtils {

	private static String SPECIAL_ARGUMENT_PATTERN = "\\?\\?([A-Z]+_+)*[A-Z]+\\?\\?";
	private static Pattern pattern = Pattern.compile(SPECIAL_ARGUMENT_PATTERN);

	/**
	 * Load a class according to the properties that are given. by default
	 * trying the locate the package of the class from <u><i>prefix</i>.<i>class
	 * name</i>.package</u> and the parameters from <u><i>prefix</i>.<i>class
	 * name</i>.params</u>. The params are comma seperator values in the
	 * following format: <b>type</b>:<i>value</i>. supported types are
	 * primitives and String
	 * 
	 * @param className
	 *            the class name to be loaded
	 * @param prefix
	 *            The prefix given to the properties in the props variable
	 * @param props
	 *            The properties
	 * @param superclass
	 *            superclass of the given class
	 * @param packageString
	 *            [optional] external property name of the loaded class. if not
	 *            given
	 * @return
	 * @throws IOException
	 */
	public static Object instantiateFromProps(String className, String prefix,
			Properties props, Class<?> superclass, String... packageString)
			throws IOException {
		if (packageString == null || packageString.length == 0) {
			packageString = new String[1];
			packageString[0] = props.getProperty(prefix + "." + className + "."
					+ "package");
		}
		if (packageString[0] == null) {
			throw new IOException("no package given for filter " + className);
		}
		Class<?> objectClass = null;
		try {
			objectClass = Class.forName(packageString[0] + "." + className);
		} catch (ClassNotFoundException e) {
			throw new IOException("no class exist for filter " + className);
		}
		String paramsString = props.getProperty(prefix + "." + className + "."
				+ "params");

		Object object;
		if (paramsString == null || paramsString.length() == 0) {
			try {
				object = objectClass.newInstance();
			} catch (Exception e) {
				throw new IOException("unable to get new instance of class "
						+ objectClass.getName() + ", with params "
						+ paramsString);
			}
		} else {
			String[] paramsSplit = paramsString.split(",");
			Class<?>[] classes = new Class<?>[paramsSplit.length];
			Object[] params = new Object[paramsSplit.length];
			for (int i = 0; i < paramsSplit.length; i++) {
				String paramString = paramsSplit[i];
				// found new parameter
				addPrimitiveParameter(paramString, i, classes, params, props);
			}
			Constructor<?> constructor;
			try {
				constructor = objectClass.getConstructor(classes);
			} catch (Exception e) {
				throw new IOException(
						"unable to get an appropriate constructor of class "
								+ objectClass.getName() + "[" + e
								+ "] with given class parameters");
			}
			try {
				object = constructor.newInstance(params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new IOException("unable to get new instance of class "
						+ objectClass.getName() + "[" + e
						+ "], parameters of wrong type", e);

			}
		}
		// filterClass.getConstructo
		if (!(superclass.isInstance(object))) {
			throw new IOException("unable to get new instance of class "
					+ objectClass.getName());
		}
		return object;
	}

	public static Object initiateFromProps(String className,
			String paramsString, Properties props) throws IOException {
		Class<?> objectClass = null;
		try {
			objectClass = Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new IOException("no class exist for filter " + className);
		}
		// String paramsString = props.getProperty(paramsKey);

		Object object;
		if (paramsString == null || paramsString.length() == 0) {
			try {
				object = objectClass.newInstance();
			} catch (Exception e) {
				throw new IOException("unable to get new instance of class "
						+ objectClass.getName() + ", with params "
						+ paramsString);
			}
		} else {
			String[] paramsSplit = paramsString.split(",");
			Class<?>[] classes = new Class<?>[paramsSplit.length];
			Object[] params = new Object[paramsSplit.length];
			for (int i = 0; i < paramsSplit.length; i++) {
				String paramString = paramsSplit[i];
				// found new parameter
				addPrimitiveParameter(paramString, i, classes, params, props);
			}
			Constructor<?> constructor;
			try {
				constructor = objectClass.getConstructor(classes);
			} catch (Exception e) {
				throw new IOException(
						"unable to get an appropriate constructor of class "
								+ objectClass.getName() + "[" + e
								+ "] with given class parameters");
			}
			try {
				object = constructor.newInstance(params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new IOException("unable to get new instance of class "
						+ objectClass.getName() + "[" + e
						+ "], parameters of wrong type", e);

			}
		}

		return object;
	}

	private static void addPrimitiveParameter(String paramData, int index,
			Class<?>[] classes, Object[] params, Properties props)
			throws IOException {

		final int offset = paramData.indexOf(':');
		String className = paramData.substring(0, offset);
		String value = paramData.substring(offset + 1);

		// check special argument
		Matcher m = pattern.matcher(value);
		if (m.find()) {

			// found special argument
			if (!props.containsKey(value))
				throw new MissingResourceException(
						"can't get default value for special argument", "",
						value);
			value = props.getProperty(value);
		}

		if (className.equals("String")) {
			classes[index] = (String.class);
			params[index] = (value);
			return;
		} else if (className.equals("int")) {
			classes[index] = (int.class);
			params[index] = ((int) Integer.parseInt(value));
			return;
		} else if (className.equals("long")) {
			classes[index] = (long.class);
			params[index] = ((long) Long.parseLong(value));
			return;
		} else if (className.equals("float")) {
			classes[index] = (float.class);
			params[index] = (Float.parseFloat(value));
			return;
		} else if (className.equals("double")) {
			classes[index] = (double.class);
			params[index] = (Double.parseDouble(value));
			return;
		} else if (className.equals("boolean")) {
			classes[index] = (boolean.class);
			params[index] = (Boolean.parseBoolean(value));
			return;
		}
		throw new IOException("unsupported primitive type " + className);
	}

}
