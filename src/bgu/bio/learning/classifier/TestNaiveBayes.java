package bgu.bio.learning.classifier;

import org.junit.Assert;
import org.junit.Test;

public class TestNaiveBayes {

	@Test
	public void test1() {
		NaiveBayes nb = new NaiveBayes();
		for (int i = 1; i <= 20; i++) {
			nb.train("spam", "penis");
		}
		for (int i = 1; i <= 31; i++) {
			nb.train("ham", "penis");
		}
		for (int i = 1; i <= 13; i++) {
			nb.train("ham", "hello");
		}
		for (int i = 1; i <= 10; i++) {
			nb.train("spam", "spam");
		}

		Assert.assertEquals("wrong dist", "spam", nb.classify("penis", "spam"));
	}

	@Test
	public void test2() {
		NaiveBayes nb = new NaiveBayes();
		for (int i = 1; i <= 20; i++) {
			nb.train("spam", "penis", "viagra");
		}
		for (int i = 1; i <= 30; i++) {
			nb.train("ham", "penis");
		}
		for (int i = 1; i <= 1; i++) {
			nb.train("ham", "penis", "viagra");
		}
		for (int i = 1; i <= 13; i++) {
			nb.train("ham", "hello");
		}
		for (int i = 1; i <= 6; i++) {
			nb.train("spam", "spam");
		}
		for (int i = 1; i <= 4; i++) {
			nb.train("spam", "spam", "viagra");
		}

		Assert.assertEquals("wrong dist", "spam",
				nb.classify("penis", "viagra"));
		Assert.assertEquals("wrong dist", "ham", nb.classify("hello"));
	}

}
