package bgu.bio.server.general;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.XMLFormatter;

import bgu.bio.com.protocol.AsyncServerProtocol;
import bgu.bio.com.protocol.ServerProtocolFactory;
import bgu.bio.com.reactor.Reactor;
import bgu.bio.server.general.protocol.ScriptProtocol;
import bgu.bio.server.general.protocol.ScriptProtocolData;

public class JobServer {

	/** The threads. */
	private LinkedList<Thread> threads;
	private Logger log;
	private Reactor reactor;


	public static void main(String[] args) throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		prop.load(new FileInputStream(args[0]));

		JobServer mainframe = new JobServer(prop, "");
		mainframe.start();
	}

	/**
	 * Instantiates a new job management server.
	 *
	 * @param props the props
	 * @param prefix the prefix
	 */
	public JobServer(Properties props,String prefix){

		//pad a dot if non exist and only if the prefix is given
		if (prefix == null)
			prefix = "";

		if (prefix.length() > 0 && prefix.endsWith(".")){
			prefix += ".";
		}

		threads = new LinkedList<Thread>();
		//Load the reactor
		int port = Integer.parseInt(props.getProperty(prefix+"port", "57999"));
		int poolSize = Integer.parseInt(props.getProperty(prefix+"pool.size", ""+Runtime.getRuntime().availableProcessors()));

		//Initiate the logger
		log = Logger.getLogger(props.getProperty(prefix
				+ "log.class"));
		try{
			FileHandler fh = new FileHandler(props.getProperty(prefix
					+ "log.filename"));
			fh.setFormatter(new XMLFormatter());
			log.addHandler(fh);
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		final ScriptProtocolData data = new ScriptProtocolData();
		
		data.log = log;
		
		ServerProtocolFactory protocol = new ServerProtocolFactory() {
			@Override
			public AsyncServerProtocol create() {
				return new ScriptProtocol(data);
			}
		};

		//run the reactor as a thread
		reactor = new Reactor(port, poolSize, protocol);
		Thread reactorThread = new Thread(reactor,"Reactor");
		threads.add(reactorThread);
	}

	/**
	 * Start.
	 */
	public void start(){
		for (Thread t : threads)
		{
			t.start();
		}

		//observer wait until done
		Scanner sc = new Scanner(System.in);
		while (true){
			String str = sc.next();
			if (str.equalsIgnoreCase("close")){
				reactor.stopReactor();
				for (int i=1;i<threads.size();i++){
					threads.get(i).interrupt();
				}
				break;
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Done.");
	}
}


