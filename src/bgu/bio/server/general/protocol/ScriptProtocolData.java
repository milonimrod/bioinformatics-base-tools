package bgu.bio.server.general.protocol;

import java.util.logging.Logger;

import bgu.bio.adt.queue.BlockingQueue;
import bgu.bio.com.sql.MySqlConnector;
import bgu.bio.io.file.json.JSONObject;

public class ScriptProtocolData {
	public Logger log;
	public BlockingQueue<JSONObject> pendingJobs;
	public MySqlConnector conn;
	
	public ScriptProtocolData() {
		
	}
}
