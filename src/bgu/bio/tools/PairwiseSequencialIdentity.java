package bgu.bio.tools;

import gnu.trove.map.hash.TIntLongHashMap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

import bgu.bio.algorithms.alignment.GlobalSequenceAlignment;
import bgu.bio.ds.rna.RNA;
import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.alphabet.RnaAlphabet;

public class PairwiseSequencialIdentity {

	ArrayList<RNA> list;
	volatile AtomicInteger startIndex;
	private CyclicBarrier barrier;
	double[][] scores;

	public void run(String[] args) {
		System.out.print("Loading data ... ");
		list = RNA.loadFromFile(args[0], false);
		System.out.println("Done.");
		scores = new double[list.size()][list.size()];
		startIndex = new AtomicInteger(0);
		int threads = 1;
		if (args.length == 2) {
			threads = Integer.parseInt(args[1]);
		}
		barrier = new CyclicBarrier(threads + 1);

		System.out.println("Starting run ... ");
		for (int i = 0; i < threads; i++) {
			Thread t = new Thread(new ComparissonRunner());
			t.start();
		}

		try {
			barrier.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
		System.out.println("Done.");

		System.out.print("Creating histogram of results ... ");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
					args[0] + ".hist")));
			TIntLongHashMap map = new TIntLongHashMap();
			final int STEP = 5;
			for (int i = 0; i <= 100; i += STEP) {
				map.put(i, 0);
			}
			for (int i = 0; i < list.size() - 1; i++) {
				for (int j = i + 1; j < list.size(); j++) {
					final int val = STEP * (int) ((100 * scores[i][j]) / STEP);
					map.put(val, map.get(val) + 1);

				}
			}
			for (int i = 0; i <= 100; i += STEP) {
				writer.write(i + "," + map.get(i) + "\n");
				System.out.println(i + "," + map.get(i) + "\n");
			}
			System.out.println("Done.");
			writer.close();
		} catch (IOException ex) {

		}

		System.out.print("Writing the matrix to file ... ");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
					args[0] + ".matrix")));

			for (int i = 0; i < list.size() - 1; i++) {
				for (int j = i + 1; j < list.size(); j++) {
					writer.write(Double.toString(scores[i][j]));
					writer.write(' ');
				}
				writer.newLine();
			}
			writer.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		System.out.println("Done.");
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Usage: java "
					+ PairwiseSequencialIdentity.class.getCanonicalName()
					+ " <input file> [threads]");
			System.exit(1);
		}
		PairwiseSequencialIdentity p = new PairwiseSequencialIdentity();
		p.run(args);
	}

	private class ComparissonRunner implements Runnable {

		@Override
		public void run() {
			GlobalSequenceAlignment seq = new GlobalSequenceAlignment(1000,
					1000, RnaAlphabet.getInstance(), new IdentityScoringMatrix(
							RnaAlphabet.getInstance(), 1, 0));
			int i = startIndex.getAndIncrement();
			while (i < list.size()) {
				final String seq1 = list.get(i).getPrimary();
				for (int j = i + 1; j < list.size(); j++) {
					final String seq2 = list.get(j).getPrimary();
					seq.setSequences(seq1, seq2);
					seq.buildMatrix();
					final double s = 2 * seq.getAlignmentScore()
							/ (seq1.length() + seq2.length());
					scores[i][j] = s;
				}
				System.out.println(Thread.currentThread().getName() + " " + i
						+ " Finished " + (list.size() - i + 1));
				i = startIndex.getAndIncrement();
			}
			try {
				barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
	}
}
