package bgu.bio.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import bgu.bio.adt.tuples.Pair;
import bgu.bio.ds.rna.RNA;
import bgu.bio.ds.rna.StemStructure;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class PairwiseStemSet {

	public ArrayList<Pair<RNA, ArrayList<StemInfo>>> loadFile(String filename)
			throws FileNotFoundException, JSONException, IOException {
		ArrayList<Pair<RNA, ArrayList<StemInfo>>> list = new ArrayList<Pair<RNA, ArrayList<StemInfo>>>();
		// load the file

		JSONObject json = new JSONObject(readFileToString(filename));
		
		return list;
	}

	/**
	 * @param filename
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String readFileToString(String filename)
			throws FileNotFoundException, IOException {
		BufferedReader reader = new BufferedReader(new FileReader(new File(
				filename)));
		StringBuilder sb = new StringBuilder();

		String line = reader.readLine();
		while (line != null) {
			sb.append(line);
			line = reader.readLine();
		}
		reader.close();
		return sb.toString();
	}

	private class StemInfo {
		private int startLeg1;
		private int startLeg2;
		private int endLeg1;
		private int endLeg2;
		private StemStructure stem;

		public int leg1Size() {
			return endLeg1 - startLeg1 + 1;
		}

		public int leg2Size() {
			return endLeg2 - startLeg2 + 1;
		}

		public double intersection(StemInfo other) {

			final int int1 = intersection(startLeg1, endLeg1, other.startLeg1,
					other.endLeg1);
			final int int2 = intersection(startLeg2, endLeg2, other.startLeg2,
					other.endLeg2);
			if (int1 > 0 && int2 > 0) {
				final double maxOverlap1 = Math.min(leg1Size(),
						other.leg1Size());
				final double maxOverlap2 = Math.min(leg2Size(),
						other.leg2Size());
				return (int1 / maxOverlap1) + (int2 / maxOverlap2);
			}

			return 0;
		}

		private int intersection(int a1, int a2, int b1, int b2) {
			final int l0 = Math.max(a1, b1);
			final int l1 = Math.min(a2, b2);
			final int diff = l1 - l0 + 1;
			if (diff <= 0) {
				return 0;
			}
			return diff;
		}

		/**
		 * @return the stem
		 */
		public final StemStructure getStem() {
			return stem;
		}

		/**
		 * @param stem
		 *            the stem to set
		 */
		public final void setStem(StemStructure stem) {
			this.stem = stem;
		}

		/**
		 * @return the startLeg1
		 */
		public final int getStartLeg1() {
			return startLeg1;
		}

		/**
		 * @param startLeg1
		 *            the startLeg1 to set
		 */
		public final void setStartLeg1(int startLeg1) {
			this.startLeg1 = startLeg1;
		}

		/**
		 * @return the startLeg2
		 */
		public final int getStartLeg2() {
			return startLeg2;
		}

		/**
		 * @param startLeg2
		 *            the startLeg2 to set
		 */
		public final void setStartLeg2(int startLeg2) {
			this.startLeg2 = startLeg2;
		}

		/**
		 * @return the endLeg1
		 */
		public final int getEndLeg1() {
			return endLeg1;
		}

		/**
		 * @param endLeg1
		 *            the endLeg1 to set
		 */
		public final void setEndLeg1(int endLeg1) {
			this.endLeg1 = endLeg1;
		}

		/**
		 * @return the endLeg2
		 */
		public final int getEndLeg2() {
			return endLeg2;
		}

		/**
		 * @param endLeg2
		 *            the endLeg2 to set
		 */
		public final void setEndLeg2(int endLeg2) {
			this.endLeg2 = endLeg2;
		}

	}

}
