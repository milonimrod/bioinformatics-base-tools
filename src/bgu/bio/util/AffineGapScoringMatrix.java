package bgu.bio.util;

import java.io.InputStream;
import java.util.MissingResourceException;

import bgu.bio.util.alphabet.AlphabetUtils;

public class AffineGapScoringMatrix extends ScoringMatrix {
	final float gapOpen;
	final float gapSpace;

	public AffineGapScoringMatrix(InputStream stream, AlphabetUtils alphabet) {
		super(stream, alphabet);
		if (!specialValuesMap.containsKey("gap.open")
				|| !specialValuesMap.containsKey("gap.extend")) {
			throw new MissingResourceException(
					"Can't locate one of the gaps scores",
					Double.class.getName(), " ");
		}
		gapOpen = specialValuesMap.get("gap.open");
		gapSpace = specialValuesMap.get("gap.extend");

	}

	public AffineGapScoringMatrix(String fileName, AlphabetUtils alphabet) {
		super(fileName, alphabet);
		if (!specialValuesMap.containsKey("gap.open")
				|| !specialValuesMap.containsKey("gap.extend")) {
			throw new MissingResourceException(
					"Can't locate one of the gaps scores",
					Double.class.getName(), " ");
		}
		gapOpen = specialValuesMap.get("gap.open");
		gapSpace = specialValuesMap.get("gap.extend");

	}

	public AffineGapScoringMatrix(AffineGapScoringMatrix other) {
		super(other.table.length);
		this.minValue = other.minValue;
		this.maxValue = other.maxValue;
		for (int i = 0; i < other.table.length; i++) {
			for (int j = 0; j < other.table[i].length; j++) {
				this.table[i][j] = other.table[i][j];

			}
		}
		gapOpen = other.gapOpen;
		gapSpace = other.gapSpace;
	}

	public AffineGapScoringMatrix(AlphabetUtils alphabet, float gapOpenCost,
			float gapSpaceCost) {
		super(alphabet);
		this.gapOpen = gapOpenCost;
		this.gapSpace = gapSpaceCost;
	}

	@Override
	public float score(short c1, short c2) {
		if (alphabet.emptyLetterHashed() == c1
				|| alphabet.emptyLetterHashed() == c2)
			return this.gapSpace;

		return super.score(c1, c2);
	}

	public final float openGapCost() {
		return this.gapOpen;
	}

	public final float extendGapCost() {
		return this.gapSpace;
	}

	public AffineGapScoringMatrix cloneMatrix() {
		return new AffineGapScoringMatrix(this);
	}
}
