package bgu.bio.util;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;

/**
 * The Class CharBuffer is helping to save chars data.
 */
public class CharBuffer implements Externalizable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5140681781114023423L;

	/** The array of chars. */
	private char[] array;

	/** The stack trace - used for Debugging of memory linkage. */
	private String stackTrace;

	/**
	 * The size of the current data in the array. size is in range of:
	 * [0..array.length]
	 */
	private int size;

	/**
	 * Constructor for deserialization.
	 */
	public CharBuffer() {
	}

	/**
	 * Instantiates a new char buffer in the given size.
	 * 
	 * @param size
	 *            the size of the buffer array
	 */
	public CharBuffer(int size) {
		this.size = size;
		this.array = new char[size];
	}

	/**
	 * Instantiates a new char buffer.
	 * 
	 * @param array
	 *            the data for the buffer. the array will be copied only if its
	 *            length is smaller then the current array in the buffer
	 */
	public CharBuffer(char[] array) {
		this.array = array;
		this.size = array.length;
	}

	/**
	 * Instantiates a new char buffer with deep copy of the array.
	 * 
	 * @param array
	 *            the array
	 * @param offset
	 *            the offset
	 * @param length
	 *            the length
	 */
	public CharBuffer(char[] array, int offset, int length) {
		this.array = new char[Math.max(length, array.length)];
		System.arraycopy(array, offset, this.array, 0, length);
		this.size = length;
	}

	/**
	 * Instantiates a new char buffer which is a copy of an existing one.
	 * 
	 * @param other
	 *            the char buffer to copy
	 */
	public CharBuffer(CharBuffer other) {
		this(Arrays.copyOf(other.array, other.length()));
	}

	/**
	 * Instantiates a new char buffer using a string.
	 * 
	 * @param str
	 *            the string that contain the char data
	 */
	public CharBuffer(String str) {
		this(str.toCharArray());
	}

	/**
	 * The method returns the char in the index'th position.
	 * 
	 * @param index
	 *            the index of the character
	 * 
	 * @return the character at the specified index of this buffer
	 */
	public char charAt(int index) {
		return array[index];
	}

	/**
	 * The method returns the chars between the start position and
	 * start+length-1 in the buffer.
	 * 
	 * @param start
	 *            the starting index for retrieval
	 * @param length
	 *            the length of the retrieval array
	 * 
	 * @return the char[] containing the characters in positions start to
	 *         start+length-1 in the buffer
	 */
	public char[] charsAt(int start, int length) {
		char[] ans = new char[length];
		System.arraycopy(this.array, start, ans, 0, length);
		return ans;
	}

	/**
	 * Copy chars at given location into an array.
	 * 
	 * @param start
	 *            Index of first char to copy.
	 * @param length
	 *            Number of chars to copy.
	 * @param dest
	 *            The destination array. Array length must be at least offset +
	 *            length.
	 * @param offset
	 *            The offset in the destination array to start copying into.
	 */
	public void charsAt(int start, int length, char[] dest, int offset) {
		System.arraycopy(this.array, start, dest, offset, length);
	}

	/**
	 * Returns the size of the current Buffer.
	 * 
	 * @return the current size of the buffer
	 */
	public int length() {
		return this.size;
	}

	/**
	 * Sets the char in the index position to be ch.
	 * 
	 * @param index
	 *            the index needed to be changed in the buffer.
	 * @param ch
	 *            the char to be the new value at position index.
	 * 
	 * @return true, if index is between 0 and {@link CharBuffer#length()}-1.
	 *         false otherwise.
	 */
	public boolean setChar(int index, char ch) {
		if (index >= this.array.length){
			throw new IndexOutOfBoundsException("index must be between zero and the size of the buffer");
		}
		this.array[index] = ch;
		return true;
	}

	/**
	 * Sets the chars at positions start to start + chars.length-1 to be the
	 * corresponding chars in the chars array.
	 * 
	 * @param start
	 *            the offset inside the buffer.
	 * @param chars
	 *            the chars array to act as source
	 * 
	 * @return true, if successful
	 */
	public boolean setChar(int start, char[] chars) {
		System.arraycopy(chars, 0, this.array, start, chars.length);
		return true;
	}

	/**
	 * Sets the chars at positions start to start + chars.length-1 to be the
	 * corresponding chars in the chars array.
	 * 
	 * @param start
	 *            the offset inside the buffer.
	 * @param chars
	 *            the chars array to act as source
	 * 
	 * @return true, if successful
	 */
	public boolean setChar(int start, char[] chars, int offset, int length) {
		if (this.array.length < length + 1) {
			this.array = new char[length];
		}
		System.arraycopy(chars, offset, this.array, start, length);
		this.size = length;
		for (int i = start + length; i < this.array.length; i++) {
			array[i] = 'N';
		}
		return true;
	}

	/**
	 * Sets the array in the buffer to be the given array.
	 * 
	 * @param array
	 *            the new array
	 */
	public void setArray(char[] array) {
		if (this.array == null || this.array.length < array.length) {
			this.array = array;
		} else if (this.array.length >= array.length) {
			this.setChar(0, array);
		}
		this.size = array.length;
	}

	/**
	 * Sets the array.
	 * 
	 * @param str
	 *            the String data will be the new buffer data.
	 */
	public void setArray(String str) {
		this.setArray(str.toCharArray());
	}

	/**
	 * Sets the array.
	 * 
	 * @param other
	 *            the new array
	 * 
	 * @see CharBuffer#setArray(char[])
	 */
	public void setArray(CharBuffer other) {
		this.setArray(other.array);
	}

	public void copyArray(char[] arrayToCopy, int length) {
		if (this.array == null || this.array.length < length) {
			this.array = Arrays.copyOf(arrayToCopy, length);
		} else {
			System.arraycopy(arrayToCopy, 0, this.array, 0, length);
		}

		this.size = length;
	}

	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Returns the current array in the buffer.
	 * 
	 * @return the char[]
	 */
	public char[] array() {
		return this.array;
	}

	/**
	 * Returns a new copy of the array.
	 * 
	 * @return a new copy of {@link #array} in the length of {@link #size}.
	 */
	public char[] copyOfArray() {
		return this.charsAt(0, this.size);
	}

	/**
	 * Append a char array to the existing buffer.
	 * 
	 * @param append
	 *            the char array to append
	 */
	public void append(char[] append) {
		if (append != null && append.length > 0) {
			char[] old = this.array;
			this.array = new char[this.array.length + append.length];
			System.arraycopy(old, 0, this.array, 0, old.length);
			System.arraycopy(append, 0, this.array, old.length, append.length);
		}
	}

	/**
	 * Append another {@link CharBuffer} to this current buffer.
	 * 
	 * @param otherBuffer
	 *            the other buffer
	 */
	public void append(CharBuffer otherBuffer) {
		this.append(otherBuffer.array);
	}

	/**
	 * Find first appearance of a char in this CharBuffer.
	 * 
	 * @param c
	 *            the required char
	 * 
	 * @return the index of the first appearance of c in this CharBuffer, -1 if
	 *         does not appear
	 */
	public int indexOf(char c) {
		for (int i = 0; i < this.size; i++) {
			if (array[i] == c) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Find first appearance of a char in this CharBuffer starting from a given
	 * offset.
	 * 
	 * @param c
	 *            the required char
	 * @param offset
	 *            the offset to start from
	 * 
	 * @return the index of the first appearance of c in this CharBuffer
	 *         starting from offset, -1 if does not appear
	 */
	public int indexOf(char c, int offset) {
		for (int i = offset; i < this.size; i++) {
			if (array[i] == c) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Find last appearance of a char in this CharBuffer.
	 * 
	 * @param c
	 *            the required char
	 * 
	 * @return the index of the last appearance of c in this CharBuffer, -1 if
	 *         does not appear
	 */
	public int lastIndexOf(char c) {
		for (int i = this.size - 1; i >= 0; i--) {
			if (array[i] == c) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Find last appearance of a char in this CharBuffer before a given
	 * location.
	 * 
	 * @param c
	 *            the required char
	 * @param before
	 *            the location before which to find c
	 * 
	 * @return the index of the last appearance of c in this CharBuffer before
	 *         the given location, -1 if does not appear
	 */
	public int lastIndexOf(char c, int before) {
		for (int i = before - 1; i >= 0; i--) {
			if (array[i] == c) {
				return i;
			}
		}
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (this.size == this.array.length) {
			return new String(array);
		}

		StringBuilder sb = new StringBuilder();
		sb.append(array, 0, this.size);
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		CharBuffer other = (CharBuffer) o;
		if (this.size != other.size) {
			return false;
		}
		for (int i = 0; i < size; i++) {
			if (this.array[i] != other.array[i]) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hashCode = this.size;
		for (int i = 0; i < this.size; i++) {
			hashCode = 31 * hashCode + array[i];
		}
		return hashCode;
	}

	/**
	 * This method is for testing only
	 * 
	 * @param override
	 *            the array
	 */
	public void overrideArray(char[] override) {
		this.array = override;
	}

	public int compareTo(CharBuffer other) {
		for (int i = 0; i < size; i++) {
			int diff = this.array[i] - other.array[i];
			if (diff != 0) {
				return diff;
			}
		}
		return 0;
	}

	public String substring(int start, int len) {
		return new String(this.charsAt(start, len));
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(size);
		for (int i = 0; i < size; i++) {
			out.writeChar(array[i]);
		}
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		size = in.readInt();
		array = new char[size];
		for (int i = 0; i < size; i++) {
			array[i] = in.readChar();
		}
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public String getStackTrace() {
		return stackTrace;
	}
}
