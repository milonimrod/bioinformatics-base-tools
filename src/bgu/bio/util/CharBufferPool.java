package bgu.bio.util;

import gnu.trove.iterator.TObjectIntIterator;
import gnu.trove.map.hash.TObjectIntHashMap;

import java.util.Arrays;

import bgu.bio.adt.queue.Pool;

public class CharBufferPool extends Pool<CharBuffer> implements CharBufferPoolStatsMBean {
	
	private static final float FACTOR = 1.4f;
	private static final int QUEUE_SIZE = 2048;
	
	private final int initialCharBufferSize;
	public static final boolean DEBUG = false;
	private TObjectIntHashMap<String> instanceMap;
	
	//statistics
	private long requeued;
	
	public CharBufferPool(){
		this(256);
	}
	public CharBufferPool(int initialCharBufferSize) {
		super(QUEUE_SIZE, false);
		this.initialCharBufferSize = initialCharBufferSize;
		this.requeued = 0;
		
		if (DEBUG){
			this.instanceMap = new TObjectIntHashMap<String>();
			this.instanceMap.put("UNKNOWN", 0);
		}
		
		for (int i=0;i<QUEUE_SIZE;i++){
			this.enqueue(new CharBuffer(initialCharBufferSize));
		}
	}

	@Override
	public synchronized boolean enqueue(CharBuffer obj) {
		if (DEBUG){
			if (obj.getStackTrace() == null){
				obj.setStackTrace("UNKNOWN");
			}
			int val = instanceMap.get(obj.getStackTrace());
			instanceMap.put(obj.getStackTrace(), val-1);
		}
		
		return super.enqueue(obj);
	}
	
	@Override
	public synchronized CharBuffer dequeue() {

		CharBuffer ans = super.dequeue();
		if (ans == null) {
			ans = new CharBuffer(this.initialCharBufferSize);
		}else{
			ans.setSize(0);
		}
		
		if(DEBUG){
			String key = Arrays.toString(Thread.currentThread().getStackTrace());
			ans.setStackTrace(key);
			if (!instanceMap.containsKey(key)){
				instanceMap.put(key, 1);
			}else{
				int val = instanceMap.get(key);
				instanceMap.put(key, val + 1);
			}
		}
		
		return ans;
	}

	public synchronized CharBuffer dequeue(int size) {

		CharBuffer ans = super.dequeue();
		if (ans == null) {
			ans = new CharBuffer((int)Math.max(size * FACTOR,initialCharBufferSize));
		}else if(ans.array().length < size){
			//reuse the answer later
			super.enqueue(ans);
			this.requeued++;
			ans = new CharBuffer((int)(size * FACTOR));
		}
		else{
			ans.setSize(0);
		}
		
		if(DEBUG){
			String key = Arrays.toString(Thread.currentThread().getStackTrace());
			ans.setStackTrace(key);
			if (!instanceMap.containsKey(key)){
				instanceMap.put(key, 1);
			}else{
				int val = instanceMap.get(key);
				instanceMap.put(key, val + 1);
			}
		}
		
		return ans;
	}

	public synchronized CharBuffer dequeue(CharBuffer array) {
		CharBuffer ans = super.dequeue();
		if (ans == null) {
			ans = new CharBuffer((int)Math.max(array.length() * FACTOR,initialCharBufferSize));
		}else if(ans.array().length < array.length()){
			//reuse the answer later
			enqueue(ans);
			this.requeued++;
			ans = new CharBuffer((int)Math.max(array.length() * FACTOR,initialCharBufferSize));
		}else{
			ans.setSize(0);
		}

		ans.copyArray(array.array(),array.length());
		
		if(DEBUG){
			String key = Arrays.toString(Thread.currentThread().getStackTrace());
			ans.setStackTrace(key);
			if (!instanceMap.containsKey(key)){
				instanceMap.put(key, 1);
			}else{
				int val = instanceMap.get(key);
				instanceMap.put(key, val + 1);
			}
		}
		
		return ans;
	}
	
	@Override
	public synchronized long[] statistics() {
		long[] superArray = super.statistics();
		long[] answer = Arrays.copyOf(superArray, superArray.length + 1);
		answer[superArray.length] = this.requeued;
		
		return answer;
	}
	
	@Override
	public synchronized String[] statisticsHeaders() {
		String[] superArray = super.statisticsHeaders();
		String[] answer = Arrays.copyOf(superArray, superArray.length + 1);
		answer[superArray.length] = "requeued";
		return answer;
	}
	
	@Override
	public String instanceMap(){
		StringBuilder sb = new StringBuilder();
		TObjectIntIterator<String> it = this.instanceMap.iterator();
		while (it.hasNext()){
			it.advance();
			final String key = it.key();
			final int val = it.value();
			if (val != 0){
				sb.append(key);
				sb.append('=');
				sb.append(val);
				sb.append('\n');
			}
		}
		return sb.toString();

	}
}
