package bgu.bio.util;

public class CharBufferPoolStats implements CharBufferPoolStatsMBean {

	private CharBufferPool pool;

	CharBufferPoolStats(CharBufferPool pool){
		this.pool = pool;
	}
	
	@Override
	public String instanceMap() {
		return pool.instanceMap();
	}

}
