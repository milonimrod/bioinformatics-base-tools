package bgu.bio.util;

public interface Filter<T> {
	public boolean shouldPass(T obj);
}


