package bgu.bio.util;

import bgu.bio.util.alphabet.AlphabetUtils;

public class IdentityAffineScoringMatrix extends AffineGapScoringMatrix {

	public IdentityAffineScoringMatrix(AlphabetUtils alphabet,float matchScore,
			float gapOpenCost, float gapSpaceCost,float misMatchCost) {
		super(alphabet, gapOpenCost, gapSpaceCost);
		
		for (int i = 0; i < alphabet.size(); i++) {
			for (int j = 0; j < alphabet.size(); j++) {
				table[i][j] = misMatchCost;
			}
			table[i][i] = matchScore;
		}
	}


}
