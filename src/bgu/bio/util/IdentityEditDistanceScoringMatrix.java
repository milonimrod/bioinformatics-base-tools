package bgu.bio.util;

import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

public class IdentityEditDistanceScoringMatrix extends ScoringMatrix {

	public IdentityEditDistanceScoringMatrix(AlphabetUtils alphabet) {
		super(alphabet);
		for (int i = 0; i < alphabet.size(); i++) {
			for (int j = 0; j < alphabet.size(); j++) {
				table[i][j] = 1;
			}
			table[i][i] = 0;
		}
	}

	public IdentityEditDistanceScoringMatrix() {
		super(RnaAlphabet.getInstance());
	}

	@Override
	public float score(char c1, char c2) {
		if (c1 == c2) {
			return 0;
		}
		return 1;
	}

	@Override
	public float score(short c1, short c2) {
		if (c1 == c2) {
			return 0;
		}
		return 1;
	}

}
