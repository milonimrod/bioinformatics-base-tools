package bgu.bio.util;

import bgu.bio.util.alphabet.AlphabetUtils;

public class IdentityScoringMatrix extends ScoringMatrix {

	public IdentityScoringMatrix(AlphabetUtils alphabet) {
		this(alphabet, 1, 0);
	}

	public IdentityScoringMatrix(AlphabetUtils alphabet, float identity,
			float other) {
		super(alphabet);
		for (int i = 0; i < alphabet.size(); i++) {
			for (int j = 0; j < alphabet.size(); j++) {
				table[i][j] = other;
			}
			table[i][i] = identity;
		}
	}

}
