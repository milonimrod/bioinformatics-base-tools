package bgu.bio.util;

public class MathOperations {
	public static final double EPSILON = Math.pow(10, -5);
	public static final double INFINITY = Double.POSITIVE_INFINITY;// =
																	// 10000000000d;
	private static final double BIG_EPSILON = 1000;

	/**
	 * Check if a number is equal to another number
	 * 
	 * @param a
	 *            the first number
	 * @param b
	 *            the second number
	 * @return true, if and only if |a - b| <= {@link #EPSILON}
	 */
	public static final boolean equals(double a, double b) {
		final double delta = a - b;
		return delta <= EPSILON && delta >= -EPSILON;
	}

	/**
	 * Check if a number is equal to another number
	 * 
	 * @param a
	 *            the first number
	 * @param b
	 *            the second number
	 * @param accuracy
	 *            the accuracy threshold for equality
	 * @return true, if and only if |a - b| <= {@link #EPSILON}
	 */
	public static final boolean equals(double a, double b, double accuracy) {
		return Math.abs(a - b) < accuracy;
	}

	/**
	 * Check if a number is greater than another number by more than
	 * {@link #EPSILON}.
	 * 
	 * @param a
	 *            the first number
	 * @param b
	 *            the second number
	 * @return true, if and only if a - b > {@link #EPSILON}
	 */
	public static final boolean greater(double a, double b) {
		final double delta = a - b;
		return delta > EPSILON;
	}

	public static boolean isInfinity(double val) {
		// return equals(INFINITY, val);
		return val + BIG_EPSILON >= INFINITY;
	}

	/**
	 * calculates the number of possible pairs that can be formed from n items
	 * 
	 * @param size
	 *            - number of items
	 * @return C(size,2)
	 */
	public static int possiblePairs(int size) {
		return size * (size - 1) / 2;
	}

	public static long gcd(long n, long m) {
		long i = m;
		long j = n;
		while (i != 0) {
			final long temp = j % i;
			j = i;
			i = temp;
		}
		return j;
	}

	public static int gcd(int n, int m) {
		int i = m;
		int j = n;
		while (i != 0) {
			final int temp = j % i;
			j = i;
			i = temp;
		}
		return j;
	}
}
