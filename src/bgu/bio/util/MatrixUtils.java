package bgu.bio.util;

import java.util.Arrays;

/**
 * A set of useful functions for Two dimensional matrices. 
 * @author milon
 *
 */
public class MatrixUtils {

	
	/** copy the given matrix to a new created matrix
	 * @param mat the matrix to be copied
	 * @return a <b>new</b> with the same sizes as the given matrix and with the same inner data 
	 */
	public static int[][] copy(int[][] mat) {
		int[][] ans = new int[mat.length][];
		for (int i = 0; i < mat.length; i++) {
			ans[i] = Arrays.copyOf(mat[i], mat[i].length);
		}
		return ans;
	}

	/**
	 * @see MatrixUtils#copyData(int[][], int[][])
	 */
	public static double[][] copy(double[][] mat) {
		double[][] ans = new double[mat.length][];
		for (int i = 0; i < mat.length; i++) {
			Arrays.copyOf(mat[i], mat[i].length);
		}
		return ans;
	}

	/**
	 * Copy the inner data between two given matrices. <b>assumes destination can contain the data from the source</b>
	 * @param src the source matrix
	 * @param dest the destination matrix. <b>must be with size greater or equal to the destination</b> 
	 */
	public static void copyData(int[][] src, int[][] dest) {
		for (int i = 0; i < src.length; i++) {
			System.arraycopy(src[i], 0, dest[i], 0, src[i].length);
		}
	}

	public static void copyData(double[][] src, double[][] dest) {
		for (int i = 0; i < src.length; i++) {
			System.arraycopy(src[i], 0, dest[i], 0, src[i].length);
		}
	}
	/**
	 * @see #tojavaArrayString(int[][])
	 */
	public static String tojavaArrayString(double[][] array) {
		StringBuilder sb = new StringBuilder(array.length * 2);
		sb.append("{");
		for (int i = 0; i < array.length; i++) {
			sb.append("{");
			for (int j = 0; j < array[i].length; j++) {
				sb.append(array[i][j]);
				if (j < array[i].length - 1)
					sb.append(",");
			}
			sb.append("}");
			if (i < array.length - 1)
				sb.append("\n,");
		}
		sb.append("};");
		return sb.toString();
	}

	/** Parse a given matrix to a {@link String} in the java array format. 
	 * @param array to be parsed
	 * @return The String representing the array
	 */
	public static String tojavaArrayString(int[][] array) {
		StringBuilder sb = new StringBuilder(array.length * 2);
		sb.append("{");
		for (int i = 0; i < array.length; i++) {
			sb.append("{");
			for (int j = 0; j < array[i].length; j++) {
				sb.append(array[i][j]);
				if (j < array[i].length - 1)
					sb.append(",");
			}
			sb.append("}");
			if (i < array.length - 1)
				sb.append("\n,");
		}
		sb.append("};");
		return sb.toString();
	}

	/**
	 * @see #equals(Object[][], Object[][])
	 */
	public static boolean equals(int[][] matrix1, int[][] matrix2) {
		if (matrix1 == matrix2) {
			return true;
		}

		if (matrix1 == null || matrix2 == null
				|| matrix1.length != matrix2.length) {
			return false;
		}

		for (int i = 0; i < matrix2.length; i++) {
			if (!Arrays.equals(matrix1[i], matrix2[i])) {
				return false;
			}
		}
		return true;
	}

	/** Check if two given matrices are equal. Each array in the matrices is compared using the {@link Arrays#equals(Object[], Object[])} method
	 * @param matrix1 the first matrix to be compared. <b>can be null</b>
	 * @param matrix2 the first matrix to be compared. <b>can be null</b>
	 * @return return true iff the matrices are equal
	 */
	public static boolean equals(Object[][] matrix1, Object[][] matrix2) {
		if (matrix1 == matrix2) {
			return true;
		}

		if (matrix1 == null || matrix2 == null
				|| matrix1.length != matrix2.length) {
			return false;
		}

		for (int i = 0; i < matrix2.length; i++) {
			if (!Arrays.equals(matrix1[i], matrix2[i])) {
				return false;
			}
		}
		return true;
	}
	/**
	 * @see #equals(Object[][], Object[][])
	 */
	public static boolean equals(double[][] matrix1, double[][] matrix2) {
		if (matrix1 == matrix2) {
			return true;
		}

		if (matrix1 == null || matrix2 == null
				|| matrix1.length != matrix2.length) {
			return false;
		}

		for (int i = 0; i < matrix2.length; i++) {
			if (!Arrays.equals(matrix1[i], matrix2[i])) {
				return false;
			}
		}
		return true;
	}
}
