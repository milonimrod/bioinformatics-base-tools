package bgu.bio.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class NCBIInformationWrapper {
	private HashMap<String, HashMap<String, String>> database;

	public NCBIInformationWrapper() {
		this.database = new HashMap<String, HashMap<String, String>>();
	}

	public HashMap<String, String> fetch(String id) {
		return fetch(id, 5);
	}

	private HashMap<String, String> fetch(String id, int attempts) {
		if (database.containsKey(id)) {
			return database.get(id);
		}

		try {
			HashMap<String, String> ans = NCBIInformation.fetch(id);
			database.put(id, ans);
			return ans;
		} catch (Exception e) {
			e.printStackTrace();
			if (attempts > 0) {
				return fetch(id, attempts - 1);
			}
		}
		return null;
	}

	public void retFromServer(HashSet<String> set) {
		retFromServer(set, 3);
	}

	private void retFromServer(HashSet<String> set, int attempts) {
		ArrayList<String> list = new ArrayList<String>();
		for (String string : set) {
			if (!database.containsKey(string)) {
				list.add(string);
			}
		}

		try {
			HashMap<String, HashMap<String, String>> ans = NCBIInformation
					.fetch(list);
			for (String string : ans.keySet()) {
				database.put(string, ans.get(string));
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (attempts > 0) {
				retFromServer(set, attempts - 1);
			}
		}
	}
}
