package bgu.bio.util;

import gnu.trove.map.hash.TObjectFloatHashMap;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import bgu.bio.util.alphabet.AlphabetUtils;

/**
 * Scoring Matrix class. the matrix can be load from file or set manually.
 */
public class ScoringMatrix {

	/** The alphabet of symbols in the matrix. */
	protected AlphabetUtils alphabet;

	/** The table with the scores. */
	protected float[][] table;

	/** The minimum value in the table. */
	protected float minValue;

	/** The max value in the table. */
	protected float maxValue;

	protected TObjectFloatHashMap<String> specialValuesMap;

	/**
	 * Instantiates a new scoring matrix from a file.
	 * 
	 * @param fileName
	 *            the file name
	 * @param alphabet
	 *            the alphabet
	 */
	public ScoringMatrix(String fileName, AlphabetUtils alphabet) {
		this(alphabet);
		try {
			this.loadData(new FileInputStream(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public ScoringMatrix(InputStream inputStream, AlphabetUtils alphabet) {
		this(alphabet);
		this.loadData(inputStream);
	}

	/**
	 * Instantiates a new scoring matrix.
	 * 
	 * @param alphabet
	 *            the alphabet
	 */
	public ScoringMatrix(AlphabetUtils alphabet) {
		this(alphabet.size());
		this.alphabet = alphabet;
	}

	/**
	 * Instantiates a new scoring matrix using only size. this is a good choice
	 * if you don't have the alphabet. Although it means that you can only use
	 * the {@link #score(char, char)} method.
	 * 
	 * @param size
	 *            the size
	 */
	public ScoringMatrix(int size) {
		this.table = new float[size][size];
		this.minValue = Float.POSITIVE_INFINITY;
		this.maxValue = Float.NEGATIVE_INFINITY;
		this.specialValuesMap = new TObjectFloatHashMap<String>();
	}

	public ScoringMatrix(ScoringMatrix other) {
		this(other.table.length);
		this.minValue = other.minValue;
		this.maxValue = other.maxValue;
		for (int i = 0; i < other.table.length; i++) {
			for (int j = 0; j < other.table[i].length; j++) {
				this.table[i][j] = other.table[i][j];

			}
		}
		this.alphabet = other.alphabet;
	}

	/**
	 * Load data from the file.
	 * 
	 * @param fileName
	 *            the file name
	 */
	private void loadData(InputStream stream) {
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(stream));
			String headerRow = br.readLine().trim();
			// Skip comments lines or empty lines
			while (headerRow.length() == 0 || headerRow.charAt(0) == '#') {
				headerRow = br.readLine().trim();
			}

			// special line
			while (headerRow.indexOf('=') >= 0) {
				String[] sp = headerRow.split("\\s*=\\s*", 1);
				this.specialValuesMap.put(sp[0].trim().toLowerCase(),
						Float.parseFloat(sp[1].trim()));
				headerRow = br.readLine().trim();
			}

			StringTokenizer tok1 = new StringTokenizer(headerRow);
			while (tok1.hasMoreElements()) {
				char char1 = this.alphabet.map((tok1.nextToken()));
				// Read a new line for tok1 from file
				StringTokenizer tok2 = new StringTokenizer(headerRow);
				final String line = br.readLine().trim();
				StringTokenizer tok3 = new StringTokenizer(line);

				if (!tok3.hasMoreElements())
					break;
				tok3.nextToken();
				while (tok2.hasMoreElements()) {
					char char2 = this.alphabet.map(tok2.nextToken());
					float d = (new Float(tok3.nextToken())).floatValue();
					setScore(char1, char2, d);
				}

			}

			// read the special chars
			String line = br.readLine();
			while (line != null) {
				if (line.indexOf('=') >= 0) {
					String[] sp = line.trim().split("\\s*=\\s*", 2);
					this.specialValuesMap.put(sp[0].trim().toLowerCase(),
							Float.parseFloat(sp[1].trim()));
				}
				line = br.readLine();
			}
			br.close();

		} catch (IOException e) {
			System.out.println(e);
			System.exit(1);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String ans = " ";
		for (int i = 0; i < this.alphabet.size(); i++) {
			ans += " " + this.alphabet.letters()[i];
		}
		ans += "\n";
		for (int i = 0; i < this.alphabet.size(); i++) {
			ans += this.alphabet.letters()[i] + " ";
			for (int j = 0; j < this.alphabet.size(); j++) {
				ans += " "
						+ this.score(this.alphabet.letters()[i],
								this.alphabet.letters()[j]);
			}
			ans += "\n";
		}
		return ans;
	}

	/**
	 * Sets the score. this method also updates the {@link #minValue} and
	 * {@link #maxValue} values if needed
	 * 
	 * @param c1
	 *            the c1
	 * @param c2
	 *            the c2
	 * @param score
	 *            the score to set for c1 and c2
	 */
	public void setScore(int c1, int c2, float score) {
		this.table[c1][c2] = score;

		// Setting the minimum and the maximum
		if (score > this.maxValue)
			this.maxValue = score;
		if (score < minValue) {
			this.minValue = score;
		}
	}

	public void setScore(int c1, int c2, double score) {
		this.setScore(c1, c2, (float) score);
	}

	/**
	 * Sets the score.
	 * 
	 * @param c1
	 *            the c1
	 * @param c2
	 *            the c2
	 * @param score
	 *            the score
	 */
	public void setScore(char c1, char c2, float score) {
		this.setScore(this.alphabet.encode(c1), this.alphabet.encode(c2), score);
	}

	public void setScore(char c1, char c2, double score) {
		this.setScore(this.alphabet.encode(c1), this.alphabet.encode(c2), score);
	}

	/**
	 * Score.
	 * 
	 * @param c1
	 *            the c1
	 * @param c2
	 *            the c2
	 * @return the double
	 */
	public float score(short c1, short c2) {
		return this.table[c1][c2];
	}

	/**
	 * Score.
	 * 
	 * @param c1
	 *            the c1
	 * @param c2
	 *            the c2
	 * @return the double
	 */
	public float score(char c1, char c2) {
		return score(this.alphabet.encode(c1), this.alphabet.encode(c2));
	}

	/**
	 * Gets the alphabet.
	 * 
	 * @return the alphabet
	 */
	public AlphabetUtils getAlphabet() {
		return alphabet;
	}

	/**
	 * Sets the alphabet.
	 * 
	 * @param alphabet
	 *            the new alphabet
	 */
	public void setAlphabet(AlphabetUtils alphabet) {
		this.alphabet = alphabet;
	}

	/**
	 * Gets the min value.
	 * 
	 * @return the min value
	 */
	public double getMinValue() {
		return minValue;
	}

	/**
	 * Gets the max value.
	 * 
	 * @return the max value
	 */
	public double getMaxValue() {
		return maxValue;
	}

	/**
	 * Calls {@link #getSpecialValue(String, double)} with default value set as
	 * Double.NaN
	 * 
	 * @param key
	 *            the key
	 * @return the special value
	 */
	public double getSpecialValue(String key) {
		return this.getSpecialValue(key, Float.NaN);
	}

	/**
	 * Return a special value for a given key. If the value doesn't exist in the
	 * table the returned value is the given default value.
	 * 
	 * @param key
	 *            the string representing the key
	 * @param defaultValue
	 *            the default value to be given if the key isn't found
	 * @return the matching key value, if the key is not in the map the returned
	 *         value is set to defaultValue
	 */
	public float getSpecialValue(String key, float defaultValue) {
		if (this.specialValuesMap.contains(key)) {
			return this.specialValuesMap.get(key);
		}
		return defaultValue;
	}

	/**
	 * indicate whether two letters are considered as a match with respect to
	 * the scoring scheme.
	 * 
	 * @param c1
	 *            the c1
	 * @param c2
	 *            the c2
	 * @return boolean true iff c1 and c2 do match
	 */
	public boolean match(char c1, char c2) {
		throw new UnsupportedOperationException("match");
	}

	public ScoringMatrix cloneMatrix() {
		return new ScoringMatrix(this);
	}

}
