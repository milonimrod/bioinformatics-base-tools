package bgu.bio.util;

import bgu.bio.util.alphabet.AlphabetUtils;

/**
 * Scoring Matrix LCS class. 
 */
public class ScoringMatrixLCS extends ScoringMatrix {

	/**
	 * Instantiates a new scoring matrix from a file.
	 * @param fileName
	 *            the file name
	 * @param alphabet
	 *            the alphabet
	 */
	public ScoringMatrixLCS(String fileName, AlphabetUtils alphabet) {
		super(fileName, alphabet);
	}

	/**
	 * Instantiates a new scoring matrix.
	 * @param alphabet
	 *            the alphabet
	 */
	public ScoringMatrixLCS(AlphabetUtils alphabet) {
		super(alphabet);
	}

	/**
	 * Instantiates a new scoring matrix using only size. this is a good choice
	 * if you don't have the alphabet. Although it means that you can only use
	 * the {@link #score(char, char)} method.
	 * @param size
	 *            the size
	 */
	public ScoringMatrixLCS(int size) {
		super(size);
	}

	/**
	 * indicate whether two letters are considered as a match with respect to the
	 * scoring scheme.
	 * @param c1
	 *            the c1
	 * @param c2
	 *            the c2
	 * @return boolean true iff c1 and c2 do match
	 */
	public boolean match(char c1, char c2) {
		return c1==c2;
	}

}
