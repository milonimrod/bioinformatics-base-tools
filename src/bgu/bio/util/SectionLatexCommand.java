package bgu.bio.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SectionLatexCommand {
	private static String LINE_SEP = System.getProperty("line.separator");

	public static int[] section(String input, int start, String... sections) {
		int[] ans = new int[sections.length + 1];
		ans[0] = start;
		int indInAns = 1;
		int posInInput = start;
		for (int i = 0; i < sections.length; i++) {
			char open = sections[i].charAt(0);
			char close = sections[i].charAt(1);
			int counter = 0;
			do {
				if (input.charAt(posInInput) == open) {
					counter++;
				} else if (input.charAt(posInInput) == close) {
					counter--;
				}
				posInInput++;
			} while (counter != 0);

			ans[indInAns] = posInInput;
			indInAns++;
		}
		return ans;
	}

	private static String clean(String input, String text, String[] secs) {
		StringBuilder sb = new StringBuilder();
		int lastEnd = 0;
		int indexOf = input.indexOf(text, lastEnd);
		while (indexOf != -1) {
			// copy the data before
			sb.append(input, lastEnd, indexOf);
			indexOf = indexOf + text.length() - 1;
			int[] sections = section(input, indexOf, secs);
			if (sections[sections.length-2] + 1 != sections[sections.length-1]) {
				// not empty
				sb.append(input, sections[sections.length-2] + 1, sections[sections.length-1] - 1);
			}
			lastEnd = sections[sections.length-1];
			indexOf = input.indexOf(text, lastEnd);
		}
		sb.append(input, lastEnd, input.length());
		return sb.toString();
	}

	public static String clean(String input) {
		String c1 = clean(input, "\\changea[", new String[] { "[]", "{}", "{}",
				"{}" });
		String c2 = clean(c1, "\\changea{", new String[] { "{}", "{}", "{}" });
		String c3 = clean(c2, "\\change[", new String[] { "[]", "{}", "{}",
				"{}" });
		String c4 = clean(c3, "\\change{", new String[] { "{}", "{}", "{}" });
		return c4;
	}

	public static void clean(File inputFile, File outputFile)
			throws IOException {
		FileReader readerFile = new FileReader(inputFile);
		BufferedReader reader = new BufferedReader(readerFile);
		String line = reader.readLine();
		StringBuilder builder = new StringBuilder();
		while (line != null) {
			builder.append(line);
			builder.append(LINE_SEP);
			line = reader.readLine();
		}
		reader.close();
		String ans = clean(builder.toString());
		FileWriter fileWriter = new FileWriter(outputFile);
		BufferedWriter writer = new BufferedWriter(fileWriter);
		writer.write(ans);
		writer.close();
	}

	public static void clean(File inputFile) throws IOException {
		clean(inputFile, inputFile);
	}

	public static void main(String[] args) throws IOException {
		for (int i = 0; i < args.length; i++) {
			System.out.println(args[i]);
			clean(new File(args[i]));
		}
	}
}
