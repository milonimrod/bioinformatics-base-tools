package bgu.bio.util.alphabet;

import java.util.Arrays;
import java.util.Random;

public abstract class AlphabetUtils {
	protected short[] rnaChar2Bits;
	protected char[] rnaComplement;
	protected char[] rnaWobble;
	protected boolean[] hasRnaWobble;
	protected boolean[] hasComplement;
	protected int size;
	protected char[] letters;
	private boolean canPair[][];

	protected AlphabetUtils(char[] letters, int[][] complements,
			int[][] wobbles, char[][] synonymous) {
		this(letters, complements, wobbles, synonymous, true);
	}

	protected AlphabetUtils(char[] letters, int[][] complements,
			int[][] wobbles, char[][] synonymous, boolean caseInSensitive) {
		this.letters = letters;
		this.size = letters.length;
		// initiate encoding tables
		rnaChar2Bits = new short[256];
		rnaComplement = new char[256];
		rnaWobble = new char[256];
		hasRnaWobble = new boolean[256];
		hasComplement = new boolean[256];

		Arrays.fill(rnaChar2Bits, (short) -1);
		Arrays.fill(hasRnaWobble, false);
		Arrays.fill(hasComplement, false);

		for (int i = 0; i < rnaChar2Bits.length; i++) {
			rnaChar2Bits[i] = -1;
			rnaComplement[i] = 0;
			rnaWobble[i] = 0;
		}

		// set table values
		for (short i = 0; i < letters.length; i++) {
			rnaChar2Bits[letters[i]] = i;
			if (caseInSensitive) {
				rnaChar2Bits[Character.toUpperCase(letters[i])] = i;
				rnaChar2Bits[Character.toLowerCase(letters[i])] = i;
			}
		}

		// set the complements
		for (int i = 0; i < complements.length; i++) {
			hasComplement[letters[complements[i][0]]] = true;
			hasComplement[letters[complements[i][1]]] = true;

			rnaComplement[letters[complements[i][0]]] = letters[complements[i][1]];
			rnaComplement[letters[complements[i][1]]] = letters[complements[i][0]];

			if (caseInSensitive) {
				rnaComplement[Character.toUpperCase(letters[complements[i][0]])] = letters[complements[i][1]];
				rnaComplement[Character.toUpperCase(letters[complements[i][1]])] = letters[complements[i][0]];
				rnaComplement[Character.toLowerCase(letters[complements[i][0]])] = letters[complements[i][1]];
				rnaComplement[Character.toLowerCase(letters[complements[i][1]])] = letters[complements[i][0]];
			}
		}

		// fix wobbles
		for (int i = 0; i < wobbles.length; i++) {
			hasRnaWobble[letters[wobbles[i][0]]] = true;

			if (caseInSensitive) {
				hasRnaWobble[Character.toUpperCase(wobbles[i][0])] = true;
				hasRnaWobble[Character.toLowerCase(wobbles[i][0])] = true;
			}

			rnaWobble[letters[wobbles[i][0]]] = letters[wobbles[i][1]];

			if (caseInSensitive) {
				rnaWobble[Character.toUpperCase(wobbles[i][0])] = letters[wobbles[i][1]];
				rnaWobble[Character.toLowerCase(wobbles[i][0])] = letters[wobbles[i][1]];
			}

			hasRnaWobble[letters[wobbles[i][1]]] = true;

			if (caseInSensitive) {
				hasRnaWobble[Character.toUpperCase(wobbles[i][1])] = true;
				hasRnaWobble[Character.toLowerCase(wobbles[i][1])] = true;
			}

			rnaWobble[letters[wobbles[i][1]]] = letters[wobbles[i][0]];

			if (caseInSensitive) {
				rnaWobble[Character.toUpperCase(wobbles[i][1])] = letters[wobbles[i][0]];
				rnaWobble[Character.toLowerCase(wobbles[i][1])] = letters[wobbles[i][0]];
			}
		}

		// fix synonymous
		for (int i = 0; i < synonymous.length; i++) {
			rnaChar2Bits[Character.toLowerCase(synonymous[i][0])] = rnaChar2Bits[synonymous[i][1]];
			rnaChar2Bits[Character.toUpperCase(synonymous[i][0])] = rnaChar2Bits[synonymous[i][1]];
			hasRnaWobble[Character.toLowerCase(synonymous[i][0])] = hasRnaWobble[synonymous[i][1]];
			hasRnaWobble[Character.toUpperCase(synonymous[i][0])] = hasRnaWobble[synonymous[i][1]];
			rnaWobble[Character.toLowerCase(synonymous[i][0])] = rnaWobble[synonymous[i][1]];
			rnaWobble[Character.toUpperCase(synonymous[i][0])] = rnaWobble[synonymous[i][1]];
			rnaComplement[Character.toLowerCase(synonymous[i][0])] = rnaComplement[synonymous[i][1]];
			rnaComplement[Character.toUpperCase(synonymous[i][0])] = rnaComplement[synonymous[i][1]];
		}

		// init canPair
		canPair = new boolean[size][size];
		for (int i = 0; i < size; i++) {
			// sets all to "false"
			for (int j = i + 1; j < size; j++) {
				if (i != j) {
					if ((rnaComplement[letters[i]] != 0 && rnaComplement[letters[i]] == letters[j])
							|| (rnaWobble[letters[i]] != 0 && rnaWobble[letters[i]] == letters[j])) {
						canPair[i][j] = true;
						canPair[j][i] = true;
					}
				}
			}
		}
	}

	public char decode(int c) {
		return letters[c];
	}

	public final short encode(char c) {
		return rnaChar2Bits[c];
	}

	public char getLetter(char c) {
		return letters[rnaChar2Bits[c]];
	}

	public int hash(String sequence) {
		return hash(sequence.toCharArray());
	}

	public int hash(char[] sequence) {
		return hash(sequence, 0, sequence.length);
	}

	public int hash(char[] sequence, int offset, int length) {
		int ans = 0;// , chrNum = -1;
		for (int i = 0; i < length; i++) {
			ans = ans * size + rnaChar2Bits[sequence[i + offset]];
		}
		return ans;
	}

	public int hash(char[] sequence, int offset, int length, int hashOfLast) {
		int ans = size
				* (hashOfLast - rnaChar2Bits[sequence[offset - 1]]
						* (int) Math.pow(size, length - 1));
		ans += rnaChar2Bits[sequence[offset + length - 1]];
		return ans;
	}

	public int size() {
		return this.size;
	}

	public char[] letters() {
		return letters;
	}

	public char complement(char c) {
		return rnaComplement[c];
	}

	public short complement(short c) {
		return this.encode(complement(this.decode(c)));
	}

	public boolean hasWobble(char c) {
		return hasRnaWobble[c];
	}

	public boolean hasComplement(char c) {
		return hasComplement[c];
	}

	public boolean hasComplement(int c) {
		return hasComplement(this.decode(c));
	}

	public char wobble(char c) {
		return rnaWobble[c];
	}

	public char[] unHash(int num) {
		if (num < size)
			return new char[] { letters[num] };

		int sizeOfArray = 1 + (int) Math.floor(Math.log(num) / Math.log(size));
		char[] ans = new char[sizeOfArray];
		int i = sizeOfArray - 1;
		while (num != 0) {
			ans[i] = letters[num % size];
			num = num / size;
			i--;
		}
		return ans;
	}

	public final boolean canPair(char i1, char i2) {
		return canPair[this.encode(i1)][this.encode(i2)];
	}

	public final boolean canPair(int i1, int i2) {
		return canPair[i1][i2];
	}

	public void unHash(int num, char[] arr) {
		if (num < size) {
			arr[arr.length - 1] = letters[num];
			for (int i = 0; i < arr.length - 1; i++) {
				arr[i] = letters[0];
			}
			return;
		}

		int i = arr.length - 1;
		while (num != 0) {
			arr[i] = letters[num % size];
			num = num / size;
			i--;
		}
		while (i >= 0) {
			arr[i] = letters[0];
			i--;
		}
	}

	public final boolean isLegal(char c) {
		return rnaChar2Bits[c] != -1;
	}

	public char nonComplement() {
		return 'n';
	}

	public abstract char map(String str);

	public abstract char emptyLetter();

	public abstract short emptyLetterHashed();

	public void randomSequence(char[] seq, int limit, Random rand) {

		for (int i = 0; i < limit; i++) {
			seq[i] = this.letters[rand.nextInt(this.letters.length - 1)];
		}
	}
}
