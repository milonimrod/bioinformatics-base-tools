package bgu.bio.util.alphabet;

import gnu.trove.list.array.TCharArrayList;
/** This is an alignment alphabet of pairs of letters from the string alphabet. */

public class RNABasePairAlphabet extends AlphabetUtils{
	
	private static RNABasePairAlphabet SINGELTON;
	private static RnaAlphabet baseAlphabet = RnaAlphabet.getInstance();
	private static char[][] map = new char[baseAlphabet.size()][baseAlphabet.size()];	
	private static char[] bases;
	
	static{
		final int size = (baseAlphabet.size()-1) * (baseAlphabet.size() - 2) / 2 + baseAlphabet.size();
		bases = new char[size];
		char start = 'a';
		int pos = 0;
		char[] arr = baseAlphabet.letters();
		for (int i=0;i<arr.length-1;i++){
			for (int j=i;j<arr.length-1;j++){
				bases[pos] = (char)(start + pos);
				map[baseAlphabet.encode(arr[i])][baseAlphabet.encode(arr[j])] = bases[pos];
				map[baseAlphabet.encode(arr[j])][baseAlphabet.encode(arr[i])] = bases[pos];
				
				pos++;
			}
		}
		bases[pos] = (char)(start + pos);
		map[baseAlphabet.emptyLetterHashed()][baseAlphabet.emptyLetterHashed()] = bases[pos];		
		pos++;
	}
	
	public static final int SIZE = bases.length;

	public static final char GAP = bases[bases.length - 1];
	private final short ENCODED_GAP;
	
	private boolean[] special;	
	
	private RNABasePairAlphabet()
	{
		super(bases,new int[0][0],new int[0][0],new char[0][0]);
		
		//mark "special" characters
		this.special = new boolean[RNABasePairAlphabet.SIZE];		
		
		ENCODED_GAP = this.encode(GAP);
	}
	
	public static RNABasePairAlphabet getInstance()
	{
		if (SINGELTON == null)
			SINGELTON = new RNABasePairAlphabet();
		
		return SINGELTON;
	}
	
	@Override
	public char complement(char c) {
		throw new UnsupportedOperationException("");
	}
	
	@Override
	public boolean hasWobble(char c)
	{
		throw new UnsupportedOperationException("");
	}
	
	@Override
	public char wobble(char c)
	{
		throw new UnsupportedOperationException("");
	}

	@Override
	public char nonComplement() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public char map(String str){
		return map(str.replace("|","").replace("_","__").toCharArray());
	}
	
	public short mapAndHash(String str){
		return this.encode(this.map(str));
	}
	
	public char map(char[] arr){
		return map[baseAlphabet.encode(arr[0])][baseAlphabet.encode(arr[1])];
	}
	
	public char map(TCharArrayList arr){
		return map[baseAlphabet.encode(arr.get(0))][baseAlphabet.encode(arr.get(1))];
	}
	
	public short mapAndHash(char[] arr){
		return this.encode(this.map(arr));
	}
	
	public short mapAndHash(TCharArrayList arr){
		return this.encode(this.map(arr));
	}
	
	@Override
	public char emptyLetter() {
		return GAP;
	}
	
	@Override
	public short emptyLetterHashed() {
		return this.ENCODED_GAP;
	}
	
	public final boolean isSpecial(short chr){
		return this.special[chr];
	}
}
