package bgu.bio.util.alphabet;

import bgu.bio.util.CharBuffer;

public class RnaAlphabet extends AlphabetUtils {

	public static char A = 0;
	public static char C = 1;
	public static char G = 2;
	public static char U = 3;
	public static char N = 4;
	public static char E = 5;

	private static RnaAlphabet SINGELTON;

	private static final char[] rnaLetters = new char[] { 'A', 'C', 'G', 'U',
			'N', '_' };
	private static final int[][] complements = new int[][] { { 0, 3 },
			{ 1, 2 }, { 4, 4 } };
	private static final int[][] wobbles = new int[][] { { 2, 3 } };
	private static final char[][] synonymous = new char[][] { { 'T', 'U' },
			{ 'Y', 'N' }, { 'R', 'N' }, { 'W', 'N' }, { 'S', 'N' },
			{ 'M', 'N' }, { 'K', 'N' }, { 'H', 'N' }, { 'B', 'N' },
			{ 'V', 'N' }, { 'D', 'N' }, { 'I', 'N' }, { 'P', 'N' } };
	public static final int SIZE = rnaLetters.length;

	private final char EMPTY_LETTER;
	private final short EMPTY_LETTER_HASHED;

	private RnaAlphabet() {
		super(rnaLetters, complements, wobbles, synonymous, true);
		EMPTY_LETTER = '_';
		EMPTY_LETTER_HASHED = encode(EMPTY_LETTER);
	}

	public static RnaAlphabet getInstance() {
		if (SINGELTON == null)
			SINGELTON = new RnaAlphabet();

		return SINGELTON;
	}

	@Override
	public char nonComplement() {
		return 'N';
	}

	@Override
	public char map(String str) {
		return str.charAt(0);
	}

	@Override
	public char emptyLetter() {
		return EMPTY_LETTER;
	}

	@Override
	public short emptyLetterHashed() {
		return EMPTY_LETTER_HASHED;
	}

	public void convertToRna(CharBuffer charBuffer) {
		for (int i = 0; i < charBuffer.length(); i++) {
			charBuffer.setChar(i, decode(encode(charBuffer.charAt(i))));
		}
	}
}