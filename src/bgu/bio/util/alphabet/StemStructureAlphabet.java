package bgu.bio.util.alphabet;

import java.util.Arrays;

public class StemStructureAlphabet extends AlphabetUtils {

	private static StemStructureAlphabet SINGELTON;

	private char[][] map = new char[128][128];
	protected char[][] mapKeys = new char[][] { { 'A', 'A' }, { 'A', 'C' },
			{ 'A', 'G' }, { 'A', 'U' }, { 'C', 'A' }, { 'C', 'C' },
			{ 'C', 'G' }, { 'C', 'U' }, { 'G', 'A' }, { 'G', 'C' },
			{ 'G', 'G' }, { 'G', 'U' }, { 'U', 'A' }, { 'U', 'C' },
			{ 'U', 'G' }, { 'U', 'U' }, { 'B', 'R' }, { 'B', 'L' },
			{ 'I', 'N' }, { '_', '_' } };

	private static char[] bases = new char[] { '1', '2', '3', 'a', '4', '5',
			'c', '6', '7', 'g', '8', 'w', 'z', '9', 'b', '0', 'r', 'l', 'i',
			'_' };
	public static final int SIZE = bases.length;

	public static final char INNER_LOOP = bases[18];
	public static final char BULDGE_LEFT = bases[17];
	public static final char BULDGE_RIGHT = bases[16];
	public static final char GAP = bases[19];
	private final short ENCODED_GAP;

	private boolean[] special;

	private StemStructureAlphabet() {
		super(bases, new int[0][0], new int[0][0], new char[0][0]);

		// set table values
		for (int i = 0; i < bases.length; i++) {
			map[mapKeys[i][0]][mapKeys[i][1]] = bases[i];

			// if contains U then switch to T and enter the same values

			for (int c = 0; c < mapKeys[i].length; c++) {
				if (mapKeys[i][c] == 'U') {
					char[] temp = Arrays.copyOf(mapKeys[i], mapKeys[i].length);
					temp[c] = 'T';
					map[temp[0]][temp[1]] = bases[i];
				}

			}
		}

		// mark "special" characters
		this.special = new boolean[StemStructureAlphabet.SIZE];
		this.special[this.encode(StemStructureAlphabet.BULDGE_LEFT)] = true;
		this.special[this.encode(StemStructureAlphabet.INNER_LOOP)] = true;
		this.special[this.encode(StemStructureAlphabet.BULDGE_RIGHT)] = true;

		ENCODED_GAP = this.encode(GAP);
	}

	public static StemStructureAlphabet getInstance() {
		if (SINGELTON == null)
			SINGELTON = new StemStructureAlphabet();

		return SINGELTON;
	}

	@Override
	public char complement(char c) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public boolean hasWobble(char c) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public char wobble(char c) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public char nonComplement() {
		throw new UnsupportedOperationException();
	}

	@Override
	public char map(String str) {
		return map(str.replace("|", "").replace("_", "__").toCharArray());
	}

	public short mapAndHash(String str) {
		return this.encode(this.map(str));
	}

	public char map(char[] arr) {
		return map(arr[0], arr[1]);
	}

	public char map(char c1, char c2) {
		return map[Character.toUpperCase(c1)][Character.toUpperCase(c2)];
	}

	public short mapAndHash(char[] arr) {
		return this.encode(this.map(arr));
	}

	@Override
	public char emptyLetter() {
		return GAP;
	}

	@Override
	public short emptyLetterHashed() {
		return this.ENCODED_GAP;
	}

	public final boolean isSpecial(char c) {
		return this.isSpecial(encode(c));
	}

	public final boolean isSpecial(short chr) {
		return this.special[chr];
	}

	public char[] decodePair(char c) {
		return this.mapKeys[encode(c)];
	}

	public char[] decodePair(short chr) {
		return this.mapKeys[chr];
	}
}
