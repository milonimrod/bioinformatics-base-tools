package bgu.bio.util.alphabet.constrain;

import bgu.bio.util.alphabet.AlphabetUtils;

/**
 * Constrained alphabet is an alphabet for the constraint itself, 
 * It has a mapping of two letters from the Sequence {@link Alphabet}
 */
public abstract class ConstrainedAlphabet extends AlphabetUtils {
	
	/** The mapping table for mapping two letters in the sequence {@link Alphabet} (first two dimensions) to a letter in the constraint alphabet (the third dimension). 
	 * the value of a letter is true iff the two letters are mapped to the letter
	 */
	protected boolean[][][] mapTable;
	
	/** The sequence {@link Alphabet} to be used in this alphabet. */
	protected AlphabetUtils stringAlphabet;
	
	/** The map vectors. for each two {@link Alphabet} letters it saves an array of corresponding letters. 
	 * (it is a minimal and specific representation of the {@link #mapTable} that keep's only the letters with true value*/
	protected int[][][] mapVectors;

	
	/**
	 * Instantiates a new constrained alphabet.
	 * 
	 * @param letters the letters in the constraint alphabet
	 * @param alphabet the sequence {@link Alphabet} that is attached to this {@link ConstrainedAlphabet} 
	 */
	public ConstrainedAlphabet(char[] letters, int[][] complements,
			int[][] wobbles, char[][] synonymous,AlphabetUtils alphabet) {
		
		super(letters, complements, wobbles, synonymous);
		this.stringAlphabet = alphabet;
		
		this.mapTable = new boolean[this.stringAlphabet.size()][this.stringAlphabet.size()][this.size];

		this.load();
		this.buildMappingVectors();
	}
	
	/**
	 * Instantiates a new constrained alphabet.
	 * 
	 * @param letters the letters in the constraint alphabet
	 * @param alphabet the sequence {@link Alphabet} that is attached to this {@link ConstrainedAlphabet} 
	 */
	public ConstrainedAlphabet(char[] letters, AlphabetUtils alphabet) {
		
		super(letters,new int[0][0],new int[0][0],new char[0][0]);
		this.stringAlphabet = alphabet;
		
		this.mapTable = new boolean[this.stringAlphabet.size()][this.stringAlphabet.size()][this.size];

		this.load();
		this.buildMappingVectors();
	}
	
	/**
	 * Builds the mapping vectors. this method should be called after the {@link ConstrainedAlphabet#load()} method
	 */
	protected void buildMappingVectors()
	{
		this.mapVectors = new int[this.stringAlphabet.size()][this.stringAlphabet.size()][];
		for (int i=0;i<this.stringAlphabet.size();i++)
		{
			for (int j=0;j<this.stringAlphabet.size();j++)
			{
				int counter = 0;
				boolean[] array = this.mapTable[i][j];
				for (int x=0;x<array.length;x++)
				{
					if (array[x])
						counter++;
				}
				int[] vec = new int[counter];
				counter=0;
				for (int x=0;x<array.length;x++)
				{
					if (array[x]){
						vec[counter] = x;
						counter++;
					}
				}
				this.mapVectors[i][j] = vec;
			}
		}
	}

	/**
	 * Defines how to build the {@link #mapTable} for this alphabet
	 */
	public abstract void load();

	/**
	 * Check if c1,c2 ({@link Alphabet} letters) are mapped to c3 ({@link ConstrainedAlphabet} letter).
	 * 
	 * @param c1 the c1 {@link Alphabet} letter
	 * @param c2 the c2 {@link Alphabet} letter
	 * @param c3 the c3 {@link ConstrainedAlphabet} letter
	 * 
	 * @return true, iff c1,c2 ({@link Alphabet} letters) are mapped to c3
	 */
	public boolean map(char c1,char c2,char c3)
	{
		return this.map(this.stringAlphabet.encode(c1), this.stringAlphabet.encode(c2), this.encode(c3));
	}

	/**
	 * The same as {@link #map(char, char, char)} only with hashed values
	 * 
	 * @param c1 the c1 {@link Alphabet} hashed letter
	 * @param c2 the c2 {@link Alphabet} hashed letter
	 * @param c3 the c3 {@link ConstrainedAlphabet} hashed letter
	 * 
	 * @return true, iff c1,c2 ({@link Alphabet} letters) are mapped to c3
	 */
	public boolean map(int c1,int c2,int c3)
	{
		return this.mapTable[c1][c2][c3];
	}
	
	/**
	 * Gets the char mapping for two given characters from the sequence {@link Alphabet}.
	 * 
	 * @param c1 a given hashed character from {@link Alphabet}
	 * @param c2 a given hashed character from {@link Alphabet}
	 * 
	 * @return an array of hashed characters of the {@link ConstrainedAlphabet} such that this hashed characters correspond to the input characters
	 */
	public int[] getCharMapping(int c1,int c2)
	{
		return this.mapVectors[c1][c2];
	}
	
	
	/**
	 * Gets the char mapping for two given characters from the sequence {@link Alphabet}.
	 * 
	 * @param c1 a given character from {@link Alphabet}
	 * @param c2 a given character from {@link Alphabet}
	 * 
	 * @return an array of hashed characters of the {@link ConstrainedAlphabet} such that this hashed characters correspond to the input characters
	 */
	public int[] getCharMapping(char c1,char c2)
	{
		return this.mapVectors[stringAlphabet.encode(c1)][stringAlphabet.encode(c2)];
	}
	
	
	public String lettersToString()
	{
		return new String(this.letters);
	}
	
	/**
	 * Parses the given string and replace complex characters (more then one char) into one character
	 * 
	 * @param input the input string to be parsed
	 * 
	 * @return the input string parsed to be a legal string
	 */
	public abstract String parse(String input);
	
	
}
