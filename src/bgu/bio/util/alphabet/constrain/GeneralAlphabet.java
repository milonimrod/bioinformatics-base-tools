package bgu.bio.util.alphabet.constrain;

import bgu.bio.util.alphabet.AlphabetUtils;

public class GeneralAlphabet extends ConstrainedAlphabet {
	
	public GeneralAlphabet(AlphabetUtils stringAlphabet) {
		super(new char[]{'0','1'}, stringAlphabet);  
	}

	@Override
	public void load() {
		for (int i = 0; i < this.stringAlphabet.size(); i++) {
			for (int j = 0; j < this.stringAlphabet.size(); j++) {
				if (i == j)
					this.mapTable[i][j][this.encode('1')] = true;
				else
					this.mapTable[i][j][this.encode('0')] = true;
				//this.mapTable[i][j][this.hash('*')] = true;
			}
		}
		this.mapTable[this.stringAlphabet.emptyLetterHashed()][this.stringAlphabet.emptyLetterHashed()][this.encode('0')] = false;
		this.mapTable[this.stringAlphabet.emptyLetterHashed()][this.stringAlphabet.emptyLetterHashed()][this.encode('1')] = false;
	}

	@Override
	public String parse(String input) {
		return input;
	}

	@Override
	public char emptyLetter() {
		return '_';
	}

	@Override
	public short emptyLetterHashed() {
		return encode(emptyLetter());
	}

	@Override
	public char map(String str) {
		return 0;
	}
}
