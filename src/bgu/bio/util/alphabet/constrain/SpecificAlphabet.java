package bgu.bio.util.alphabet.constrain;

import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bgu.bio.util.alphabet.AlphabetUtils;

public class SpecificAlphabet extends ConstrainedAlphabet {

	private final Pattern pattern;
	private Hashtable<String, String> hashedMapped;
	private static final String CHAR_SEPERATOR = "!";
	private static final String PATTERN = "[\\w]"+CHAR_SEPERATOR+"[\\w]";
	
	public SpecificAlphabet(AlphabetUtils alphabet) {
		super(calculateTheLettersSize(alphabet), alphabet);
		pattern = Pattern.compile(PATTERN);
	}
	
	/**
	 * Calculate the letters in the alphabet.
	 * 
	 * @param alphabet the alphabet
	 * 
	 * @return the letters in this alphabet
	 */
	private static char[] calculateTheLettersSize(AlphabetUtils alphabet){
		final int manualSize = 2; //+2 is for the 1 / 0
		int limit = manualSize + (int)Math.pow(alphabet.size(), 2); 
		char[] ans = new char[limit];
		
		// manual letters 
		ans[limit - 1] = '1';
		ans[limit - 2] = '0';
		
		ans[0] = 'a';
		
		//automatic
		for (int start = 1;start < limit - manualSize;start++)
		{
			ans[start] = (char)(ans[start - 1] + 1);
		}
		
		return ans;
	}

	@Override
	public void load() {
		
		int letterPos = 0;
		this.hashedMapped = new Hashtable<String, String>();
		
		for (short i = 0; i < this.stringAlphabet.size(); i++) {
			for (short j = 0; j < this.stringAlphabet.size(); j++) {
				if (i == j){
					this.mapTable[i][j][this.encode('1')] = true;
				}
				else{
					this.mapTable[i][j][this.encode('0')] = true;
				}
				
				//assign the letterPos letter to the pair i,j
				this.mapTable[i][j][this.encode(this.letters[letterPos])] = true;
		
				//add the pair to the hash table
				this.hashedMapped.put(this.stringAlphabet.decode(i) + CHAR_SEPERATOR + this.stringAlphabet.decode(j),"" + this.letters[letterPos]);
				
				letterPos++;
			}
		}
	}

	@Override
	public String parse(String input) {
		Matcher matcher = pattern.matcher(input);
		StringBuffer sb = new StringBuffer();
		while (matcher.find())
		{
			matcher.appendReplacement(sb, this.hashedMapped.get(matcher.group()));
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	@Override
	public char emptyLetter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public short emptyLetterHashed() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public char map(String str) {
		// TODO Auto-generated method stub
		return 0;
	}
}
