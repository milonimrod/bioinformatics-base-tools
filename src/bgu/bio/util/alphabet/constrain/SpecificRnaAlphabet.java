package bgu.bio.util.alphabet.constrain;

import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

public class SpecificRnaAlphabet extends ConstrainedAlphabet {

	private final Pattern pattern;
	private Hashtable<String, String> hashedMapped;
	public static final String CHAR_SEPERATOR = "!";
	private static final String PATTERN = "([\\w]"+CHAR_SEPERATOR+"[\\w])" + "|(\\*|_)"+CHAR_SEPERATOR+"(\\*|_)";
	
	private static final char INSERTION = 'i'; //'2';
	private static final char DELETION = 'd';//'3';
	private static final char SUBSTITUTION = 's'; //'4';
	
	public SpecificRnaAlphabet() {
		super(calculateTheLettersSize(RnaAlphabet.getInstance()), RnaAlphabet.getInstance());
		pattern = Pattern.compile(PATTERN);
	}
	
	/**
	 * Calculate the letters in the alphabet.
	 * 
	 * @param alphabet the alphabet
	 * 
	 * @return the letters in this alphabet
	 */
	private static char[] calculateTheLettersSize(AlphabetUtils alphabet){
		final int manualSize = 5; //+2 is for the 1 / 0
		int limit = manualSize + (int)Math.pow(alphabet.size(), 2); 
		char[] ans = new char[limit];
		
		// manual letters 
		ans[limit - 1] = '0';
		ans[limit - 2] = '1';
		ans[limit - 3] = INSERTION;
		ans[limit - 4] = DELETION;
		ans[limit - 5] = SUBSTITUTION;
		
		ans[0] = 'a';
		
		//automatic
		for (int start = 1;start < limit - manualSize;start++)
		{
			char ch = (char)(ans[start - 1] + 1);
			while (ch==INSERTION || ch==DELETION || ch == SUBSTITUTION ){
				ch++;
			}
			ans[start] = ch;
		}
		
		return ans;
	}

	protected boolean lettersFits(int i, int j){
		return this.stringAlphabet.decode(i) == this.stringAlphabet.decode(j);
	}
	
	@Override
	public void load() {
		int letterPos = 0;
		this.hashedMapped = new Hashtable<String, String>();
		
		//hash the data for the special chars
		this.hashedMapped.put(this.stringAlphabet.emptyLetter() + CHAR_SEPERATOR + "*","" + INSERTION);
		this.hashedMapped.put("*" + CHAR_SEPERATOR + this.stringAlphabet.emptyLetter(),"" + DELETION);
		this.hashedMapped.put("*" + CHAR_SEPERATOR + "*","" + SUBSTITUTION);
		
		for (byte i = 0; i < this.stringAlphabet.size(); i++) {
			for (short j = 0; j < this.stringAlphabet.size(); j++) {
				
				if (lettersFits( i,  j)){
					this.mapTable[i][j][this.encode('1')] = true;
				}
				else{
					this.mapTable[i][j][this.encode('0')] = true;
					if (this.stringAlphabet.emptyLetter() == this.stringAlphabet.decode(i) && this.stringAlphabet.emptyLetter() != this.stringAlphabet.decode(j))
					{
						this.mapTable[i][j][this.encode(INSERTION)] = true;
					}
					else if (this.stringAlphabet.emptyLetter() != this.stringAlphabet.decode(i) && this.stringAlphabet.emptyLetter() == this.stringAlphabet.decode(j))
					{
						this.mapTable[i][j][this.encode(DELETION)] = true;
					}
					else if (this.stringAlphabet.emptyLetter() != this.stringAlphabet.decode(i) && this.stringAlphabet.emptyLetter() != this.stringAlphabet.decode(j))
					{
						this.mapTable[i][j][this.encode(SUBSTITUTION)] = true;
					}
					
				}
				
				//assign the letterPos letter to the pair i,j
				this.mapTable[i][j][this.encode(this.letters[letterPos])] = true;
				
				//add the pair to the hash table
				this.hashedMapped.put(this.stringAlphabet.decode(i) + CHAR_SEPERATOR + this.stringAlphabet.decode(j),
						"" + this.letters[letterPos]);
				
				letterPos++;
			}
		}
	}

	@Override
	public String parse(String input) {
		Matcher matcher = pattern.matcher(input.toUpperCase());
		StringBuffer sb = new StringBuffer();
		while (matcher.find())
		{
			matcher.appendReplacement(sb, this.hashedMapped.get(matcher.group()));
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
	
	public void printMapTable(){
		for (short i=0;i<stringAlphabet.size();i++){
			System.out.println("," + stringAlphabet.decode(i));
		}
		for (int i=0;i<stringAlphabet.size();i++)
		{
			System.out.print(stringAlphabet.unHash(i));
			for (int j=0;j<stringAlphabet.size();j++)
			{
				System.out.println(",");
				for (int x=0;x<letters.length;x++){
					System.out.print("|" + mapTable[i][j][x]);
				}
			}
			System.out.println();
		}
	}

	@Override
	public char emptyLetter() {
		return 0;
	}

	@Override
	public short emptyLetterHashed() {
		return 0;
	}

	@Override
	public char map(String str) {
		return 0;
	}
}
