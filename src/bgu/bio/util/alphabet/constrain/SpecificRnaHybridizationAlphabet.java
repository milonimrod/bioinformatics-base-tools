package bgu.bio.util.alphabet.constrain;


public class SpecificRnaHybridizationAlphabet extends SpecificRnaAlphabet {

	protected boolean lettersFits(int i, int j){
		return this.stringAlphabet.canPair(this.stringAlphabet.decode(i), this.stringAlphabet.decode(j));
	}
	

}
