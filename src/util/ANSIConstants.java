package util;

public class ANSIConstants {
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public static final String ANSI_BOLD_ON = "\u001B[1m";
	public static final String ANSI_BOLD_OFF = "\u001B[21m";
	public static final String ANSI_UNDERLINE_ON = "\u001B[4m";
	public static final String ANSI_UNDERLINE_OFF = "\u001B[24m";
}
