package util;

public class TimeFormat {
	public static String formatMilliseconds(long time) {
		long left = time;
		int days = (int) (left / 86400000l);
		left = left % 86400000l;
		int hours = (int) (left / 3600000l);
		left = left % 3600000l;
		int minutes = (int) (left / 60000l);
		left = left % 60000l;
		int seconds = (int) (left / 1000l);
		left = left % 1000l;
		if (days != 0) {
			return String.format("%d days %d hours %d minutes %d seconds",
					days, hours, minutes, seconds);
		} else if (hours != 0) {
			return String.format("%d hours %d minutes %d seconds", hours,
					minutes, seconds);
		} else if (minutes != 0) {
			return String.format("%d minutes %d.%d seconds", minutes, seconds,
					left);

		} else {
			return String.format("%d.%d seconds", seconds, left);
		}
	}

	public static void main(String[] args) {
		System.out.println(formatMilliseconds(62300));
	}
}
