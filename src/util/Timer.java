package util;

import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.hash.TObjectIntHashMap;

public class Timer {
	private TObjectIntHashMap<String> keys;
	private TLongArrayList accumulate;
	private TLongArrayList current;

	public Timer() {
		keys = new TObjectIntHashMap<String>();
		accumulate = new TLongArrayList();
		current = new TLongArrayList();
	}

	public int register() {
		this.accumulate.add(0);
		this.current.add(0);
		return accumulate.size() - 1;
	}

	public int register(String name) {
		this.accumulate.add(0);
		this.current.add(0);
		keys.put(name, accumulate.size() - 1);
		return accumulate.size() - 1;
	}

	public void start(String name) {
		this.start(keys.get(name));
	}

	public void start(int id) {
		this.current.set(id, System.currentTimeMillis());
	}

	public void tick(String name) {
		this.tick(keys.get(name));
	}

	public void tick(int id) {
		final long val = this.accumulate.getQuick(id)
				+ (System.currentTimeMillis() - this.current.getQuick(id));
		this.accumulate.set(id, val);
		this.start(id);
	}

}
