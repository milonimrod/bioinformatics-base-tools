package bgu.bio.adt;

import org.junit.Assert;
import org.junit.Test;

public class TestTrie {

	@Test
	public void test1() {
		Trie t = new Trie();
		t.add("amy");
		t.add("ann");
		t.add("emma");
		t.add("emmy");
		t.add("rob");
		t.add("roger");
		t.add("e");
		Assert.assertEquals(false, t.find("em"));
		Assert.assertEquals(true, t.find("ann"));
		Assert.assertEquals(false, t.find("anni"));
		Assert.assertEquals(false, t.find("r"));
		Assert.assertEquals(true, t.find("rob"));
		Assert.assertEquals(false, t.find("f"));
	}
	
	

}
