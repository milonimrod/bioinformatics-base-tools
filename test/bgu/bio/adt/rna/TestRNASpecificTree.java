package bgu.bio.adt.rna;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.util.MatrixUtils;

public class TestRNASpecificTree {
	// test build from vienna
	@Test
	public void testStructureSimple1() {
		int[][] correct = new int[][] { { 1 }, { 0, 2, 3, 17 }, { 1 },
				{ 1, 4 }, { 3, 5 }, { 4, 6, 10, 11, 16 }, { 5, 7 }, { 6, 8 },
				{ 7, 9 }, { 8 }, { 5 }, { 5, 12 }, { 11, 13 }, { 12, 14 },
				{ 13, 15 }, { 14 }, { 5 }, { 1 } };

		RNASpecificTree tree = new RNASpecificTree();
		tree.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUUUCAUCUUAA".toCharArray(),
				"...((((...))...(((...)))..))....".toCharArray());

		int[][] ans = tree.getEdges();
		Assert.assertEquals("The structure is not the same as expected", true,
				MatrixUtils.equals(ans, correct));
	}

	@Test
	public void testGroups() {

		RNASpecificTree tree = new RNASpecificTree();
		tree.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUUUCAUCUUAA".toCharArray(),
				"...((((...))...(((...)))..))....".toCharArray());
		tree.groupByStructure();
		try {
			tree.toDotFile("/home/milon/tmp/1.dot", true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int t = 0; t < tree.getNodeNum(); t++) {
			int deg = tree.outDeg(t);
			RNANodeLabel tLabel = (RNANodeLabel) tree.getLabel(t);
			for (int d = 0; d < deg; d++) {
				int v = tree.getNeighbor(t, d);
				RNANodeLabel vLabel = (RNANodeLabel) tree.getLabel(v);
				if ((tLabel.getType() == RNANodeLabel.BASE_PAIR && vLabel
						.getType() == RNANodeLabel.BASE_PAIR)) {
					Assert.assertEquals("Should have the same group", true,
							tLabel.getGroup() == vLabel.getGroup());
				}
			}
		}
	}

	@Test
	public void testEquals() {
		RNASpecificTree tree1 = new RNASpecificTree();
		tree1.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUUUCAUCUUAAAUAUAUGGGCGAUAUAU"
						.toCharArray(),
				"...((((...))...(((...)))..))....((((((.....))))))"
						.toCharArray());
		RNASpecificTree tree2 = new RNASpecificTree();
		tree2.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUUUCAUCUUAAAUAUAUGGGCGAUAUAU"
						.toCharArray(),
				"...((((...))...(((...)))..))....((((((.....))))))"
						.toCharArray());
		RNASpecificTree tree3 = new RNASpecificTree();
		tree3.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUAUCAUCUUAAAUAUAUGGGCGAUAUAU"
						.toCharArray(),
				"...((((...))...(((...)))..))....((((((.....))))))"
						.toCharArray());
		RNASpecificTree tree4 = new RNASpecificTree();
		tree4.buildFromViennaFormat("CGACAGGAAACUGACAA".toCharArray(),
				"...((((...))))...".toCharArray());

		Assert.assertEquals("Trees should be the same", true,
				tree1.equals(tree2));
		Assert.assertEquals("Trees should not be the same", false,
				tree1.equals(tree3));
		Assert.assertEquals("Trees should not be the same", false,
				tree3.equals(tree4));
	}

	@Test
	public void testStructureSimple2() {
		int[][] correct = new int[][] { { 1 }, { 0, 2, 3, 17, 18 }, { 1 },
				{ 1, 4 }, { 3, 5 }, { 4, 6, 10, 11, 16 }, { 5, 7 }, { 6, 8 },
				{ 7, 9 }, { 8 }, { 5 }, { 5, 12 }, { 11, 13 }, { 12, 14 },
				{ 13, 15 }, { 14 }, { 5 }, { 1 }, { 1, 19 }, { 18, 20 },
				{ 19, 21 }, { 20, 22 }, { 21, 23 }, { 22, 24 }, { 23, 25 },
				{ 24 } };

		RNASpecificTree tree = new RNASpecificTree();
		tree.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUUUCAUCUUAAAUAUAUGGGCGAUAUAU"
						.toCharArray(),
				"...((((...))...(((...)))..))....((((((.....))))))"
						.toCharArray());

		int[][] ans = tree.getEdges();

		Assert.assertEquals("The structure is not the same as expected", true,
				MatrixUtils.equals(ans, correct));
	}

	@Test
	public void testStructureSimple3() {
		int[][] correct = new int[][] { { 1 }, { 0, 2 }, { 1, 3 }, { 2, 4 },
				{ 3, 5 }, { 4, 6 }, { 5, 7 }, { 6, 8 },
				{ 7, 9, 10, 38, 39, 68 }, { 8 }, { 8, 11 }, { 10, 12 },
				{ 11, 13 }, { 12, 14, 37 }, { 13, 15 }, { 14, 16 }, { 15, 17 },
				{ 16, 18 }, { 17, 19, 20, 27, 28, 36 }, { 18 }, { 18, 21 },
				{ 20, 22 }, { 21, 23 }, { 22, 24 }, { 23, 25 }, { 24, 26 },
				{ 25 }, { 18 }, { 18, 29 }, { 28, 30 }, { 29, 31, 35 },
				{ 30, 32 }, { 31, 33 }, { 32, 34 }, { 33 }, { 30 }, { 18 },
				{ 13 }, { 8 }, { 8, 40 }, { 39, 41 }, { 40, 42 },
				{ 41, 43, 44, 67 }, { 42 }, { 42, 45 }, { 44, 46 }, { 45, 47 },
				{ 46, 48 }, { 47, 49 }, { 48, 50, 51, 59, 60, 66 }, { 49 },
				{ 49, 52 }, { 51, 53 }, { 52, 54 }, { 53, 55 }, { 54, 56 },
				{ 55, 57 }, { 56, 58 }, { 57 }, { 49 }, { 49, 61 }, { 60, 62 },
				{ 61, 63 }, { 62, 64 }, { 63, 65 }, { 64 }, { 49 }, { 42 },
				{ 8 } };

		RNASpecificTree tree = new RNASpecificTree();
		tree.buildFromViennaFormat(
				"aaaaaaAAAgggggggAAaaaaaAAAuuuuuAAuuuuAAAaaAaaAAccccAcccAAAAAAgggAAAgggggAAccccccAAAggggggAAaaaaAAAAAuuuuAAcccccAAAcccAAAAuuuuuu"
						.toCharArray(),
				"((((((...(((((((..(((((...)))))..((((...)).))..)))).)))......(((...(((((..((((((...))))))..((((.....))))..)))))...)))....))))))"
						.toCharArray());

		int[][] ans = tree.getEdges();
		Assert.assertEquals("The structure is not the same as expected", true,
				MatrixUtils.equals(ans, correct));
	}

	@Test
	public void testShuffle1() {
		System.out.println("Test Shuffle");
		RNASpecificTree tree1 = new RNASpecificTree();
		tree1.buildFromViennaFormat(
				"aaaaaaAAAgggggggAAaaaaaAAAuuuuuAAuuuuAAAaaAaaAAccccAcccAAAAAAgggAAAgggggAAccccccAAAggggggAAaaaaAAAAAuuuuAAcccccAAAcccAAAAuuuuuu"
						.toCharArray(),
				"((((((...(((((((..(((((...)))))..((((...)).))..)))).)))......(((...(((((..((((((...))))))..((((.....))))..)))))...)))....))))))"
						.toCharArray());

		RNASpecificTree tree2 = new RNASpecificTree();
		tree2.buildFromViennaFormat(
				"aaaaaaAAAgggggggAAaaaaaAAAuuuuuAAuuuuAAAaaAaaAAccccAcccAAAAAAgggAAAgggggAAccccccAAAggggggAAaaaaAAAAAuuuuAAcccccAAAcccAAAAuuuuuu"
						.toCharArray(),
				"((((((...(((((((..(((((...)))))..((((...)).))..)))).)))......(((...(((((..((((((...))))))..((((.....))))..)))))...)))....))))))"
						.toCharArray());
		tree2.shuffle();
		tree2.shuffle();

		Assert.assertEquals(
				"The probabilty is slim!, can't get the same tree after the shuffle",
				false, tree1.equals(tree2));
	}

	@Test
	public void testToViennaFormat() {
		RNASpecificTree tree = new RNASpecificTree();
		char[] sequence = "GGGGCAGAGAACCUAAGCCGUCUGGUGGAUGGCUCGGCUCGGGGCGCCGACGAAGGGCGUGGCAAGCUGCGAUAAGCCCCGGCGAGGCGCAGGCAGCCGUCGAACCGGGGAUUCCCGAAUGGGACCUCCCGCGGCUUUUGCCGCACUCCCAGUCGGGAGGGGGAACGCGGGGAAUUGAAACAUCUUAGUACCCGCAGGAAAAGAAAGCAAAAGCGAUGCCGUUAGUAGGGGCGACCGAAAGCGGCACAGGGCAAACUGAACCCUCCGGGGAAACCCGGAGGGGAUGUGGUGUAGUAGGGCCCUGCACUGGAGCCUCGAGGGUGAAGCCGAAGUCCGCUGGAACGCGGCGCCGUAGAGGGUGAAAGCCCCGUAGGCGUAAGCCCUCAGGCUCCUGCAGGGUUCCUGAGUACCGUCGGUCGGAUAUCCGGCGGGAAGCUGGGAGGCAUCGGCUCCCAACCCUAAAUACGUCCCGAGACCGAUAGCGAACUAGUACCGUGAGGGAAAGCUGAAAAGCACCCCUGGCAGGGGGUGAAAAGAGCCUGAAACCAGACGGCGAUAGGUGGGUGCGGCCCGAAAGGGUUGACCCUCCCCGAAGGAAACACGGGCGACCGUGGAGUACGAGGGGAGGCGACCGGGGUUGCACCGUCCGUCUUGGAUCACGGGGCAGGGAGUUCACGGCCGUGGCGAGGUUAAGGGGGUUAACCCCGAAGCCACAGGGAAACCGACAGGUCCGCAGCCCGUAAGGGUGAGGGACGGGGUGUGAAAGCGCCCGGAGUCACGGCCGUGAGACCCGAAGCCGGUCGAUCUAGCCCGGGGCAGGGUGAAGUCCCUCAACAGAGGGAUGGAGGCCCGAUAGGGGUGCUGACGUGCAAUUCGCUCCCGUGACCCCGGGCUAGGGGUGAAAGGCCAAUCGAGGCCGGCGAUAGCUGGUUCCCGCCGAAUUAUCCCUCAGGAUAGCCCGGCCGGAGGUAGGUGGUGGGGUAGAGCACUGAUUGGGGGUUUAGGGGGAGAAAUCCCCCGGCUCCCUGUCAAACUCCGAACCCACUGCCGCCGUAGAAGGCCGGAGUAGGGUGACGGUGUAAGCCGUCAACCGAGAGGGGAACAACCCAGACCGGGGUUAAGGCCCCUAAAUGCCGGCUAAGUGUUACUCCAAAGGGCGUCCCUGGCCUUAGACAGCGGGGAGGUAGGCUUAGAAGCAGCCAUCCUUUAAAGAGUGCGUAACAGCUCACCCGUCGAGGUCAGGGGCCCCGAAAAUGGACGGGGCUUAAGCCGGCUGCCGAGACCCCGGCGCACGGACCGAUUGGUCCGUGAUCGGGUAGGCGGGCGUGCCGGUGGGGUGGAAGCCGGGCCGUAAGGUCCGGUGGACCCGUCGGUAUUGUGGAUCCUGCCGGGAGUAGCAGCAUAGCCGGGUGAGAAUCCCGGCCGCCGAAGGGGCCAGGGUUCCACGGCAAUGUUCAUCAGCCGUGGGUUAGUCGGUCCUAAGCCAGUCCGUAACUCGGCGCUGGCGAAAGGGAAACGGGUUUAUAUUCCCGUACCGCGGUGGUAGGUGCGGCAACGCAAGCCCGAGGGGUGACGCCUCGGGGUAGGCGGACCGGCCCACAAGGCCGGCUAAGCGUAUAAGUCCGGGGAGUGCCGUAAUGGCGAGAACCGGAUGAAAGCGCGAAUGGCCUCCCGUAAGGGGGGUUCCGCCGAUCCCUGGGGCCCGUGAAAAGCCCUCGGGAACGAUCCACCGCGACCGUACCGAGAACCGACACAGGUGCCCCUGGGUGAGAAGCCUAAGGCGUGUCGGGGGAAACCCAGCCGAGGGAACUCGGCAAAUUGGCCCCGUAACUUCGGGAUAAGGGGUGCCUGCGGGUGCGUAACCCGCAGGUCGCAGUGACUCGGGGGACCCGACUGUUUAGUAAAAACACAGGUCCCAGCUAGCCCGAAAGGGUUUGUACUGGGGCCGACGCCUGCCCAGUGCCGGUAUGUGAAGCCCGGGUCCAACCGGGUGAAGCACCGGUAAACGGCGGGGGUAACUAUAACCCUCUUAAGGUAGCGAAAUUCCUUGUCGGUUAAAUGCCGACCUGCAUGAAUGGCGUAACGAGGUCCCCACUGUCCCCGGCUGGGGCCCGGCGAAACCACUGCCAGGCGCAUAUGCCUGGGACCUCCGGUGGGAAGCGAAGACCCCAUGGAGCUUUACUGCAGCCUGCCGUUGCCGUACGGCGGGGGGUGCGCAGCGUAGGCGGGAGGCGUCGAAGCCCGCCUUCCGGGGCGGGUGGAGCCGUCCAUGAGACACCGCCCACCCUCUGCCGUACGGCUAACCCCCGACGGGGGGACAGCGGUAGGUGGGCAGUUUGGCUGGGGCGGCACACCCUCGAAAAGGUAUCGAGGGUGCCCUAAGGUCGGCUCAGGCGGGUCAGGAAUCCGCCGUAGAGUGCAAGGGCAAAAGCCGGCCUGACUGGACCCGUAACAGAGGCGGGUCCAGCCGCGAAAGCGUGGCCUAGCGAACCCCUGUGCCUCCCCGGUGGGGGCCAGGGAUGACAGAAAAGCUACCCUGGGGAUAACAGAGUCGUCUCGGGCGAGAGCCCAUAUCGACCCCGAGGCUUGCUACCUCGCUGUCGGCUCUUCCCAUCCUGGCCCUGCAGCAGGGGCCAAGGGUGGGGGUGUUCACCCAUUAAAGGGGAACGUGAGCUGGGUUUAGACCGUCGUGAGACAGGUCGGAUGCUAUCUACCGGAGGUGUUGGCCGCCUGAGGGGAAGGCUCCCCCAGUACGAGAGGAACAGGGAGCCGCGGCCUCUGGUCUACCGGUUGUCCUACAGGGCACAGCCGGGCAGCUACGCCGUGUCCGAUAAGGCCUGAAAGCAUCUAAGGCCGAAGCGGUCCCCGAAAAUAGGCGGCCACUCCCAGGCGCAGGGGGUCGGGCGACCGGUCCUUUGCCUGGGACGAGGGCUCGGGAAGAAGACCCGUUUGAUGGGGCGGGGAUGUAAGCGGGAAGGGAAACCGACCCGUUCAGUCUGCCGCUCCCAAUCGCCCGAGGUUUCUGCCUC"
				.toCharArray();
		char[] structrue = "(((((((((......((((((((((.....(((..(((((((((((.......(((((((........))).....((((((...(((.......)))......))))))...))))...(((....)))(((((....))))).(((((....)))))......((((((.....((....))......))))).).........((....))...(((((.....((.....))....)))))..((((....(((..(((((((((....)))))))))....))).......((((((((((..(((((((.((((((...(((...(.((((......)))))..(...).(((.......)))...)))....)))))))))))))))))))).)))......(((((((........)))))))......(((((.......)))))..)))).....))))))))).))...............((....))....)).........((((.....))))...........).....)))))))))).......((((((((((....((......(((((((...(....(((((....))))).....)..)))))))...)).))))))))))..((((........))))((((((....(((((((((((((..((((..((((.....))))..))))...((....)).....((((...((((....))))...))))..(((((....)))))....)))))))))))))..((.(...((((((...((((((((((((.((((...(((((((....)))))))....))))....((((.((.((........))))))))..).))))))))))).(((.....)))......))))))......).))..((((((....(((((....))))).(((((((...(((.((((((((....(((...((((((((((((..((((((....))))))))))))))))))..)).)....)))))))))))......)))))).)..(((((((((......)))))))))....(((......)))...(((((((...(((......(.((((((................(((.((((((((((.......(((((.(((..(((.........)))..)))(...)(...((......)).).)))))...)))))))))))))...................))))))))))...)))))))((((((((((....))))))))..))....))))))(.((((((((((......((((((((....))))))))...))))))))))..).....))))))...............((((((.......))))))((((....((((..((...((((((...((.....)).))))))......((((((...(((((((((.....))).))))))....))...(((((........)))))..(((((((((((.((((....))))..(((((.((((...(((((((((..((((((((((((.....)))))))...((((....((((((((((((((.....))))))))))))))....)))).....(((((((....))))))).)))))...))))))))).........)))))))))..))).))))))))))))..)).....((......)).)))).((((.......)))).))))...((((.....((((((((((((..............(((((...((....))....)))))((((((((((.....))))))))))(...).......(((((((((..(((.........)))..(((((((((..((((....))))..)..))))))))..((((.(((..(((((((..(((....(((((......)))))....))))))))..)))))((((((.......))))))(...)...............((((((.....))))))..........))))...)).)))))))....)))))))))))).))))............(((((........)))))..((((((((.(((.((......((((.(((((((.((((...(((((((..((((((((((((((((((.........(((((..(((......((((((((....))))))))...)))............)))))))))))))))))))))))...(((((...))))).....)))))))....))))..((((.....))).)((((((...........))))))..((....((((((((.((((((.......))))))...))((......))....))))))....(((((((((.........))))))))).(((....))).))..........(((((.(((((.......)))))))))).........)))))..))))))........(((.((((((((...((.......))...)))))))..))))..........(((((((((((..((.((((((......))))))...)).(((((.....))))).....)))))..)..)))).).....(((((((....))).))))....))..)))))))))))....((((((((..((((..((((((((...............))))))))(((((...(((....((((((((((....)))))..)))))....))).)))))..(((....(((((...........)))))....))).))))......))))))))..((((((((..((((((((((....))))))))))))))))))...(((((((((.......)))))....(((((((((((..(..(((((((((....))))))))))...))).))))).)))...)))).....)))))))))"
				.toCharArray();
		tree.buildFromViennaFormat(sequence, structrue);
		char[][] ans = tree.toViennaFormat();
		Assert.assertArrayEquals(sequence, ans[0]);
		Assert.assertArrayEquals(structrue, ans[1]);
	}
}
