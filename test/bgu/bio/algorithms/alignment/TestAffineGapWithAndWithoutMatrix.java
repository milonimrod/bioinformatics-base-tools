package bgu.bio.algorithms.alignment;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.util.AffineGapScoringMatrix;
import bgu.bio.util.IdentityAffineScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

public class TestAffineGapWithAndWithoutMatrix {

	@Test
	public void randomTest() {
		AlphabetUtils alphabet = RnaAlphabet.getInstance();
		AffineGapScoringMatrix matrix = new IdentityAffineScoringMatrix(
				alphabet, 1, -10f, -1f, 0f);

		AffineGapGlobalSequenceAlignment alignMat = new AffineGapGlobalSequenceAlignment(
				0, 0, alphabet, matrix);
		AffineGapGlobalSequenceAlignmentNoMatrix alignNoMat = new AffineGapGlobalSequenceAlignmentNoMatrix(
				0, 0, alphabet, matrix);

		Random rand = new Random();
		final int size = 200;
		final int repeats = 12000;
		char[] seq1 = new char[size];
		char[] seq2 = new char[size / 2];
		for (int i = 0; i < repeats; i++) {

			alphabet.randomSequence(seq1, size, rand);
			alphabet.randomSequence(seq2, size / 2, rand);

			alignMat.setSequences(new String(seq1), new String(seq2));
			alignNoMat.setSequences(seq1, seq2);

			alignMat.buildMatrix();
			alignNoMat.buildMatrix();

			if (alignMat.getAlignmentScore() - alignNoMat.getAlignmentScore() != 0) {
				alignMat.getAlignment();
				System.out.println("------------");
				alignMat.printDPMatrix();
				System.out.println("------------");
				alignNoMat.printDPMatrix();
				Assert.fail("Problem on test " + i + "\nSeq1: "
						+ new String(seq1) + "\nSeq2: " + new String(seq2)
						+ "\nWith score: " + alignMat.getAlignmentScore()
						+ "\nWithout score: " + alignNoMat.getAlignmentScore());

			}

		}
	}

	@Test
	public void test1() {
		String str1 = "CNCNANGUNC";
		String str2 = "UNUUG";

		AlphabetUtils alphabet = RnaAlphabet.getInstance();
		AffineGapScoringMatrix matrix = new IdentityAffineScoringMatrix(
				alphabet, 1, -10f, -0.5f, 0f);

		AffineGapGlobalSequenceAlignment alignMat = new AffineGapGlobalSequenceAlignment(
				0, 0, alphabet, matrix);
		AffineGapGlobalSequenceAlignmentNoMatrix alignNoMat = new AffineGapGlobalSequenceAlignmentNoMatrix(
				0, 0, alphabet, matrix);

		alignMat.setSequences(str1, str2);
		alignNoMat.setSequences(str1.toCharArray(), str2.toCharArray());

		alignMat.buildMatrix();
		alignNoMat.buildMatrix();

		if (alignMat.getAlignmentScore() - alignNoMat.getAlignmentScore() != 0) {
			alignMat.getAlignment();
			System.out.println("------------");
			alignMat.printDPMatrix();
			System.out.println("------------");
			alignNoMat.printDPMatrix();
			Assert.fail("Problem on test\nSeq1: " + str1 + "\nSeq2: " + str2
					+ "\nWith score: " + alignMat.getAlignmentScore()
					+ "\nWithout score: " + alignNoMat.getAlignmentScore());

		}
	}

	@Test
	public void test2() {
		String str1 = "CNCNANGUNC";
		String str2 = "CNCNANGUNCAAAAAA";

		AlphabetUtils alphabet = RnaAlphabet.getInstance();
		AffineGapScoringMatrix matrix = new IdentityAffineScoringMatrix(
				alphabet, 1, -10f, -0.5f, 0f);

		AffineGapGlobalSequenceAlignment alignMat = new AffineGapGlobalSequenceAlignment(
				0, 0, alphabet, matrix);
		AffineGapGlobalSequenceAlignmentNoMatrix alignNoMat = new AffineGapGlobalSequenceAlignmentNoMatrix(
				0, 0, alphabet, matrix);

		alignMat.setSequences(str1, str2);
		alignNoMat.setSequences(str1.toCharArray(), str2.toCharArray());

		alignMat.buildMatrix();
		alignNoMat.buildMatrix();

		Assert.assertEquals("Wrong result for run with matrix",
				10 - (10 + 0.5 * 6), alignMat.getAlignmentScore(), 0.001);
		Assert.assertEquals("Wrong result for run without matrix",
				10 - (10 + 0.5 * 6), alignNoMat.getAlignmentScore(), 0.001);

	}
}
