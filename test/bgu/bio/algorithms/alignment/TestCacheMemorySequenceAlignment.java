package bgu.bio.algorithms.alignment;

import gnu.trove.list.array.TCharArrayList;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.alphabet.RnaAlphabet;

public class TestCacheMemorySequenceAlignment {

	@Test
	public void testRunRandom() {
		SequenceAlignment aligner = new GlobalSequenceAlignmentNoMatrix(1, 1,
				RnaAlphabet.getInstance(), new IdentityScoringMatrix(
						RnaAlphabet.getInstance()));
		int size = 6;
		CacheMemorySequenceAlignment cache = new CacheMemorySequenceAlignment(
				size, aligner);

		int amount = 220000;
		char[] letters = new char[] { 'A', 'U', 'C', 'G' };
		Random rand = new Random();

		TCharArrayList[] sequences1 = new TCharArrayList[amount];
		TCharArrayList[] sequences2 = new TCharArrayList[amount];
		double[] answers = new double[amount];
		boolean[] shouldBeCached = new boolean[amount];

		for (int i = 0; i < amount; i++) {
			TCharArrayList seq1 = new TCharArrayList();
			TCharArrayList seq2 = new TCharArrayList();

			int s = rand.nextInt(size * 2) + 1;
			for (int x = 0; x < s; x++) {
				seq1.add(letters[rand.nextInt(letters.length)]);
			}

			s = rand.nextInt(size * 2) + 1;
			for (int x = 0; x < s; x++) {
				seq2.add(letters[rand.nextInt(letters.length)]);
			}

			cache.setSequences(seq1, seq2);
			cache.buildMatrix();

			answers[i] = cache.getAlignmentScore();

			shouldBeCached[i] = seq1.size() <= size && seq2.size() <= size;
			sequences1[i] = seq1;
			sequences2[i] = seq2;
		}

		for (int i = 0; i < amount; i++) {
			cache.setSequences(sequences1[i], sequences2[i]);
			cache.buildMatrix();
			double ans = cache.getAlignmentScore();
			Assert.assertEquals("didn't get the same results "+ sequences1[i]
					+ " and " + sequences2[i],
					answers[i],ans, 0.0001);
			Assert.assertEquals("Wrong usage of cache " + sequences1[i]
					+ " and " + sequences2[i], shouldBeCached[i],
					cache.getUsedCache());
		}

		for (int i = 0; i < amount; i++) {
			cache.setSequences(sequences2[i], sequences1[i]);
			cache.buildMatrix();
			double ans = cache.getAlignmentScore();
			Assert.assertEquals("didn't get the same results " + sequences1[i]
					+ " and " + sequences2[i],
					answers[i],ans, 0.0001);
			Assert.assertEquals("Wrong usage of cache " + sequences1[i]
					+ " and " + sequences2[i], shouldBeCached[i],
					cache.getUsedCache());
		}
	}

	@Test
	public void testRun1() {
		SequenceAlignment aligner = new GlobalSequenceAlignmentNoMatrix(1, 1,
				RnaAlphabet.getInstance(), new IdentityScoringMatrix(
						RnaAlphabet.getInstance()));
		int size = 6;
		CacheMemorySequenceAlignment cache = new CacheMemorySequenceAlignment(
				size, aligner);

		cache.setSequences("AUA", "AUAAA");
		cache.buildMatrix();
		double ans1 = cache.getAlignmentScore();

		cache.setSequences("AUAAA", "AUA");
		cache.buildMatrix();
		double ans2 = cache.getAlignmentScore();
		Assert.assertEquals("didn't get the same results !", ans1, ans2, 0.0001);
		Assert.assertEquals("Wrong usage of cache", cache.getUsedCache(), true);
	}
}
