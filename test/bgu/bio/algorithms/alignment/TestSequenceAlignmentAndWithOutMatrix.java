package bgu.bio.algorithms.alignment;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.util.AffineGapScoringMatrix;
import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.AlphabetUtils;
import bgu.bio.util.alphabet.RnaAlphabet;

public class TestSequenceAlignmentAndWithOutMatrix {

	@Test
	public void testAffineSmall1() {
		AffineGapGlobalSequenceAlignment affine = new AffineGapGlobalSequenceAlignment(
				10, 10, RnaAlphabet.getInstance(), new AffineGapScoringMatrix(
						"matrix/NUC-Affine-sim-RIBOSUM85-60.matrix",
						RnaAlphabet.getInstance()));
		affine.setSequences("GA", "");
		affine.buildMatrix();
		Assert.assertEquals(-3.0, affine.getAlignmentScore(), 0.001);
		
		affine.setSequences("GA".toCharArray(),2, "GA".toCharArray(),0);
		affine.buildMatrix();
		Assert.assertEquals(-3.0, affine.getAlignmentScore(), 0.001);
	}

	@Test
	public void randomTest() {
		AlphabetUtils alphabet = RnaAlphabet.getInstance();
		ScoringMatrix matrix = new IdentityScoringMatrix(alphabet);

		GlobalSequenceAlignment alignMat = new GlobalSequenceAlignment(0, 0,
				alphabet, matrix);
		GlobalSequenceAlignmentNoMatrix alignNoMat = new GlobalSequenceAlignmentNoMatrix(
				0, 0, alphabet, matrix);

		Random rand = new Random();
		final int size = 200;
		final int repeats = 1000;
		char[] seq1 = new char[size];
		char[] seq2 = new char[size];
		for (int i = 0; i < repeats; i++) {

			alphabet.randomSequence(seq1, size, rand);
			alphabet.randomSequence(seq2, size, rand);

			alignMat.setSequences(new String(seq1), new String(seq2));
			alignNoMat.setSequences(seq1, seq2);

			alignMat.buildMatrix();
			alignNoMat.buildMatrix();

			if (alignMat.getAlignmentScore() - alignNoMat.getAlignmentScore() != 0) {
				alignMat.printAlignments();
				System.out.println("------------");
				alignMat.printDPMatrix();
				System.out.println("------------");
				alignNoMat.printDPMatrix();
				Assert.fail("Problem on test " + i + "\nSeq1: "
						+ new String(seq1) + "\nSeq2: " + new String(seq2)
						+ "\nWith score: " + alignMat.getAlignmentScore()
						+ "\nWithout score: " + alignNoMat.getAlignmentScore());

			}

		}
	}
}
