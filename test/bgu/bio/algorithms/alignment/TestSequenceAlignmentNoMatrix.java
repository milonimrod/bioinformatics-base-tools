package bgu.bio.algorithms.alignment;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.alphabet.RnaAlphabet;

public class TestSequenceAlignmentNoMatrix {

	@Test
	public void testGlobal1() {
		SequenceAlignment sequence = initGlobal();
		sequence.setSequences("ACGUACGU", "ACGUACGUAUUU");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", 4.0,
				sequence.getAlignmentScore(), 0.001);

		sequence.setSequences("", "AUUU");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", -4.0,
				sequence.getAlignmentScore(), 0.001);
	}

	@Test
	public void testLocal1() {
		SequenceAlignment sequence = initLocal();
		sequence.setSequences("ACGUACGU", "ACGUACGUAUUU");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", 8.0,
				sequence.getAlignmentScore(), 0.001);

		sequence.setSequences("", "AUUU");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", 0.0,
				sequence.getAlignmentScore(), 0.001);

		sequence.setSequences("G", "AUGC");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", 1.0,
				sequence.getAlignmentScore(), 0.001);
	}

	@Test
	public void testSuffixFree1() {
		SequenceAlignment sequence = initSuffixFree();
		sequence.setSequences("ACGUACGU", "AUUUACGUACGU");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", 8.0,
				sequence.getAlignmentScore(), 0.001);

		sequence.setSequences("", "AUUU");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", 0.0,
				sequence.getAlignmentScore(), 0.001);

		sequence.setSequences("UG", "AUGC");
		sequence.buildMatrix();
		Assert.assertEquals("Got wrong score", 1.0,
				sequence.getAlignmentScore(), 0.001);
	}

	private SequenceAlignment initLocal() {
		return new LocalSequenceAlignmentNoMatrix(10, 10,
				RnaAlphabet.getInstance(), new IdentityScoringMatrix(
						RnaAlphabet.getInstance(), 1, -1));
	}

	private SequenceAlignment initGlobal() {
		return new GlobalSequenceAlignmentNoMatrix(10, 10,
				RnaAlphabet.getInstance(), new IdentityScoringMatrix(
						RnaAlphabet.getInstance(), 1, -1));
	}

	private SequenceAlignment initSuffixFree() {
		return new SuffixFreeLocalAlignment(10, 10, RnaAlphabet.getInstance(),
				new IdentityScoringMatrix(RnaAlphabet.getInstance(), 1, -1));
	}

}
