package bgu.bio.algorithms.alignment.stems;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import bgu.bio.algorithms.alignment.GlobalSequenceAlignment;
import bgu.bio.ds.rna.StemStructure;
import bgu.bio.util.AffineGapScoringMatrix;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.RnaAlphabet;
import bgu.bio.util.alphabet.StemStructureAlphabet;

public class TestAffineGapStemSimilarity {

	@Test
	public void testSameSimilarity() {
		GlobalStemSimilarity simGlobal = new GlobalStemSimilarity(
				new ScoringMatrix("matrix/tests/STEMS-test2.matrix",
						StemStructureAlphabet.getInstance()),
				new GlobalSequenceAlignment(10, 10, RnaAlphabet.getInstance(),
						new IdentityScoringMatrix(RnaAlphabet.getInstance())));

		AffineGapStemSimilarity simAffine = new AffineGapStemSimilarity(
				new AffineGapScoringMatrix(
						"matrix/tests/STEMS-test2-affine.matrix",
						StemStructureAlphabet.getInstance()),
				new GlobalSequenceAlignment(10, 10, RnaAlphabet.getInstance(),
						new IdentityScoringMatrix(RnaAlphabet.getInstance())));

		StemStructure str1 = new StemStructure(
				">ID: add , START: 1 , STRAND: UNKNOWN , ENERGY: -14.1 (base)",
				new CharBuffer("UCUACCGGGCAAAGUCCGACUAUGGG"), new CharBuffer(
						"((((.(((((...)))))....))))"));

		StemStructure str2 = new StemStructure(
				">ID: add , START: 1 , STRAND: UNKNOWN , ENERGY: -14.1 (base)",
				new CharBuffer("GGGGGGUCUACCGGGCAAAGUCCGACUAUGGGCCCCCC"),
				new CharBuffer("((((((((((.(((((...)))))....))))))))))"));

		StemStructure str15 = new StemStructure(
				">ID: add , START: 1 , STRAND: UNKNOWN , ENERGY: -14.1 (base)",
				new CharBuffer("UCUACCGGGCAAAGUCCGACUAUGGG"), new CharBuffer(
						"((((.(((((...)))))....))))"));

		simGlobal.setStem1(str1);
		simGlobal.setStem2(str15);
		simGlobal.run();
		double scoreGlobal = simGlobal.getAlignmentScore();

		simAffine.setStem1(str1);
		simAffine.setStem2(str2);
		simAffine.run();
		double scoreAffine = simAffine.getAlignmentScore();

		String[] b = simGlobal.getAlignment();
		System.out.println(b[0]);
		System.out.println(b[1]);
		String[] a = simAffine.getAlignment();
		System.out.println(a[0]);
		System.out.println(a[1]);

		translate(a);

		simAffine.printDPMatrix();

		System.out.println(scoreGlobal);
		System.out.println(scoreAffine);

		assertEquals("Wrong score", scoreGlobal - (5 + 6 * 1), scoreAffine,
				0.001);
		System.out.println(scoreAffine);
	}

	private void translate(String[] a) {
		StringBuilder s1 = new StringBuilder();
		StringBuilder s2 = new StringBuilder();
		for (int i = 0; i < a[0].length(); i++) {
			if (a[0].charAt(i) != '_') {
				s1.append(" "
						+ Arrays.toString(StemStructureAlphabet.getInstance()
								.decodePair(a[0].charAt(i))));
			} else {
				s1.append(" " + "_");
			}

			if (a[1].charAt(i) != '_') {
				s2.append(" "
						+ Arrays.toString(StemStructureAlphabet.getInstance()
								.decodePair(a[1].charAt(i))));
			} else {
				s2.append(" " + "_");
			}
		}
		System.out.println(s1);
		System.out.println(s2);
	}
}
