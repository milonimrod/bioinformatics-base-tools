package bgu.bio.algorithms.alignment.stems;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.algorithms.alignment.AffineGapGlobalSequenceAlignment;
import bgu.bio.algorithms.alignment.GlobalSequenceAlignment;
import bgu.bio.algorithms.alignment.LocalSequenceAlignment;
import bgu.bio.ds.rna.StemStructure;
import bgu.bio.util.AffineGapScoringMatrix;
import bgu.bio.util.CharBuffer;
import bgu.bio.util.IdentityScoringMatrix;
import bgu.bio.util.ScoringMatrix;
import bgu.bio.util.alphabet.RnaAlphabet;
import bgu.bio.util.alphabet.StemStructureAlphabet;

public class TestGlobalStemSimilarity {

	@Test
	public void testSameSimilarity() {
		GlobalStemSimilarity sim = new GlobalStemSimilarity(new ScoringMatrix(
				"matrix/tests/STEMS-test2.matrix",
				StemStructureAlphabet.getInstance()),
				new GlobalSequenceAlignment(10, 10, RnaAlphabet.getInstance(),
						new IdentityScoringMatrix(RnaAlphabet.getInstance())));

		StemStructure str1 = new StemStructure(
				">ID: add , START: 1 , STRAND: UNKNOWN , ENERGY: -14.1 (base)",
				new CharBuffer("UCUACCGGGCAAAGUCCGACUAUGGG"), new CharBuffer(
						"((((.(((((...)))))....))))"));

		StemStructure str2 = new StemStructure(
				">ID: add , START: 1 , STRAND: UNKNOWN , ENERGY: -14.1 (base)",
				new CharBuffer(
						"UCACACCCGUUCCCAUACCGAACACGGAAACCGAUGGUAGUCGGGGGUUUCCCCCUGUGA"),
				new CharBuffer(
						"(((((...(..(((...((((.(((((...))).))....)))))))...)....)))))"));
		sim.setStem1(str1);
		sim.setStem2(str2);
		sim.run();
		double score = sim.getAlignmentScore();
		String[] alignment = sim.getAlignment();
		for (int i = 0; i < alignment[0].length(); i++) {
			System.out.print(" "
					+ Arrays.toString(StemStructureAlphabet.getInstance()
							.decodePair(alignment[0].charAt(i))));
		}
		System.out.println();
		for (int i = 0; i < alignment[1].length(); i++) {
			System.out.print(" "
					+ Arrays.toString(StemStructureAlphabet.getInstance()
							.decodePair(alignment[1].charAt(i))));
		}
		System.out.println();
		System.out.println(score);
	}

	@Test
	public void testSameSimilarity2() {
		final StemStructureAlphabet stemAlphabet = StemStructureAlphabet
				.getInstance();
		ScoringMatrix structureScoringMatrix = new ScoringMatrix(
				"matrix/Structure-sim-RIBOSUM85-60.matrix", stemAlphabet);

		GlobalStemSimilarity sim = new GlobalStemSimilarity(
				structureScoringMatrix, new AffineGapGlobalSequenceAlignment(
						10, 10, RnaAlphabet.getInstance(),
						new AffineGapScoringMatrix(
								"matrix/NUC-Affine-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())));

		StemStructure str1 = new StemStructure(">1", new CharBuffer(
				"AGAGGUAAAAUGCAUCU"), new CharBuffer("((..(((...)))..))"));

		StemStructure str2 = new StemStructure(">1", new CharBuffer(
				"AGAGGUAAAAUGCAUUU"), new CharBuffer("((..(((...)))..))"));

		sim.setStem1(str1);
		sim.setStem2(str2);
		sim.run();
		sim.buildMatrix();
		double score = sim.getAlignmentScore();
		String[] alignment = sim.getAlignment();
		for (int i = 0; i < alignment[0].length(); i++) {
			System.out.print(" "
					+ Arrays.toString(stemAlphabet.decodePair(alignment[0]
							.charAt(i))));
		}
		System.out.println();
		for (int i = 0; i < alignment[1].length(); i++) {
			System.out.print(" "
					+ Arrays.toString(stemAlphabet.decodePair(alignment[1]
							.charAt(i))));
		}
		System.out.println();

		sim.setStem1(str1);
		sim.setStem2(str1);
		sim.run();
		double scoreSelf = sim.getAlignmentScore();
		double diff = scoreSelf - score;
		double expected = structureScoringMatrix.score(
				stemAlphabet.mapAndHash("G|C"), stemAlphabet.mapAndHash("G|C"))
				- structureScoringMatrix.score(stemAlphabet.mapAndHash("G|C"),
						stemAlphabet.mapAndHash("G|U"));
		Assert.assertEquals(expected, diff, 0.01);
	}

	@Test
	public void testSameSimilarity3() {
		final StemStructureAlphabet stemAlphabet = StemStructureAlphabet
				.getInstance();
		ScoringMatrix structureScoringMatrix = new ScoringMatrix(
				"matrix/Structure-sim-RIBOSUM85-60.matrix", stemAlphabet);

		GlobalStemSimilarity sim = new GlobalStemSimilarity(
				structureScoringMatrix, new AffineGapGlobalSequenceAlignment(
						10, 10, RnaAlphabet.getInstance(),
						new AffineGapScoringMatrix(
								"matrix/NUC-Affine-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())));

		StemStructure str1 = new StemStructure(">1", new CharBuffer(
				"AGAGGUAAAAUGCAUCU"), new CharBuffer("((..(((...)))..))"));

		StemStructure str2 = new StemStructure(">1", new CharBuffer(
				"AGAGAAAAUAUCU"), new CharBuffer("((..(...)..))"));

		sim.setStem1(str1);
		sim.setStem2(str2);
		sim.run();
		double score = sim.getAlignmentScore();
		String[] alignment = sim.getAlignment();
		for (int i = 0; i < alignment[0].length(); i++) {
			System.out.print(" "
					+ Arrays.toString(stemAlphabet.decodePair(alignment[0]
							.charAt(i))));
		}
		System.out.println();
		for (int i = 0; i < alignment[1].length(); i++) {
			System.out.print(" "
					+ Arrays.toString(stemAlphabet.decodePair(alignment[1]
							.charAt(i))));
		}
		System.out.println();

		sim.setStem1(str1);
		sim.setStem2(str1);
		sim.run();
		double scoreSelf = sim.getAlignmentScore();
		double diff = scoreSelf - score;
		double expected = structureScoringMatrix.score(
				stemAlphabet.mapAndHash("U|G"), stemAlphabet.mapAndHash("U|G"))
				+ structureScoringMatrix.score(stemAlphabet.mapAndHash("G|C"),
						stemAlphabet.mapAndHash("G|C"))
				- (structureScoringMatrix.score(stemAlphabet.mapAndHash("U|G"),
						stemAlphabet.emptyLetterHashed()) + structureScoringMatrix
						.score(stemAlphabet.mapAndHash("G|C"),
								stemAlphabet.emptyLetterHashed()));
		Assert.assertEquals(expected, diff, 0.01);
	}

	@Test
	public void testSimilarityWithDangling() {
		final StemStructureAlphabet stemAlphabet = StemStructureAlphabet
				.getInstance();
		ScoringMatrix structureScoringMatrix = new ScoringMatrix(
				"matrix/Structure-sim-RIBOSUM85-60.matrix", stemAlphabet);

		GlobalStemSimilarity sim = new GlobalStemSimilarity(
				structureScoringMatrix, new AffineGapGlobalSequenceAlignment(
						10, 10, RnaAlphabet.getInstance(),
						new AffineGapScoringMatrix(
								"matrix/NUC-Affine-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())),
				new LocalSequenceAlignment(10, 10, RnaAlphabet.getInstance(),
						new ScoringMatrix("matrix/NUC-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())));

		StemStructure str1 = new StemStructure(">1", new CharBuffer(
				"AAAGAGGUAAAAUGCAUCUAA"), new CharBuffer(
				"..((..(((...)))..)).."));

		StemStructure str2 = new StemStructure(">1", new CharBuffer(
				"AAAGAGAAAAUAUCUAA"), new CharBuffer("..((..(...)..)).."));

		StemStructure str3 = new StemStructure(">1", new CharBuffer(
				"UUUUAAACAGAGAAAAUAUCUCAAAGGGG"), new CharBuffer(
				"........((..(...)..))........"));

		sim.setStem1(str1);
		sim.setStem2(str2);
		sim.run();
		double score1 = sim.getAlignmentScore();
		System.out.println(score1);

		sim.setStem1(str1);
		sim.setStem2(str3);
		sim.run();
		double score2 = sim.getAlignmentScore();
		System.out.println(score2);

		Assert.assertEquals("Score should be the same", score1, score2, 0.0001);
	}

	@Test
	public void testSimilarityWithDangling1() {
		final StemStructureAlphabet stemAlphabet = StemStructureAlphabet
				.getInstance();
		ScoringMatrix structureScoringMatrix = new ScoringMatrix(
				"matrix/Structure-sim-RIBOSUM85-60.matrix", stemAlphabet);

		GlobalStemSimilarity sim = new GlobalStemSimilarity(
				structureScoringMatrix, new AffineGapGlobalSequenceAlignment(
						10, 10, RnaAlphabet.getInstance(),
						new AffineGapScoringMatrix(
								"matrix/NUC-Affine-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())),
				new LocalSequenceAlignment(10, 10, RnaAlphabet.getInstance(),
						new ScoringMatrix("matrix/NUC-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())));

		StemStructure str1 = new StemStructure(">1", new CharBuffer(
				"AGAGGUAAAAUGCAUCU"), new CharBuffer("((..(((...)))..))"));

		StemStructure str2 = new StemStructure(">1", new CharBuffer(
				"AGAGAAAAUAUCU"), new CharBuffer("((..(...)..))"));

		StemStructure str3 = new StemStructure(">1", new CharBuffer(
				"UUUUAAACAGAGAAAAUAUCUCAAAGGGG"), new CharBuffer(
				"........((..(...)..))........"));

		sim.setStem1(str1);
		sim.setStem2(str2);
		sim.run();
		double score1 = sim.getAlignmentScore();
		System.out.println(score1);

		sim.setStem1(str1);
		sim.setStem2(str3);
		sim.run();
		double score2 = sim.getAlignmentScore();
		System.out.println(score2);

		Assert.assertEquals("Score should be the same", score1, score2, 0.0001);
	}

	@Test
	public void testSimilarityWithDangling2() {
		final StemStructureAlphabet stemAlphabet = StemStructureAlphabet
				.getInstance();
		ScoringMatrix structureScoringMatrix = new ScoringMatrix(
				"matrix/Structure-sim-RIBOSUM85-60.matrix", stemAlphabet);

		GlobalStemSimilarity sim = new GlobalStemSimilarity(
				structureScoringMatrix, new AffineGapGlobalSequenceAlignment(
						10, 10, RnaAlphabet.getInstance(),
						new AffineGapScoringMatrix(
								"matrix/NUC-Affine-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())),
				new LocalSequenceAlignment(10, 10, RnaAlphabet.getInstance(),
						new ScoringMatrix("matrix/NUC-sim-RIBOSUM85-60.matrix",
								RnaAlphabet.getInstance())));

		StemStructure str1 = new StemStructure(
				">1",
				new CharBuffer(
						"AUUUCAUGAAGAGUGGCUGAGGGACGGGCCCUGUGACGCCACGACAACCGGCUCGAGCAAUUCGGGACAUCGGUGUCAACUCUCUCC"),
				new CharBuffer(
						"..............((..(((((........................................................))))).))"));

		StemStructure str2 = new StemStructure(
				">1",
				new CharBuffer(
						"UGAGACUUAUUACUGCACCAUAAUUAAGCCAUCCAAUGAGGAAGACUCGAUUUUGGUGGAAAGGUUUAUCUGGCACAAUGGUAGUGUUUUG"),
				new CharBuffer(
						"((((((..((((((.................................................................))))))))))))"));

		StemStructure str1no = new StemStructure(
				">1",
				new CharBuffer(
						"GGCUGAGGGACGGGCCCUGUGACGCCACGACAACCGGCUCGAGCAAUUCGGGACAUCGGUGUCAACUCUCUCC"),
				new CharBuffer(
						"((..(((((........................................................))))).))"));

		sim.setStem1(str1);
		sim.setStem2(str2);
		sim.run();
		double score1 = sim.getAlignmentScore();
		System.out.println(score1);

		sim.setStem1(str1no);
		sim.setStem2(str2);
		sim.run();
		double score2 = sim.getAlignmentScore();
		System.out.println(score2);

		Assert.assertEquals("Score should be the same", score1, score2, 0.0001);
	}
}
