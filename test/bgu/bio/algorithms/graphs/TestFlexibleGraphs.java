package bgu.bio.algorithms.graphs;

import gnu.trove.list.array.TIntArrayList;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.adt.graphs.FlexibleGraph;
import bgu.bio.adt.graphs.FlexibleUndirectedGraph;
import bgu.bio.io.file.json.JSONException;
import bgu.bio.io.file.json.JSONObject;

public class TestFlexibleGraphs {

	@Test
	public void testDirected1() {
		FlexibleGraph graph = new FlexibleGraph();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addEdge(0, 1);
		graph.addEdge(0, 4);
		graph.addEdge(0, 3);
		graph.addEdge(3, 2);
		graph.addEdge(5, 3);
		Assert.assertEquals("wrong number of leaves", 4,
				graph.getNumberOfLeaves());
		Assert.assertEquals("wrong maximal out degree", 3, graph.getMaxOutDeg());
		Assert.assertEquals("wrong number of edges", 5, graph.getEdgeNum());
	}

	@Test
	public void testDirected2() {
		FlexibleGraph graph = new FlexibleGraph();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addEdge(6, 3);
		graph.addEdge(0, 1);
		graph.addEdge(0, 4);
		graph.addEdge(0, 3);
		graph.addEdge(3, 2);
		graph.addEdge(5, 3);
		Assert.assertEquals("wrong number of incoming edges", 3, graph.inDeg(3));
		graph.removeEdge(6, 3);
		Assert.assertEquals("wrong number of leaves", 4,
				graph.getNumberOfLeaves());
		Assert.assertEquals("wrong maximal out degree", 3, graph.getMaxOutDeg());
		Assert.assertEquals("wrong number of edges", 5, graph.getEdgeNum());

	}

	@Test
	public void testUnDirected1() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addEdge(0, 1);
		graph.addEdge(0, 4);
		graph.addEdge(0, 3);
		graph.addEdge(3, 2);
		graph.addEdge(5, 3);
		Assert.assertEquals("wrong maximal out degree", 3, graph.getMaxDeg());
		Assert.assertEquals("wrong number of edges", 5, graph.getEdgeNum());
	}

	@Test
	public void testUnDirectedColoring1() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addNode();
		graph.addEdge(0, 1);
		graph.addEdge(0, 4);
		graph.addEdge(0, 3);
		graph.addEdge(3, 2);
		graph.addEdge(5, 3);

		GraphColoring coloring = new GraphColoring();
		coloring.setGraph(graph);
		coloring.greedyColoringByDegenracy();
		checkColoring(graph, coloring.getColoring());
	}

	@Test
	public void testSplitGraph() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		for (int i = 0; i < 7; i++) {
			graph.addNode();
		}
		graph.addEdge(0, 1);
		graph.addEdge(0, 2);
		graph.addEdge(0, 3);
		graph.addEdge(1, 4);
		graph.addEdge(2, 4);
		graph.addEdge(2, 5);
		graph.addEdge(3, 4);
		graph.addEdge(3, 6);
		graph.addEdge(4, 5);

		FlexibleUndirectedGraph graph1 = new FlexibleUndirectedGraph();
		FlexibleUndirectedGraph graph2 = new FlexibleUndirectedGraph();
		graph.split(new TIntArrayList(new int[] { 4, 5, 6 }), graph1, graph2);
		System.out.println(graph1.toDOTFormat());
		System.out.println(graph2.toDOTFormat());
		Assert.assertEquals("Number of nodes is not correct",
				graph.getNodeNum(), graph1.getNodeNum());
		Assert.assertEquals("Number of nodes is not correct",
				graph.getNodeNum(), graph2.getNodeNum());
		Assert.assertEquals("Number of edges is not correct", 1,
				graph1.getEdgeNum());
		Assert.assertEquals("Number of edges is not correct", 3,
				graph2.getEdgeNum());
	}

	private void checkColoring(FlexibleUndirectedGraph graph, int[] color) {
		for (int u = 0; u < graph.getNodeNum(); u++) {
			int deg = graph.deg(u);
			for (int d = 0; d < deg; d++) {
				int v = graph.getNeighbor(u, d);
				if (graph.isNeighbours(u, v) && color[u] == color[v]) {
					Assert.fail("Found two nodes that are connected and have the same color");
				}
			}
		}
	}

	@Test
	public void testUnDirectedDegeneracy1() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		int nodes = 10;
		for (int i = 0; i < nodes; i++) {
			graph.addNode();
		}

		for (int i = 1; i < nodes; i++) {
			graph.addEdge(0, i);
		}

		for (int i = 2; i < nodes; i++) {
			graph.addEdge(1, i);
		}

		Assert.assertEquals("wrong degeneracy", 2, graph.degeneracy());

	}

	@Test
	public void testUndirectedClear() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		int nodes = 52;
		for (int i = 0; i < nodes; i++) {
			graph.addNode();
		}

		int[][] edges = new int[][] { { 0, 9 }, { 0, 10 }, { 0, 11 },
				{ 0, 12 }, { 0, 13 }, { 0, 17 }, { 0, 18 }, { 0, 19 },
				{ 0, 20 }, { 0, 24 }, { 0, 30 }, { 0, 41 }, { 0, 45 },
				{ 0, 46 }, { 0, 47 }, { 0, 48 }, { 1, 9 }, { 1, 10 },
				{ 1, 11 }, { 1, 12 }, { 1, 17 }, { 1, 18 }, { 1, 19 },
				{ 1, 20 }, { 1, 24 }, { 1, 30 }, { 1, 37 }, { 1, 41 },
				{ 1, 45 }, { 1, 46 }, { 1, 47 }, { 1, 48 }, { 1, 49 },
				{ 2, 9 }, { 2, 10 }, { 2, 11 }, { 2, 14 }, { 2, 17 },
				{ 2, 18 }, { 2, 19 }, { 2, 20 }, { 2, 24 }, { 2, 36 },
				{ 2, 41 }, { 2, 45 }, { 2, 46 }, { 2, 47 }, { 2, 48 },
				{ 2, 49 }, { 3, 5 }, { 3, 9 }, { 3, 10 }, { 3, 11 }, { 3, 14 },
				{ 3, 17 }, { 3, 18 }, { 3, 19 }, { 3, 20 }, { 3, 24 },
				{ 3, 36 }, { 3, 41 }, { 3, 45 }, { 3, 46 }, { 3, 47 },
				{ 3, 48 }, { 4, 11 }, { 4, 18 }, { 4, 22 }, { 4, 28 },
				{ 4, 40 }, { 4, 41 }, { 4, 44 }, { 4, 45 }, { 4, 46 },
				{ 4, 50 }, { 4, 51 }, { 5, 14 }, { 5, 24 }, { 5, 36 },
				{ 5, 41 }, { 5, 45 }, { 5, 46 }, { 5, 47 }, { 5, 48 },
				{ 6, 18 }, { 6, 46 }, { 6, 51 }, { 7, 12 }, { 7, 24 },
				{ 7, 31 }, { 7, 34 }, { 7, 37 }, { 7, 39 }, { 7, 41 },
				{ 7, 45 }, { 7, 46 }, { 7, 47 }, { 8, 24 }, { 8, 41 },
				{ 8, 46 }, { 8, 47 }, { 9, 15 }, { 9, 22 }, { 9, 26 },
				{ 9, 28 }, { 9, 34 }, { 9, 35 }, { 9, 36 }, { 9, 40 },
				{ 9, 41 }, { 9, 44 }, { 9, 46 }, { 9, 51 }, { 10, 13 },
				{ 10, 15 }, { 10, 16 }, { 10, 21 }, { 10, 23 }, { 10, 26 },
				{ 10, 31 }, { 10, 34 }, { 10, 36 }, { 10, 39 }, { 10, 40 },
				{ 10, 41 }, { 10, 44 }, { 10, 45 }, { 10, 46 }, { 10, 50 },
				{ 10, 51 }, { 11, 18 }, { 11, 35 }, { 11, 36 }, { 11, 46 },
				{ 11, 51 }, { 12, 27 }, { 12, 34 }, { 12, 36 }, { 12, 40 },
				{ 12, 41 }, { 12, 44 }, { 12, 45 }, { 12, 46 }, { 12, 50 },
				{ 12, 51 }, { 13, 17 }, { 13, 28 }, { 13, 34 }, { 13, 35 },
				{ 13, 41 }, { 13, 46 }, { 14, 31 }, { 14, 34 }, { 14, 37 },
				{ 14, 39 }, { 14, 41 }, { 14, 45 }, { 14, 46 }, { 14, 47 },
				{ 14, 49 }, { 15, 35 }, { 15, 46 }, { 15, 48 }, { 15, 51 },
				{ 16, 17 }, { 16, 28 }, { 16, 34 }, { 16, 35 }, { 16, 40 },
				{ 16, 41 }, { 16, 44 }, { 16, 46 }, { 16, 51 }, { 17, 26 },
				{ 17, 34 }, { 17, 36 }, { 17, 40 }, { 17, 41 }, { 17, 44 },
				{ 17, 46 }, { 17, 51 }, { 19, 26 }, { 19, 27 }, { 19, 31 },
				{ 19, 34 }, { 19, 36 }, { 19, 39 }, { 19, 40 }, { 19, 41 },
				{ 19, 44 }, { 19, 45 }, { 19, 46 }, { 19, 50 }, { 19, 51 },
				{ 20, 21 }, { 20, 23 }, { 20, 26 }, { 20, 34 }, { 20, 36 },
				{ 20, 40 }, { 20, 41 }, { 20, 44 }, { 20, 45 }, { 20, 46 },
				{ 20, 50 }, { 20, 51 }, { 21, 28 }, { 21, 35 }, { 21, 51 },
				{ 22, 48 }, { 22, 51 }, { 23, 35 }, { 23, 36 }, { 23, 40 },
				{ 23, 41 }, { 23, 44 }, { 23, 51 }, { 24, 31 }, { 24, 34 },
				{ 24, 35 }, { 24, 36 }, { 24, 40 }, { 24, 41 }, { 24, 44 },
				{ 24, 45 }, { 24, 51 }, { 25, 41 }, { 25, 45 }, { 26, 35 },
				{ 26, 48 }, { 26, 51 }, { 27, 34 }, { 27, 35 }, { 27, 39 },
				{ 27, 49 }, { 28, 36 }, { 28, 48 }, { 28, 51 }, { 29, 41 },
				{ 29, 45 }, { 30, 34 }, { 30, 36 }, { 30, 39 }, { 30, 40 },
				{ 30, 41 }, { 30, 44 }, { 30, 45 }, { 30, 50 }, { 30, 51 },
				{ 31, 34 }, { 31, 36 }, { 31, 40 }, { 31, 41 }, { 31, 44 },
				{ 31, 51 }, { 33, 49 }, { 34, 40 }, { 34, 51 }, { 35, 36 },
				{ 35, 51 }, { 37, 50 }, { 39, 45 }, { 39, 51 }, { 40, 51 },
				{ 44, 51 }, { 47, 50 }, { 47, 51 }, { 48, 51 }, { 49, 50 } };

		for (int i = 0; i < edges.length; i++) {
			graph.addEdge(edges[i][0], edges[i][1]);
		}

		graph.clear();
		Assert.assertEquals("Size is not empty after clear",
				graph.getNodeNum(), 0);
		Assert.assertEquals("Number of edges is not empty after clear",
				graph.getEdgeNum(), 0);
		graph.addNode();
		graph.addNode();
		Assert.assertEquals("Size is not as expected", graph.getNodeNum(), 2);
		Assert.assertEquals("Number of edges isn't as expected",
				graph.getEdgeNum(), 0);
		Assert.assertEquals("Maximal degree isn't as expected",
				graph.getMaxDeg(), 0);
		graph.addEdge(0, 1);
		try {
			graph.addEdge(10, 9);
			Assert.fail("Allowed an illegal edge in the graph");
		} catch (IndexOutOfBoundsException e) {
		}

	}

	@Test
	public void testUnDirectedDegeneracy2() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		int nodes = 100;
		for (int i = 0; i < nodes; i++) {
			graph.addNode();
		}

		for (int i = 0; i < nodes; i++) {
			for (int j = i + 1; j < nodes; j++) {
				graph.addEdge(i, j);
			}
		}
		Assert.assertEquals("wrong degeneracy", nodes - 1, graph.degeneracy());

	}

	@Test
	public void testUnDirectedDegeneracy3() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		int nodes = 52;
		for (int i = 0; i < nodes; i++) {
			graph.addNode();
		}

		int[][] edges = new int[][] { { 0, 9 }, { 0, 10 }, { 0, 11 },
				{ 0, 12 }, { 0, 13 }, { 0, 17 }, { 0, 18 }, { 0, 19 },
				{ 0, 20 }, { 0, 24 }, { 0, 30 }, { 0, 41 }, { 0, 45 },
				{ 0, 46 }, { 0, 47 }, { 0, 48 }, { 1, 9 }, { 1, 10 },
				{ 1, 11 }, { 1, 12 }, { 1, 17 }, { 1, 18 }, { 1, 19 },
				{ 1, 20 }, { 1, 24 }, { 1, 30 }, { 1, 37 }, { 1, 41 },
				{ 1, 45 }, { 1, 46 }, { 1, 47 }, { 1, 48 }, { 1, 49 },
				{ 2, 9 }, { 2, 10 }, { 2, 11 }, { 2, 14 }, { 2, 17 },
				{ 2, 18 }, { 2, 19 }, { 2, 20 }, { 2, 24 }, { 2, 36 },
				{ 2, 41 }, { 2, 45 }, { 2, 46 }, { 2, 47 }, { 2, 48 },
				{ 2, 49 }, { 3, 5 }, { 3, 9 }, { 3, 10 }, { 3, 11 }, { 3, 14 },
				{ 3, 17 }, { 3, 18 }, { 3, 19 }, { 3, 20 }, { 3, 24 },
				{ 3, 36 }, { 3, 41 }, { 3, 45 }, { 3, 46 }, { 3, 47 },
				{ 3, 48 }, { 4, 11 }, { 4, 18 }, { 4, 22 }, { 4, 28 },
				{ 4, 40 }, { 4, 41 }, { 4, 44 }, { 4, 45 }, { 4, 46 },
				{ 4, 50 }, { 4, 51 }, { 5, 14 }, { 5, 24 }, { 5, 36 },
				{ 5, 41 }, { 5, 45 }, { 5, 46 }, { 5, 47 }, { 5, 48 },
				{ 6, 18 }, { 6, 46 }, { 6, 51 }, { 7, 12 }, { 7, 24 },
				{ 7, 31 }, { 7, 34 }, { 7, 37 }, { 7, 39 }, { 7, 41 },
				{ 7, 45 }, { 7, 46 }, { 7, 47 }, { 8, 24 }, { 8, 41 },
				{ 8, 46 }, { 8, 47 }, { 9, 15 }, { 9, 22 }, { 9, 26 },
				{ 9, 28 }, { 9, 34 }, { 9, 35 }, { 9, 36 }, { 9, 40 },
				{ 9, 41 }, { 9, 44 }, { 9, 46 }, { 9, 51 }, { 10, 13 },
				{ 10, 15 }, { 10, 16 }, { 10, 21 }, { 10, 23 }, { 10, 26 },
				{ 10, 31 }, { 10, 34 }, { 10, 36 }, { 10, 39 }, { 10, 40 },
				{ 10, 41 }, { 10, 44 }, { 10, 45 }, { 10, 46 }, { 10, 50 },
				{ 10, 51 }, { 11, 18 }, { 11, 35 }, { 11, 36 }, { 11, 46 },
				{ 11, 51 }, { 12, 27 }, { 12, 34 }, { 12, 36 }, { 12, 40 },
				{ 12, 41 }, { 12, 44 }, { 12, 45 }, { 12, 46 }, { 12, 50 },
				{ 12, 51 }, { 13, 17 }, { 13, 28 }, { 13, 34 }, { 13, 35 },
				{ 13, 41 }, { 13, 46 }, { 14, 31 }, { 14, 34 }, { 14, 37 },
				{ 14, 39 }, { 14, 41 }, { 14, 45 }, { 14, 46 }, { 14, 47 },
				{ 14, 49 }, { 15, 35 }, { 15, 46 }, { 15, 48 }, { 15, 51 },
				{ 16, 17 }, { 16, 28 }, { 16, 34 }, { 16, 35 }, { 16, 40 },
				{ 16, 41 }, { 16, 44 }, { 16, 46 }, { 16, 51 }, { 17, 26 },
				{ 17, 34 }, { 17, 36 }, { 17, 40 }, { 17, 41 }, { 17, 44 },
				{ 17, 46 }, { 17, 51 }, { 19, 26 }, { 19, 27 }, { 19, 31 },
				{ 19, 34 }, { 19, 36 }, { 19, 39 }, { 19, 40 }, { 19, 41 },
				{ 19, 44 }, { 19, 45 }, { 19, 46 }, { 19, 50 }, { 19, 51 },
				{ 20, 21 }, { 20, 23 }, { 20, 26 }, { 20, 34 }, { 20, 36 },
				{ 20, 40 }, { 20, 41 }, { 20, 44 }, { 20, 45 }, { 20, 46 },
				{ 20, 50 }, { 20, 51 }, { 21, 28 }, { 21, 35 }, { 21, 51 },
				{ 22, 48 }, { 22, 51 }, { 23, 35 }, { 23, 36 }, { 23, 40 },
				{ 23, 41 }, { 23, 44 }, { 23, 51 }, { 24, 31 }, { 24, 34 },
				{ 24, 35 }, { 24, 36 }, { 24, 40 }, { 24, 41 }, { 24, 44 },
				{ 24, 45 }, { 24, 51 }, { 25, 41 }, { 25, 45 }, { 26, 35 },
				{ 26, 48 }, { 26, 51 }, { 27, 34 }, { 27, 35 }, { 27, 39 },
				{ 27, 49 }, { 28, 36 }, { 28, 48 }, { 28, 51 }, { 29, 41 },
				{ 29, 45 }, { 30, 34 }, { 30, 36 }, { 30, 39 }, { 30, 40 },
				{ 30, 41 }, { 30, 44 }, { 30, 45 }, { 30, 50 }, { 30, 51 },
				{ 31, 34 }, { 31, 36 }, { 31, 40 }, { 31, 41 }, { 31, 44 },
				{ 31, 51 }, { 33, 49 }, { 34, 40 }, { 34, 51 }, { 35, 36 },
				{ 35, 51 }, { 37, 50 }, { 39, 45 }, { 39, 51 }, { 40, 51 },
				{ 44, 51 }, { 47, 50 }, { 47, 51 }, { 48, 51 }, { 49, 50 } };

		for (int i = 0; i < edges.length; i++) {
			graph.addEdge(edges[i][0], edges[i][1]);
		}
		Assert.assertEquals("wrong degeneracy", 9, graph.degeneracy());

	}

	@Test
	public void testJSON() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		int nodes = 52;
		for (int i = 0; i < nodes; i++) {
			graph.addNode();
		}

		int[][] edges = new int[][] { { 0, 9 }, { 0, 10 }, { 0, 11 },
				{ 0, 12 }, { 0, 13 }, { 0, 17 }, { 0, 18 }, { 0, 19 },
				{ 0, 20 }, { 0, 24 }, { 0, 30 }, { 0, 41 }, { 0, 45 },
				{ 0, 46 }, { 0, 47 }, { 0, 48 }, { 1, 9 }, { 1, 10 },
				{ 1, 11 }, { 1, 12 }, { 1, 17 }, { 1, 18 }, { 1, 19 },
				{ 1, 20 }, { 1, 24 }, { 1, 30 }, { 1, 37 }, { 1, 41 },
				{ 1, 45 }, { 1, 46 }, { 1, 47 }, { 1, 48 }, { 1, 49 },
				{ 2, 9 }, { 2, 10 }, { 2, 11 }, { 2, 14 }, { 2, 17 },
				{ 2, 18 }, { 2, 19 }, { 2, 20 }, { 2, 24 }, { 2, 36 },
				{ 2, 41 }, { 2, 45 }, { 2, 46 }, { 2, 47 }, { 2, 48 },
				{ 2, 49 }, { 3, 5 }, { 3, 9 }, { 3, 10 }, { 3, 11 }, { 3, 14 },
				{ 3, 17 }, { 3, 18 }, { 3, 19 }, { 3, 20 }, { 3, 24 },
				{ 3, 36 }, { 3, 41 }, { 3, 45 }, { 3, 46 }, { 3, 47 },
				{ 3, 48 }, { 4, 11 }, { 4, 18 }, { 4, 22 }, { 4, 28 },
				{ 4, 40 }, { 4, 41 }, { 4, 44 }, { 4, 45 }, { 4, 46 },
				{ 4, 50 }, { 4, 51 }, { 5, 14 }, { 5, 24 }, { 5, 36 },
				{ 5, 41 }, { 5, 45 }, { 5, 46 }, { 5, 47 }, { 5, 48 },
				{ 6, 18 }, { 6, 46 }, { 6, 51 }, { 7, 12 }, { 7, 24 },
				{ 7, 31 }, { 7, 34 }, { 7, 37 }, { 7, 39 }, { 7, 41 },
				{ 7, 45 }, { 7, 46 }, { 7, 47 }, { 8, 24 }, { 8, 41 },
				{ 8, 46 }, { 8, 47 }, { 9, 15 }, { 9, 22 }, { 9, 26 },
				{ 9, 28 }, { 9, 34 }, { 9, 35 }, { 9, 36 }, { 9, 40 },
				{ 9, 41 }, { 9, 44 }, { 9, 46 }, { 9, 51 }, { 10, 13 },
				{ 10, 15 }, { 10, 16 }, { 10, 21 }, { 10, 23 }, { 10, 26 },
				{ 10, 31 }, { 10, 34 }, { 10, 36 }, { 10, 39 }, { 10, 40 },
				{ 10, 41 }, { 10, 44 }, { 10, 45 }, { 10, 46 }, { 10, 50 },
				{ 10, 51 }, { 11, 18 }, { 11, 35 }, { 11, 36 }, { 11, 46 },
				{ 11, 51 }, { 12, 27 }, { 12, 34 }, { 12, 36 }, { 12, 40 },
				{ 12, 41 }, { 12, 44 }, { 12, 45 }, { 12, 46 }, { 12, 50 },
				{ 12, 51 }, { 13, 17 }, { 13, 28 }, { 13, 34 }, { 13, 35 },
				{ 13, 41 }, { 13, 46 }, { 14, 31 }, { 14, 34 }, { 14, 37 },
				{ 14, 39 }, { 14, 41 }, { 14, 45 }, { 14, 46 }, { 14, 47 },
				{ 14, 49 }, { 15, 35 }, { 15, 46 }, { 15, 48 }, { 15, 51 },
				{ 16, 17 }, { 16, 28 }, { 16, 34 }, { 16, 35 }, { 16, 40 },
				{ 16, 41 }, { 16, 44 }, { 16, 46 }, { 16, 51 }, { 17, 26 },
				{ 17, 34 }, { 17, 36 }, { 17, 40 }, { 17, 41 }, { 17, 44 },
				{ 17, 46 }, { 17, 51 }, { 19, 26 }, { 19, 27 }, { 19, 31 },
				{ 19, 34 }, { 19, 36 }, { 19, 39 }, { 19, 40 }, { 19, 41 },
				{ 19, 44 }, { 19, 45 }, { 19, 46 }, { 19, 50 }, { 19, 51 },
				{ 20, 21 }, { 20, 23 }, { 20, 26 }, { 20, 34 }, { 20, 36 },
				{ 20, 40 }, { 20, 41 }, { 20, 44 }, { 20, 45 }, { 20, 46 },
				{ 20, 50 }, { 20, 51 }, { 21, 28 }, { 21, 35 }, { 21, 51 },
				{ 22, 48 }, { 22, 51 }, { 23, 35 }, { 23, 36 }, { 23, 40 },
				{ 23, 41 }, { 23, 44 }, { 23, 51 }, { 24, 31 }, { 24, 34 },
				{ 24, 35 }, { 24, 36 }, { 24, 40 }, { 24, 41 }, { 24, 44 },
				{ 24, 45 }, { 24, 51 }, { 25, 41 }, { 25, 45 }, { 26, 35 },
				{ 26, 48 }, { 26, 51 }, { 27, 34 }, { 27, 35 }, { 27, 39 },
				{ 27, 49 }, { 28, 36 }, { 28, 48 }, { 28, 51 }, { 29, 41 },
				{ 29, 45 }, { 30, 34 }, { 30, 36 }, { 30, 39 }, { 30, 40 },
				{ 30, 41 }, { 30, 44 }, { 30, 45 }, { 30, 50 }, { 30, 51 },
				{ 31, 34 }, { 31, 36 }, { 31, 40 }, { 31, 41 }, { 31, 44 },
				{ 31, 51 }, { 33, 49 }, { 34, 40 }, { 34, 51 }, { 35, 36 },
				{ 35, 51 }, { 37, 50 }, { 39, 45 }, { 39, 51 }, { 40, 51 },
				{ 44, 51 }, { 47, 50 }, { 47, 51 }, { 48, 51 }, { 49, 50 } };

		for (int i = 0; i < edges.length; i++) {
			graph.addEdge(edges[i][0], edges[i][1]);
		}

		try {
			JSONObject json = graph.toJSON();
			FlexibleUndirectedGraph graph2 = FlexibleUndirectedGraph
					.fromJSON(json);
			Assert.assertEquals("Wrong number of nodes", graph.getNodeNum(),
					graph2.getNodeNum());
			Assert.assertEquals("Wrong number of edges", graph.getEdgeNum(),
					graph2.getEdgeNum());
			for (int i = 0; i < edges.length; i++) {
				Assert.assertEquals("Missed an edge in the new graph "
						+ edges[i][0] + " " + edges[i][1], true,
						graph2.isNeighbours(edges[i][0], edges[i][1]));
			}
		} catch (JSONException e) {
			Assert.fail("Got error in JSON format " + e.getMessage());
		}

	}
}
