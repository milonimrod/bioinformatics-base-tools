package bgu.bio.algorithms.graphs;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.set.hash.TIntHashSet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import bgu.bio.adt.graphs.Tree;
import bgu.bio.adt.rna.NodeLabel;
import bgu.bio.adt.tuples.Pair;
import bgu.bio.algorithms.graphs.hsa.HSA;
import bgu.bio.algorithms.graphs.hsa.matchers.UnorderedMatcherFactory;

public class TestHSAOnInteger {

	private static void run(String file1, String file2) throws IOException {
		Tree t = buildTree(file1);
		Tree s = buildTree(file2);
		t.toDotFile("/home/milon/1.dot", true);
		s.toDotFile("/home/milon/2.dot", true);

		CostFunction w = new CostFunction() {

			@Override
			public double cost(NodeLabel l1, NodeLabel l2) {
				if (l1.equals(l2)) {
					return -2;
				}

				return Double.POSITIVE_INFINITY;
			}
		};

		TIntArrayList[] alignment = new TIntArrayList[3];

		UnorderedMatcherFactory matcherFactory = new UnorderedMatcherFactory();
		HSA hsa = new HSA(w, matcherFactory,false);

		System.out.println("Cost t-s: " + hsa.computeHSA(t, s, alignment));
		for (int i = 0; i < alignment[0].size(); ++i) {
			System.out.print("(" + alignment[0].get(i) + ","
					+ alignment[1].get(i) + "), ");
		}

		System.out.println();
		for (int i = 0; i < alignment[0].size(); ++i) {
			System.out.println("(" + t.getLabel(alignment[0].get(i)) + ","
					+ s.getLabel(alignment[1].get(i)) + ") ");
		}
	}

	private static Tree buildTree(String file1) throws IOException {
		ArrayList<Pair<String, String>> list1 = readFile(file1);
		ArrayList<TIntHashSet> nodes = new ArrayList<TIntHashSet>();

		int idCount = 0;
		TObjectIntHashMap<String> label2Id = new TObjectIntHashMap<String>();
		TIntObjectHashMap<String> id2Label = new TIntObjectHashMap<String>();
		for (Pair<String, String> pair : list1) {
			int currentId1;
			String curLabel = pair.getFirst();
			if (label2Id.contains(curLabel)) {
				currentId1 = label2Id.get(curLabel);
			} else {
				currentId1 = idCount;
				label2Id.put(curLabel, currentId1);
				id2Label.put(currentId1, curLabel);
				idCount++;
			}
			int currentId2;
			curLabel = pair.getSecond();
			if (label2Id.contains(curLabel)) {
				currentId2 = label2Id.get(curLabel);
			} else {
				currentId2 = idCount;
				label2Id.put(curLabel, currentId2);
				id2Label.put(currentId2, curLabel);
				idCount++;
			}

			increaseSize(nodes, currentId1);
			nodes.get(currentId1).add(currentId2);

			increaseSize(nodes, currentId2);
			nodes.get(currentId2).add(currentId1);
		}
		/*
		nodes.add(new TIntHashSet(new int[]{0}));
		nodes.get(0).add(nodes.size() - 1);
		label2Id.put("ROOT", nodes.size() - 1);
		id2Label.put(nodes.size() - 1, "ROOT");
		*/

		int[][] tEdges = new int[nodes.size()][];
		double[][] tWeights = new double[nodes.size()][];
		for (int i = 0; i < tEdges.length; i++) {
			tEdges[i] = nodes.get(i).toArray();
			tWeights[i] = new double[tEdges[i].length];
		}

		double[] tSmoothing = new double[nodes.size()];
		NodeLabel[] tLabels = new NodeLabel[nodes.size()];

		for (int i = 0; i < tEdges.length; i++) {
			tLabels[i] = new NodeLabel(id2Label.get(i));
		}

		Tree t = new Tree(tEdges, tWeights, tSmoothing, tLabels);
		return t;
	}

	private static void increaseSize(ArrayList<TIntHashSet> nodes,
			int currentId1) {
		while (nodes.size() <= currentId1) {
			nodes.add(new TIntHashSet());
		}
	}

	private static ArrayList<Pair<String, String>> readFile(String file1)
			throws IOException {
		ArrayList<Pair<String, String>> list = new ArrayList<Pair<String, String>>();
		BufferedReader reader = new BufferedReader(new FileReader(file1));
		reader.readLine();
		String line = reader.readLine();
		while (line != null) {
			String[] sp = line.split("\t");
			String[] nodes = sp[0].split(" \\(pp\\) ");
			list.add(new Pair<String, String>(nodes[0], nodes[1]));
			line = reader.readLine();
		}
		reader.close();
		return list;
	}

	public static void main(String[] args) throws IOException {
		run("/home/milon/Dropbox/Tree_compare/ex1_edges.txt",
				"/home/milon/Dropbox/Tree_compare/ex2_edges.txt");
	}
}
