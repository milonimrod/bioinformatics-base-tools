package bgu.bio.algorithms.graphs;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.adt.graphs.FlexibleUndirectedGraph;
import bgu.bio.adt.tuples.IntDoublePair;

public class TestWeightedClique {

	@Test
	public void testClique1() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		int nodes = 9;
		for (int i = 0; i < nodes; i++) {
			graph.addNode();
		}

		graph.addEdge(1, 2);
		graph.addEdge(1, 3);
		graph.addEdge(2, 3);
		graph.addEdge(3, 4);
		graph.addEdge(3, 5);
		graph.addEdge(3, 6);
		graph.addEdge(4, 5);
		graph.addEdge(4, 6);
		graph.addEdge(5, 6);

		WeightedCliqueTree clique = new WeightedCliqueTree();
		clique.setGraph(graph);
		double[] weights = new double[] { 1, 1, 1, 1, 1, 1, 1, 1 };
		IntDoublePair[] nodesAsPairs = new IntDoublePair[8];
		for (int i = 0; i < nodesAsPairs.length; i++) {
			nodesAsPairs[i] = new IntDoublePair(i, weights[i]);
		}

		Assert.assertEquals("Size of clique should be 0", 0, clique.clique()
				.size());
		clique.add(nodesAsPairs[1]);
		Assert.assertEquals("Size of clique should be 1", 1, clique.clique()
				.size());
		Assert.assertEquals("Potential of clique should be 2", 2,
				clique.potential());
		clique.add(nodesAsPairs[2]);
		Assert.assertEquals("Size of clique should be 2", 2, clique.clique()
				.size());
		Assert.assertEquals("Potential of clique should be 1", 1,
				clique.potential());
		clique.removeLast();
		Assert.assertEquals("Size of clique should be 1", 1, clique.clique()
				.size());
		Assert.assertEquals("Potential of clique should be 2", 2,
				clique.potential());
	}

	@Test
	public void testClique2() {
		FlexibleUndirectedGraph graph = new FlexibleUndirectedGraph();
		int nodes = 8;
		for (int i = 0; i < nodes; i++) {
			graph.addNode();
		}

		graph.addEdge(1, 2);
		graph.addEdge(1, 3);
		graph.addEdge(1, 4);
		graph.addEdge(1, 7);

		graph.addEdge(2, 3);
		graph.addEdge(2, 5);
		graph.addEdge(2, 7);

		graph.addEdge(3, 6);
		double[] weights = new double[] { 1, 1, 10, 3.4, 1, 1, 1, 1 };
		IntDoublePair[] nodesAsPairs = new IntDoublePair[8];
		for (int i = 0; i < nodesAsPairs.length; i++) {
			nodesAsPairs[i] = new IntDoublePair(i, weights[i]);
		}
		WeightedCliqueTree clique = new WeightedCliqueTree();
		clique.setGraph(graph);

		Assert.assertEquals("Size of clique should be 0", 0, clique.clique()
				.size());
		clique.add(nodesAsPairs[1]);
		Assert.assertEquals("Size of clique should be 1", 1, clique.clique()
				.size());
		Assert.assertEquals("Potential of clique should be 4", 4,
				clique.potential());
		clique.add(nodesAsPairs[2]);
		Assert.assertEquals("Size of clique should be 2", 2, clique.clique()
				.size());
		Assert.assertEquals("Potential of clique should be 2", 2,
				clique.potential());
		Assert.assertEquals("Weight of clique should be 11", 11.0,
				clique.weight(), 0.0001);
		clique.removeLast();
		Assert.assertEquals("Size of clique should be 1", 1, clique.clique()
				.size());
		Assert.assertEquals("Potential of clique should be 4", 4,
				clique.potential());
		clique.add(nodesAsPairs[2]);
		clique.add(nodesAsPairs[3]);
		Assert.assertEquals("Size of clique should be 3", 3, clique.clique()
				.size());
		Assert.assertEquals("Potential of clique should be 0", 0,
				clique.potential());
		Assert.assertEquals("Weight of clique should be 14.4", 14.4,
				clique.weight(), 0.0001);
	}
}
