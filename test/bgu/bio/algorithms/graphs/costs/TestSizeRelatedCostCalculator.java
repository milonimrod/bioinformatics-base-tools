package bgu.bio.algorithms.graphs.costs;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.adt.rna.RNASpecificTree;
import bgu.bio.algorithms.graphs.costs.SizeRelatedTreePruning;


public class TestSizeRelatedCostCalculator {
	@Test
	public void testSimple1(){
		RNASpecificTree t = new RNASpecificTree();
		t.buildFromViennaFormat(
				"CGACAGGAAACUGACAAACCCUUUCAUCUUAA".toCharArray(),
				"...((((...))...(((...)))..))....".toCharArray());
		
		SizeRelatedTreePruning cost = new SizeRelatedTreePruning(1);
		cost.calculateCost(t);
		
		Assert.assertEquals(17, t.getWeight(10, t.getNeighborIx(10, 5)),0.01);
		Assert.assertEquals(3, t.getWeight(6, t.getNeighborIx(6, 7)),0.01);
	}
}
