package bgu.bio.ds.rna;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.adt.tuples.IntPair;

public class TestRNA {

	@Test
	public void testExtractSimple1() {
		RNA rna = new RNA(1, "test", "AAACCCUUU", "(((...)))");
		ArrayList<IntPair> expList = new ArrayList<IntPair>();
		expList.add(new IntPair(0, 8));
		expList.add(new IntPair(1, 7));
		expList.add(new IntPair(2, 6));

		checkLists(expList, rna.extractPairs());
	}

	@Test
	public void testExtractPK1() {
		RNA rna = new RNA(1, "test", "AAACCCUUUAAGGGCCC", "((([[[)))..]]]...");
		ArrayList<IntPair> expList = new ArrayList<IntPair>();
		expList.add(new IntPair(0, 8));
		expList.add(new IntPair(1, 7));
		expList.add(new IntPair(2, 6));
		expList.add(new IntPair(3, 13));
		expList.add(new IntPair(4, 12));
		expList.add(new IntPair(5, 11));
		checkLists(expList, rna.extractPairs());
	}

	@Test
	public void testSplit1() {
		RNA rna = new RNA(
				1,
				"test",
				"AAAUUUAUAGGGAGGGCCACUGUUCUCACUGUUGCGCUACAUCUGGCUAUUCCGCUCAAAUGGAAGCCAGACCACACGCCUGUGUGGAUUGACCAGUGGCCCCUCCCUGAAGGUAAACUUGUAGCGCU",
				"........((((((((((((((...........[[[[[[[[((((((..(((((......)))))))))))((((((....))))))......))))))))).)))))...........]]]]]]]].");
		ArrayList<RNA> ans = rna.splitToStems(6);
		ArrayList<RNA> expected = new ArrayList<RNA>();
		expected.add(new RNA(
				1,
				"",
				"AGGGAGGGCCACUGUUCUCACUGUUGCGCUACAUCUGGCUAUUCCGCUCAAAUGGAAGCCAGACCACACGCCUGUGUGGAUUGACCAGUGGCCCCUCCCU",
				"((((((((((((((.......................................................................))))))))).)))))"));
		expected.add(new RNA(2, "", "UCUGGCUAUUCCGCUCAAAUGGAAGCCAGA",
				"((((((..(((((......)))))))))))"));
		expected.add(new RNA(3, "", "CCACACGCCUGUGUGG", "((((((....))))))"));
		expected.add(new RNA(
				4,
				"",
				"GCGCUACAUCUGGCUAUUCCGCUCAAAUGGAAGCCAGACCACACGCCUGUGUGGAUUGACCAGUGGCCCCUCCCUGAAGGUAAACUUGUAGCGC",
				"((((((((..............................................................................))))))))"));

		checkRNAList(expected, ans);
	}

	@Test
	public void testSplit2() {
		RNA rna = new RNA(1, "test", "uuuGGGAAAGGuuuCCCCCuuUUU",
				"...((([[[((...)))))..]]]");
		ArrayList<RNA> ans = rna.splitToStems(4);
		ArrayList<RNA> expected = new ArrayList<RNA>();
		expected.add(new RNA(1, "", "GGGAAAGGuuuCCCCC", "(((...((...)))))"));
		expected.add(new RNA(2, "", "AAAGGuuuCCCCCuuUUU", "(((............)))"));

		checkRNAList(expected, ans);
	}

	@Test
	public void testSplitMaxGap1() {
		RNA rna = new RNA(1, "test", "GGGAUAGAUCAUUGCAAUUGUUGGUCUUCAAC",
				"(....((((((..........))))))....)");
		ArrayList<RNA> ans = rna.splitToStems(3);
		ArrayList<RNA> expected = new ArrayList<RNA>();
		expected.add(new RNA(1, "", "AGAUCAUUGCAAUUGUUGGUCU",
				"((((((..........))))))"));
		expected.add(new RNA(2, "", "GGGAUAGAUCAUUGCAAUUGUUGGUCUUCAAC",
				"(..............................)"));

		checkRNAList(expected, ans);
	}

	private static void checkLists(ArrayList<?> expList, ArrayList<?> answerList) {
		if (expList.size() != answerList.size()) {
			Assert.fail("wrong amount of items in the answer list ("
					+ answerList.size() + " instead of " + expList.size() + ")");
		}
		for (Object obj : expList) {
			if (!answerList.contains(obj)) {
				System.out.println(answerList);
				Assert.fail("Can't find " + obj + " in list");
			}

		}
	}

	private static void checkRNAList(ArrayList<RNA> expList,
			ArrayList<RNA> answerList) {
		if (expList.size() != answerList.size()) {
			Assert.fail("wrong amount of items in the answer list ("
					+ answerList.size() + " instead of " + expList.size() + ")");
		}
		for (RNA rna1 : expList) {
			boolean found = false;
			for (int i = 0; i < answerList.size() && !found; i++) {
				RNA rna2 = answerList.get(i);
				found = rna1.getPrimary().equals(rna2.getPrimary())
						&& rna1.getSecondary().equals(rna2.getSecondary());

			}
			if (!found) {
				System.out.println(answerList);
				Assert.fail("Can't find " + rna1 + " in list");
			}

		}
	}
}
