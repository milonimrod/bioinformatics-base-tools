package bgu.bio.ds.tree;

import java.util.Collections;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.Test;

import bgu.bio.adt.tree.AVLNode;
import bgu.bio.adt.tree.AVLTree;


public class TestAVLTree {

	@Test
	public void testSucc(){
		AVLTree<Integer, Integer> tree = new AVLTree<Integer, Integer>(new IntegerComparator());
		LinkedList<Integer> list = new LinkedList<Integer>();
		for (int i=0;i<10000;i++){
			list.add(i);
		}
		Collections.shuffle(list);

		for (Integer num : list){
			tree.add(num, num);
		}

		AVLNode<Integer, Integer> start = tree.findMinNode();
		int n = 0;
		while (start != null){
			if (n != start.getKey().intValue())
				Assert.fail("Wrong order");
			start = start.getSucc();
			n++;
		}
	}

	@Test
	public void testSuccAfterRemove(){
		for (int x = 0;x< 1000;x++){
			AVLTree<Integer, Integer> tree = new AVLTree<Integer, Integer>(new IntegerComparator());
			LinkedList<Integer> list = new LinkedList<Integer>();

			for (int i=0;i<2000;i++){
				list.add(i);
			}
			Collections.shuffle(list);
			for (Integer num : list){
				tree.add(num, num);
			}

			for (Integer num : list){
				if (num % 2 ==0){
					tree.remove(num);

				}
			}
			
			AVLNode<Integer, Integer> start = tree.findMinNode();
			while (start != null){
				if (start.getKey().intValue() % 2 == 0)
					Assert.fail("foud even number");
				start = start.getSucc();
			}
		}
	}
}
