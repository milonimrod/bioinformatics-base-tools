package bgu.bio.ds.tree;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import bgu.bio.adt.tree.FloatAVLTree;

public class TestFloatAVLTree {

	@Test
	public void test1() {
		FloatAVLTree<Integer> tree = new FloatAVLTree<Integer>();
		tree.add(20, 20);
		tree.add(10, 10);
		tree.add(40, 40);
		tree.add(7, 7);
		tree.add(11, 11);
		tree.add(30, 30);

		tree.add(55, 55);
		tree.add(6, 6);
		tree.add(35, 35);
		tree.add(50, 50);
		tree.add(60, 60);
		tree.add(65, 65);
		Assert.assertArrayEquals(new int[] { 20, 10, 7, 6, 11, 40, 30, 35, 55,
				50, 60, 65 }, toArray(tree.traversePreOrder()));
		tree.remove(20);
		Assert.assertArrayEquals(new int[] { 30, 10, 7, 6, 11, 55, 40, 35, 50,
				60, 65 }, toArray(tree.traversePreOrder()));
	}

	private int[] toArray(List<Integer> list) {
		int[] ans = new int[list.size()];
		for (int i = 0; i < ans.length; i++) {
			ans[i] = list.get(i).intValue();
		}
		return ans;
	}

}
