package bgu.bio.util.alphabet;

import java.util.Arrays;

import junit.framework.Assert;

import org.junit.Test;

public class TestRnaAlphabet {

	@Test
	public void testCanPair() {
		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		char[][] pairs = new char[][] { { 'A', 'U' }, { 'G', 'U' },
				{ 'A', 'T' }, { 'G', 'T' }, { 'G', 'C' } };
		for (int i = 0; i < pairs.length; i++) {
			Assert.assertEquals(
					"wrong pairing scheme, pair " + Arrays.toString(pairs[i])
							+ " should pair", true,
					alphabet.canPair(pairs[i][0], pairs[i][1]));
			Assert.assertEquals(
					"wrong pairing scheme, pair " + Arrays.toString(pairs[i])
							+ " should pair", true,
					alphabet.canPair(pairs[i][1], pairs[i][0]));
		}
	}

	@Test
	public void testCanPair2() {
		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		char[] letters = new char[] { 'A', 'G', 'C', 'U', 'T' };
		for (int i = 0; i < letters.length; i++) {
			Assert.assertEquals("wrong pairing scheme, pair " + letters[i]
					+ ", " + 'N' + " should not pair", false,
					alphabet.canPair(letters[i], 'N'));
			Assert.assertEquals("wrong pairing scheme, pair N," + letters[i]
					+ " should not pair", false,
					alphabet.canPair('N', letters[i]));
		}
	}

	@Test
	public void testCanPairN() {
		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		Assert.assertEquals("wrong pairing scheme, pair N " + ", " + 'N'
				+ " should not pair", false, alphabet.canPair('N', 'N'));
	}

	@Test
	public void testComplements() {
		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		Assert.assertEquals("wrong complement ", 'U', alphabet.complement('A'));
		Assert.assertEquals("wrong complement ", 'A', alphabet.complement('U'));
		Assert.assertEquals("wrong complement ", 'G', alphabet.complement('C'));
		Assert.assertEquals("wrong complement ", 'C', alphabet.complement('G'));
		Assert.assertEquals("wrong complement ", 'N', alphabet.complement('N'));
	}

	@Test
	public void testEncode() {
		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		char[] letters = new char[] { 'A', 'G', 'C', 'U', 'N' };
		for (int i = 0; i < letters.length; i++) {
			Assert.assertEquals("encode / decode are wrong on letter "
					+ letters[i], alphabet.decode(alphabet.encode(letters[i])),
					letters[i]);
		}
	}

	@Test
	public void testCanPairEncoded() {
		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		char[][] pairs = new char[][] { { 'A', 'U' }, { 'G', 'U' },
				{ 'A', 'T' }, { 'G', 'T' }, { 'G', 'C' } };
		for (int i = 0; i < pairs.length; i++) {
			Assert.assertEquals(
					"wrong pairing scheme, pair " + Arrays.toString(pairs[i])
							+ " should pair",
					true,
					alphabet.canPair(alphabet.encode(pairs[i][0]),
							alphabet.encode(pairs[i][1])));
			Assert.assertEquals(
					"wrong pairing scheme, pair " + Arrays.toString(pairs[i])
							+ " should pair",
					true,
					alphabet.canPair(alphabet.encode(pairs[i][1]),
							alphabet.encode(pairs[i][0])));
		}
	}

	@Test
	public void testCanPairEncoded2() {
		RnaAlphabet alphabet = RnaAlphabet.getInstance();
		char[] letters = new char[] { 'A', 'G', 'C', 'U', 'T' };
		short n = alphabet.encode('N');
		for (int i = 0; i < letters.length; i++) {
			Assert.assertEquals("wrong pairing scheme, pair " + letters[i]
					+ ", " + 'N' + " should not pair", false,
					alphabet.canPair(alphabet.encode(letters[i]), n));
			Assert.assertEquals("wrong pairing scheme, pair N," + letters[i]
					+ " should not pair", false,
					alphabet.canPair(alphabet.encode(letters[i]), n));
		}
	}
}
